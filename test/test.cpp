/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 1.0 which is available at
 * http://www.eclipse.org/legal/epl-1.0.
 *
 * SPDX-License-Identifier: EPL-1.0
 *
 **********************************************************************************/


// ---------------------------------------- Interval Extension Settings ----------------------------------------
#define USE_FILIB  	// <-- Specify to use FILIB++ for interval arithmetic
					// For using MC++'s built-in interval extensions, undef USE_FILIB
// ----------------------

#ifdef USE_FILIB
	#include "mcfilib.hpp"
	/**
	*  @typedef filib::interval<double> I
	*  
	*  @brief A type definition for an Interval variable using FILIB++ library
	*/
	typedef filib::interval<double> I;
#else
	#include "interval.hpp"
	/**
	*  @typedef mc::INTERVAL I
	*  
	*  @brief A type definition for an Interval variable using MC++ library
	*/
	typedef mc::Interval I;
#endif
#include "mccormick.hpp"
/**
*  @typedef mc::McCormick<I> MC
*  
*  @brief A type definition for a McCormick variable
*/
typedef mc::McCormick<I> MC;

#include "ffunc.hpp"

int main() {

	MC test = MC(I(0,1),2);
	test.sub(2);
	mc::FFGraph DAG;	
	
	std::cout << "MC++ seems to work..." << std::endl;
	std::cout << "Note that this is merely intended to test the repository / CMake setup and not functionality..." << std::endl;
	std::cout << "Further tests can be found in src/test" << std::endl;
	
	return 0;

}