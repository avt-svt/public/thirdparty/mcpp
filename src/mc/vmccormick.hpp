// Copyright (C) 2009-2017 Benoit Chachuat, Imperial College London.
// All Rights Reserved.

/*!
\page page_vMCCORMICK vector McCormick Relaxation Arithmetic for Factorable Functions
\author Jaromil Najman

A convex relaxation \f$f^{\rm cv}\f$ of a function \f$f\f$ on the convex domain \f$D\f$ is a function that is (i) convex on \f$D\f$ and (ii) underestimates \f$f\f$  on \f$D\f$. Likewise, a concave relaxation \f$f^{\rm cc}\f$ of a function \f$f\f$ on the convex domain \f$D\f$ is a function that is (i) concave on \f$D\f$ and (ii) overestimates \f$f\f$  on \f$D\f$. McCormick's technique [McCormick, 1976] provides a means for computing pairs of convex/concave relaxations of a multivariate function on interval domains provided that this function is factorable and that the intrinsic univariate functions in its factored form have known convex/concave envelopes or, at least, relaxations.

<CENTER><TABLE BORDER=0>
<TR>
<TD>\image html McCormick_relax.png</TD>
</TR>
</TABLE></CENTER>


The class mc::vMcCormick provides an implementation of the McCormick relaxation technique and its recent extensions; see [McCormick, 1976; Scott <i>et al.</i>, 2011; Tsoukalas & Mitsos, 2012; Wechsung & Barton, 2013]. mc::McCormick also has the capability to propagate subgradients for these relaxations, which are guaranteed to exist in the interior of the domain of definition of any convex/concave function. This propagation is similar in essence to the forward mode of automatic differentiation; see [Mitsos <i>et al.</i>, 2009]. We note that mc::McCormick is <b>not a verified implementation</b> in the sense that rounding errors are not accounted for in computing convex/concave bounds and subgradients.

The implementation of mc::vMcCormick relies on the operator/function overloading mechanism of C++. This makes the computation of convex/concave relaxations both simple and intuitive, similar to computing function values in real arithmetics or function bounds in interval arithmetic (see \ref page_INTERVAL). Moreover, mc::McCormick can be used as the template parameter of other classes of MC++, for instance mc::TModel and mc::TVar. Likewise, mc::McCormick can be used as the template parameter of the classes fadbad::F, fadbad::B and fadbad::T of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A> for computing McCormick relaxations and subgradients of the partial derivatives or the Taylor coefficients of a factorable function (see \ref sec_MCCORMICK_fadbad).

mc::McCormick itself is templated in the type used to propagate the supporting interval bounds. By default, mc::McCormick can be used with the non-verified interval type mc::Interval of MC++. For reliability, however, it is strongly recommended to use verified interval arithmetic such as <A href="http://www.ti3.tu-harburg.de/Software/PROFILEnglisch.html">PROFIL</A> or <A href="http://www.math.uni-wuppertal.de/~xsc/software/filib.html">FILIB++</A>. We note that Taylor models as provided by the classes mc::TModel and mc::TVar can also be used as the template parameter (see \ref page_TAYLOR).

Examples of McCormick relaxations constructed with mc::vMcCormick are shown on the left plot of the figure below for the factorable function \f$f(x)=\cos(x^2)\,\sin(x^{-3})\f$ for \f$x\in [\frac{\pi}{6},\frac{\pi}{3}]\f$. Also shown on the right plot are the affine relaxations constructed from a subgradient at \f$\frac{\pi}{4}\f$ of the McCormick relaxations of \f$f\f$ on \f$[\frac{\pi}{6},\frac{\pi}{3}]\f$.

<CENTER><TABLE BORDER=0>
<TR>
<TD>\image html MC-1D_relax.png</TD>
<TD>\image html MC-1D_linearize.png</TD>
</TR>
</TABLE></CENTER>


\section sec_MCCORMICK_use How do I compute McCormick relaxations of a factorable function?

Suppose one wants to compute McCormick relaxation of the real-valued function \f$f(x,y)=x(\exp(x)-y)^2\f$ for \f$(x,y)\in [-2,1]\times[-1,2]\f$, at the point \f$(x,y)=(0,1)\f$. For simplicity, the supporting interval bounds are calculated using the default interval type mc::Interval here:

\code
      #include "interval.hpp"
      typedef mc::Interval I;
      typedef mc::vMcCormick<I> vMC;
\endcode

First, the variables \f$x\f$ and \f$y\f$ which are to be computed at  points $-2,-1,0,1$ and $-1,0,1,2$ are defined as follows:

\code
      std::vector<double> xPoints = {-2,-1,0,1};
	  std::vector<double> yPoints = {-1,0,1,2};
      vMC X( I( -2., 1. ), xPoints );
      vMC Y( I( -1., 2. ), xPoints );
\endcode

Essentially, the first line means that <tt>X</tt> is a variable of type mc::vMcCormick, belonging to the interval \f$[-2,1]\f$, and whose current values are \f$-2,-1,0,1\f$. The same holds for the McCormick variable <tt>Y</tt>, which belonging to the interval \f$[-1,2]\f$ and has values of \f$-1,0,1,2\f$.

Having defined the variables, McCormick relaxations of \f$f(x,y)=x(\exp(x)-y)^2\f$ on \f$[-2,1]\times[-1,2]\f$ at \f$(-2,-1),(-1,0),(0,1),(1,2)\f$ are simply computed as:

\code
      vMC F = X*pow(exp(X)-Y,2);
\endcode

The third point of the relaxations can be displayed as:

\code
      std::cout << "f relaxations at (0,1): "; F.print(std::cout,2);
\endcode

which produces the following output:

\verbatim
f relaxations at (0,1): [ -2.76512e+01 :  1.38256e+01 ] [ -1.38256e+01 :  8.52245e+00 ]
\endverbatim

All four poins of the relaxations can be displayed as:

\code
      F.print_all(std::cout);
\endcode

which produces the following output:

\verbatim
........
........
[ -2.76512e+01 :  1.38256e+01 ] [ -1.38256e+01 :  8.52245e+00 ]
........
\endverbatim

Here, the first pair of bounds correspond to the supporting interval bounds, as obtained with mc::Interval, and which are valid over the entire domain \f$[-2,1]\times[-1,2]\f$. The second pair of bounds are the values of the convex and concave relaxations at the selected point \f$(0,1)\f$. In order to describe the convex and concave relaxations on the entire range \f$[-2,1]\times[-1,2]\f$, it would be necessary to increase the point vectors to different points. The current point can be set/modified by using the method mc::McCormick::c, for instance at \f$(-1,0)\f$

\code
      X.c( -1. );
      Y.c( 0. );
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ]
\endverbatim

The values of the McCormick convex and concave relaxations of \f$f(x,y)\f$ can be retrieved, respectively, as:

\code
      double Fcv = F.cv();
      double Fcc = F.cc();
\endcode

Likewise, the lower and upper bounds of the supporting interval bounds can be retrieved as:

\code
      double Flb = F.l();
      double Fub = F.u();
\endcode


\section sec_MCCORMICK_sub How do I compute a subgradient of the McCormick relaxations?

Computing a subgradient of a McCormick relaxation requires specification of the independent variables via the .sub method, prior to evaluating the function in mc::McCormick type. Continuing the previous example, the function has two independent variables \f$x\f$ and \f$y\f$. Defining \f$x\f$ and \f$y\f$ as the subgradient components \f$0\f$ and \f$1\f$ (indexing in C/C++ start at 0 by convention!), respectively, is done as follows:

\code
      X.sub( 2, 0 );
      Y.sub( 2, 1 );
\endcode

Similar to above, the McCormick convex and concave relaxations of \f$f(x,y)\f$ at \f$(-1,0)\f$ along with subgradients of these relaxations are computed as:

\code
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations and subgradients at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
\endverbatim

The additional information displayed corresponds to, respectively, a subgradient of the McCormick convex underestimator at (-1,0) and a subgradient of the McCormick concave overestimator at (-1,0). In turn, these subgradients can be used to construct affine relaxations on the current range, or passed to a bundle solver to locate the actual minimum or maximum of the McCormick relaxations.

The subgradients of the McCormick relaxations of \f$f(x,y)\f$ at the current point can be retrieved as follows:

\code
      const double* Fcvsub = F.cvsub();
      const double* Fccsub = F.ccsub();
\endcode

or, alternatively, componentwise as:

\code
      double Fcvsub_X = F.cvsub(0);
      double Fcvsub_Y = F.cvsub(1);
      double Fccsub_X = F.ccsub(0);
      double Fccsub_Y = F.ccsub(1);
\endcode

Directional subgradients can be propagated too. In the case that subgradients are to computed along the direction (1,-1) for both the convex and concave relaxations, we define:

\code
      const double sub_dir[2] = { 1., -1 };
      X.sub( 1, &sub_dir[0], &sub_dir[0] );
      Y.sub( 1, &sub_dir[1], &sub_dir[1] );
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations and subgradients along direction (1,-1) at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients along direction (1,-1) at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-6.89910e+00) : ( 3.44955e+00) ]
\endverbatim


\section sec_MCCORMICK_fadbad How do I compute McCormick relaxations of the partial derivatives or the Taylor coefficients of a factorable function using FADBAD++?

Now suppose one wants to compute McCormick relaxation not only for a given factorable function, but also for its partial derivatives. Continuing the previous example, the partial derivatives of \f$f(x,y)=x(\exp(x)-y)^2\f$ with respect to its independent variables \f$x\f$ and \f$y\f$ can be obtained via automatic differentiation (AD), either forward or reverse AD. This can be done for instance using the classes fadbad::F, and fadbad::B of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A>.

Considering forward AD first, we include the following header files:

\code
      #include "mcfadbad.hpp" // available in MC++
      #include "fadiff.h"     // available in FADBAD++
      typedef fadbad::F<MC> FMC;
\endcode

The variables are initialized and the derivatives and subgradients are seeded as follows:

\code
      FMC FX = X;             // initialize FX with McCormick variable X
      FX.diff(0,2);           // differentiate with respect to x (index 0 of 2)
      FX.x().sub(2,0);        // seed subgradient of x (index 0 of 2)

      FMC FY = Y;             // initialize FY with McCormick variable Y
      FY.diff(1,2);           // differentiate with respect to y (index 1 of 2)
      FY.x().sub(2,1);        // seed subgradient of y (index 1 of 2)
\endcode

As previously, the McCormick convex and concave relaxations of \f$f\f$, \f$\frac{\partial f}{\partial x}\f$, and \f$\frac{\partial f}{\partial y}\f$ at \f$(-1,0)\f$ on the range \f$[-2,1]\times[-1,2]\f$, along with subgradients of these relaxations, are computed as:

\code
      FMC FF = FX*pow(exp(FX)-FY,2);
      std::cout << "f relaxations and subgradients at (-1,0): " << FF.x() << std::endl;
      std::cout << "df/dx relaxations and subgradients at (-1,0): " << FF.d(0) << std::endl;
      std::cout << "df/dy relaxations and subgradients at (-1,0): " << FF.d(1) << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
df/dx relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -2.33469e+01 :  2.90549e+01 ] [ (-2.31383e+01,-1.94418e-01) : ( 1.59593e+00,-1.85362e+00) ]
df/dy relaxations and subgradients at (-1,0): [ -7.45866e+00 :  1.48731e+01 ] [ -5.96505e+00 :  7.71460e+00 ] [ (-5.96505e+00,-4.00000e+00) : ( 7.17326e+00,-4.00000e+00) ]
\endverbatim

Relaxations of the partial derivatives can also be computed using the backward mode of AD, which requires the additional header file:

\code
      #include "badiff.h"     // available in FADBAD++
      typedef fadbad::B<MC> BMC;
\endcode

then initialize and seed new variables and compute the function as follows:

\code
      BMC BX = X;             // initialize FX with McCormick variable X
      BX.x().sub(2,0);        // seed subgradient as direction (1,0)
      BMC BY = Y;             // initialize FY with McCormick variable Y
      BY.x().sub(2,1);        // seed subgradient as direction (1,0)

      BMC BF = BX*pow(exp(BX)-BY,2);
      BF.diff(0,1);           // differentiate f (index 0 of 1)
      std::cout << "f relaxations and subgradients at (-1,0): " << BF.x() << std::endl;
      std::cout << "df/dx relaxations and subgradients at (-1,0): " << BX.d(0) << std::endl;
      std::cout << "df/dy relaxations and subgradients at (-1,0): " << BY.d(0) << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
df/dx relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -1.37142e+01 :  1.60092e+01 ] [ (-1.35056e+01,-1.94418e-01) : ( 8.82498e+00,-1.31228e+00) ]
df/dy relaxations and subgradients at (-1,0): [ -7.45866e+00 :  1.48731e+01 ] [ -5.96505e+00 :  7.71460e+00 ] [ (-5.96505e+00,-4.00000e+00) : ( 7.17326e+00,-4.00000e+00) ]
\endverbatim

It is noteworthy that the bounds, McCormick relaxations and subgradients for the partial derivatives as computed with the forward and backward mode, although valid, may not be the same since the computations involve different sequences or even operations. In the previous examples, for instance, forward and backward AD produce identical interval bounds for \f$\frac{\partial f}{\partial x}\f$ and \f$\frac{\partial f}{\partial y}\f$ at \f$(-1,0)\f$, yet significantly tighter McCormick relaxations are obtained with backward AD for \f$\frac{\partial f}{\partial x}\f$ at \f$(-1,0)\f$.

Another use of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A> involves computing McCormick relaxations of the Taylor coefficients in the Taylor expansion of a factorable function in a given direction up to a certain order. Suppose we want to compute McCormick relaxation of the first 5 Taylor coefficients of \f$f(x,y)=x(\exp(x)-y)^2\f$ in the direction \f$(1,0)\f$, i.e. the direction of \f$x\f$. This information can be computed by using the classes fadbad::T, which requires the following header file:

\code
      #include "tadiff.h"     // available in FADBAD++
      typedef fadbad::T<MC> TMC;
\endcode

The variables are initialized and the derivatives and subgradients are seeded as follows:

\code
      TMC TX = X;             // initialize FX with McCormick variable X
      TX[0].sub(2,0);        // seed subgradient as direction (1,0)
      TMC TY = Y;             // initialize FY with McCormick variable Y
      TY[0].sub(2,1);        // seed subgradient as direction (1,0)
      TX[1] = 1.;             // Taylor-expand with respect to x

      TMC TF = TX*pow(exp(TX)-TY,2);
      TF.eval(5);            // Taylor-expand f to degree 5
      for( unsigned int i=0; i<=5; i++ )
        std::cout << "d^" << i << "f/dx^" << i << " relaxations and subgradients at (-1,0): " << TF[i] << std::endl;
\endcode

producing the output:

\verbatim
d^0f/dx^0 relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
d^1f/dx^1 relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -2.33469e+01 :  2.90549e+01 ] [ (-2.31383e+01,-1.94418e-01) : ( 1.59593e+00,-1.85362e+00) ]
d^2f/dx^2 relaxations and subgradients at (-1,0): [ -4.51302e+01 :  3.77111e+01 ] [ -1.97846e+01 :  2.25846e+01 ] [ (-1.97113e+01, 0.00000e+00) : ( 7.36023e+00,-4.06006e-01) ]
d^3f/dx^3 relaxations and subgradients at (-1,0): [ -2.65667e+01 :  2.82546e+01 ] [ -1.02662e+01 :  1.27412e+01 ] [ (-1.00820e+01,-4.51118e-02) : ( 7.66644e+00,-1.80447e-01) ]
d^4f/dx^4 relaxations and subgradients at (-1,0): [ -1.19764e+01 :  1.59107e+01 ] [ -4.29280e+00 :  6.13261e+00 ] [ (-4.25007e+00,-2.25559e-02) : ( 4.86086e+00,-5.63897e-02) ]
d^5f/dx^5 relaxations and subgradients at (-1,0): [ -4.44315e+00 :  7.16828e+00 ] [ -1.49744e+00 :  2.55611e+00 ] [ (-1.44773e+00,-6.76676e-03) : ( 2.29932e+00,-1.35335e-02) ]
\endverbatim

The zeroth Taylor coefficient corresponds to the function \f$f\f$ itself. It can also be checked that the relaxations of the first Taylor coefficient of \f$f\f$ matches those obtained with forward AD for \f$\frac{\partial f}{\partial x}\f$.

Naturally, the classes fadbad::F, fadbad::B and fadbad::T can be nested to produce relaxations of higher-order derivative information.


\section sec_MCCORMICK_fct Which functions are overloaded in McCormick relaxation arithmetic?

As well as overloading the usual functions <tt>exp</tt>, <tt>log</tt>, <tt>sqr</tt>, <tt>sqrt</tt>, <tt>pow</tt>, <tt>inv</tt>, <tt>cos</tt>, <tt>sin</tt>, <tt>tan</tt>, <tt>acos</tt>, <tt>asin</tt>, <tt>atan</tt>, <tt>erf</tt>, <tt>erfc</tt>, <tt>min</tt>, <tt>max</tt>, <tt>fabs</tt>, mc::McCormick also defines the following functions:
- <tt>fstep(x)</tt> and <tt>bstep(x)</tt>, implementing the forward step function (switching value from 0 to 1 for x>=0) and the backward step function (switching value from 1 to 0 for x>=0). These functions can be used to model a variety of discontinuous functions as first proposed in [Wechsung & Barton, 2012].
- <tt>ltcond(x,y,z)</tt> and <tt>gtcond(x,y,z)</tt>, similar in essence to <tt>fstep(x)</tt> and <tt>bstep(x)</tt>, and implementing disjunctions of the form { y if x<=0; z otherwise } and { y if x>=0; z otherwise }, respectively.
- <tt>inter(x,y,z)</tt>, computing the intersection \f$x = y\cap z\f$ and returning true/false if the intersection is nonempty/empty.
- <tt>hull(x,y)</tt>, computing convex/concave relaxations of the union \f$x\cup y\f$.
.


\section sec_MCCORMICK_opt What are the options in mc::McCormick and how are they set?

The class mc::McCormick has a public static member called mc::McCormick::options that can be used to set/modify the options; e.g.,

\code
      MC::options.ENVEL_USE = true;
      MC::options.ENVEL_TOL = 1e-12;
      MC::options.ENVEL_MAXIT = 100;
      MC::options.MVCOMP_USE = true;
\endcode

The available options are the following:

<TABLE border="1">
<CAPTION><EM>Options in mc::McCormick::Options: name, type and description</EM></CAPTION>
     <TR><TH><b>Name</b>  <TD><b>Type</b><TD><b>Default</b>
         <TD><b>Description</b>
     <TR><TH><tt>ENVEL_USE</tt> <TD><tt>bool</tt> <TD>true
         <TD>Whether to compute convex/concave envelopes for the neither-convex-nor-concave univariate functions such as odd power terms, sin, cos, asin, acos, tan, atan, erf, erfc. This provides tighter McCormick relaxations, but it is more time consuming. Junction points are computed using the Newton or secant method first, then the more robust golden section search method if unsuccessful.
     <TR><TH><tt>ENVEL_TOL</tt> <TD><tt>double</tt> <TD>1e-10
         <TD>Termination tolerance for determination function points in convex/concave envelopes of univariate terms.
     <TR><TH><tt>ENVEL_MAXIT</tt> <TD><tt>int</tt> <TD>100
         <TD>Maximum number of iterations for determination function points in convex/concave envelopes of univariate terms.
     <TR><TH><tt>MVCOMP_USE</tt> <TD><tt>bool</tt> <TD>false
         <TD>Whether to use Tsoukalas & Mitsos's multivariate composition result for min/max, product, and division terms; see [Tsoukalas & Mitsos, 2012]. This provides tighter McCormick relaxations, but it is more time consuming.
     <TR><TH><tt>MVCOMP_TOL</tt> <TD><tt>double</tt> <TD>1e1*machprec()
         <TD>Tolerance for equality test in subgradient propagation for product terms with Tsoukalas & Mitsos's multivariate composition result; see [Tsoukalas & Mitsos, 2012].
     <TR><TH><tt>DISPLAY_DIGITS</tt> <TD><tt>unsigned int</tt> <TD>5
         <TD>Number of digits in output stream
</TABLE>


\section sec_MC_err What Errors Can Be Encountered during the Computation of Convex/Concave Bounds?

Errors are managed based on the exception handling mechanism of the C++ language. Each time an error is encountered, a class object of type mc::McCormick::Exceptions is thrown, which contains the type of error. It is the user's responsibility to test whether an exception was thrown during a McCormick relaxation, and then make the appropriate changes. Should an exception be thrown and not caught by the calling program, the execution will stop.

Possible errors encountered during the computation of a McCormick relaxation are:

<TABLE border="1">
<CAPTION><EM>Errors during Computation of a McCormick relaxation</EM></CAPTION>
     <TR><TH><b>Number</b> <TD><b>Description</b>
     <TR><TH><tt>1</tt> <TD>Division by zero
     <TR><TH><tt>2</tt> <TD>Inverse with zero in range
     <TR><TH><tt>3</tt> <TD>Log with negative values in range
     <TR><TH><tt>4</tt> <TD>Square-root with nonpositive values in range
     <TR><TH><tt>5</tt> <TD>Inverse sine or cosine with values outside of \f$[-1,1]\f$ range
     <TR><TH><tt>6</tt> <TD>Tangent with values outside of \f$[-\frac{\pi}{2}+k\pi,\frac{\pi}{2}+k\pi]\f$ range
     <TR><TH><tt>-1</tt> <TD>Inconsistent size of subgradient between two mc::McCormick variables
     <TR><TH><tt>-2</tt> <TD>Failed to compute the convex or concave envelope of a univariate term
     <TR><TH><tt>-3</tt> <TD>Failed to propagate subgradients for a product term with Tsoukalas & Mitsos's multivariable composition result
</TABLE>

\section sec_MC_refs References
- Bompadre, A., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-011-9685-2">Convergence rate of McCormick relaxations</A>, <I>Journal of Global Optimization</I> <B>52</B>(1):1-28, 2012
- Bongartz, D., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-017-0547-4">Deterministic global optimization of process flowsheets in a reduced space using McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>in press</b>, 2017
- McCormick, G. P., <A href="http://dx.doi.org/10.1007/BF01580665">Computability of global solutions to factorable nonconvex programs: Part I. Convex underestimating problems</A>, <i>Mathematical Programming</i>, <b>10</b>(2):147-175, 1976
- Mitsos, A., B. Chachuat, and P.I. Barton, <A href="http://dx.doi.org/10.1137/080717341">McCormick-based relaxations of algorithms</A>, <i>SIAM Journal on Optimization</i>, <b>20</b>(2):573-601, 2009
- Najman, J., D. Bongartz, A. Tsoukalas, A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-016-0470-0">Erratum to: Multivariate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>68</b>:219-225, 2017
- Najman, J., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-016-0408-6">Convergence analysis of multivariate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>66</b>(4):597-, 2016
- Scott, J.K., M.D. Stuber, P.I. Barton, <A href="http://dx.doi.org/10.1007/s10898-011-9664-7">Generalized McCormick relaxations</A>. <i>Journal of Global Optimization</i>, <b>51</b>(4), 569-606, 2011
- Tsoukalas, A., and A. Mitsos, <A href="http://www.optimization-online.org/DB_HTML/2012/05/3473.html">Multi-variate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>59</b>, 633-662, 2014
- Wechsung, A., and P.I. Barton, <A href="http://dx.doi.org/10.1007/s10898-013-0060-3">Global optimization of bounded factorable functions with discontinuities</A>, <i>Journal of Global Optimization</i>, <b>58</b>(1), 1-30, 2013.
.
*/

#ifndef MC__VMCCORMICK_H
#define MC__VMCCORMICK_H

#include <iostream>
#include <iomanip>
#include <stdarg.h>
#include <cassert>
#include <string>
#include <limits>

#include "mcfunc.hpp"
#include "mcop.hpp"
// #include "vmccormick_linearizations.hpp"

// preprocessor variable for easier debugging
#undef MC__VMCCORMICK_DEBUG

namespace mc
{
//! @brief C++ class for vector McCormick relaxation arithmetic for factorable function
////////////////////////////////////////////////////////////////////////
//! mc::vMcCormick is a C++ class computing possibly multiple McCormick
//! convex/concave relaxations of factorable functions on a box at once,
//! as well as doing subgradient propagation. The template parameter
//! corresponds to the type used in the underlying interval arithmetic
//! computations.
////////////////////////////////////////////////////////////////////////
template <typename T>
class vMcCormick
////////////////////////////////////////////////////////////////////////
{
  template <typename U> friend class vMcCormick;

  template <typename U> friend vMcCormick<U> operator+
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator+
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator+
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator+
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> operator-
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator-
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator-
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator-
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> operator*
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator*
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator*
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> operator/
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator/
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> operator/
    ( const vMcCormick<U>&, const double );
  template <typename U> friend std::ostream& operator<<
    ( std::ostream&, const vMcCormick<U>& );
  template <typename U> friend bool operator==
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool operator!=
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool operator<=
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool operator>=
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool operator<
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool operator>
    ( const vMcCormick<U>&, const vMcCormick<U>& );

  template <typename U> friend vMcCormick<U> inv
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> sqr
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> exp
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> log
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> cos
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> sin
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> tan
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> acos
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> asin
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> atan
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> cosh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> sinh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> tanh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> coth
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> acosh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> asinh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> atanh
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> acoth
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> fabs
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> sqrt
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> root  // root function for the calculation of the n-th root
    ( const vMcCormick<U>&, const int );
  template <typename U> friend vMcCormick<U> xlog
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> fabsx_times_x
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> xexpax
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> centerline_deficit
    ( const vMcCormick<U>&, const double, const double );
  template <typename U> friend vMcCormick<U> wake_profile
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> wake_deficit
    ( const vMcCormick<U>&, const McCormick<U>&, const double, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> power_curve
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> lmtd
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> rlmtd
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> euclidean_norm_2d
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> expx_times_y
    ( const vMcCormick<U>&, const vMcCormick<U>& );
 template <typename U> friend vMcCormick<U> vapor_pressure
    (const vMcCormick<U>&, const double, const double, const double, const double, const double,
	 const double, const double, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> ideal_gas_enthalpy
    (const vMcCormick<U>&, const double, const double, const double, const double, const double, const double,
	 const double, const double, const double );
  template <typename U> friend vMcCormick<U> saturation_temperature
    (const vMcCormick<U>&, const double, const double, const double, const double, const double,
	 const double, const double, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> enthalpy_of_vaporization
    (const vMcCormick<U>&, const double, const double, const double, const double, const double, const double,
	 const double );
  template <typename U> friend vMcCormick<U> cost_function
    (const vMcCormick<U>&, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> sum_div
    ( const std::vector< vMcCormick<U> >&, const std::vector<double>& );
  template <typename U> friend vMcCormick<U> xlog_sum
    ( const std::vector< vMcCormick<U> >&, const std::vector<double>& );
  template <typename U> friend vMcCormick<U> nrtl_tau
    (const vMcCormick<U>&, const double, const double, const double, const double );
   template <typename U> friend vMcCormick<U> nrtl_dtau
    (const vMcCormick<U>&, const double, const double, const double );
  template <typename U> friend vMcCormick<U> nrtl_G
    (const vMcCormick<U>&, const double, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> nrtl_Gtau
    (const vMcCormick<U>&, const double, const double, const double, const double, const double );
  template <typename U> friend vMcCormick<U> nrtl_Gdtau
    (const vMcCormick<U>&, const double, const double, const double, const double, const double);
  template <typename U> friend vMcCormick<U> nrtl_dGtau
    (const vMcCormick<U>&, const double, const double, const double, const double, const double);
 template <typename U> friend vMcCormick<U> iapws
    (const vMcCormick<U>&, const double);
 template <typename U> friend vMcCormick<U> iapws
    (const vMcCormick<U>&, const vMcCormick<U>&, const double);
  template <typename U> friend vMcCormick<U> p_sat_ethanol_schroeder
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> rho_vap_sat_ethanol_schroeder
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> rho_liq_sat_ethanol_schroeder
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> covariance_function
    (const vMcCormick<U>&, const double);
  template <typename U> friend vMcCormick<U> acquisition_function
    (const vMcCormick<U>&, const vMcCormick<U>&, const double, const double);
  template <typename U> friend vMcCormick<U> gaussian_probability_density_function
    (const vMcCormick<U>&);
  template <typename U> friend vMcCormick<U> regnormal
    (const vMcCormick<U>&, const double, const double);
  template <typename U> friend vMcCormick<U> arh
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> erf
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> erfc
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> fstep
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> bstep
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> pow
    ( const vMcCormick<U>&, const int );
  template <typename U> friend vMcCormick<U> pow
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> pow
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> pow
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> prod
    ( const unsigned int, const vMcCormick<U>* );
  template <typename U> friend vMcCormick<U> monom
    ( const unsigned int, const vMcCormick<U>*, const unsigned* );
  template <typename U> friend vMcCormick<U> cheb
    ( const vMcCormick<U>&, const unsigned );
  template <typename U> friend vMcCormick<U> min
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> max
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> min
    ( const vMcCormick<U>&,const double );
  template <typename U> friend vMcCormick<U> max
    ( const vMcCormick<U>&, const double );
  template <typename U> friend vMcCormick<U> min
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> max
    ( const double, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> min
    ( const unsigned int, const vMcCormick<U>* );
  template <typename U> friend vMcCormick<U> max
    ( const unsigned int, const vMcCormick<U>* );
  template <typename U> friend vMcCormick<U> pos
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> neg
    ( const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> lb_func
    ( const vMcCormick<U>&, const double);
  template <typename U> friend vMcCormick<U> ub_func
    ( const vMcCormick<U>&, const double);
  template <typename U> friend vMcCormick<U> bounding_func
    ( const vMcCormick<U>&, const double, const double);
  template <typename U> friend vMcCormick<U> squash_node
    ( const vMcCormick<U>&, const double, const double);
  template <typename U> friend vMcCormick<U> mc_print
	( const vMcCormick<U>&, const int);
  template <typename U> friend vMcCormick<U> ltcond
    ( const vMcCormick<U>&, const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> ltcond
    ( const U&, const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> gtcond
    ( const vMcCormick<U>&, const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> gtcond
    ( const U&, const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend bool inter
    ( vMcCormick<U>&, const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> hull
    ( const vMcCormick<U>&, const vMcCormick<U>& );
  template <typename U> friend vMcCormick<U> cut
    ( const vMcCormick<U>& );

public:

  vMcCormick<T>& operator=
    ( const vMcCormick<T>& );
  vMcCormick<T>& operator=
    ( const T& );
  vMcCormick<T>& operator=
    ( const double );
  vMcCormick<T>& operator+=
    ( const vMcCormick<T>& );
  vMcCormick<T>& operator+=
    ( const double );
  vMcCormick<T>& operator-=
    ( const vMcCormick<T>& );
  vMcCormick<T>& operator-=
    ( const double );
  vMcCormick<T>& operator*=
    ( const vMcCormick<T>& );
  vMcCormick<T>& operator*=
    ( const double );
  vMcCormick<T>& operator/=
    ( const vMcCormick<T>& );
  vMcCormick<T>& operator/=
    ( const double );

  /** @defgroup VMCCORMICK Vector McCormick Relaxation Arithmetic for Factorable Functions
   *  @{
   */
  //! @brief Options of mc::vMcCormick
  static struct Options
  {
    //! @brief Constructor
    Options():
      ENVEL_USE(true), ENVEL_MAXIT(100), ENVEL_TOL(1e-9), MVCOMP_USE(false),
      MVCOMP_TOL(1e-9), DISPLAY_DIGITS(5), SUB_INT_HEUR_USE(false), /*COMPUTE_ADDITIONAL_LINS(false),*/ COMPUTE_POINT_RANKING(false)
      {}
    //! @brief Whether to compute convex/concave envelopes for the neither-convex-nor-concave univariate functions such as odd power terms, sin, cos, asin, acos, tan, atan, erf, erfc. This provides tighter McCormick relaxations, but it is more time consuming. Junction points are computed using the Newton or secant method first, then the more robust golden section search method if unsuccessful.
    bool ENVEL_USE;
    //! @brief Maximum number of iterations for determination function points in convex/concave envelopes of univariate terms.
    unsigned int ENVEL_MAXIT;
    //! @brief Termination tolerance for determination function points in convex/concave envelopes of univariate terms.
    double ENVEL_TOL;
    //! @brief Whether to use Tsoukalas & Mitsos's multivariate composition result for min/max, product, and division terms; see [Tsoukalas & Mitsos, 2012]. This provides tighter McCormick relaxations, but it is more time consuming.
    bool MVCOMP_USE;
    //! @brief Tolerance for testing equality in subgradient propagation for product terms with Tsoukalas & Mitsos's multivariate composition result; see [Tsoukalas & Mitsos, 2012].
    double MVCOMP_TOL;
    //! @brief Number of digits displayed with << operator (default=5)
    unsigned int DISPLAY_DIGITS;
	//! @brief Whether to use Najman & Mitsos's subgradient interval heuristic; see [Najman & Mitsos, 2018]. This often provides tighter relaxations, but is more time consuming.
    bool SUB_INT_HEUR_USE;
	//! @brief Whether to compute additional linearization points.
    // bool COMPUTE_ADDITIONAL_LINS;
	//! @brief Whether to compute a ranking of given points. The higher the value the possibly better the lin point
	bool COMPUTE_POINT_RANKING;
  } options;

   ////////////////////////////////////////////////////////////////////////
  //! mc::subHeur is a C++ structure for storing precomputed intervals,
  //! e.g., by the subgradient interval heuristic [Najman&Mitsos 2018], which can be used in order to improve the final McCormick relaxation.
  //! Note that this structure differs from the one in mccormick.hpp, since here we use all points to compute the best lower and upper bound candidate.
  ////////////////////////////////////////////////////////////////////////
  static thread_local struct SubHeur{

	SubHeur():
		intervals(std::vector<T>()), itIntervals(intervals.begin()), originalLowerBounds(0), originalUpperBounds(0),
		usePrecomputedIntervals(false), referencePoints(0), lowerBoundValues(std::vector<double>()), upperBoundValues(std::vector<double>()),
		bestLowerBound(-std::numeric_limits<double>::max()), bestUpperBound(std::numeric_limits<double>::max())
		{}

  	std::vector<T> intervals;						                /*<! vector holding precomputed intervals */
  	typename std::vector<T>::iterator itIntervals;	                /*<! iterator for getting the correct interval */
  	const std::vector<double>* originalLowerBounds;	                /*<! pointer to the vector of original lower bounds */
  	const std::vector<double>* originalUpperBounds;	                /*<! pointer to the vector of original upper bounds */
  	bool usePrecomputedIntervals;					                /*<! boolean telling whether Intervals should be used */
  	const std::vector<std::vector<double>>* referencePoints;		/*<! pointer to the vector holding the reference point */
    std::vector<double> lowerBoundValues;                           /*<! vector holding interval lower bound values for all points in the current operation */
    std::vector<double> upperBoundValues;                           /*<! vector holding interval upper bound values for all points in the current operation */
    double bestLowerBound;                                          /*<! value of best lower bound used as candidate for the heuristic in the current operation */
    double bestUpperBound;                                          /*<! value of best upper bound used as candidate for the heuristic in the current operation */
	std::vector<double> subHeurRanking;                             /*<! vector holding score values for points in the subgradient heuristic */
	unsigned bestPointCvIndex;                                      /*<! index of the point with highest score for convex relaxation in a given operation */
	unsigned bestPointCcIndex;                                      /*<! index of the point with highest score for concave relaxation in a given operation */

	/*
	* @brief Function for reserving enough space for the intervals
	*
	* @param[in] size is the size of the space to reserve, it should equal the number of operations to guarantee correctness
	*/
  	void reserve_size(unsigned int size){
  		intervals.reserve(size);
  	}

  	/*
	* @brief Function for resizing the candidate vectors
	*
	* @param[in] npts is the number of linearization points
	*/
    void resize_vectors(unsigned int npts){
		lowerBoundValues.clear();
		lowerBoundValues.resize(npts);
		upperBoundValues.clear();
		upperBoundValues.resize(npts);
		subHeurRanking.clear();
		subHeurRanking.resize(npts);
	}

  	/*
	* @brief Function for clearing pointers and the boolean. The candidates do not have to be cleared as they will be overwritten during the computation
	*/
  	void clear(){
  		intervals.clear();
  		itIntervals = intervals.begin();
  		originalLowerBounds = 0;
  		originalUpperBounds = 0;
  		referencePoints = 0;
  		usePrecomputedIntervals = false;
  	}

  	/*
	* @brief Function setting the candidate values to min and max double values
	*/
	void reset_candidates(){
		bestLowerBound = -std::numeric_limits<double>::max();
		bestUpperBound =  std::numeric_limits<double>::max();
	}

  	/*
	* @brief Function setting the value of lower and upper bound candidate at lin point ipt
	*
	* @param[in] cv is the min value of the convex affine relaxation
	* @param[in] cc is the max value of the concave affine relaxation
	* @param[in] ipt is the number of linearization point
	*/
	void set_lower_upper_bound_value(const double cv, const double cc, const unsigned ipt){
		if(!usePrecomputedIntervals){
			lowerBoundValues[ipt] = cv;
			upperBoundValues[ipt] = cc;
		}
	}

  	/*
	* @brief Function adding the slope + point value to the candidate value at lin point ipt
	*
	* @param[in] cvsub is the convex slope
	* @param[in] ccsub is the concave slope
	* @param[in] ipt is the number of linearization point
	* @param[in] var is the variable number
	*/
	void add_to_lower_upper_bound_values(const double cvsub, const double ccsub, const unsigned ipt, const unsigned var){
		if(!usePrecomputedIntervals){
			double p = cvsub>0 ? (*originalLowerBounds)[var] : (*originalUpperBounds)[var];
			lowerBoundValues[ipt] += cvsub * (p- (*referencePoints)[var][ipt]) ;
			p = ccsub>0 ? (*originalUpperBounds)[var] : (*originalLowerBounds)[var];
			upperBoundValues[ipt] += ccsub * (p- (*referencePoints)[var][ipt]);
		}
	}

  	/*
	* @brief Function updating the best candidate comparing to lower and upper bounds of point ipt
	*
	* @param[in] ipt is the number of linearization point
	*/
	void update_best_values(const unsigned ipt){
		if(!usePrecomputedIntervals){
			if(vMcCormick<T>::options.COMPUTE_POINT_RANKING){
				if(bestLowerBound < lowerBoundValues[ipt]){
					bestLowerBound = lowerBoundValues[ipt];
					bestPointCvIndex = ipt;
				}
				if(bestUpperBound > upperBoundValues[ipt]){
					bestUpperBound = upperBoundValues[ipt];
					bestPointCcIndex = ipt;
				}
			}
			else{
				bestLowerBound = std::max(bestLowerBound, lowerBoundValues[ipt]);
				bestUpperBound = std::min(bestUpperBound, upperBoundValues[ipt]);
			}

		}
	}

  	/*
	* @brief Function resetting the intervals vector iterator
	*/
  	void reset_iterator(){
  		itIntervals = intervals.begin();
  	}

  	/*
	* @brief Function for inserting intervals into the intervals vector
	*
	* @param[in] interval is the interval of the current operation
	*/
  	void insert_interval(T interval){
  		intervals.push_back(interval);
  	}

  	/*
	* @brief Function returning the interval from the intervals vector pointed by the itIntervals iterator
	*/
  	T get_interval(){
  		return (*itIntervals);
  	}

  	/*
	* @brief Function to increase the iterator
	*/
	void increase_iterator(){
		itIntervals++;
	}

  } subHeur;

  // static vMcCormick_Linearizations additionalLins;

  //! @brief Exceptions of mc::vMcCormick
  class Exceptions
  {
  public:
    //! @brief Enumeration type for McCormick exception handling
    enum TYPE{
      DIV=1,	//!< Division by zero
      INV,	//!< Inverse with zero in range
      LOG,	//!< Log with negative values in range
      SQRT,	//!< Square-root with nonpositive values in range
	  DPOW, //!< Power with negative double value in exponent
      ASIN,	//!< Inverse sine or cosine with values outside of \f$[-1,1]\f$ range
      TAN,	//!< Tangent with values outside of \f$[-\frac{\pi}{2}+k\pi,\frac{\pi}{2}+k\pi]\f$ range
      COTH,	//!< Hyperbolic tangent with zero in range
      CHEB,	//!< Chebyshev basis function different from [-1,1] range
	  LMTD, //!< LMTD function with non-positive values in range
	  RLMTD, //!< RLMTD function with non-positive values in range
	  VAPOR_PRESSURE, //!< Temperature <= 0 in range
	  IDEAL_GAS_ENTHALPY, //!< Temperature <= 0 in range
      SATURATION_TEMPERATURE, //!< Pressure <= 0 in range
	  ENTHALPY_OF_VAPORIZATION, //!< Temperature <= 0 in range
	  COST_FUNCTION, //!< Cost function with input <= 0 in range
	  COST_FUNCTION_MON, //!< Cost function is not monotonically increasing
	  NRTL_TAU, //!< NRTL tau function with Temperature <= 0 in range
	  NRTL_DTAU, //!< der NRTL tau function with Temperature <= 0 in range
	  NRTL_G, //!< NRTL G function with Temperature <= 0 in range
	  NRTL_GTAU, //!< NRTL Gtau function with Temperature <= 0 in range
	  NRTL_GDTAU, //!< NRTL Gdtau function with Temperature <= 0 in range
	  NRTL_DGTAU, //!< NRTL dGtau function with Temperature <= 0 in range
	  IAPWS, //!< Domain violation in IAPWS models
	  P_SAT_ETHANOL_SCHROEDER, //!< P_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
	  RHO_VAP_SAT_ETHANOL_SCHROEDER, //!< RHO_VAP_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
	  RHO_LIQ_SAT_ETHANOL_SCHROEDER, //!< RHO_LIQ_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
	  COVARIANCE_FUNCTION, //!< Covariance function with input < 0 in range
	  ACQUISITION_FUNCTION, //!< Acquisition function with input < 0 in range of sigma
	  REGNORMAL, //!< Regnormal function with parameters <= 0 in range
	  POS, //!< Pos function called with concave relaxation < MVCOMP_TOL
	  NEGAT, //!< Neg function called with convex relaxation > MVCOMP_TOL
	  LB_FUNC, //!< LB_func function called with concave relaxation < LB
	  UB_FUNC, //!< UB_func function called with convex relaxation > UB
	  SUM_DIV_VAR, //!< SUM_DIV function called with non-positive variables
	  SUM_DIV_COEFF, //!< SUM_DIV function called with non-positive coefficient
	  XLOG_SUM_VAR, //!< XLOG_SUM function called with non-positive variables
	  XLOG_SUM_COEFF, //!< XLOG_SUM function called with non-positive coefficient
	  DEBUG,//!< Error in vMcCormick Debug mode
	  MULTSUB=-4,	//!< Failed to propagate subgradients for a product term with Tsoukalas & Mitsos's multivariable composition result
      ENVEL, 	//!< Failed to compute the convex or concave envelope of a univariate term
      SUB,	//!< Inconsistent subgradient dimension between two mc::vMcCormick variables
	  PTS  //!< Inconsistent number of evaluation points
    };
    //! @brief Constructor for error <a>ierr</a>
    Exceptions( TYPE ierr ) : _ierr( ierr ){}
    //! @brief Inline function returning the error flag
    int ierr(){ return _ierr; }
    //! @brief Return error description
    std::string what() const {
      switch( _ierr ){
      case DIV:
        return "mc::vMcCormick\t Relaxation of Division with zero in range. Check if your denominators pass zero and use the pos or neg function.";
      case INV:
        return "mc::vMcCormick\t Relaxation of Inverse with zero in range. Check if your denominators pass zero and use the pos or neg function.";
      case LOG:
        return "mc::vMcCormick\t Relaxation of Log with negative values in range. ";
      case SQRT:
        return "mc::vMcCormick\t Relaxation of Square-root with nonpositive values in range.";
      case DPOW:
        return "mc::vMcCormick\t Relaxation of power function with nonpositive values in range.";
      case ASIN:
        return "mc::vMcCormick\t Inverse sine with values outside of [-1,1] range";
      case TAN:
        return "mc::vMcCormick\t Tangent with values pi/2+k*pi in range";
      case COTH:
        return "mc::vMcCormick\t Hyperbolic tangent with zero in range";
      case CHEB:
        return "mc::vMcCormick\t Chebyshev basis outside of [-1,1] range";
      case MULTSUB:
        return "mc::vMcCormick\t Subgradient propagation failed";
      case ENVEL:
        return "mc::vMcCormick\t Convex/concave envelope computation failed";
      case SUB:
        return "mc::vMcCormick\t Inconsistent subgradient dimension";
	  case PTS:
		return "mc::vMcCormick\t Inconsistent number of evaluation points";
      case LMTD:
        return "mc::vMcCormick\t Relaxation of LMTD with non-positive values in range. ";
      case RLMTD:
        return "mc::vMcCormick\t Relaxation of RLMTD with non-positive values in range. ";
	  case VAPOR_PRESSURE:
		return "mc::vMcCormick\t Relaxation of Vapor Pressure with Temperature <= 0 in range.";
	  case IDEAL_GAS_ENTHALPY:
		return "mc::vMcCormick\t Relaxation of Ideal Gas Enthalpy with Temperature <= 0 in range.";
	  case SATURATION_TEMPERATURE:
		return "mc::vMcCormick\t Relaxation of Saturation Temperature with pressure <= 0 in range.";
	  case ENTHALPY_OF_VAPORIZATION:
		return "mc::vMcCormick\t Relaxation of Enthalpy of Vaporization with Temperature <= 0 in range.";
	  case COST_FUNCTION:
		return "mc::vMcCormick\t Relaxation of Cost function with input <= 0 in range.";
	  case COST_FUNCTION_MON:
		return "mc::vMcCormick\t Cost function is not monotonically increasing. Please check your model.";
	  case NRTL_TAU:
		return "mc::vMcCormick\t Relaxation of NRTL tau function with Temperature <= 0 in range.";
	  case NRTL_DTAU:
		return "mc::vMcCormick\t Relaxation of derivative of NRTL tau function with Temperature <= 0 in range.";
	  case NRTL_G:
		return "mc::vMcCormick\t Relaxation of NRTL G function with Temperature <= 0 in range.";
      case NRTL_GTAU:
		return "mc::vMcCormick\t NRTL G*Tau with temperature <= 0 in range";
      case NRTL_GDTAU:
		return "mc::vMcCormick\t NRTL G*dTau/dT with temperature <= 0 in range";
      case NRTL_DGTAU:
		return "mc::vMcCormick\t NRTL dG/dT*tau with temperature <= 0 in range";
      case IAPWS:
		return "mc::vMcCormick\t Domain violation in IAPWS model.";
      case P_SAT_ETHANOL_SCHROEDER:
		return "mc::vMcCormick\t p_sat_ethanol_schroeder with temperature <= 0 in range.";
      case RHO_LIQ_SAT_ETHANOL_SCHROEDER:
		return "mc::vMcCormick\t rho_liq_sat_ethanol_schroeder with temperature <= 0 in range.";
      case RHO_VAP_SAT_ETHANOL_SCHROEDER:
		return "mc::vMcCormick\t rho_vap_sat_ethanol_schroeder with temperature <= 0 in range.";
	  case COVARIANCE_FUNCTION:
		return "mc::vMcCormick\t Relaxation of Covariance function with input < 0 in range.";
	  case ACQUISITION_FUNCTION:
		return "mc::vMcCormick\t Relaxation of Acquisition function with input < 0 in range of sima.";
	  case REGNORMAL:
		return "mc::vMcCormick\t Regnormal function with parameters <= 0.";
      case POS:
	  {
		std::ostringstream errmsg;
		errmsg << "mc::vMcCormick\t Pos function called with concave relaxation < " << std::setprecision(16) << machprec() << ".";
		return errmsg.str();
	  }
	  case NEGAT:
	  {
		std::ostringstream errmsg;
		errmsg << "mc::vMcCormick\t Neg function called with convex relaxation > -" << std::setprecision(16) << machprec() << ".";
		return errmsg.str();
	  }
	  case LB_FUNC:
		return "mc::vMcCormick\t LB_func function called with concave relaxation < lower bound.";
	  case UB_FUNC:
		return "mc::vMcCormick\t UB_func function called with convex relaxation > upper bound.";
	  case SUM_DIV_VAR:
		return "mc::vMcCormick\t SUM_DIV function called with non-positive variable.";
	  case SUM_DIV_COEFF:
		return "mc::vMcCormick\t SUM_DIV function called with non-convex coefficient.";
	  case XLOG_SUM_VAR:
		return "mc::vMcCormick\t XLOG_SUM function called with non-positive variable.";
	  case XLOG_SUM_COEFF:
		return "mc::vMcCormick\t XLOG_SUM function called with non-convex coefficient.";
	  case DEBUG:
	    return "mc::vMcCormick\t Error in vMcCormick Debug Mode. An incorrect calculation has occurred.";
	  }
      return "mc::vMcCormick\t Undocumented error";
    }

  private:
    TYPE _ierr;
  };

  //! @brief Default constructor (needed to declare arrays of vMcCormick class)
  vMcCormick():
    _npts(0), _nsub(0), _cv(0), _cc(0), _cvsub(0), _ccsub(0), _const(true)
    {}
  //! @brief Constructor for a constant value <a>c</a>
  vMcCormick
    ( const double c ):
    _npts(1), _nsub(0), _cv(new double[1]), _cc(new double[1]), _cvsub(new double*[1]), _ccsub(new double*[1]), _const(true)
    {
		_cv[0] = c;
		_cc[0] = c;
		_cvsub[0] = 0;
		_ccsub[0] = 0;
		Op<T>::I(_I,c);
    }
  //! @brief Constructor for an interval I
  vMcCormick
    ( const T&I ):
    _npts(1), _nsub(0), _cv(new double[1]), _cc(new double[1]), _cvsub(new double*[1]), _ccsub(new double*[1]), _const(true)
    {
		Op<T>::I(_I,I);
		_cv[0] = Op<T>::l(I); _cc[0] = Op<T>::u(I);
		_cvsub[0] = 0;
		_ccsub[0] = 0;
    }
  //! @brief Constructor for a variable, whose range is <a>I</a> and value is <a>c</a>
  vMcCormick
    ( const T&I, const double c ):
    _npts(1), _nsub(0), _cv(new double[1]), _cc(new double[1]), _cvsub(new double*[1]), _ccsub(new double*[1]), _const(false)
    {
		Op<T>::I(_I,I);
		_cv[0] = c; _cc[0] = c;
		_cvsub[0] = 0;
		_ccsub[0] = 0;
    }
  //! @brief Constructor for a variable, whose range is <a>I</a> and convex and concave bounds are <a>cv</a> and <a>cc</a>
  vMcCormick
    ( const T&I, const double cv, const double cc ):
    _npts(1), _nsub(0), _cv(new double[1]), _cc(new double[1]), _cvsub(new double*[1]), _ccsub(new double*[1]), _const(false)
    {
		Op<T>::I(_I,I);
		_cv[0] = cv;
		_cc[0] = cc;
		_cvsub[0] = 0;
		_ccsub[0] = 0;
		cut();
    }
  //! @brief Constructor for a constant value <a>c</a> with <a>npts</a> points
  vMcCormick
    ( const double c, const unsigned int npts ):
    _npts(npts), _nsub(0), _cv(new double[npts]), _cc(new double[npts]), _cvsub(new double*[npts]), _ccsub(new double*[npts]), _const(true)
    {
		for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = c;
			_cc[ipt] = c;
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
		Op<T>::I(_I,c);
    }
  //! @brief Constructor for an interval I with <a>npts</a> points
  vMcCormick
    ( const T&I, const unsigned int npts ):
    _npts(npts), _nsub(0), _cv(new double[npts]), _cc(new double[npts]), _cvsub(new double*[npts]), _ccsub(new double*[npts]), _const(true)
    {
		Op<T>::I(_I,I);
		for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = Op<T>::l(I); _cc[ipt] = Op<T>::u(I);
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
    }
  //! @brief Constructor for a variable with <a>npts</a> points, whose range is <a>I</a> and values are <a>c</a>
  vMcCormick
    ( const T&I, const std::vector<double> c, const unsigned int npts ):
    _npts(npts), _nsub(0), _cv(new double[npts]), _cc(new double[npts]), _cvsub(new double*[npts]), _ccsub(new double*[npts]), _const(false)
    {
		if(c.size() != _npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS);
		Op<T>::I(_I,I);
		for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = c[ipt]; _cc[ipt] = c[ipt];
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
    }
  //! @brief Constructor for a variable with multiple points, whose range is <a>I</a> and values are <a>c</a>
  vMcCormick
    ( const T&I, const std::vector<double> c ):
    _npts(c.size()), _nsub(0), _cv(new double[c.size()]), _cc(new double[c.size()]), _cvsub(new double*[c.size()]), _ccsub(new double*[c.size()]), _const(false)
    {
		if(c.size() != _npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS);
		Op<T>::I(_I,I);
		for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = c[ipt]; _cc[ipt] = c[ipt];
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
    }
  //! @brief Constructor for a variable with <a>npts</a> points, whose range is <a>I</a> and convex and concave bounds are <a>cv</a> and <a>cc</a>
  vMcCormick
    ( const T&I, const std::vector<double> cv, const std::vector<double> cc, const unsigned int npts ):
    _npts(npts), _nsub(0), _cv(new double[npts]), _cc(new double[npts]), _cvsub(new double*[npts]), _ccsub(new double*[npts]), _const(false)
    {
		if(cv.size() != _npts || cc.size() != _npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS);
		Op<T>::I(_I,I);
	    for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = cv[ipt];
			_cc[ipt] = cc[ipt];
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
		cut();
    }
  //! @brief Constructor for a variable with multiple points, whose range is <a>I</a> and convex and concave bounds are <a>cv</a> and <a>cc</a>
  vMcCormick
    ( const T&I, const std::vector<double> cv, const std::vector<double> cc ):
    _npts(cv.size()), _nsub(0), _cv(new double[cv.size()]), _cc(new double[cv.size()]), _cvsub(new double*[cv.size()]), _ccsub(new double*[cv.size()]), _const(false)
    {
		if(cv.size() != cc.size()) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS);
		Op<T>::I(_I,I);
	    for(unsigned int ipt = 0;ipt<_npts;ipt++){
			_cv[ipt] = cv[ipt];
			_cc[ipt] = cc[ipt];
			_cvsub[ipt] = 0;
			_ccsub[ipt] = 0;
		}
		cut();
    }
  //! @brief Copy constructor
  vMcCormick
    ( const vMcCormick<T>&MC ):
    _npts(MC._npts), _nsub(MC._nsub),
	_cv(MC._npts>0?new double[_npts]:0), _cc(MC._npts>0?new double[_npts]:0),
    _cvsub(MC._npts>0?new double*[_npts]:0), _ccsub(MC._npts>0?new double*[_npts]:0),
    _const(MC._const)
    {
		Op<T>::I(_I,MC._I);
		for(unsigned int ipt=0; ipt<_npts; ipt++){
			_cv[ipt] = MC._cv[ipt];
			_cc[ipt] = MC._cc[ipt];
			if(_nsub>0){
				_cvsub[ipt] = new double[_nsub];
				_ccsub[ipt] = new double[_nsub];
				for ( unsigned int i=0; i<_nsub; i++ ){
					_cvsub[ipt][i] = MC._cvsub[ipt][i];
					_ccsub[ipt][i] = MC._ccsub[ipt][i];
				}
			}else{
				_cvsub[ipt] = 0;
				_ccsub[ipt] = 0;
			}
		}
    }
  //! @brief Copy constructor doing type conversion for underlying interval
  template <typename U> vMcCormick
    ( const vMcCormick<U>&MC ):
    _npts(MC._npts), _nsub(MC._nsub),
	_cv(MC._npts>0?new double[_npts]:0), _cc(MC._npts>0?new double[_npts]:0),
    _cvsub(MC._npts>0?new double*[_npts]:0), _ccsub(MC._npts>0?new double*[_npts]:0),
    _const(MC._const)
    {
		Op<T>::I(_I,MC._I);
		for(unsigned int ipt=0; ipt<_npts; ipt++){
			_cv[ipt] = MC._cv[ipt];
			_cc[ipt] = MC._cc[ipt];
			if(_nsub>0){
				_cvsub[ipt] = new double[_nsub];
				_ccsub[ipt] = new double[_nsub];
				for ( unsigned int i=0; i<_nsub; i++ ){
					_cvsub[ipt][i] = MC._cvsub[ipt][i];
					_ccsub[ipt][i] = MC._ccsub[ipt][i];
				}
			}else{
				_cvsub[ipt] = 0;
				_ccsub[ipt] = 0;
			}
		}
    }

  //! @brief Destructor
  ~vMcCormick()
  {
	delete [] _cv;
	delete [] _cc;
	for(unsigned int ipt =0;ipt<_npts;ipt++){
		delete [] _cvsub[ipt];
		delete [] _ccsub[ipt];
	}
	delete [] _cvsub;
	delete [] _ccsub;
  }

  //! @brief Number of propagated points
  unsigned int& npts()
  {
	  return _npts;
  }
  unsigned int npts() const
  {
	  return _npts;
  }

  //! @brief Number of subgradient components/directions
  unsigned int& nsub()
	{
	  return _nsub;
	}
  unsigned int nsub() const
    {
      return _nsub;
    }
  //! @brief Interval bounds
  T& I()
    {
      return _I;
    }
  const T& I() const
    {
      return _I;
    }
  //! @brief Lower bound
  double l() const
    {
      return Op<T>::l(_I);
    }
  //! @brief Upper bound
  double u() const
    {
      return Op<T>::u(_I);
    }
  //! @brief Convex bound at point <a>ipt</a>
  double& cv(unsigned int ipt)
    {
      return _cv[ipt];
    }
  double cv(unsigned int ipt) const
    {
      return _cv[ipt];
    }
  //! @brief Concave bound at point <a>i</a>
  double& cc(unsigned int ipt)
    {
      return _cc[ipt];
    }
  double cc(unsigned int ipt) const
    {
      return _cc[ipt];
    }
  //! @brief Pointer to convex bounds
  double*& cv()
    {
      return _cv;
    }
  const double* cv() const
    {
      return _cv;
    }
  //! @brief Pointer to concave bounds
  double*& cc()
    {
      return _cc;
    }
  const double* cc() const
    {
      return _cc;
    }
  //! @brief Pointer to a subgradient of convex underestimator at point <a>ipt</a>
  double*& cvsub(unsigned int ipt)
    {
      return _cvsub[ipt];
    }
  const double* cvsub(unsigned int ipt) const
    {
      return _cvsub[ipt];
    }
  //! @brief Pointer to a subgradient of concave overestimator at point <a>ipt</a>
  double*& ccsub(unsigned int ipt)
    {
      return _ccsub[ipt];
    }
  const double* ccsub(unsigned int ipt) const
    {
      return _ccsub[ipt];
    }
  //! @brief <a>j</a>th component of a subgradient of convex underestimator at <a>ipt</a>th point
  double& cvsub
    ( const unsigned int ipt, const unsigned int j )
    {
      return _cvsub[ipt][j];
    }
  double cvsub
    ( const unsigned int ipt,  const unsigned int j ) const
    {
      return _cvsub[ipt][j];
    }
  //! @brief <a>j</a>th component of a subgradient of concave overestimator at <a>ipt</a>th point
  double& ccsub
    ( const unsigned int ipt, const unsigned int j )
    {
      return _ccsub[ipt][j];
    }
  double ccsub
    ( const unsigned int ipt, const unsigned int j ) const
    {
      return _ccsub[ipt][j];
    }

  //! @brief Set interval bounds
  void I
    ( const T& I )
    {
      Op<T>::I(_I,I);
    }
  //! @brief Set convex bound at point <a>ipt</a> to <a>cv</a>
  void cv
    ( unsigned int ipt, const double& cv )
    {
      _cv[ipt] = cv;
      _const = false;
    }
  //! @brief Set convex bounds to <a>cv</a>
  void cv
	( const std::vector<double>& cv )
	{
	  if( _npts != cv.size() ) throw Exceptions(Exceptions::PTS);
	  for(unsigned int ipt=0;ipt<_npts;ipt++){
		_cv[ipt] = cv[ipt];
	  }
      _const = false;
    }
  //! @brief Set concave bound at point <a>ipt</a> to <a>cc</a>
  void cc
    ( unsigned int ipt, const double& cc )
    {
      _cc[ipt] = cc;
      _const = false;
    }
  //! @brief Set concave bounds to <a>cc</a>
  void cc
	( const std::vector<double>& cc )
	{
	  if( _npts != cc.size() ) throw Exceptions(Exceptions::PTS);
	  for(unsigned int ipt=0;ipt<_npts;ipt++){
		_cc[ipt] = cc[ipt];
	  }
      _const = false;
    }
  //! @brief Set both convex and concave bounds at point <a>ipt</a> to <a>c</a>
  void c
    ( unsigned int ipt, const double& c )
    {
      _cv[ipt] = _cc[ipt] = c;
      _const = false;
    }
  //! @brief Set both convex and concave bounds to <a>c</a>
  void c
	( const std::vector<double>& c )
	{
	  if( _npts != c.size() ){
		 throw Exceptions(Exceptions::PTS);
	  }
	  for(unsigned int ipt=0;ipt<_npts;ipt++){
		_cv[ipt] = _cc[ipt] = c[ipt];
	  }
      _const = false;
    }

  //! @brief Set dimension of subgradient of all points to <a>nsub</a>
  vMcCormick<T>& sub
    ( const unsigned int nsub);
  //! @brief Set dimension of subgradient of all points to <a>nsub</a> and variable index <a>isub</a> (starts at 0)
  vMcCormick<T>& sub
    ( const unsigned int nsub, const unsigned int isub );
  //! @brief Set dimension of subgradient to <a>nsub</a>, the number of points to <a>npts</a> and subgradient values for the convex and concave relaxations to <a>cvsub</a> and <a>ccsub</a>
  vMcCormick<T>& sub
    ( const unsigned int nsub, const std::vector< std::vector<double> >& cvsub, const std::vector< std::vector<double> >& ccsub, const unsigned int npts );

  //! @brief Cut convex/concave relaxations at interval bound
  vMcCormick<T>& cut();
  //! @brief Apply the subgradient interval heuristic by [Najman&Mitsos 2018]
  vMcCormick<T>& apply_subgradient_interval_heuristic();
  //! @brief Compute affine underestimator at <a>p</a> based on a subgradient value of the convex underestimator at <a>pref</a> using point <a>ipt</a>
  double laff
    ( const unsigned int ipt, const double*p, const double*pref ) const;
  //! @brief Compute lower bound on range <a>Ip</a> based on an affine underestimator of the convex underestimator at <a>pref</a> using point <a>ipt</a>
  double laff
    ( const unsigned int ipt, const T*Ip, const double*pref ) const;
  //! @brief Compute affine overestimator at <a>p</a> based on a subgradient value of the concave overestimator at <a>pref</a> using point <a>ipt</a>
  double uaff
    ( const unsigned int ipt, const double*p, const double*pref ) const;
  //! @brief Compute upper bound on range <a>Ip</a> based on an affine overestimator of the concave overestimator at <a>pref</a> using point <a>ipt</a>
  double uaff
    ( const unsigned int ipt, const T*Ip, const double*pref ) const;
  //! @brief Print point <a>ipt</a> and its subgradient to ostream
  void print
   ( std::ostream&out, unsigned int ipt );
   //! @brief Print all points and its subgradients to ostream
  void print_all
   ( std::ostream&out );
  /** @} */

private:

  //! @brief Number of relaxation points
  unsigned int _npts;
  //! @brief Number of subgradient components
  unsigned int _nsub;
  //! @brief Interval bounds
  T _I;
  //! @brief Convex bounds
  double *_cv;
  //! @brief Concave bounds
  double *_cc;
  //! @brief Subgradients of convex underestimator
  double **_cvsub;
  //! @brief Subgradients of concave overestimator
  double **_ccsub;
  //! @brief Whether the convex/concave bounds are constant
  bool _const;

  //! @brief Enum for easier understanding of heuristics
  enum CONVEXITY{
	  CONV_NONE = 0,	//!< not convex or concave
	  CONVEX,			//!< convex
	  CONCAVE			//!< concave
  };

  //! @brief Enum for easier understanding of heuristics
  enum MONOTONICITY{
	  MON_NONE = 0,		//!< not increasing or decreasing
	  MON_INCR,			//!< increasing
	  MON_DECR			//!< decreasing
  };

  //! @brief Set subgradient size to <a>nsub</a> and set constness of convex/concave bounds to <a>cst</a>, only possible if number of points is set correctly
  void _sub
    ( const unsigned int nsub, const bool cst, const unsigned int npts );
  //! @brief Reset subgradient arrays of all points
  void _sub_reset();
  //! @brief Resize subgradient arrays and set the number of points to <a>npts</a>
  void _sub_resize
    ( const unsigned int nsub, const unsigned int npts );
  //! @brief Copy subgradient arrays of all points
  void _sub_copy
    ( const vMcCormick<T>&MC );
  //! @brief Reset all arrays of all points
  void _pts_reset();
  //! @brief Resize all arrays to <a>npts</a> points
  void _pts_resize
    ( const unsigned int npts );
   //! @brief Copy convex, concave and const arrays of all points
  void _pts_copy
    ( const vMcCormick<T>&MC );
  //! @brief Copy all arrays of all points and subgradients
  void _pts_sub_copy
    ( const vMcCormick<T>&MC );
  //! @brief Set subgradient size to <a>nsub</a>, set number of points to <a>npts</a> and set constness of convex/concave bounds to <a>cst</a>
  void _pts_sub
	( const unsigned int nsub, const bool cst, const unsigned int npts );

  //! @brief Compute vMcCormick relaxation of summation term u1+u2 with u2 constant
  vMcCormick<T>& _sum1
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of summation term u1+u2 with neither u1 nor u2 constant
  vMcCormick<T>& _sum2
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );

  //! @brief Compute vMcCormick relaxation of subtraction term u1-u2 with u2 constant
  vMcCormick<T>& _sub1
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of subtraction term u1-u2 with u1 constant
  vMcCormick<T>& _sub2
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of subtraction term u1-u2 with neither u1 nor u2 constant
  vMcCormick<T>& _sub3
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );

  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1 >= 0 and u2l >= 0, with u2 constant
  vMcCormick<T>& _mul1_u1pos_u2pos
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1 >= 0 and u2l >= 0, with neither u1 nor u2 constant
  vMcCormick<T>& _mul2_u1pos_u2pos
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with u2 constant
  vMcCormick<T>& _mul1_u1pos_u2mix
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with u1 constant
  vMcCormick<T>& _mul2_u1pos_u2mix
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with neither u1 nor u2 constant
  vMcCormick<T>& _mul3_u1pos_u2mix
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1l <= 0 <= u1u and u2l <= 0 <= u2u, with u2 constant
  vMcCormick<T>& _mul1_u1mix_u2mix
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 in the case u1l <= 0 <= u1u and u2l <= 0 <= u2u, with neither u1 nor u2 constant
  vMcCormick<T>& _mul2_u1mix_u2mix
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );
  //! @brief Compute vMcCormick relaxation of product term u1*u2 using Tsoukalas & Mitsos multivariable composition result
  vMcCormick<T>& _mulMV
    ( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 );

  //! @brief Prototype function for finding junction points in convex/concave envelopes of univariate terms
  typedef double (puniv)
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr );
  //! @brief Newton method for root finding
  static double _newton
    ( const double x0, const double xL, const double xU, puniv f,
      puniv df, const double*rusr, const int*iusr=0, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Secant method for root finding
  static double _secant
    ( const double x0, const double x1, const double xL, const double xU,
      puniv f, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Golden section search method for root finding
  static double _goldsect
    ( const double xL, const double xU, puniv f, const double*rusr,
      const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Golden section search iterations
  static double _goldsect_iter
    ( const bool init, const double a, const double fa, const double b,
      const double fb, const double c, const double fc, puniv f,
      const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of odd power terms
  static double* _oddpowcv
    ( const double x, const int iexp, const double xL, const double xU );
  //! @brief Compute concave envelope of odd power terms
  static double* _oddpowcc
    ( const double x, const int iexp, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of odd power terms
  static double _oddpowenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of odd power terms
  static double _oddpowenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

 //! @brief Compute convex envelope of odd Chebyshev terms
  static double* _oddchebcv
    ( const double x, const int iord, const double xL, const double xU );
  //! @brief Compute concave envelope of odd Chebyshev terms
  static double* _oddchebcc
    ( const double x, const int iord, const double xL, const double xU );
  //! @brief Compute convex envelope of even Chebyshev terms
  static double* _evenchebcv
    ( const double x, const int iord, const double xL, const double xU );

  //! @brief Compute convex envelope of erf terms
  static double* _erfcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of erf terms
  static double* _erfcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of erf terms
  static double _erfenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of erf terms
  static double _erfenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of atan terms
  static double* _atancv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of atan terms
  static double* _atancc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of atan terms
  static double _atanenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of atan terms
  static double _atanenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of sinh terms
  static double* _sinhcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of sinh terms
  static double* _sinhcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of sinh terms
  static double _sinhenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of sinh terms
  static double _sinhenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of tanh terms
  static double* _tanhcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of tanh terms
  static double* _tanhcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of tanh terms
  static double _tanhenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of tanh terms
  static double _tanhenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

   //! @brief Compute convex envelope of xexpax = x*exp(a*x) terms
  static double* _xexpaxcv
    ( const double x, const double xL, const double xU, const double a );
  //! @brief Compute concave envelope of xexpax = x*exp(a*x) terms
  static double* _xexpaxcc
    ( const double x, const double xL, const double xU, const double a );
  //! @brief Compute residual value for junction points in the envelope of xexpax = x*exp(a*x) terms
  static double _xexpaxenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of xexpax = x*exp(a*x) terms
  static double _xexpaxenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute value of convex relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double _xlog_sum_cv_val
    ( const std::vector<double> &x, const std::vector<double> &coeffs, const std::vector<double> &borderPoint,
      const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds );
  //! @brief Compute value of concave relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double _xlog_sum_cc_val
    ( const std::vector<double> &x, const std::vector<double> &coeffs, const std::vector<double> &borderPoint,
      const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds );
  //! @brief Compute convex relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double* _xlog_sum_cv
    ( std::vector<double> &xcv, const double xcc, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, const std::vector<double> &xcc_vec);
  //! @brief Compute concave relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double* _xlog_sum_cc
    ( std::vector<double> &xcc, const double xcv, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds );
  //! @brief Compute residual derivative for junction points in the first convex relaxation of x*log(a1*x+b1*y1+...) terms
  static double _xlog_sum_cv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual value for junction points in the first convex relaxation of x*log(a1*x+b1*y1+...) terms
  static double _xlog_sum_cv_ddfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute derivative/subgradient value of the convex relaxation of xlog_sum w.r.t. some direction
  static double _xlog_sum_cv_partial_derivative
	( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, unsigned facet, unsigned direction );
  //! @brief Compute derivative/subgradient value of the concave relaxation of xlog_sum w.r.t. some direction
  static double _xlog_sum_cc_partial_derivative
	( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, unsigned facet, unsigned direction );

  //! @brief Compute convex envelope of nonlinear function 1
  static double* _regnormal_cv
    ( const double x, const double a, const double b, const double xL, const double xU );
  //! @brief Compute concave envelope of nonlinear function 1
  static double* _regnormal_cc
    ( const double x, const double a, const double b, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of nonlinear function 1
  static double _regnormal_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of nonlinear function 1
  static double _regnormal_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Provide solution point of the equality x*(fixed_point - x) = 1 - exp( 0.5*sqr(x)^2 - 0.5 *(starting_point)^2)
  static double _gpdf_compute_sol_point
    ( const double xL, const double xU, const double starting_point, const double fixed_point );
  //! @brief Compute residual value for junction points in the envelope of gaussian probability density function
  static double _gpdf_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of gaussian probability density function
  static double _gpdf_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of a step at 0
  static double* _stepcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of a step at 0
  static double* _stepcc
    ( const double x, const double xL, const double xU );

  //! @brief Compute arg min & arg max for the cos envelope
  static double* _cosarg
    (  const double xL, const double xU );
  //! @brief Compute convex envelope of cos terms
  static double* _coscv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of cos terms
  static double* _coscc
    ( const double x, const double xL, const double xU );
  //! @brief Compute convex envelope of cos terms in [-PI,PI]
  static double* _coscv2
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of cos terms
  static double _cosenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of cos terms
  static double _cosenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of asin terms
  static double* _asincv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of asin terms
  static double* _asincc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of asin terms
  static double _asinenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of asin terms
  static double _asinenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of tan terms
  static double* _tancv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of tan terms
  static double* _tancc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of tan terms
  static double _tanenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of tan terms
  static double _tanenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  // @AVT.SVT 01.09.2017 added for computation of convex envelope of enthalpy of vaporization models
  //! @brief Compute convex envelope of enthalpy of vaporization models
  static double _dhvapenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr );
  /**
	* @brief Compute monotonicity and convexity of the Guthrie cost function
	*
	* added by AVT.SVT 06.11.2017
	*
	* @param[in,out] monotonicity	defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity      defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] p1      			parameter p1 of Guthrie function
	* @param[in] p2      			parameter p2 of Guthrie function
	* @param[in] p3      			parameter p3 of Guthrie function
	* @param[in] l     				lower bound
	* @param[in] u      			upper bound
	*/
  static void _cost_Guthrie_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double p1, const double p2, const double p3, const double l, const double u );
  /**
	* @brief Compute monotonicity and convexity of the tau function of an NRTL model
	*
	* added by AVT.SVT 23.11.2017
	*
	* @param[in,out] monotonicity	defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity      defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] a      			parameter a of tau function
	* @param[in] b      			parameter b of tau function
	* @param[in] e      			parameter e of tau function
	* @param[in] f      			parameter f of tau function
	* @param[in] l     				lower bound
	* @param[in] u      			upper bound
	* @param[in,out] new_l     		lower range bound computed in the case of non-monotonicity
	* @param[in,out] new_u      	upper range bound computed in the case of non-monotonicity
	* @param[in,out] zmin     		point where lower bound computed in the case of non-monotonicity is attained
	* @param[in,out] zmax      		point where upper bound computed in the case of non-monotonicity is attained
	*/
  static void _nrtl_tau_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double a, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax );
  /**
	* @brief Compute monotonicity and convexity of the derivative of the tau function of an NRTL model
	*        Theorems 1 and 2 of Najman, Bongartz & Mitsos 2019 Relaxations of thermodynamic property and costing models in process engineering
	*
	* added by AVT.SVT 23.11.2017
	*
	* @param[in,out] monotonicity	defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity      defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] b      			parameter b of tau function
	* @param[in] e      			parameter e of tau function
	* @param[in] f      			parameter f of tau function
	* @param[in] l     				lower bound
	* @param[in] u      			upper bound
	* @param[in,out] new_l     		lower range bound computed in the case of non-monotonicity
	* @param[in,out] new_u      	upper range bound computed in the case of non-monotonicity
	* @param[in,out] zmin     		point where lower bound computed in the case of non-monotonicity is attained
	* @param[in,out] zmax      		point where upper bound computed in the case of non-monotonicity is attained
	*/
  static void _nrtl_der_tau_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax );


#ifdef  MC__VMCCORMICK_DEBUG
  /**
	* @brief Perform a simple check if the vMcCormick result of a univariate operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin			the variable inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const vMcCormick<T> &MCin, const vMcCormick<T> &MCout, std::string &operation);
   /**
	* @brief Perform a simple check if the vMcCormick result of a multivariate operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin1			the 1st variable inserted into the operation
	* @param[in] MCin2			the 2nd variable inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const vMcCormick<T> &MCin1, const vMcCormick<T> &MCin2, const vMcCormick<T> &MCout, std::string &operation);

	/**
	* @brief Perform a simple check if the vMcCormick result of a multivariate (>2D) operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin			the variables inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const std::vector<vMcCormick<T>> &MCin, const vMcCormick<T> &MCout, std::string &operation);
#endif
  //! @brief  array containing precomputed roots of Q^k(x) up to power 2*k+1 for k=1,...,10 [Liberti & Pantelides (2002), "Convex envelopes of Monomial of Odd Degree]
  static double _Qroots[10];
};

////////////////////////////////////////////////////////////////////////

template <typename T>
double vMcCormick<T>::_Qroots[10] = { -0.5000000000, -0.6058295862, -0.6703320476, -0.7145377272, -0.7470540749,
                                     -0.7721416355, -0.7921778546, -0.8086048979, -0.8223534102, -0.8340533676 };

template <typename T> inline void
vMcCormick<T>::_sub_reset()
{
  for(unsigned int ipt=0; ipt<_npts; ipt++){
	delete [] _cvsub[ipt];
	delete [] _ccsub[ipt];
    _cvsub[ipt] = _ccsub[ipt] = 0;
  }
}

template <typename T> inline void
vMcCormick<T>::_sub_resize
( const unsigned int nsub, const unsigned int npts )
{
  if( _npts != npts ) throw Exceptions( Exceptions::PTS); // we can only resize subgradients if the number of points is correct
  if( _nsub != nsub ){
	for(unsigned int ipt=0; ipt<_npts; ipt++){
		delete [] _cvsub[ipt];
		delete [] _ccsub[ipt];
	}
    _nsub = nsub;
	for(unsigned int ipt=0; ipt<_npts;ipt++){
		if( _nsub > 0 ){
		  _cvsub[ipt] = new double[_nsub];
		  _ccsub[ipt] = new double[_nsub];
		}
		else{
		  _cvsub[ipt] = _ccsub[ipt] = 0;
		}
	}
  }
  // check for null pointers, e.g., when points had to be reset, we construct _npts subs with 0 pointers
  if (_cvsub[0] == NULL || _cvsub[0] == 0 || _ccsub[0] == NULL || _ccsub == 0) {
	  for (unsigned int ipt = 0; ipt < _npts;ipt++) {
		  if (_nsub > 0) {
			  _cvsub[ipt] = new double[_nsub];
			  _ccsub[ipt] = new double[_nsub];
		  }
		  else {
			  _cvsub[ipt] = _ccsub[ipt] = 0;
		  }
	  }
  }
}

template <typename T> inline void
vMcCormick<T>::_sub_copy
( const vMcCormick<T>&MC )
{
  _sub_resize( MC._nsub, MC._npts );
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  for ( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = MC._cvsub[ipt][i];
		_ccsub[ipt][i] = MC._ccsub[ipt][i];
	  }
  }
  return;
}

template <typename T> inline void
vMcCormick<T>::_sub
( const unsigned int nsub, const bool cst, const unsigned int npts )
{
  _sub_resize( nsub, npts );
  for(unsigned int ipt=0; ipt<npts;ipt++){
	  for ( unsigned int i=0; i<nsub; i++ ){
		_cvsub[ipt][i] = _ccsub[ipt][i] = 0.;
	  }
	  _const = cst;
  }
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::sub
( const unsigned int nsub )
{
  _sub( nsub, false, _npts );
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::sub
( const unsigned int nsub, const unsigned int isub )
{
  if( isub >= nsub ) throw Exceptions( Exceptions::SUB );
  sub( nsub );
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	_cvsub[ipt][isub] = _ccsub[ipt][isub] = 1.;
  }
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::sub
( const unsigned int nsub, const std::vector< std::vector<double> >& cvsub, const std::vector< std::vector<double> >& ccsub, const unsigned int npts )
{
  if( npts != cvsub.size() || npts != ccsub.size()) throw Exceptions( Exceptions::PTS);
  for(unsigned int ipt=0;ipt<npts;ipt++){
	if( nsub != cvsub[ipt].size() || nsub != ccsub[ipt].size() ) throw Exceptions( Exceptions::SUB );
  }
  _sub( nsub, false, npts );
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  for ( unsigned int i=0; i<nsub; i++ ){
		_cvsub[ipt][i] = cvsub[ipt][i];
		_ccsub[ipt][i] = ccsub[ipt][i];
	  }
  }
  return *this;
}

template <typename T> inline void
vMcCormick<T>::_pts_reset()
{
  for(unsigned int ipt=0; ipt<_npts; ipt++){
	delete [] _cvsub[ipt];
	delete [] _ccsub[ipt];
  }
  delete [] _cvsub;
  delete [] _ccsub;
  _cvsub = _ccsub = 0;
  delete [] _cv;
  delete [] _cc;
  _cv = _cc = 0;
}

template <typename T> inline void
vMcCormick<T>::_pts_resize
( const unsigned int npts )
{
  if( _npts != npts ){
	_pts_reset();
	_npts = npts;
	_cvsub = new double*[_npts];
	_ccsub = new double*[_npts];
	for(unsigned int ipt=0;ipt<_npts;ipt++){
		_cvsub[ipt] = _ccsub[ipt] = 0;
	}
	_cv = new double[_npts];
	_cc = new double[_npts];
  }
}

template <typename T> inline void
vMcCormick<T>::_pts_copy
( const vMcCormick<T>&MC )
{
  _pts_resize( MC._npts );
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	 _cv[ipt] = MC._cv[ipt];
	 _cc[ipt] = MC._cc[ipt];
  }
  return;
}

template <typename T> inline void
vMcCormick<T>::_pts_sub_copy
( const vMcCormick<T>&MC )
{
  _pts_copy(MC);
  _sub_copy(MC);
  return;
}

template <typename T> inline void
vMcCormick<T>::_pts_sub
( const unsigned int nsub, const bool cst, const unsigned int npts)
{
	_pts_resize(npts);
	_sub(nsub,cst,npts);
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::cut()
{
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  if( _cv[ipt] < Op<T>::l(_I) ){
		_cv[ipt] = Op<T>::l(_I);
		for( unsigned int i=0; i<_nsub; i++ ) _cvsub[ipt][i] = 0.;
	  }
	  if( _cc[ipt] > Op<T>::u(_I) ){
		_cc[ipt] = Op<T>::u(_I);
		for( unsigned int i=0; i<_nsub; i++ ) _ccsub[ipt][i] = 0.;
	  }
  }
  return *this;
}

/**
 * @brief Possibly improve the interval bounds through subgradients, for further info see [Najman&Mitsos2018]
 *        This differs from the one defined in the McCormick class, since here we use all points to determine the best lower and upper bound.
 */
template <typename T> inline vMcCormick<T>&
vMcCormick<T>::apply_subgradient_interval_heuristic()
{
  if(!mc::vMcCormick<T>::subHeur.usePrecomputedIntervals){
	  double l = Op<T>::l(_I);
	  double u = Op<T>::u(_I);
	  // Only do something if the object is not constant
	  if(Op<T>::l(_I) < Op<T>::u(_I)){
		  double tempcv = mc::vMcCormick<T>::subHeur.bestLowerBound;
		  double tempcc = mc::vMcCormick<T>::subHeur.bestUpperBound;
		  if(tempcv <= tempcc){ // We don't want to violate the interval consistency
			 // If we improved, replace the appropriate bound
			  if(tempcv > Op<T>::l(_I) && !isequal(tempcv, Op<T>::l(_I), mc::vMcCormick<T>::options.MVCOMP_TOL, mc::vMcCormick<T>::options.MVCOMP_TOL)){
				if(vMcCormick<T>::options.COMPUTE_POINT_RANKING){
					vMcCormick<T>::subHeur.subHeurRanking[vMcCormick<T>::subHeur.bestPointCvIndex] += tempcv - l;
				}
				l = tempcv;
			  }
			  if(tempcc < Op<T>::u(_I) && !isequal(tempcc, Op<T>::u(_I), mc::vMcCormick<T>::options.MVCOMP_TOL, mc::vMcCormick<T>::options.MVCOMP_TOL)){
				if(vMcCormick<T>::options.COMPUTE_POINT_RANKING){
					vMcCormick<T>::subHeur.subHeurRanking[vMcCormick<T>::subHeur.bestPointCcIndex] += u-tempcc;
				}
				u = tempcc;
			  }
		  }
#ifdef MC__VMCCORMICK_DEBUG
		  else{
			 if(!isequal(tempcv,tempcc, mc::vMcCormick<T>::options.MVCOMP_TOL, mc::vMcCormick<T>::options.MVCOMP_TOL)){
				 std::cout << "    Bound crossing in subgradient interval heuristic! " << std::endl;
				 std::cout << "    tempcv = " << std::setprecision(16) << tempcv << " > " << std::setprecision(16) << tempcc << " = tempcc " << std::endl;
                 throw std::runtime_error("    mc::vMcCormick\t Error in subgradient heuristic.");
			 }
		  }
#endif
		  _I = T(l,u);
	  }// End of if l < u
	  mc::vMcCormick<T>::subHeur.insert_interval(_I);
  }
  else{
	  _I = mc::vMcCormick<T>::subHeur.get_interval();
	  mc::vMcCormick<T>::subHeur.increase_iterator();
  }
  vMcCormick<T>::subHeur.reset_candidates();
  return *this;
}

template <typename T> inline double
vMcCormick<T>::laff
( const unsigned int ipt, const double*p, const double*pref ) const
{
  if(ipt>=_npts){ throw Exceptions( Exceptions::PTS ); }
  double _laff = _cv[ipt];
  for( unsigned int i=0; i<_nsub; i++ ){
    _laff += _cvsub[ipt][i]*(p[i]-pref[i]);
  }
  return _laff;
}

template <typename T> inline double
vMcCormick<T>::laff
( const unsigned int ipt, const T*Ip, const double*pref ) const
{
  if(ipt>=_npts){ throw Exceptions( Exceptions::PTS ); }
  double _laff = _cv[ipt];
  for( unsigned int i=0; i<_nsub; i++ ){
    _laff += Op<T>::l(_cvsub[ipt][i]*(Ip[i]-pref[i]));
  }
  return _laff;
}

template <typename T> inline double
vMcCormick<T>::uaff
( const unsigned int ipt, const double*p, const double*pref ) const
{
  double _uaff = _cc[ipt];
  for( unsigned int i=0; i<_nsub; i++ ){
    _uaff += _ccsub[ipt][i]*(p[i]-pref[i]);
  }
  return _uaff;
}

template <typename T> inline double
vMcCormick<T>::uaff
( const unsigned int ipt, const T*Ip, const double*pref ) const
{
  if(ipt>=_npts){ throw Exceptions( Exceptions::PTS ); }
  double _uaff = _cc[ipt];
  for( unsigned int i=0; i<_nsub; i++ ){
    _uaff += Op<T>::u(_ccsub[ipt][i]*(Ip[i]-pref[i]));
  }
  return _uaff;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator=
( const double c )
{
  _I = c;
  _const = true;
  _sub_reset();
  _nsub = 0;
  if(_npts == 0){
	  _npts = 1;
	  _cv = new double[1];
	  _cc = new double[1];
	  _cvsub = new double*[1];
	  _ccsub = new double*[1];
	  _cvsub[0] = 0;
	  _ccsub[0] = 0;
  }
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = _cc[ipt] = c;
  }
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator=
( const T&I )
{
  _I = I;
  _const = true;
  _sub_reset();
  _nsub = 0;
  if(_npts == 0){
	  _npts = 1;
	  _cv = new double[1];
	  _cc = new double[1];
	  _cvsub = new double*[1];
	  _ccsub = new double*[1];
	  _cvsub[0] = 0;
	  _ccsub[0] = 0;
  }
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = Op<T>::l(I);
	  _cc[ipt] = Op<T>::u(I);
  }
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator=
( const vMcCormick<T>&MC )
{
  if( this == &MC ) return *this;
  _I = MC._I;
  _pts_sub_copy( MC );
  _const = MC._const;
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator+=
( const double a )
{
  _I += a;
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] += a;
	  _cc[ipt] += a;
  }
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator+=
( const vMcCormick<T> &MC )
{
	vMcCormick<T> MC2 =*this+MC;
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "+=";
	vMcCormick<T>::_debug_check(MC,*this, MC2, str);
#endif
    *this = MC2;
    return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_sum1
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I + MC2._I;
  double MC2cv = MC2._cv[0];
  double MC2cc = MC2._cc[0];
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = MC1._cv[ipt] + MC2cv;
	  _cc[ipt] = MC1._cc[ipt] + MC2cc;
	  for( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = MC1._cvsub[ipt][i];
		_ccsub[ipt][i] = MC1._ccsub[ipt][i];
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "sum1";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_sum2
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // no factor is const
{
  _I = MC1._I + MC2._I;
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = MC1._cv[ipt] + MC2._cv[ipt];
	  _cc[ipt] = MC1._cc[ipt] + MC2._cc[ipt];
	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt], _cc[ipt], ipt);
	  for( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = MC1._cvsub[ipt][i] + MC2._cvsub[ipt][i];
		_ccsub[ipt][i] = MC1._ccsub[ipt][i] + MC2._ccsub[ipt][i];
		if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i], _ccsub[ipt][i], ipt, i);
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "sum2";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_bivariate_operation
	                 // ( MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, Op<T>::l(MC1._I), Op<T>::u(MC1._I),
	                   // MC2._cv, MC2._cc, MC2._cvsub, MC2._ccsub, Op<T>::l(MC2._I), Op<T>::u(MC2._I), _npts, vMcCormick<T>::additionalLins.PLUS);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_bivariate(_cv,_cc,_cvsub,_ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                         // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
  // }
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator-=
( const double a )
{
  _I -= a;
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] -= a;
	  _cc[ipt] -= a;
  }
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator-=
( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2 = *this - MC;
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "-=";
	vMcCormick<T>::_debug_check(*this, MC, MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_sub1
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I - MC2._I;
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = MC1._cv[ipt] - MC2._cc[0];
	  _cc[ipt] = MC1._cc[ipt] - MC2._cv[0];
	  for( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = MC1._cvsub[ipt][i];
		_ccsub[ipt][i] = MC1._ccsub[ipt][i];
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "sub1";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_sub2
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC1 is const so no subgradient heuristic needed
{
  _I = MC1._I - MC2._I;
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = MC1._cv[0] - MC2._cc[ipt];
	  _cc[ipt] = MC1._cc[0] - MC2._cv[ipt];
	  for( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = -MC2._ccsub[ipt][i];
		_ccsub[ipt][i] = -MC2._cvsub[ipt][i];
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "sub2";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_sub3
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  _I = MC1._I - MC2._I;
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for( unsigned int ipt=0;ipt<_npts;ipt++){
	  _cv[ipt] = MC1._cv[ipt] - MC2._cc[ipt];
	  _cc[ipt] = MC1._cc[ipt] - MC2._cv[ipt];
	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt], _cc[ipt], ipt);
	  for( unsigned int i=0; i<_nsub; i++ ){
		_cvsub[ipt][i] = MC1._cvsub[ipt][i] - MC2._ccsub[ipt][i];
		_ccsub[ipt][i] = MC1._ccsub[ipt][i] - MC2._cvsub[ipt][i];
		if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i], _ccsub[ipt][i], ipt, i);
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "sub3";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_bivariate_operation
	                 // ( MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, Op<T>::l(MC1._I), Op<T>::u(MC1._I),
	                   // MC2._cv, MC2._cc, MC2._cvsub, MC2._ccsub, Op<T>::l(MC2._I), Op<T>::u(MC2._I), _npts, vMcCormick<T>::additionalLins.MINUS);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_bivariate(_cv,_cc,_cvsub,_ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                         // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound, vMcCormick<T>::options.SUB_INT_HEUR_USE);
  // }
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator*=
( const double a )
{
  vMcCormick<T> MC2 = a * (*this);
  *this = MC2;
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator*=
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2 = MC * (*this);
  // if( _const && !MC._const ) _pts_sub(MC._nsub, MC._const, MC._npts);
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "*=";
	vMcCormick<T>::_debug_check(MC,*this,MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul1_u1pos_u2pos
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[0]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::l(MC1._I) * MC2._cv[0]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2 ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i];
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i];
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::u(MC1._I) * MC2._cc[0]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[0]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);

	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i];
	  }
	  else{
		_cc[ipt] = cc2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i];
	  }
  } // end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul1_u1pos_u2pos";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul2_u1pos_u2pos
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[ipt]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::l(MC1._I) * MC2._cv[ipt]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2 ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::u(MC1._I) * MC2._cvsub[ipt][i];
		}
	  }
	  else{                            // else here was missing before, added on 08.07.2016 at the AVT.SVT
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::l(MC1._I) * MC2._cvsub[ipt][i];
		}
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::u(MC1._I) * MC2._cc[ipt]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[ipt]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::u(MC1._I) * MC2._ccsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
		}
	  }
	  else{
		_cc[ipt] = cc2;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::l(MC1._I) * MC2._ccsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
	    }
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }//end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul2_u1pos_u2pos";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul1_u1pos_u2mix
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[0]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cv[0]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
	  if ( cv1 > cv2 ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i];
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i];
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cc[0]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[0]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i];
	  }
	  else{
		_cc[ipt] = cc2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i];
	  }
  }// end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul1_u1pos_u2mix";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul2_u1pos_u2mix
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC1 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[0] + Op<T>::u(MC1._I) * MC2._cv[ipt]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cc[0] + Op<T>::l(MC1._I) * MC2._cv[ipt]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2  ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::u(MC1._I) * MC2._cvsub[ipt][i];
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::l(MC1._I) * MC2._cvsub[ipt][i];
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cv[0] + Op<T>::u(MC1._I) * MC2._cc[ipt]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[0] + Op<T>::l(MC1._I) * MC2._cc[ipt]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);

	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::u(MC1._I) * MC2._ccsub[ipt][i];
	  }
	  else{
		_cc[ipt] = cc2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::l(MC1._I) * MC2._ccsub[ipt][i];
	  }
  }// end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul2_u1pos_u2mix";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul3_u1pos_u2mix
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[ipt]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cv[ipt]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2  ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::u(MC1._I) * MC2._cvsub[ipt][i];
		}
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::l(MC1._I) * MC2._cvsub[ipt][i];
		}
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cc[ipt]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[ipt]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);

	  if ( cc1  < cc2 ){
		_cc[ipt] = cc1;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::u(MC1._I) * MC2._ccsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
		}
	  }
	  else{
		_cc[ipt] = cc2;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::l(MC1._I) * MC2._ccsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
		}
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }// end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul3_u1pos_u2mix";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul1_u1mix_u2mix
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[0]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[0]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2  ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i];
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i];
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cc[0]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cv[0]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);

	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i];
	  }
	  else{
		_cc[ipt] = cc2;
		for( unsigned int i=0; i<_nsub; i++ )
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i];
	  }
  }// end for-loop _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul1_u1mix_u2mix";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mul2_u1mix_u2mix
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  double cv1 = Op<T>::u(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cv[ipt]
		- Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
	  double cv2 = Op<T>::l(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cc[ipt]
		- Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

	  if ( cv1 > cv2  ){
		_cv[ipt] = cv1;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::u(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::u(MC1._I) * MC2._cvsub[ipt][i];
		}
	  }
	  else{
		_cv[ipt] = cv2;
		for( unsigned int i=0; i<_nsub; i++ ){
		  _cvsub[ipt][i] = Op<T>::l(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::l(MC1._I) * MC2._ccsub[ipt][i];
		}
	  }

	  double cc1 = Op<T>::l(MC2._I) * MC1._cv[ipt] + Op<T>::u(MC1._I) * MC2._cc[ipt]
		- Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
	  double cc2 = Op<T>::u(MC2._I) * MC1._cc[ipt] + Op<T>::l(MC1._I) * MC2._cv[ipt]
		- Op<T>::l(MC1._I) * Op<T>::u(MC2._I);

	  if ( cc1 < cc2 ){
		_cc[ipt] = cc1;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::l(MC2._I) * MC1._cvsub[ipt][i] + Op<T>::u(MC1._I) * MC2._ccsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
		}
	  }
	  else{
		_cc[ipt] = cc2;
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt],_cc[ipt], ipt);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = Op<T>::u(MC2._I) * MC1._ccsub[ipt][i] + Op<T>::l(MC1._I) * MC2._cvsub[ipt][i];
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i],_ccsub[ipt][i], ipt, i);
		}
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "mul2_u1mix_u2mix";
	vMcCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::_mulMV
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
 // Note that the range interval has already been computer in operator*
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
 // Convex underestimator part
 {const double k = - Op<T>::diam(MC2._I) / Op<T>::diam(MC1._I);
  const double z = ( Op<T>::u(MC1._I)*Op<T>::u(MC2._I)
                   - Op<T>::l(MC1._I)*Op<T>::l(MC2._I) )
                   / Op<T>::diam(MC1._I);
  struct fct{
    static double t1
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return Op<T>::u(MC2._I) * x1 + Op<T>::u(MC1._I) * x2
	     - Op<T>::u(MC2._I) * Op<T>::u(MC1._I); }
    static double t2
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return Op<T>::l(MC2._I) * x1 + Op<T>::l(MC1._I) * x2
	     - Op<T>::l(MC2._I) * Op<T>::l(MC1._I); }
    static double t
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return std::max( t1(x1,x2,MC1,MC2), t2(x1,x2,MC1,MC2) ); }
  };

  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  int imid[4] = { -1, -1, -1, -1 };
	  double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	  double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	  double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
	  double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
	  // 23.08.2016
	  // modified by AVT.SVT after realizing that the closed form for multiplication given in
	  // Tsoukalas & Mitsos 2014 is not correct
	  double x1t[6] = { MC1_cv, MC1_cc,
							  mid_ndiff( MC1_cv, MC1_cc, (MC2_cv-z)/k, imid[0] ),
							  mid_ndiff( MC1_cv, MC1_cc, (MC2_cc-z)/k, imid[1] ),
							  // added:
							  MC1_cv, MC1_cc
							};
	  double x2t[6] = { mid_ndiff( MC2_cv, MC2_cc, k*MC1_cv+z, imid[2] ),
							  mid_ndiff( MC2_cv, MC2_cc, k*MC1_cc+z, imid[3] ),
							  MC2_cv, MC2_cc,
							  // added:
							  MC2_cv, MC2_cc
							};

	  double v[6] = { fct::t( x1t[0], x2t[0], MC1, MC2 ), fct::t( x1t[1], x2t[1], MC1, MC2 ),
					  fct::t( x1t[2], x2t[2], MC1, MC2 ), fct::t( x1t[3], x2t[3], MC1, MC2 ),
	// added the two corners (MC1._cv,MC2._cv),(MC1._cc,MC2._cc) for the convex relaxation specifically, since
	// they can be excluded by the mid() if the envelope of the multiplication is monotone
							fct::t( x1t[4], x2t[4], MC1, MC2 ),
							fct::t( x1t[5], x2t[5], MC1, MC2 )
							};

	  unsigned int ndx = argmin( 6, v ); // 6 elements now
	  _cv[ipt] = v[ndx];

	  if( _nsub ){
		double myalpha;
		if( isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ),
					 fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),
			 options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 ) ){
		  std::pair<double,double> alpha( 0., 1. );
		 bool MC1thin = isequal( MC1_cv, MC1_cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?true: false;
		  if( !MC1thin && x1t[ndx] > MC1_cv ){
					// 29.08.2016
					// modified by AVT.SVT
					// we had to add an additional if-statement where we question if the two values x[ndx] and MC._cv
					// are equal since we only work with a given tolerance (MVCOMP_TOL)
					// the if-statement is added 4 times in the convex subgradient and 4 times in the concave subgradient
			if(!isequal(x1t[ndx], MC1_cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.second = std::min( alpha.second, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
					}
		  }
		  if( !MC1thin && x1t[ndx] < MC1_cc ){
		   if(!isequal(x1t[ndx], MC1_cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					alpha.first = std::max( alpha.first, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
		   }
		  }
		  bool MC2thin = isequal( MC2_cv, MC2_cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?true: false;

		  if( !MC2thin && x2t[ndx] > MC2_cv ){
			if(!isequal(x2t[ndx], MC2_cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.second = std::min( alpha.second, -Op<T>::l(MC1._I)/Op<T>::diam(MC1._I) );
					}
		  }
		  if( !MC2thin && x2t[ndx] < MC2_cc ){
			if(!isequal(x2t[ndx], MC2_cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.first = std::max( alpha.first, -Op<T>::l(MC1._I)/Op<T>::diam(MC1._I) );
					}
		  }
		  bool alphathin = isequal( alpha.first, alpha.second, options.MVCOMP_TOL, options.MVCOMP_TOL )?true: false;
		  if( !alphathin && alpha.first > alpha.second ){
	#ifdef MC__VMCCORMICK_DEBUG
			std::cout << "WARNING1: alphaL= " << std::setprecision(std::numeric_limits<double>::digits10+1)  << alpha.first << "  alphaU= " << alpha.second << std::setprecision(6)
					  << std::endl;
			std::cout << "x1t: " << std::setprecision(std::numeric_limits<double>::digits10+1) << x1t[ndx] << " x2t: " << x2t[ndx] << std::endl;
			std::cout << "f(x1t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) << " f(x2t): " << fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) << std::endl;
			std::cout << "f(x1t)-f(x2t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) - fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) << std::endl;
			std::cout << "MC1thin: " << MC1thin << " MC2thin: " << MC2thin <<std::endl;
			std::cout << "x1t[ndx] > MC1._cv: " << (x1t[ndx] > MC1_cv) << " x1t[ndx] < MC1._cc " << (x1t[ndx] < MC1_cc) << std::endl;
			std::cout << "isequal(x1t, MC1._cv): " << isequal(x1t[ndx], MC1_cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x1t, MC1._cc): " << isequal(x1t[ndx], MC1_cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
			std::cout << "x2t[ndx] > MC2._cv: " << (x2t[ndx] > MC2_cv) << " x2t[ndx] < MC2._cc " << (x2t[ndx] < MC2_cc) <<std::endl;
			std::cout << "isequal(x2t, MC2._cv): " << isequal(x2t[ndx], MC2_cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x2t, MC2._cc): " << isequal(x2t[ndx], MC2_cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
			std::cout << "MC1.cv: "<< std::setprecision(std::numeric_limits<double>::digits10+1) << MC1_cv << " MC1.cc:" << std::setprecision(std::numeric_limits<double>::digits10+1) << MC1_cc <<std::endl;
			std::cout << "MC2.cv: " << MC2_cv << " MC2.cc:" << MC2_cc <<std::endl;
			std::cout << "MC1.l: " << Op<T>::l(MC1._I) << " MC1.u:" << Op<T>::u(MC1._I) << " MC1.diam: " << Op<T>::diam(MC1._I) << std::endl;
			std::cout << "MC2.l: " << Op<T>::l(MC2._I) << " MC2.u:" << Op<T>::u(MC2._I) << " MC2.diam: " << Op<T>::diam(MC2._I)<< std::endl;
			std::cout << "_cv: " << _cv[ipt] << " _cc: " << _cc[ipt] <<std::endl;
			std::cout << "_nsub: " << _nsub <<std::endl;
			std::cout << "subcv1: " << MC1._cvsub[ipt][0] << " subcc1: " << MC1._ccsub[ipt][0] <<std::endl;
			std::cout << "subcv2: " << MC2._cvsub[ipt][0] << " subcc2: " << MC2._ccsub[ipt][0] <<std::endl;
			std::cout << "isequal: " <<  isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ), fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),options.MVCOMP_TOL, options.MVCOMP_TOL ) <<std::endl;
			std::cout << "MVCOMP_TOL " << options.MVCOMP_TOL <<std::endl;
			std::cout << "Exception in multivariate binary product: Try increasing MC_mvcomp_tol and MC_envel_tol in the MAiNGO Settings file." <<std::endl;
			throw Exceptions( Exceptions::MULTSUB );
	#endif
			// currently testing
			alpha.first  = 1;
			alpha.second = 0;
		  }
		  myalpha = 0.5*( alpha.first + alpha.second );
		}
		else if( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) > fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) ){
		  myalpha = 1.;
		}else{
		  myalpha = 0.;
		}
		double sigma1cv = Op<T>::l(MC2._I) + myalpha * Op<T>::diam(MC2._I),
			   sigma2cv = Op<T>::l(MC1._I) + myalpha * Op<T>::diam(MC1._I);
		for( unsigned int i=0; i<_nsub; i++ )
		  _cvsub[ipt][i] = ( sigma1cv>=0? (MC1._const? 0.:MC1._cvsub[ipt][i]):
									      (MC1._const? 0.:MC1._ccsub[ipt][i]) ) * sigma1cv
					     + ( sigma2cv>=0? (MC2._const? 0.:MC2._cvsub[ipt][i]):
							              (MC2._const? 0.:MC2._ccsub[ipt][i]) ) * sigma2cv;

	  }

	}//end for-loop over _npts
 }//end of convex part

 // Concave overestimator part
 {const double k = Op<T>::diam(MC2._I) / Op<T>::diam(MC1._I);
  const double z = ( Op<T>::u(MC1._I)*Op<T>::l(MC2._I)
                   - Op<T>::l(MC1._I)*Op<T>::u(MC2._I) )
                   / Op<T>::diam(MC1._I);
  struct fct{
    static double t1
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return Op<T>::l(MC2._I) * x1 + Op<T>::u(MC1._I) * x2
	     - Op<T>::l(MC2._I) * Op<T>::u(MC1._I); }
    static double t2
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return Op<T>::u(MC2._I) * x1 + Op<T>::l(MC1._I) * x2
	     - Op<T>::u(MC2._I) * Op<T>::l(MC1._I); }
    static double t
      ( const double x1, const double x2, const vMcCormick<T>&MC1,
        const vMcCormick<T>&MC2 )
      { return std::min( t1(x1,x2,MC1,MC2), t2(x1,x2,MC1,MC2) ); }
  };

  // 23.08.2016
  // modified by AVT.SVT after realizing that the closed form for multiplication given in
  // Tsoukalas & Mitsos 2014 is not correct

  for(unsigned int ipt=0;ipt<_npts;ipt++){
	  int imid[4] = { -1, -1, -1, -1 };
	  double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	  double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	  double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
	  double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
	  double x1t[6] = { MC1_cv, MC1_cc,
						mid_ndiff( MC1_cv, MC1_cc, (MC2_cv-z)/k, imid[0] ),
						mid_ndiff( MC1_cv, MC1_cc, (MC2_cc-z)/k, imid[1] ),
						// Added:
						MC1_cv, MC1_cc
						};
	  double x2t[6] = { mid_ndiff( MC2_cv, MC2_cc, k*MC1_cv+z, imid[2] ),
						mid_ndiff( MC2_cv, MC2_cc, k*MC1_cc+z, imid[3] ),
						// Added:
						MC2_cv, MC2_cc,
						MC2_cc,MC2_cv
						};
	  double v[6] = { fct::t( x1t[0], x2t[0], MC1, MC2 ), fct::t( x1t[1], x2t[1], MC1, MC2 ),
					  fct::t( x1t[2], x2t[2], MC1, MC2 ), fct::t( x1t[3], x2t[3], MC1, MC2 ),
	   // added the two corners (MC1._cv,MC2._cc),(MC1._cc,MC2._cv) for the concave relaxation specifically, since
	   // they can be excluded by the mid() if the envelope of the multiplication is monotone
							fct::t( x1t[4], x2t[4], MC1, MC2 ),
							fct::t( x1t[5], x2t[5], MC1, MC2 )
							};

	  unsigned int ndx = argmax( 6, v );	 // 6 elements now
	  _cc[ipt] = v[ndx];
	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(_cv[ipt], _cc[ipt], ipt); // It is ok to do it only here, since the convex part has to be computed anyways

	  if( _nsub ){
		double myalpha;
		if( isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ),
					 fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),
			 options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 ) ){
		  std::pair<double,double> alpha( 0., 1. );
		  bool MC1thin = isequal( MC1_cv, MC1_cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
			true: false;
		  if( !MC1thin && x1t[ndx] > MC1_cv ){
					// 29.08.2016
					// modified by AVT.SVT
					// we had to add an additional if-statement where we question if the two values x[ndx] and MC._cv
					// are equal since we only work with a given tolerance (MVCOMP_TOL)
					// the if-statement is added 4 times in the convex subgradient and 4 times in the concave subgradient
					if(!isequal(x1t[ndx], MC1_cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.first = std::max( alpha.first, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
					}
		  }
		  if( !MC1thin && x1t[ndx] < MC1_cc ){
					if(!isequal(x1t[ndx], MC1_cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.second = std::min( alpha.second, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
					}
		  }
		  bool MC2thin = isequal( MC2_cv, MC2_cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
			true: false;
		  if( !MC2thin && x2t[ndx] > MC2_cv ){
					if(!isequal(x2t[ndx], MC2_cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.second = std::min( alpha.second, Op<T>::u(MC1._I)/Op<T>::diam(MC1._I) );
					}
		  }
		  if( !MC2thin && x2t[ndx] < MC2_cc ){
					if(!isequal(x2t[ndx], MC2_cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
					  alpha.first = std::max( alpha.first, Op<T>::u(MC1._I)/Op<T>::diam(MC1._I) );
					}
		  }

		  bool alphathin = isequal( alpha.first, alpha.second, options.MVCOMP_TOL, options.MVCOMP_TOL )?
			true: false;
		  if( !alphathin && alpha.first > alpha.second ){
	#ifdef MC__VMCCORMICK_DEBUG
			std::cout << "WARNING2: alphaL= " <<  std::setprecision(std::numeric_limits<double>::digits10+1) << alpha.first << "  alphaU= " << alpha.second << std::setprecision(6)
					  << std::endl;
			std::cout << "x1t: " << std::setprecision(std::numeric_limits<double>::digits10+1) << x1t[ndx] << " x2t: " << x2t[ndx] <<std::endl;
			std::cout << "f(x1t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) << " f(x2t): " << fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) <<std::endl;
			std::cout << "f(x1t)-f(x2t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) - fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) <<std::endl;
			std::cout << "MC1thin: " << MC1thin << " MC2thin: " << MC2thin <<std::endl;
			std::cout << "x1t[ndx] > MC1._cv: " << (x1t[ndx] > MC1_cv) << " x1t[ndx] < MC1._cc " << (x1t[ndx] < MC1_cc) <<std::endl;
			std::cout << "isequal(x1t, MC1._cv): " << isequal(x1t[ndx], MC1_cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x1t, MC1._cc): " << isequal(x1t[ndx], MC1_cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
			std::cout << "x2t[ndx] > MC2._cv: " << (x2t[ndx] > MC2_cv) << " x2t[ndx] < MC2._cc " << (x2t[ndx] < MC2_cc) <<std::endl;
			std::cout << "isequal(x2t, MC2._cv): " << isequal(x2t[ndx], MC2_cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x2t, MC2._cc): " << isequal(x2t[ndx], MC2_cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
			std::cout << "MC1.cv: "<< std::setprecision(std::numeric_limits<double>::digits10+1) << MC1_cv << " MC1.cc:" << std::setprecision(std::numeric_limits<double>::digits10+1) << MC1_cc <<std::endl;
			std::cout << "MC2.cv: " << MC2_cv << " MC2.cc:" << MC2_cc <<std::endl;
			std::cout << "MC1.l: " << Op<T>::l(MC1._I) << " MC1.u:" << Op<T>::u(MC1._I) << " MC1.diam: " << Op<T>::diam(MC1._I) <<std::endl;
			std::cout << "MC2.l: " << Op<T>::l(MC2._I) << " MC2.u:" << Op<T>::u(MC2._I) << " MC2.diam: " << Op<T>::diam(MC2._I)<<std::endl;
			std::cout << "_cv: " << _cv[ipt] << " _cc: " << _cc[ipt] <<std::endl;
			std::cout << "_nsub: " << _nsub <<std::endl;
			std::cout << "subcv1: " << MC1._cvsub[ipt][0] << " subcc1: " << MC1._ccsub[ipt][0] <<std::endl;
			std::cout << "subcv2: " << MC2._cvsub[ipt][0] << " subcc2: " << MC2._ccsub[ipt][0] <<std::endl;
			std::cout << "isequal: " <<  isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ), fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ), options.MVCOMP_TOL, options.MVCOMP_TOL ) <<std::endl;
			std::cout << "MVCOMP_TOL " << options.MVCOMP_TOL <<std::endl;
			std::cout << "Exception in multivariate binary product: Try increasing MC_mvcomp_tol and MC_envel_tol in the MAiNGO Settings file." <<std::endl;
			throw Exceptions( Exceptions::MULTSUB );
	#endif
			// currently testing
			alpha.first  = 1;
			alpha.second = 0;
		  }
		  myalpha = 0.5*( alpha.first + alpha.second );
		}
		else if( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) < fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) ){
		  myalpha = 0.;
		}else{
		  myalpha = 1.;
		}
		double sigma1cc = Op<T>::l(MC2._I) + myalpha * Op<T>::diam(MC2._I),
			   sigma2cc = Op<T>::u(MC1._I) - myalpha * Op<T>::diam(MC1._I);
		for( unsigned int i=0; i<_nsub; i++ ){
		  _ccsub[ipt][i] = ( sigma1cc>=0? (MC1._const? 0.:MC1._ccsub[ipt][i]):
									 (MC1._const? 0.:MC1._cvsub[ipt][i]) ) * sigma1cc
						 + ( sigma2cc>=0? (MC2._const? 0.:MC2._ccsub[ipt][i]):
									 (MC2._const? 0.:MC2._cvsub[ipt][i]) ) * sigma2cc;
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(_cvsub[ipt][i], _ccsub[ipt][i], ipt, i);
		}
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt); // It is ok to do it only here since the convex part has to be computed anyway
  }// end of for-loop over _npts
 }// end of concave part
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "multivariate mult";
	vMcCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator/=
( const double a )
{
  vMcCormick<T> MC2 = (*this) / a;
  *this = MC2;
  return *this;
}

template <typename T> inline vMcCormick<T>&
vMcCormick<T>::operator/=
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2 = (*this) / MC;
  // if( _const && !MC._const ) _pts_sub(MC._nsub, MC._const, MC._npts);
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "/=";
	vMcCormick<T>::_debug_check( *this,MC,MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline double*
vMcCormick<T>::_erfcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::erf(x), cv[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::erf(xU)-std::erf(xL))/(xU-xL) );
    cv[0] = std::erf(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _erfenv_func, _erfenv_dfunc, &xU );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _erfenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::erf(x), cv[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::erf(xU)-std::erf(xj))/(xU-xj) );
  cv[0] = std::erf(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_erfcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::erf(xU)-std::erf(xL))/(xU-xL) );
    cc[0] = std::erf(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::erf(x), cc[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _erfenv_func, _erfenv_dfunc, &xL );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _erfenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::erf(x), cc[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::erf(xL)-std::erf(xj))/(xL-xj) );
  cc[0] = std::erf(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_erfenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*exp(-z^2)-sqrt(pi)/2.*(erf(z)-erf(a)) = 0
  return (x-*rusr)*std::exp(-mc::sqr(x))-std::sqrt(PI)/2.*(std::erf(x)-std::erf(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_erfenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*z*(z-a)*exp(-z^2)
  return -2.*x*(x-*rusr)*std::exp(-2.*mc::sqr(x));
}

template <typename T> inline double*
vMcCormick<T>::_atancv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::atan(x), cv[1] = 1./(1.+mc::sqr(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::atan(xU)-std::atan(xL))/(xU-xL) );
    cv[0] = std::atan(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _atanenv_func, _atanenv_dfunc, &xU, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _atanenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::atan(x), cv[1] = 1./(1.+mc::sqr(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::atan(xU)-std::atan(xj))/(xU-xj) );
  cv[0] = std::atan(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_atancc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::atan(xU)-std::atan(xL))/(xU-xL) );
    cc[0] = std::atan(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::atan(x), cc[1] = 1./(1.+mc::sqr(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _atanenv_func, _atanenv_dfunc, &xL, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _atanenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::atan(x), cc[1] = 1./(1.+mc::sqr(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::atan(xL)-std::atan(xj))/(xL-xj) );
  cc[0] = std::atan(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_atanenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = z-a-(1+z^2)*(atan(z)-atan(a)) = 0
  return (x-*rusr)-(1.+mc::sqr(x))*(std::atan(x)-std::atan(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_atanenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*z*(atan(z)-atan(a))
  return -2.*x*(std::atan(x)-std::atan(*rusr));
}

template <typename T> inline double*
vMcCormick<T>::_sinhcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::sinh(x), cv[1] = std::cosh(x);
    return cv;
  }

  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:
      (std::sinh(xU)-std::sinh(xL))/(xU-xL) );
    cv[0] = std::sinh(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _sinhenv_func, _sinhenv_dfunc, &xL, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _sinhenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::sinh(x), cv[1] = std::cosh(x);
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.:
    (std::sinh(xj)-std::sinh(xL))/(xj-xL) );
  cv[0] = std::sinh(xL)+r*(x-xL), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_sinhcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:
      (std::sinh(xU)-std::sinh(xL))/(xU-xL) );
    cc[0] = std::sinh(xL)+r*(x-xL), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::sinh(x), cc[1] = std::cosh(x);
    return cc;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _sinhenv_func, _sinhenv_dfunc, &xU, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _oddpowenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::sinh(x), cc[1] = std::cosh(x);
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.:
    (std::sinh(xj)-std::sinh(xU))/(xj-xU) );
  cc[0] = std::sinh(xU)+r*(x-xU), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_sinhenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr  )
{
  // f(z) = (z-a)*cosh(z)-(sinh(z)-sinh(a)) = 0
  return (x-*rusr)*std::cosh(x)-(std::sinh(x)-std::sinh(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_sinhenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr  )
{
  // f'(z) = (z-a)*sinh(z)
  return (x-*rusr)*std::sinh(x);
}

template <typename T> inline double*
vMcCormick<T>::_tanhcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::tanh(x), cv[1] = 1.-mc::sqr(std::tanh(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::tanh(xU)-std::tanh(xL))/(xU-xL) );
    cv[0] = std::tanh(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _tanhenv_func, _tanhenv_dfunc, &xU, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _tanhenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::tanh(x), cv[1] = 1.-mc::sqr(std::tanh(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::tanh(xU)-std::tanh(xj))/(xU-xj) );
  cv[0] = std::tanh(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_tanhcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::tanh(xU)-std::tanh(xL))/(xU-xL) );
    cc[0] = std::tanh(xL)+r*(x-xL), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::tanh(x), cc[1] = 1.-mc::sqr(std::tanh(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _tanhenv_func, _tanhenv_dfunc, &xL, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _tanhenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::tanh(x), cc[1] = 1.-mc::sqr(std::tanh(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::tanh(xL)-std::tanh(xj))/(xL-xj) );
  cc[0] = std::tanh(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_tanhenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*(1-tanh(z)^2)-(tanh(z)-tanh(a)) = 0
  return (x-*rusr)*(1.-mc::sqr(std::tanh(x)))-(std::tanh(x)-std::tanh(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_tanhenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*(z-a)*tanh(z)*(1-tanh(z)^2)
  return -2.*(x-*rusr)*std::tanh(x)*(1.-mc::sqr(std::tanh(x)));
}


template <typename T> inline double*
vMcCormick<T>::_xexpaxcv
( const double x, const double xL, const double xU, const double a )
{
  static thread_local double cv[2];
  double dummy[2] = {xL,a};
  double p = -2.0/a; // turning point
  double xj;
    try{
      xj = _newton( xU, p, xU, _xexpaxenv_func, _xexpaxenv_dfunc, dummy );
    }
    catch( vMcCormick<T>::Exceptions ){
      xj = _goldsect(p, xU, _xexpaxenv_func, dummy, 0 );
    }
    if( x > xj ){	 // convex part
      cv[0] = mc::xexpax(x,a), cv[1] = std::exp(x*a)*(1.0+a*x);
      return cv;
    }
    double r,pt;
	if(isequal( xj, xL )){
		r = 0.;
		pt = mc::xexpax(xL,a)<mc::xexpax(xU,a) ? xL : xU;
	}
	else{
		r = (mc::xexpax(xL,a)-mc::xexpax(xj,a))/(xL-xj) ;
		pt = xL;
	}
    cv[0] = mc::xexpax(pt,a)+r*(x-pt), cv[1] = r;

  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_xexpaxcc
( const double x, const double xL, const double xU, const double a )
{
  static thread_local double cc[2];
  double dummy[2] = {xU,a};
  double p = -2.0/a; // turning point
  double xj;
  try{
    xj = _newton( xL, xL, p, _xexpaxenv_func, _xexpaxenv_dfunc, dummy );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, p, _xexpaxenv_func, dummy, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = mc::xexpax(x,a), cc[1] = std::exp(x*a)*(1.0+a*x);
    return cc;
  }
  double r,pt;
	if(isequal( xj, xL )){
		r = 0.;
		pt = mc::xexpax(xL,a)>mc::xexpax(xU,a) ? xL : xU;
	}
	else{
		r = (mc::xexpax(xU,a)-mc::xexpax(xj,a))/(xU-xj);
		pt = xU;
	}
  cc[0] = mc::xexpax(pt,a)+r*(x-pt), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_xexpaxenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = exp(a*x)*(x - xL)*(1+a*x) - (x*exp(a*x) - xL*exp(a* xL)) = 0
  return std::exp(rusr[1]*x)*(x- rusr[0])*(1+rusr[1]*x) - (mc::xexpax(x,rusr[1]) - mc::xexpax(rusr[0],rusr[1]));
}

template <typename T> inline double
vMcCormick<T>::_xexpaxenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = a*exp(a*x)*(x - xL)*(2 + a*x)
  return rusr[1]*std::exp(rusr[1]*x)*(x - rusr[0])*(2.0+rusr[1]*x);
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cv_val
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &borderPoint,
  const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds  ){
	//    fcv1(x) = f(xL,y1L,y2U,...,y(n-1)U,yn) + f(xL,y1L,y2U,...,y(n-1),ynU) + .. + f(x,y1L,y2U...y(n-1)U,ynU) - n*f(xL,y1L,y2U,...ynU)
	// or fcv2(x) = f(xU,y1U,...,y(n-1)U,yn) + f(xU,y1U,...,y(n-1),ynU) + .. + f(x,y1U,...y(n-1)U,ynU) - n*f(xU,y1U,...ynU)
	// borderPoint holds xL, y1L,y2U,...,ynU or xU, y1U,... depending on which cv we want
	std::vector<std::vector<double>> points(x.size(),borderPoint);
	for(size_t i = 0;i<x.size();i++){
		points[i][i] = x[i] ;
	}
	unsigned n = x.size()-1;
	double res = -(n*mc::xlog_sum_componentwise_convex(borderPoint,coeff,intervalLowerBounds,intervalUpperBounds,n));
	for(size_t i = 0;i<points.size();i++){
		res += mc::xlog_sum_componentwise_convex(points[i],coeff,intervalLowerBounds,intervalUpperBounds,n);
	}
	return res;
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr ){
	// f'(x) = log(a1*x+b1*y1L+b2*y2L+...) + a1*x/(a1*x+b1*y1L+b2*y2L+...)
	// vusr holds coefficients a1,b1,...
	// rusr holds xL,y1L,...ynL
	double sum = vusr[0]*x;
	for(size_t i = 1;i<vusr.size();i++){
		sum += vusr[i]*rusr[i];
	}
	return std::log(sum) + vusr[0]*x/sum;
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cv_ddfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr ){
	// f''(x) = 2*a1/(a1*x+b1*y1L+...) - a1^2*x/(a1*x+b1*y1L+...)^2
	// vusr holds coefficients a1,b1,...
	// rusr holds xL,y1L,...ynL or xU,y1U,...ynU
	unsigned int size = *iusr;
	double sum1 = rusr[0]*x;
	double sum2 = rusr[0]*x;
	for(unsigned int i=1;i< size - 1;i+=2){
	  sum1 += 2*rusr[i]*rusr[i+1];
	  sum2 += rusr[i]*rusr[i+1];
	}
	return rusr[0]*sum1/std::pow(sum2,2);
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cc_val
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &borderPoint,
  const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds  ){
	//    fcv1(x) = f(xL,y1L,...,y(n-1)L,yn) + f(xL,y1L,...,y(n-1),ynL) + .. + f(x,y1L,...y(n-1)L,ynL) - n*f(xL,y1L,...ynL)
	// or fcv2(x) = f(xU,y1U,...,y(n-1)U,yn) + f(xU,y1U,...,y(n-1),ynU) + .. + f(x,y1U,...y(n-1)U,ynU) - n*f(xU,y1U,...ynU)
	// borderPoint holds xU, y1L,... or xL, y1U,... depending on which cc we want
	std::vector<std::vector<double>> points(x.size(),borderPoint);
	for(size_t i = 0;i<x.size();i++){
		points[i][i] = x[i] ;
	}
	unsigned n = x.size()-1;
	double res = -(n*mc::xlog_sum_componentwise_concave(borderPoint,coeff,intervalLowerBounds,intervalUpperBounds));
	for(size_t i = 0;i<points.size();i++){
		res += mc::xlog_sum_componentwise_concave(points[i],coeff,intervalLowerBounds,intervalUpperBounds);
	}
	return res;
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cv_partial_derivative
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds, const std::vector<double> &upperIntervalBounds,
  unsigned facet, unsigned direction )
{

	double val = 0;
	int dummy = 0.;
	switch(facet){
		case 1:
		{
			switch(direction){
				case 0:
				{
					val = _xlog_sum_cv_dfunc(x[0],lowerIntervalBounds.data(),&dummy,coeff);
					break;
				}
				default:
				{
					if(isequal(upperIntervalBounds[direction],lowerIntervalBounds[direction])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<x.size();i++){
							sum += lowerIntervalBounds[i]*coeff[i];
						}
						val = (lowerIntervalBounds[0]*std::log(sum-lowerIntervalBounds[direction]*coeff[direction]+upperIntervalBounds[direction]*coeff[direction])
							 - lowerIntervalBounds[0]*std::log(sum) )
							  /(upperIntervalBounds[direction]-lowerIntervalBounds[direction]);
						}
					break;
				}
			}
			break;
		}
		case 2:
		{
			switch(direction){
				case 0:
				{
					val = _xlog_sum_cv_dfunc(x[0],upperIntervalBounds.data(),&dummy,coeff);
					break;
				}
				default:
				{
					if(isequal(upperIntervalBounds[direction],lowerIntervalBounds[direction])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<x.size();i++){
							sum += upperIntervalBounds[i]*coeff[i];
						}
						val = (upperIntervalBounds[0]*std::log(sum)
							 - upperIntervalBounds[0]*std::log(sum-upperIntervalBounds[direction]*coeff[direction]+lowerIntervalBounds[direction]*coeff[direction]) )
							  /(upperIntervalBounds[direction]-lowerIntervalBounds[direction]);
					}
					break;
				}
			}
			break;
		}
		default:
		  break;
	}
	return val;
}

template <typename T> inline double
vMcCormick<T>::_xlog_sum_cc_partial_derivative
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds, const std::vector<double> &upperIntervalBounds,
  unsigned facet, unsigned direction )
{

	double val = 0;
	int dummy = 0.;
	switch(facet){
		case 1:
		{
			switch(direction){
				case 0:
				{
					if(isequal(upperIntervalBounds[0],lowerIntervalBounds[0])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<lowerIntervalBounds.size();i++){
							sum += lowerIntervalBounds[i]*coeff[i]; // a1*xL+b1*y1L+...
						}
						val = (upperIntervalBounds[0]*std::log(sum-lowerIntervalBounds[0]*coeff[0]+upperIntervalBounds[0]*coeff[0])
							 - lowerIntervalBounds[0]*std::log(sum) )
							 /(upperIntervalBounds[0]-lowerIntervalBounds[0]);
					}
					break;
				}
				default:
				{
					double sum = coeff[0]*upperIntervalBounds[0];
					for(size_t i = 1;i<x.size();i++){
						if(i==direction){
							sum += coeff[direction]*x[direction];
						}
						else{
							sum += lowerIntervalBounds[i]*coeff[i];
						}
					}
					val = coeff[direction]*upperIntervalBounds[0]/(sum);
					break;
				}
			}
			break;
		}
		case 2:
		{
			switch(direction){
				case 0:
				{
					if(isequal(upperIntervalBounds[0],lowerIntervalBounds[0])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<lowerIntervalBounds.size();i++){
							sum += upperIntervalBounds[i]*coeff[i]; // a1*xU+b1*y1U+...
						}
						val = (upperIntervalBounds[0]*std::log(sum)
							 - lowerIntervalBounds[0]*std::log(sum-upperIntervalBounds[0]*coeff[0]+lowerIntervalBounds[0]*coeff[0]) )
							 /(upperIntervalBounds[0]-lowerIntervalBounds[0]);
					}
					break;
				}
				default:
				{
					double sum = coeff[0]*lowerIntervalBounds[0];
					for(size_t i = 1;i<x.size();i++){
						if(i==direction){
							sum += coeff[direction]*x[direction];
						}
						else{
							sum += upperIntervalBounds[i]*coeff[i];
						}
					}
					val = coeff[direction]*lowerIntervalBounds[0]/(sum);
					break;
				}
			break;
			}
		}
		default:
		  break;
	}
	return val;
}

template <typename T> inline double*
vMcCormick<T>::_xlog_sum_cv
( std::vector<double> &xcv, const double xcc, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
  const std::vector<double> &upperIntervalBounds, const std::vector<double> &xcc_vec)
{

    static thread_local double cv[2]; // first is value of convex relaxation, second is which of the two convex relaxations has been used,
                         // and third is the point xj which is later used for subgradient computation
    int c = coeffs.size()-1;
    double xj1, xj2, val1, val2;
	try{
	  xj1 = _newton( xcv[0], xcv[0], xcc, _xlog_sum_cv_dfunc, _xlog_sum_cv_ddfunc, lowerIntervalBounds.data(), &c, coeffs );
	}
	catch( vMcCormick<T>::Exceptions ){
	  xj1 = _goldsect(xcv[0], xcc, _xlog_sum_cv_dfunc, lowerIntervalBounds.data(), &c, coeffs );
	}
    // The ^U relaxation is always valid
	try{
      xj2 = _newton( xcv[0], xcv[0], xcc, _xlog_sum_cv_dfunc, _xlog_sum_cv_ddfunc, upperIntervalBounds.data(), &c, coeffs );
    }
    catch( vMcCormick<T>::Exceptions ){
      xj2 = _goldsect( xcv[0], xcc, _xlog_sum_cv_dfunc, upperIntervalBounds.data(), &c, coeffs );
    }
	xcv[0] = xj1;
	val1 = _xlog_sum_cv_val(xcv,coeffs,lowerIntervalBounds,lowerIntervalBounds,upperIntervalBounds); // ^L relaxation
	xcv[0] = xj2;
	val2 = _xlog_sum_cv_val(xcv,coeffs,upperIntervalBounds,lowerIntervalBounds,upperIntervalBounds); // ^U relaxation

	if( val1 >= val2 ){
	  cv[0] =val1;
	  cv[1] = 1;
	  xcv[0] = xj1;
	}
	else{
	  cv[0] =val2;
	  cv[1] = 2;
	  xcv[0] = xj2;
	}

  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_xlog_sum_cc
( std::vector<double> &xcc, double xcv, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
  const std::vector<double> &upperIntervalBounds )
{

	static thread_local double cc[2]; // first is value of convex relaxation, second is which of the two convex relaxations has been used

	// Check whether the relaxation function is increasing or decreasing w.r.t. x
	double sum = 0.;
	for(size_t i = 0;i<lowerIntervalBounds.size();i++){
		sum += lowerIntervalBounds[i]*coeffs[i]; // a1*xL+b1*y1L+...
	}
	double val = (upperIntervalBounds[0]*std::log(sum-lowerIntervalBounds[0]*coeffs[0]+upperIntervalBounds[0]*coeffs[0])
	     - lowerIntervalBounds[0]*std::log(sum) )
	     /(upperIntervalBounds[0]-lowerIntervalBounds[0]);

	// The function is decreasing w.r.t. x
	if(val<0){
		xcc[0] = xcv;
	}

	std::vector<double> borderPoint1(lowerIntervalBounds);
	std::vector<double> borderPoint2(upperIntervalBounds);
	borderPoint1[0] = upperIntervalBounds[0];
	borderPoint2[0] = lowerIntervalBounds[0];

	double val1 = _xlog_sum_cc_val(xcc,coeffs,borderPoint1,lowerIntervalBounds,upperIntervalBounds); // xL,yiU, relaxation
	double val2 = _xlog_sum_cc_val(xcc,coeffs,borderPoint2,lowerIntervalBounds,upperIntervalBounds); // xU,yiL, relaxation

	if( val1 <= val2 ){
		cc[0] =val1;
		cc[1] = 1;
	}
	else{
		cc[0] =val2;
		cc[1] = 2;
	}

	return cc;
}

template <typename T> inline double*
vMcCormick<T>::_regnormal_cv
( const double x, const double a, const double b, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = mc::regnormal(x,a,b), cv[1] = mc::der_regnormal(x,a,b);
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xL,a,b))/(xU-xL) );
    cv[0] = mc::regnormal(xL,a,b)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  double params [3] = {a,b,xU};
  try{
    xj = _newton( xL, xL, 0., _regnormal_func, _regnormal_dfunc, params, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _regnormal_func, params, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = mc::regnormal(x,a,b), cv[1] = mc::der_regnormal(x,a,b);
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xj,a,b))/(xU-xj) );
  cv[0] = mc::regnormal(xj,a,b)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_regnormal_cc
( const double x, const double a, const double b, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xL,a,b))/(xU-xL) );
    cc[0] = mc::regnormal(xU,a,b)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = mc::regnormal(x,a,b), cc[1] = mc::der_regnormal(x,a,b);
    return cc;
  }

  double xj;
  double params [3] = {a,b,xL};
  try{
    xj = _newton( xU, 0., xU, _regnormal_func, _regnormal_dfunc, params, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _regnormal_func, params, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = mc::regnormal(x,a,b), cc[1] = mc::der_regnormal(x,a,b);
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(mc::regnormal(xL,a,b)-mc::regnormal(xj,a,b))/(xL-xj) );
  cc[0] = mc::regnormal(xj,a,b)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_regnormal_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(x) = (x-w)*der_regnormal(x,a,b) - (regnormal(x,a,b)-regnormal(w,a,b)) = 0
  return (x-rusr[2])*mc::der_regnormal(x,rusr[0],rusr[1])-(mc::regnormal(x,rusr[0],rusr[1])-mc::regnormal(rusr[2],rusr[0],rusr[1]));
}

template <typename T> inline double
vMcCormick<T>::_regnormal_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(x) = 3*a*b*x*(w-x) / (a + b*x^2)^(5/2)
  return 3*rusr[0]*rusr[1]*x*(rusr[2]-x) / std::pow(rusr[0] + rusr[1]*std::pow(x,2),5./2.);
}

template <typename T> inline double
vMcCormick<T>::_gpdf_compute_sol_point
( const double xL, const double xU, const double starting_point, const double fixed_point )
{
  double xj;
  try{
    xj = _newton( starting_point, xL, xU, _gpdf_func, _gpdf_dfunc, &fixed_point, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, xU, _gpdf_func, &fixed_point, 0 );
  }
  return xj;
}

template <typename T> inline double
vMcCormick<T>::_gpdf_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(x) = (gaussian_probability_density_function(x)-gaussian_probability_density_function(xJ))/(x-xJ) - der_gaussian_probability_density_function(x)
  return (mc::gaussian_probability_density_function(x)- mc::gaussian_probability_density_function(*rusr)) - mc::der_gaussian_probability_density_function(x)*(x - (*rusr));
}

template <typename T> inline double
vMcCormick<T>::_gpdf_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(x) = mc::gaussian_probability_density_function(x)*(1-x^2)*(x-xJ)
  return mc::gaussian_probability_density_function(x) * (1 - std::pow(x,2))*(x - (*rusr));
}

template <typename T> inline double*
vMcCormick<T>::_oddpowcv
( const double x, const int iexp, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    double v = std::pow(x,iexp-1);
    cv[0] = x*v, cv[1] = iexp*v;
    return cv;
  }

  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:
      (std::pow(xU,iexp)-std::pow(xL,iexp))/(xU-xL) );
    cv[0] = std::pow(xL,iexp)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  if( iexp > 21 ){ // if the odd exponent is larger than 21, we use newton to compute the envelope
    try{
      xj = _newton( xU, 0., xU, _oddpowenv_func, _oddpowenv_dfunc, &xL, &iexp );
    }
    catch( vMcCormick<T>::Exceptions ){
      xj = _goldsect( 0., xU, _oddpowenv_func, &xL, &iexp );
    }
  }
  else{// if the odd exponent is lesser or equal than 21, we use the tagnent points given in Table 1
       // of [Liberti & Pantelides 2002, "Convex Envelopes of Monomials of Odd Degree"]
    xj = _Qroots[ (iexp-1)/2-1 ] * xL;
  }

  if( x > xj ){	 // convex part
    double v = std::pow(x,iexp-1);
    cv[0] = x*v, cv[1] = iexp*v;
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.:
    (std::pow(xj,iexp)-std::pow(xL,iexp))/(xj-xL) );
  cv[0] = std::pow(xL,iexp)+r*(x-xL), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_oddpowcc
( const double x, const int iexp, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:
      (std::pow(xU,iexp)-std::pow(xL,iexp))/(xU-xL) );
    cc[0] = std::pow(xL,iexp)+r*(x-xL), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    double v = std::pow(x,iexp-1);
    cc[0] = x*v, cc[1] = iexp*v;
    return cc;
  }

  double xj;
  if( iexp > 21 ){ // if the odd exponent is larger than 21, we use newton to compute the envelope
    try{
      xj = _newton( xL, xL, 0., _oddpowenv_func, _oddpowenv_dfunc, &xU, &iexp );
    }
    catch( vMcCormick<T>::Exceptions ){
      xj = _goldsect( xL, 0., _oddpowenv_func, &xU, &iexp );
    }
  }
  else{// if the odd exponent is lesser or equal than 21, we use the tagnent points given in Table 1
       // of [Liberti & Pantelides 2002, "Convex Envelopes of Monomials of Odd Degree"]
    xj = _Qroots[ (iexp-1)/2-1 ] * xU;
  }

  if( x < xj ){	 // concave part
    double v = std::pow(x,iexp-1);
    cc[0] = x*v, cc[1] = iexp*v;
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.:
    (std::pow(xj,iexp)-std::pow(xU,iexp))/(xj-xU) );
  cc[0] = std::pow(xU,iexp)+r*(x-xU), cc[1] = r;
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_oddpowenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (p-1)*z^p - a*p*z^{p-1} + a^p = 0
  return ((*iusr-1)*x-(*rusr)*(*iusr))*std::pow(x,*iusr-1)
    + std::pow(*rusr,*iusr);
}

template <typename T> inline double
vMcCormick<T>::_oddpowenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = p*(p-1)*z^{p-1} - a*p*(p-1)*z^{p-2}
  return ((*iusr)*(*iusr-1)*x-(*rusr)*(*iusr)*(*iusr-1))*std::pow(x,*iusr-2);
}

template <typename T> inline double*
vMcCormick<T>::_evenchebcv
( const double x, const int iord, const double xL, const double xU )
{
  double xjL = std::cos(PI-PI/(double)iord);
  double xjU = std::cos(PI/(double)iord);
  static thread_local double cv[2];
  if( x <= xjL || x >= xjU )
    cv[0] = mc::cheb(x,iord), cv[1] = iord*mc::cheb2(x,iord-1);
  else
    cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_oddchebcv
( const double x, const int iord, const double xL, const double xU )
{
  static thread_local double cv[2];
  double xj = std::cos(PI/(double)iord);
  if( x >= xj ){	 // convex part
    cv[0] = mc::cheb(x,iord), cv[1] = iord*mc::cheb2(x,iord-1);
    return cv;
  }
  cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_oddchebcc
( const double x, const int iord, const double xL, const double xU )
{
  static thread_local double cc[2];
  double xj = std::cos(PI-PI/(double)iord);
  if( x <= xj ){	 // concave part
    cc[0] = mc::cheb(x,iord), cc[1] = iord*mc::cheb2(x,iord-1);
    return cc;
  }
  cc[0] = 1., cc[1] = 0.;
  return cc;
}

template <typename T> inline double*
vMcCormick<T>::_stepcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];

  if( x < 0. ){
    cv[0] = cv[1] = 0.;
    return cv;
  }

  if( xL >= 0. ){
    cv[0] = 1., cv[1] = 0.;
    return cv;
  }

  cv[0] = x/xU, cv[1] = 1./xU;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_stepcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];

  if( x >= 0. ){
    cc[0] = 1., cc[1] = 0.;
    return cc;
  }

  else if( xU < 0. ){
    cc[0] = 0., cc[1] = 0.;
    return cc;
  }

  cc[0] = 1.-x/xL, cc[1] = -1./xL;
  return cc;
}

template <typename T> inline double*
vMcCormick<T>::_cosarg
( const double xL, const double xU )
{
  static thread_local double arg[2];
  const int kL = std::ceil(-(1.+xL/PI)/2.);
  const double xL1 = xL+2.*PI*kL, xU1 = xU+2.*PI*kL;
  assert( xL1 >= -PI && xL1 <= PI );
  if( xL1 <= 0 ){
    if( xU1 <= 0 ) arg[0] = xL, arg[1] = xU;
    else if( xU1 >= PI ) arg[0] = PI*(1.-2.*kL), arg[1] = -PI*2.*kL;
    else arg[0] = std::cos(xL1)<=std::cos(xU1)?xL:xU, arg[1] = -PI*2.*kL;
    return arg;
  }
  if( xU1 <= PI ) arg[0] = xU, arg[1] = xL;
  else if( xU1 >= 2.*PI ) arg[0] = PI*(1-2.*kL), arg[1] = 2.*PI*(1.-kL);
  else arg[0] = PI*(1.-2.*kL), arg[1] = std::cos(xL1)>=std::cos(xU1)?xL:xU;
  return arg;
}

template <typename T> inline double*
vMcCormick<T>::_coscv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  const int kL = std::ceil(-(1.+xL/PI)/2.);
  if( x <= PI*(1-2*kL) ){
    const double xL1 = xL+2.*PI*kL;
    if( xL1 >= 0.5*PI ){
      cv[0] = std::cos(x), cv[1] = -std::sin(x);
      return cv;
    }
    const double xU1 = std::min(xU+2.*PI*kL,PI);
    if( xL1 >= -0.5*PI && xU1 <= 0.5*PI ){
      double r, pt;
	  if(isequal( xL, xU )){
		  r = 0.;
		  pt = std::cos(xL)<std::cos(xU) ? xL : xU;
	  }
	  else{
		  r =  (std::cos(xU)-std::cos(xL))/(xU-xL);
		  pt = xL;
	  }
      cv[0] = std::cos(pt)+r*(x-pt), cv[1] = r;
      return cv;
    }
    return _coscv2( x+2.*PI*kL, xL1, xU1 );
  }

  const int kU = std::floor((1.-xU/PI)/2.);
  if( x >= PI*(-1-2*kU) ){
    const double xU2 = xU+2.*PI*kU;
    if( xU2 <= -0.5*PI ){
      cv[0] = std::cos(x), cv[1] = -std::sin(x);
      return cv;
    }
    return _coscv2( x+2.*PI*kU, std::max(xL+2.*PI*kU,-PI), xU2 );
  }

  cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_coscv2
( const double x, const double xL, const double xU )
{
  bool left;
  double x0, xm;
  if( std::fabs(xL)<=std::fabs(xU) )
    left = false, x0 = xU, xm = xL;
  else
    left = true, x0 = xL, xm = xU;

  double xj;
  try{
    xj = _newton( x0, xL, xU, _cosenv_func, _cosenv_dfunc, &xm, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, xU, _cosenv_func, &xm, 0 );
  }
  static thread_local double cv[2];
  if(( left && x<xj ) || ( !left && x>xj )){
    cv[0] = std::cos(x), cv[1] = -std::sin(x);
    return cv;
  }
  double r, pt;
  if(isequal( xm, xj )){
	  r = 0.;
	  pt = std::cos(xm)<std::cos(xj) ? xm : xj;
  }
  else{
	  r = (std::cos(xm)-std::cos(xj))/(xm-xj);
	  pt = xm;
  }
  cv[0] = std::cos(pt)+r*(x-pt), cv[1] = r;
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_coscc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  const double*cvenv = _coscv( x-PI, xL-PI, xU-PI );
  cc[0] = -cvenv[0], cc[1] = -cvenv[1];
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_cosenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*sin(z)+cos(z)-cos(a) = 0
  return ((x-*rusr)*std::sin(x)+std::cos(x)-std::cos(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_cosenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = (z-a)*cos(z)
  return ((x-*rusr)*std::cos(x));
}

template <typename T> inline double*
vMcCormick<T>::_asincv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::asin(x), cv[1] = 1./std::sqrt(1-x*x);
    return cv;
  }
  if( xU <= 0. ){	 // concave part
    double r,pt;
	if(isequal( xL, xU )){
		r = 0.;
		pt = std::asin(xL)<std::asin(xU) ? xL : xU;
	}
	else{
		r = (std::asin(xU)-std::asin(xL))/(xU-xL);
		pt = xL;
	}
    cv[0] = std::asin(pt)+r*(x-pt), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _secant( 0., xU, 0., xU, _asinenv_func, &xL, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _asinenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::asin(x), cv[1] = 1./std::sqrt(1-x*x);
    return cv;
  }
  double r,pt;
  if(isequal( xL, xj )){
  	r = 0.;
  	pt = std::asin(xL)<std::asin(xj) ? xL : xj;
  }
  else{
  	r = (std::asin(xU)-std::asin(xj))/(xU-xj);
  	pt = xL;
  }
  cv[0] = std::asin(pt)+r*(x-pt), cv[1] = r;	// linear part
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_asincc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
   double r,pt;
	if(isequal( xL, xU )){
		r = 0.;
		pt = std::asin(xL)>std::asin(xU) ? xL : xU;
	}
	else{
		r = (std::asin(xU)-std::asin(xL))/(xU-xL);
		pt = xU;
	}
    cc[0] = std::asin(pt)+r*(x-pt), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::asin(x), cc[1] = 1./std::sqrt(1-x*x);
    return cc;
  }

  double xj;
  try{
    xj = _secant( 0., xL, xL, 0., _asinenv_func, &xU, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _asinenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::asin(x), cc[1] = 1./std::sqrt(1-x*x);
    return cc;
  }
  double r,pt;
  if(isequal( xU, xj )){
  	r = 0.;
  	pt = std::asin(xU)>std::asin(xj) ? xU : xj;
  }
  else{
  	r = (std::asin(xU)-std::asin(xj))/(xU-xj);
  	pt = xU;
  }
  cc[0] = std::asin(pt)+r*(x-pt), cc[1] = r;	// secant part
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_asinenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = z-a-sqrt(1-z^2)*(asin(z)-asin(a)) = 0
  return x-(*rusr)-std::sqrt(1.-x*x)*(std::asin(x)-std::asin(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_asinenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = z/sqrt(1-z^2)*(asin(z)-asin(a))
  return x/std::sqrt(1.-x*x)*(std::asin(x)-std::asin(*rusr));
}

template <typename T> inline double*
vMcCormick<T>::_tancv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::tan(x), cv[1] = 1.+mc::sqr(std::tan(x));
    return cv;
  }
  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.: (std::tan(xU)-std::tan(xL))/(xU-xL));
    cv[0] = std::tan(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _secant( 0., xU, 0., xU, _tanenv_func, &xL, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _tanenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::tan(x), cv[1] = 1.+mc::sqr(std::tan(x));
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.: (std::tan(xj)-std::tan(xL))/(xj-xL));
  cv[0] = std::tan(xL)+r*(x-xL), cv[1] = r;	// secant part
  return cv;
}

template <typename T> inline double*
vMcCormick<T>::_tancc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.: (std::tan(xU)-std::tan(xL))/(xU-xL));
    cc[0] = std::tan(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::tan(x), cc[1] = 1.+mc::sqr(std::tan(x));
    return cc;
  }

  double xj;
  try{
    xj = _secant( 0., xL, xL, 0., _tanenv_func, &xU, 0 );
  }
  catch( vMcCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _tanenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::tan(x), cc[1] = 1.+mc::sqr(std::tan(x));
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.: (std::tan(xj)-std::tan(xU))/(xj-xU));
  cc[0] = std::tan(xU)+r*(x-xU), cc[1] = r;	// secant part
  return cc;
}

template <typename T> inline double
vMcCormick<T>::_tanenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)-(tan(z)-tan(a))/(1+tan(z)^2) = 0
  return (x-(*rusr))-(std::tan(x)-std::tan(*rusr))/(1.+mc::sqr(std::tan(x)));
}

template <typename T> inline double
vMcCormick<T>::_tanenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = (tan(z)-tan(a))/(1+tan(z)^2)*2*tan(z)
  return 2.*std::tan(x)/(1.+mc::sqr(std::tan(x)))*(std::tan(x)-std::tan(*rusr));
}

template <typename T> inline double
vMcCormick<T>::_newton
( const double x0, const double xL, const double xU, puniv f,
  puniv df, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  double xk = std::max(xL,std::min(xU,x0));
  double fk = f(xk,rusr,iusr,vusr);

  for( unsigned int it=0; it<options.ENVEL_MAXIT; it++ ){
    if( std::fabs(fk) < options.ENVEL_TOL ) return xk;
    double dfk = df(xk,rusr,iusr,vusr);
    if( dfk == 0 ) throw Exceptions( Exceptions::ENVEL );
    if( isequal(xk,xL) && fk/dfk>0 ) return xk;
    if( isequal(xk,xU) && fk/dfk<0 ) return xk;
    xk = std::max(xL,std::min(xU,xk-fk/dfk));
    fk = f(xk,rusr,iusr,vusr);
  }

  throw Exceptions( Exceptions::ENVEL );
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// @AVT.SVT added 01.09.2017
template <typename T> inline double
vMcCormick<T>::_dhvapenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
	switch(*iusr){
		case 1:
			return mc::enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4])
					+ mc::der_enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4])*(*rusr - x);
		case 2:
			return mc::enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4],vusr[5])
				+ mc::der_enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4],vusr[5])*(*rusr - x);
		default:
			throw std::runtime_error("mc::vMcCormick\t dhvapenv_func called with an unknown type.");
			break;
	}

}

template <typename T> inline void
vMcCormick<T>::_cost_Guthrie_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double p1, const double p2, const double p3, const double l, const double u )
{
	// Test w.r.t. parameter p3
	if(p3 == 0.){ // there is no root of the first derivative so simply check corners
		// monotonicity first
		monotonicity = (p2 >= 0. )? MON_INCR : MON_DECR;
		// Second, check convexity/concavity
		if(std::pow(p2,2)-p2 >= 0.){
			convexity = CONVEX;
		}
		else{
			convexity = CONCAVE;
		}
	}
	else{
		// monotonicity first
        //we have exactly one root
		double root = std::exp(-p2*std::log(10.)/(2.0*p3));
		if( root <= l){ // left of the interval
			monotonicity = (p3 >= 0. )? MON_INCR : MON_DECR;//if p3 >=0 then it's a minimum
		}
		else if( root >= u){ // left of the interval
			monotonicity = (p3 >= 0. )? MON_DECR : MON_INCR; //if p3 >=0 then it's a minimum
		}
		else{
			monotonicity = MON_NONE;
		}
		// Second, check convexity/concavity
		double min = std::exp((std::log(10.) - 2*p2*std::log(10.))/(4*p3));	// it's always a min
		if( l < min && min < u ){ // check if it lies within the interval bounds

			if( std::pow(p2,2) + (4*p2*p3*std::log(min))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(min),2))/(std::pow(std::log(10.),2))
				+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(min))/std::log(10.) >= 0. ){ // if the sub_part of the second derivative of Guthrie at the minimum is >= 0, we can deduce that the function is convex
				convexity = CONVEX;
			}else{	// else we have to check the corners of the sub_part function of the second derivative of Guthrie
				double r1 = std::pow(p2,2) + (4*p2*p3*std::log(l))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(l),2))/(std::pow(std::log(10.),2))
							+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(l))/std::log(10.);
				double r2 = std::pow(p2,2) + (4*p2*p3*std::log(u))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(u),2))/(std::pow(std::log(10.),2))
							+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(u))/std::log(10.);

				if( r1 <= 0. && r2 <= 0. ){
					convexity = CONCAVE;
				}
			}
		}
		else{ //end of if {l < min && min < u} // if the minimum is not within the interval bounds, we can simply check corners to get convexity information

			double r1 = std::pow(p2,2) + (4*p2*p3*std::log(l))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(l),2))/(std::pow(std::log(10.),2))
						+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(l))/std::log(10.);
			double r2 = std::pow(p2,2) + (4*p2*p3*std::log(u))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(u),2))/(std::pow(std::log(10.),2))
						+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(u))/std::log(10.);

			if( r1 <= 0. && r2 <= 0.){
				convexity = CONCAVE;
			}
			else if( r1 >= 0. && r2 >= 0.){
				convexity = CONVEX;
			}

		}//end of else {l < min && min < u}
	}

}


template <typename T> inline void
vMcCormick<T>::_nrtl_tau_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double a, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax )
{
	// for further details, please refer to Najman, Bongartz & Mitsos 2019 Relaxations of thermodynamic property and costing models in process engineering
	// monotonicity
	if(f == 0 && e == 0){ // there is no root of derivative so simply check corners
		monotonicity = (b <= 0 )? MON_INCR : MON_DECR;

	}
	else if(f == 0){
		//the root of derivative is b/e
		double root = b/e;
		if(root <= 0){ // if it is not > 0, then do a simple corner check
			monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
		}
		else if(root <= l ){ // left of the interval
			monotonicity = (std::pow(e,3)/std::pow(b,2)>0 )? MON_INCR : MON_DECR; //check if its a minimum or maximum
		}
		else if(root >= u ){ // left of the interval
			monotonicity = (std::pow(e,3)/std::pow(b,2)>0 )? MON_DECR : MON_INCR; //check if its a minimum or maximum
		}
		else{ // the root lies in the interval and the function is not monotonic but we can still get tight interval bounds
			monotonicity = MON_NONE;
			if(std::pow(e,3)/std::pow(b,2)>0 ){ //minimum
				new_l = mc::nrtl_tau(b/e,a,b,e,f);
				zmin = b/e;
				if(mc::nrtl_tau(l,a,b,e,f)<=mc::nrtl_tau(u,a,b,e,f)){
					new_u =  mc::nrtl_tau(u,a,b,e,f);
					zmax = u;
				}
				else{
					new_u = mc::nrtl_tau(l,a,b,e,f);
					zmax = l;
				}
			}
			else{	//maximum
				new_u = mc::nrtl_tau(b/e,a,b,e,f);
				zmax = b/e;
				if(mc::nrtl_tau(l,a,b,e,f)<=mc::nrtl_tau(u,a,b,e,f)){
					new_l =  mc::nrtl_tau(l,a,b,e,f);
					zmin = l;
				}
				else{
					new_l = mc::nrtl_tau(u,a,b,e,f);
					zmin = u;
				}
			}
		}
	}
	else{

	    double val = std::pow(e,2) +4.0*b*f;
        if(val < 0.){ // check of Assumption 1 we have no roots, so simply check corners
			monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
		}
		else{
			//in this case, we have two roots with r1 < r2
			double r1 = std::min(-(e+std::sqrt(val ))/(2.0*f),-(e-std::sqrt(val ))/(2.0*f));
			double r2 = std::max(-(e+std::sqrt(val ))/(2.0*f),-(e-std::sqrt(val ))/(2.0*f));

			// if the right root does not lie in the valid domain T>0 then its second derivative is invalid for the following checks
			if(r2 <= 0. ){ // if it is not > 0, then do a simple corner check
				monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
			}
			else{
				// note that checking only der2_nrtl_tau is cheaper than checking nrtl_tau twice when evaluating corner points
				// the right root does not lie in the interval and is left of the interval
				if(r2 <= l){
					monotonicity = (mc::der2_nrtl_tau(r2,b,e) > 0. )? MON_INCR : MON_DECR; //check if its a minimum or maximum
				}
				// the left root does not lie in the interval and is right of the interval
				else if(r1 >= u){
					monotonicity = (mc::der2_nrtl_tau(r1,b,e) > 0. )? MON_DECR : MON_INCR; //check if its a minimum or maximum
				}
				// both roots are outside the interval
				else if (r1 <= l && u <= r2){
					if(r1 <= 0.){		 // the smaller root can still be invalid
						monotonicity = (mc::der2_nrtl_tau(r2,b,e) > 0. )? MON_DECR : MON_INCR; //check if its a minimum or maximum
					}
					else{
						monotonicity = (mc::der2_nrtl_tau(r1,b,e) > 0. )? MON_INCR : MON_DECR; //check if its a minimum or maximum
					}
				}
				// at least one root lies within the interval bounds
				else{ // we can still get tight interval bounds
					monotonicity = MON_NONE;
					// check if left root lies in the interval bounds
					if(l<r1){
						// save value at root and root
						if(mc::der2_nrtl_tau(r1,b,e) > 0.){	// minimum
							new_l = mc::nrtl_tau(r1,a,b,e,f);
							zmin = r1;
						}
						else{	// maximum
							new_u = mc::nrtl_tau(r1,a,b,e,f);
							zmax = r1;
						}
					}
					// check if right root lies in the interval bounds
					if(r2<u){
						if(mc::der2_nrtl_tau(r2,b,e) > 0.){	// minimum
							new_l = mc::nrtl_tau(r2,a,b,e,f);
							zmin = r2;
						}
						else{	// maximum
							new_u = mc::nrtl_tau(r2,a,b,e,f);
							zmax = r2;
						}
					}

					if(mc::nrtl_tau(l,a,b,e,f) < new_l){ new_l = mc::nrtl_tau(l,a,b,e,f); zmin = l; }
					if(mc::nrtl_tau(u,a,b,e,f) < new_l){ new_l = mc::nrtl_tau(u,a,b,e,f); zmin = u; }
					if(mc::nrtl_tau(l,a,b,e,f) > new_u){ new_u = mc::nrtl_tau(l,a,b,e,f); zmax = l; }
					if(mc::nrtl_tau(u,a,b,e,f) > new_u){ new_u = mc::nrtl_tau(u,a,b,e,f); zmax = u; }
				}
			}
		}
	}

	//convexity
	if(b == 0 || e == 0 ){
		convexity = (mc::der2_nrtl_tau((l+u)/2,b,e)>=0)? CONVEX:CONCAVE;
	}
	else{
		//we have only one root of the second derivative so we simply check whether it lies within the interval
		double root = 2.0*b/e;
		if(root <= l || root >= u ){
			convexity = (mc::der2_nrtl_tau((l+u)/2,b,e)>=0)? CONVEX:CONCAVE;
		}else{
			convexity = CONV_NONE;
		}
	}
}


template <typename T> inline void
vMcCormick<T>::_nrtl_der_tau_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax )
{

	if(e == 0){
		//the function is then simply f - b/T^2  ---> first derivative = 2*b/T^3  ---> second derivative -6*b/T^4, T>0
		if(b>=0){
			monotonicity = MON_INCR;
			convexity = CONCAVE;
		}else{
			monotonicity = MON_DECR;
			convexity = CONVEX;
		}
	}
	else{
	    //monotonicity
	    //in this case, we have one root
		double root = 2.0*b/e;

		if(root <= l){
			monotonicity = ( b<=0 )? MON_INCR : MON_DECR; //check if its a minimum or maximum, if b <= 0 then it's a minimum
		}
		else if(root >= u){
			monotonicity = ( b<=0 )? MON_DECR : MON_INCR; //check if its a minimum or maximum, if b <= 0 then it's a minimum
		}
		else{ // we can still get tight interval bounds
			monotonicity = MON_NONE;
			if(b<=0){ // it's a minimum
				//lower bound and min point
				new_l = mc::nrtl_dtau(root,b,e,f);
				zmin = root;
				//upper bound and max point
				if(mc::nrtl_dtau(l,b,e,f) <= mc::nrtl_dtau(u,b,e,f)){
					new_u = mc::nrtl_dtau(u,b,e,f);
					zmax = u;
				}else{
					new_u = mc::nrtl_dtau(l,b,e,f);
					zmax = u;
				}
			}else{ // it is a maximum
				//lower bound and min point
				if(mc::nrtl_dtau(l,b,e,f) <= mc::nrtl_dtau(u,b,e,f)){
					new_l = mc::nrtl_dtau(l,b,e,f);
					zmin = l;
				}else{
					new_l = mc::nrtl_dtau(u,b,e,f);
					zmin = u;
				}
				//upper bound and max point
				new_u = mc::nrtl_dtau(root,b,e,f);
				zmax = root;
			}
		}

		//convexity
		root = 3.0*b/e;
		if( root <= l || u <= root){ //if the root of second derivative is not within the interval, then just check any point of the second derivative
				convexity = ( 2.0*e/std::pow((l+u)/2,2) - 6.0*b/std::pow((l+u)/2,2) >0 )? CONVEX : CONCAVE;
		}
		else{ //the root lies within the interval and the function is convex-concave
			convexity = CONV_NONE;
		}
	}
}


template <typename T> inline void
vMcCormick<T>::print
(std::ostream&out, const unsigned int ipt)
{
	out << std::scientific << std::setprecision(vMcCormick<T>::options.DISPLAY_DIGITS) << std::right
      << "[ " << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << Op<T>::l(_I) << " : "
              << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << Op<T>::u(_I) << " ]";
	if(_npts<=ipt) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS);
	out <<	" [ "  << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cv[ipt] << " : "
                   << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cc[ipt] << " ]";
	   if( _nsub ){
		out << " [ (";
		for( unsigned int i=0; i<_nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cvsub[ipt][i] << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cvsub[ipt][_nsub-1] << ") : (";
		for( unsigned int i=0; i<_nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _ccsub[ipt][i] << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _ccsub[ipt][_nsub-1] << ") ]";
		}
		out << "\n";
}

template <typename T> inline void
vMcCormick<T>::print_all
(std::ostream&out)
{
	for(unsigned int ipt=0;ipt<_npts;ipt++){
		out << std::scientific << std::setprecision(vMcCormick<T>::options.DISPLAY_DIGITS) << std::right
			<< "[ " << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << Op<T>::l(_I) << " : "
					<< std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << Op<T>::u(_I) << " ] ";
		out <<	"[ "  << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cv[ipt] << " : "
				      << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cc[ipt] << " ]";
	   if( _nsub ){
		out << " [ (";
		for( unsigned int i=0; i<_nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cvsub[ipt][i] << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _cvsub[ipt][_nsub-1] << ") : (";
		for( unsigned int i=0; i<_nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _ccsub[ipt][i] << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << _ccsub[ipt][_nsub-1] << ") ]";
		}
		out << "\n";
	}
}

#ifdef MC__VMCCORMICK_DEBUG
template <typename T> inline void
vMcCormick<T>::_debug_check
( const vMcCormick<T> &MCin, const vMcCormick<T> &MCout, std::string &operation) {

  bool error = false;
  // check if lower interval bound > upper interval bound
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  for(unsigned int ipt=0;ipt<MCout._npts;ipt++){
	  // check if any of the subgradients is a NaN
	  for(unsigned int i = 0; i < MCout._nsub; i++){
			if(!(MCout._cvsub[ipt][i]==MCout._cvsub[ipt][i]) || !(MCout._ccsub[ipt][i]==MCout._ccsub[ipt][i])){
					error = true;
				std::cout << "convex subgradient[" << ipt << "][" << i << "]:" << MCout._cvsub[ipt][i] << " concave subgradient[" << ipt << "][" << i << "]:" << MCout._ccsub[ipt][i] << std::endl;
			}
	  }
	  // check if lower interval bound > cc
	  if(Op<T>::l(MCout._I)>MCout._cc[ipt] && !isequal(Op<T>::l(MCout._I),MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	   // check if upper interval bound < cv
	  if(MCout._cv[ipt] > Op<T>::u(MCout._I) && !isequal(MCout._cv[ipt],Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
	  }
	  // check if convex >= concave
	  if(MCout._cv[ipt]>MCout._cc[ipt] && !isequal(MCout._cv[ipt],MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	  // check for NaNs in intervals
	  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) || Op<T>::l(MCout._I) != Op<T>::l(MCout._I) || Op<T>::u(MCin._I) != Op<T>::u(MCin._I) || Op<T>::l(MCin._I) != Op<T>::l(MCin._I)){
		  error = true;
	  }
	  // check if any is
	  if(MCout._cv[ipt] == std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == std::numeric_limits<double>::infinity( )
			|| MCout._cv[ipt] == -std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == -std::numeric_limits<double>::infinity( ) || error){
			std::cout << "operation: " << operation << " in point: " << ipt << std::endl;
			std::cout << "MCin.l: " << std::setprecision(16) << Op<T>::l(MCin._I) << " MCin.cv: " <<  MCin._cv[ipt] << " MCin.cc: " <<  MCin._cc[ipt] << " MCin.u " <<  Op<T>::u(MCin._I) << std::endl;
			std::cout << "subgradient MCin:" << std::endl;
			for(unsigned int i = 0; i < MCout._nsub; i++){
				std::cout << "cv sub[" << ipt << "][" << i << "]:" << MCin._cvsub[ipt][i] << " cc sub[" << ipt << "][" << i << "]:" << MCin._ccsub[ipt][i] << std::endl;
			}
			std::cout << "MCout.l: " <<  Op<T>::l(MCout._I) << " MCout.cv: " <<  MCout._cv[ipt] << " MCout.cc: " <<  MCout._cc[ipt] << " MCout.u " <<  Op<T>::u(MCout._I) << std::endl;
			std::cout << "subgradient MCout:" << std::endl;
			for(unsigned int i = 0; i < MCout._nsub; i++){
				std::cout << "cv sub[" << ipt << "][" << i << "]:" << MCout._cvsub[ipt][i] << " cc sub[" << ipt << "][" << i << "]:" << MCout._ccsub[ipt][i] << std::endl;
			}
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::DEBUG );
		}
  }
}

template <typename T> inline void
vMcCormick<T>::_debug_check
( const vMcCormick<T> &MCin1, const vMcCormick<T> &MCin2, const vMcCormick<T> &MCout, std::string &operation){

  bool error = false;
   // check if lower interval bound > upper interval bound
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  for(unsigned int ipt=0;ipt<MCout._npts;ipt++){
	 // check if any of the subgradients is a NaN
	  for(unsigned int i = 0; i < MCout._nsub; i++){
			if(!(MCout._cvsub[ipt][i]==MCout._cvsub[ipt][i]) || !(MCout._ccsub[ipt][i]==MCout._ccsub[ipt][i])){
					error = true;
				std::cout << "convex subgradient[" << ipt << "][" << i << "]:" << MCout._cvsub[ipt][i] << " concave subgradient[" << ipt << "][" << i << "]:" << MCout._ccsub[ipt][i] << std::endl;
			}
	  }
	  // check if lower interval bound > cc
	  if(Op<T>::l(MCout._I)>MCout._cc[ipt] && !isequal(Op<T>::l(MCout._I),MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	   // check if upper interval bound < cv
	  if(MCout._cv[ipt] > Op<T>::u(MCout._I) && !isequal(MCout._cv[ipt],Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
	  }
	  // check if convex >= concave
	  if(MCout._cv[ipt]>MCout._cc[ipt] && !isequal(MCout._cv[ipt],MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	   // check for NaNs in intervals
	  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) ||Op<T>::l(MCout._I) != Op<T>::l(MCout._I)  || Op<T>::u(MCin2._I) != Op<T>::u(MCin2._I)
	   || Op<T>::l(MCin1._I) != Op<T>::l(MCin1._I) || Op<T>::u(MCin1._I) != Op<T>::u(MCin1._I) || Op<T>::l(MCin2._I) != Op<T>::l(MCin2._I)){
		  error = true;
	  }
	  // check if any is
	  if(MCout._cv[ipt] == std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == std::numeric_limits<double>::infinity( )
			|| MCout._cv[ipt] == -std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == -std::numeric_limits<double>::infinity( ) || error){
			std::cout << "operation: " << operation << " in point: " << ipt << std::endl;
			std::cout << "MCin1.l: " << std::setprecision(16) << Op<T>::l(MCin1._I) << " MCin1.cv: " <<  MCin1._cv[ipt] << " MCin1.cc: " <<  MCin1._cc[ipt] << " MCin1.u " <<  Op<T>::u(MCin1._I) << std::endl;
			std::cout << "MCin2.l: " <<  Op<T>::l(MCin2._I) << " MCin2.cv: " <<  MCin2._cv[ipt] << " MCin2.cc: " <<  MCin2._cc[ipt] << " MCin2.u " <<  Op<T>::u(MCin2._I) << std::endl;
			std::cout << "MCout.l: " <<  Op<T>::l(MCout._I) << " MCout.cv: " <<  MCout._cv[ipt] << " MCout.cc: " <<  MCout._cc[ipt] << " MCout.u  " <<  Op<T>::u(MCout._I) << std::endl;
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::DEBUG );
		}
  }
}

template <typename T> inline void
vMcCormick<T>::_debug_check
( const std::vector<vMcCormick<T>> &MCin, const vMcCormick<T> &MCout, std::string &operation) {

	  bool error = false;
   // check if lower interval bound > upper interval bound
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  for(unsigned int ipt=0;ipt<MCout._npts;ipt++){
	 // check if any of the subgradients is a NaN
	  for(unsigned int i = 0; i < MCout._nsub; i++){
			if(!(MCout._cvsub[ipt][i]==MCout._cvsub[ipt][i]) || !(MCout._ccsub[ipt][i]==MCout._ccsub[ipt][i])){
					error = true;
				std::cout << "convex subgradient[" << ipt << "][" << i << "]:" << MCout._cvsub[ipt][i] << " concave subgradient[" << ipt << "][" << i << "]:" << MCout._ccsub[ipt][i] << std::endl;
			}
	  }
	  // check if lower interval bound > cc
	  if(Op<T>::l(MCout._I)>MCout._cc[ipt] && !isequal(Op<T>::l(MCout._I),MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	   // check if upper interval bound < cv
	  if(MCout._cv[ipt] > Op<T>::u(MCout._I) && !isequal(MCout._cv[ipt],Op<T>::u(MCout._I),vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
	  }
	  // check if convex >= concave
	  if(MCout._cv[ipt]>MCout._cc[ipt] && !isequal(MCout._cv[ipt],MCout._cc[ipt],vMcCormick<T>::options.MVCOMP_TOL,vMcCormick<T>::options.MVCOMP_TOL)){
		  error=true;
		  std::cout << "CV[" << ipt << "]=" << MCout._cv[ipt] << ">" << MCout._cc[ipt] << "=CC[" << ipt << "]" << std::endl;
	  }
	   // check for NaNs in intervals
	  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) ||Op<T>::l(MCout._I) != Op<T>::l(MCout._I)){
		  error = true;
	  }
	  for(size_t i = 0;i<MCin.size();i++){
		  if(Op<T>::l(MCin[i]._I) != Op<T>::l(MCin[i]._I) || Op<T>::u(MCin[i]._I) != Op<T>::u(MCin[i]._I)){
			  error = true;
		  }
	  }

	  // check if any is
	  if(MCout._cv[ipt] == std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == std::numeric_limits<double>::infinity( )
			|| MCout._cv[ipt] == -std::numeric_limits<double>::infinity( ) || MCout._cc[ipt] == -std::numeric_limits<double>::infinity( ) || error){
			std::cout << "operation: " << operation << " in point: " << ipt << std::endl;
			for(size_t i = 0;i<MCin.size();i++){
				std::cout << "MCin[" << i << "].l: " << std::setprecision(16) << Op<T>::l(MCin[i]._I) << " MCin[" << i << "].cv: " <<  MCin[i]._cv[ipt]
				          << " MCin[" << i << "].cc: " <<  MCin[i]._cc[ipt] << " MCin[" << i << "].u " <<  Op<T>::u(MCin[i]._I) << std::endl;
			}
			std::cout << "MCout.l: " <<  Op<T>::l(MCout._I) << " MCout.cv: " <<  MCout._cv[ipt] << " MCout.cc: " <<  MCout._cc[ipt] << " MCout.u  " <<  Op<T>::u(MCout._I) << std::endl;
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::DEBUG );
		}
  }
}
#endif

template <typename T> inline double
vMcCormick<T>::_secant
( const double x0, const double x1, const double xL, const double xU,
  puniv f, const double*rusr, const int*iusr, const std::vector<double>&vusr)
{
  double xkm = std::max(xL,std::min(xU,x0));
  double fkm = f(xkm,rusr,iusr,vusr);
  double xk = std::max(xL,std::min(xU,x1));

  for( unsigned int it=0; it<options.ENVEL_MAXIT; it++ ){
    double fk = f(xk,rusr,iusr,vusr);
    if( std::fabs(fk) < options.ENVEL_TOL ) return xk;
    double Bk = (fk-fkm)/(xk-xkm);
    if( Bk == 0 ) throw Exceptions( Exceptions::ENVEL );
    if( isequal(xk,xL) && fk/Bk>0 ) return xk;
    if( isequal(xk,xU) && fk/Bk<0 ) return xk;
    xkm = xk;
    fkm = fk;
    xk = std::max(xL,std::min(xU,xk-fk/Bk));
  }

  throw Exceptions( Exceptions::ENVEL );
}

template <typename T> inline double
vMcCormick<T>::_goldsect
( const double xL, const double xU, puniv f, const double*rusr,
  const int*iusr, const std::vector<double>&vusr)
{
  const double phi = 2.-(1.+std::sqrt(5.))/2.;
  const double fL = f(xL,rusr,iusr,vusr), fU = f(xU,rusr,iusr,vusr);
  if( fL*fU > 0 ) throw Exceptions( Exceptions::ENVEL );
  const double xm = xU-phi*(xU-xL), fm = f(xm,rusr,iusr,vusr);
  return _goldsect_iter( true, xL, fL, xm, fm, xU, fU, f, rusr, iusr, vusr );
}

template <typename T> inline double
vMcCormick<T>::_goldsect_iter
( const bool init, const double a, const double fa, const double b,
  const double fb, const double c, const double fc, puniv f,
  const double*rusr, const int*iusr, const std::vector<double>&vusr )
// a and c are the current bounds; the minimum is between them.
// b is a center point
{
  static thread_local unsigned int iter;
  iter = ( init? 1: iter+1 );
  const double phi = 2.-(1.+std::sqrt(5.))/2.;
  bool b_then_x = ( c-b > b-a );
  double x = ( b_then_x? b+phi*(c-b): b-phi*(b-a) );
  if( std::fabs(c-a) < options.ENVEL_TOL*(std::fabs(b)+std::fabs(x))
   || iter > options.ENVEL_MAXIT ) return (c+a)/2.;
  double fx = f(x,rusr,iusr,vusr);
  if( b_then_x )
    return( fa*fx<0? _goldsect_iter( false, a, fa, b, fb, x, fx, f, rusr, iusr, vusr ):
                      _goldsect_iter( false, b, fb, x, fx, c, fc, f, rusr, iusr, vusr ) );
  return( fa*fb<0? _goldsect_iter( false, a, fa, x, fx, b, fb, f, rusr, iusr, vusr ):
                    _goldsect_iter( false, x, fx, b, fb, c, fc, f, rusr, iusr, vusr ) );
}

////////////////////////////////////////////////////////////////////////

template <typename T> inline vMcCormick<T>
cut
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2( MC );
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
operator+
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2( MC );
  return MC2;
}

template <typename T> inline vMcCormick<T>
operator+
( const double a, const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = a + MC._I;
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  MC2._cv[ipt] = a + MC._cv[ipt];
	  MC2._cc[ipt] = a + MC._cc[ipt];
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[ipt][i] = MC._cvsub[ipt][i];
		MC2._ccsub[ipt][i] = MC._ccsub[ipt][i];
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
   	std::string str = "sum double+MC";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2;
}

template <typename T> inline vMcCormick<T>
operator+
( const vMcCormick<T>&MC, const double a )
{
  return a + MC;
}

template <typename T> inline vMcCormick<T>
operator+
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if( MC2._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    return MC3._sum1( MC1, MC2 );
  }
  if( MC1._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
    return MC3._sum1( MC2, MC1 );
  }
  if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );

  vMcCormick<T> MC3;
  MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );
  return MC3._sum2( MC1, MC2 );
}

template <typename T> inline vMcCormick<T>
operator-
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = -MC._I;
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  MC2._cv[ipt] = -MC._cc[ipt];
	  MC2._cc[ipt] = -MC._cv[ipt];
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[ipt][i] = -MC._ccsub[ipt][i];
		MC2._ccsub[ipt][i] = -MC._cvsub[ipt][i];
	  }
  }
  return MC2;
}

template <typename T> inline vMcCormick<T>
operator-
( const vMcCormick<T>&MC, const double a )
{
  return MC + (-a);
}

template <typename T> inline vMcCormick<T>
operator-
( const double a, const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = a - MC._I;
  for(unsigned int ipt=0;ipt<MC._npts;ipt++){
	  MC2._cv[ipt] = a - MC._cc[ipt];
	  MC2._cc[ipt] = a - MC._cv[ipt];
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[ipt][i] = -MC._ccsub[ipt][i];
		MC2._ccsub[ipt][i] = -MC._cvsub[ipt][i];
	  }
  }
  return MC2;
}

template <typename T> inline vMcCormick<T>
operator-
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if( &MC1 == &MC2 ) return 0;

  if( MC2._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    return MC3._sub1( MC1, MC2 );
  }
  if( MC1._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
    return MC3._sub2( MC1, MC2 );
  }
  if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  if( MC1._npts != MC2._npts )
	throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );

  vMcCormick<T> MC3;
  MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );
  return MC3._sub3( MC1, MC2 );
}

template <typename T> inline vMcCormick<T>
operator*
( const double a, const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = a * MC._I;
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  if ( a >= 0 ){
		MC2._cv[ipt] = a * MC._cv[ipt];
		MC2._cc[ipt] = a * MC._cc[ipt];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = a * MC._cvsub[ipt][i];
		  MC2._ccsub[ipt][i] = a * MC._ccsub[ipt][i];
		}
	  }
	  else{
		MC2._cv[ipt] = a * MC._cc[ipt];
		MC2._cc[ipt] = a * MC._cv[ipt];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = a * MC._ccsub[ipt][i];
		  MC2._ccsub[ipt][i] = a * MC._cvsub[ipt][i];
		}
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
   	std::string str = "mult double*MC a =" + std::to_string(a);
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2;
}

template <typename T> inline vMcCormick<T>
operator*
( const vMcCormick<T>&MC, const double a )
{
  return a * MC;
}

template <typename T> inline vMcCormick<T>
operator*
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{

  if( &MC1 == &MC2 ) return sqr(MC1);

  bool thin1 = isequal( Op<T>::diam(MC1._I), 0. );
  bool thin2 = isequal( Op<T>::diam(MC2._I), 0. );

  if ( vMcCormick<T>::options.MVCOMP_USE && !(thin1||thin2) ){
    vMcCormick<T> MC3;
    if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    else if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
    else if( MC1._nsub != MC2._nsub )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
    else if( MC1._npts != MC2._npts )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
    else
      MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

    MC3._I = MC1._I * MC2._I;
    return MC3._mulMV( MC1, MC2 ).cut();
  }

  if ( Op<T>::l(MC1._I) >= 0. ){
    if ( Op<T>::l(MC2._I) >= 0. ){
      if( MC2._const ){
        vMcCormick<T> MC3;
        MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
        return MC3._mul1_u1pos_u2pos( MC1, MC2 ).cut();
      }
      if( MC1._const ){
        vMcCormick<T> MC3;
        MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
        return MC3._mul1_u1pos_u2pos( MC2, MC1 ).cut();
      }
      if( MC1._nsub != MC2._nsub )
        throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
	  if( MC1._npts != MC2._npts )
		throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );

      vMcCormick<T> MC3;
      MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );
      return MC3._mul2_u1pos_u2pos( MC1, MC2 ).cut();
    }
    if ( Op<T>::u(MC2._I) <= 0. ){
      return -( MC1 * (-MC2) );
    }
    if( MC2._const ){
      vMcCormick<T> MC3;
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
      return MC3._mul1_u1pos_u2mix( MC1, MC2 ).cut();
    }
    if( MC1._const ){
      vMcCormick<T> MC3;
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
      return MC3._mul2_u1pos_u2mix( MC1, MC2 ).cut();
    }
    if( MC1._nsub != MC2._nsub )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
	if( MC1._npts != MC2._npts )
	  throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
    vMcCormick<T> MC3;
    MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );
    return MC3._mul3_u1pos_u2mix( MC1, MC2 ).cut();
  }

  if ( Op<T>::u(MC1._I) <= 0. ){
    if ( Op<T>::l(MC2._I) >= 0. ){
      return -( (-MC1) * MC2);
    }
    if ( Op<T>::u(MC2._I) <= 0. ){
      return (-MC1) * (-MC2);
    }
    return -( MC2 * (-MC1) );
  }

  if ( Op<T>::l(MC2._I) >= 0. ){
    return MC2 * MC1;
  }
  if ( Op<T>::u(MC2._I) <= 0. ){
    return -( (-MC2) * MC1 );
  }
  if( MC2._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    return MC3._mul1_u1mix_u2mix( MC1, MC2 ).cut();
  }
  if( MC1._const ){
    vMcCormick<T> MC3;
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
    return MC3._mul1_u1mix_u2mix( MC2, MC1 ).cut();
  }
  if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  if( MC1._npts != MC2._npts )
	throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );

  vMcCormick<T> MC3;
  MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );
  return MC3._mul2_u1mix_u2mix( MC1, MC2 ).cut();
}

template <typename T> inline vMcCormick<T>
operator/
( const vMcCormick<T>&MC, const double a )
{
  if ( isequal( a, 0. ))
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::DIV );
  return (1./a) * MC;
}

template <typename T> inline vMcCormick<T>
operator/
( const double a, const vMcCormick<T>&MC )
{
  if( a == 0.) {return 0.;}
  return a * inv( MC );
}

template <typename T> inline vMcCormick<T>
operator/
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{

  if( &MC1 == &MC2 ) return 1.;

  // Added @ AVT.SVT, Aug 30, 2016: When not checking for constant denominator, multivariate gives unnecessarily lose relaxations for linear function
  if( (MC2._const) && (Op<T>::l(MC2._I)==Op<T>::u(MC2._I)) ) {
	if ( Op<T>::l(MC2._I) == 0. ) throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::INV );
	return (1./Op<T>::l(MC2._I)) * MC1;
  }

  bool posorthant = ( Op<T>::l(MC1._I) > 0. && Op<T>::l(MC2._I) > 0. );

  if ( vMcCormick<T>::options.MVCOMP_USE && posorthant){
    vMcCormick<T> MC3;
	if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    else if( MC1._const )
		// Removed @ AVT.SVT, Apr 07, 2017: Otherwise subgradient dimension can get lost!
	// if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
    else if( MC1._nsub != MC2._nsub )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
	else if( MC1._npts != MC2._npts )
		throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
    else
      MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

    MC3._I = MC1._I / MC2._I;

    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		int imidcv1 = 1, imidcv2 = 2; // We can easily solve the convex problem given in Theorem 2 in Tsoukalas & Mitsos 2014,
									  // since the minimum of the convex relaxation is at point f1cv, f2cc, which can be shown through directional derivatives
		double fmidcv1 = ( mid( MC1._const ? MC1._cv[0] : MC1._cv[ipt], MC1._const ? MC1._cc[0] : MC1._cc[ipt], Op<T>::l(MC1._I), imidcv1)
		  + std::sqrt(Op<T>::l(MC1._I) * Op<T>::u(MC1._I)) )
		  / ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) );
		double fmidcv2 = mid(MC2._const ? MC2._cv[0] : MC2._cv[ipt], MC2._const ? MC2._cc[0] : MC2._cc[ipt], Op<T>::u(MC2._I), imidcv2);
		MC3._cv[ipt] = mc::sqr(fmidcv1) / fmidcv2;
		for( unsigned int i=0; i<MC3._nsub; i++ )
		  MC3._cvsub[ipt][i] = 2. * fmidcv1 / fmidcv2
			/ ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) )
			* (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcv1 ))
			- mc::sqr( fmidcv1 / fmidcv2 )
			* (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcv2 ));

		int imidcc1 = -1, imidcc2 = -1; // we can't know which relaxation to use
		// Fixed @ AVT.SVT, Aug 24, 2016; Division was broken (e.g. log(x2)/x1 on[10,10.1]x[20,50])
		//double fmidcc1 = mid(MC1._cv, MC1._cc, Op<T>::l(MC1._I), imidcc1);
		//double fmidcc2 = mid(MC2._cv, MC2._cc, Op<T>::u(MC2._I), imidcc2);
		double fmidcc1 = mid_ndiff(MC1._const ? MC1._cv[0] : MC1._cv[ipt], MC1._const ? MC1._cc[0] : MC1._cc[ipt], Op<T>::u(MC1._I), imidcc1);
		double fmidcc2 = mid_ndiff(MC2._const ? MC2._cv[0] : MC2._cv[ipt], MC2._const ? MC2._cc[0] : MC2._cc[ipt], Op<T>::l(MC2._I), imidcc2);
		double gcc1 = Op<T>::u(MC2._I) * fmidcc1 - Op<T>::l(MC1._I) * fmidcc2
					 + Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
		double gcc2 = Op<T>::l(MC2._I) * fmidcc1 - Op<T>::u(MC1._I) * fmidcc2
					 + Op<T>::u(MC1._I) * Op<T>::u(MC2._I);

		if(  gcc1 <= gcc2  ){ //uses equation (31) in multivariate McCormick paper
		  MC3._cc[ipt] = gcc1 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
	      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since the convex part has to be computed anyways
		  for( unsigned int i=0; i<MC3._nsub; i++ ){
			MC3._ccsub[ipt][i] = 1. / Op<T>::l(MC2._I)
			  * (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcc1 ))
			  - Op<T>::l(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) )
			  * (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcc2 ));
		    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
		  }
		}
		else{
		  MC3._cc[ipt] = gcc2 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
	      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt);
		  for( unsigned int i=0; i<MC3._nsub; i++ ){
			MC3._ccsub[ipt][i] = 1. / Op<T>::u(MC2._I)
			  * (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcc1 ))
			  - Op<T>::u(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) )
			  * (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcc2 ));
		    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
		  }
		}
        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
	}//end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "MC1/MC2";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
    return MC3.cut();
  }

  return MC1 * inv( MC2 );
}

template <typename T> inline vMcCormick<T>
inv
( const vMcCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. && Op<T>::u(MC._I) >= 0. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::INV );
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::inv( MC._I );


  if ( Op<T>::l(MC._I) > 0. ){
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = 2; // convex envelope of 1/x is decreasing for x^L>0 so we use the cc relaxation for cv
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		  MC2._cv[ipt] = 1./vmid;
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = - mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid )
			  / ( vmid * vmid );
		  }
		}
		{ int imid = 1; // concave envelope of 1/x is decreasing for x^L>0 so we use the cv relaxation for cc
		  MC2._cc[ipt] = 1. / Op<T>::l(MC._I) + 1. / Op<T>::u(MC._I) - mid( MC._cv[ipt], MC._cc[ipt],Op<T>::l(MC._I), imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = - mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
		}
	  }
  }
  else{
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = 2; // convex envelope of 1/x is decreasing for x^L<0 so we use the cc relaxation for cv
		  MC2._cv[ipt] = 1. / Op<T>::l(MC._I) + 1. / Op<T>::u(MC._I) - mid( MC._cv[ipt], MC._cc[ipt],Op<T>::u(MC._I), imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = - mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
		}
		{ int imid = 1; // concave envelope of 1/x is decreasing for x^L>0 so we use the cv relaxation for cc
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid);
		  MC2._cc[ipt] = 1. / vmid;
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = - mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) / ( vmid * vmid );
		}
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "inv";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.INV);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
sqr
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::sqr( MC._I );
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
		imid = -1;
		MC2._cv[ipt] = mc::sqr( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ) );
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = 2 * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid )* mid( MC._cv[ipt], MC._cc[ipt], zmin, imid );
		}
	  }

	  { int imid = -1;
		double zmax = (mc::sqr( Op<T>::l(MC._I) )>mc::sqr( Op<T>::u(MC._I) ) ? Op<T>::l(MC._I) : Op<T>::u(MC._I));
		double r;
		double pt;
		if(isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		 r = 0.;
		 pt = mc::sqr(Op<T>::l(MC._I))>mc::sqr(Op<T>::u(MC._I)) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
		}
		else{
		 r = ( mc::sqr( Op<T>::u(MC._I) ) - mc::sqr( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
		 pt = Op<T>::l(MC._I);
		}
		MC2._cc[ipt] = mc::sqr( pt ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ) - pt );
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	  }
  }// end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "sqr";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.SQR);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
exp
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::exp( MC._I );
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = 1;
		MC2._cv[ipt] = std::exp( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ));
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * MC2._cv[ipt];
	  }

	  { int imid = 2;
		double r = 0.;
		if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
		  r = ( std::exp( Op<T>::u(MC._I) ) - std::exp( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		MC2._cc[ipt] = std::exp( Op<T>::u(MC._I) ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid )- Op<T>::u(MC._I) );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
	  }
  }//end of for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "exp";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.EXP);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
arh
( const vMcCormick<T>&MC, const double k )
{
  if( Op<T>::l(MC._I) <= 0. || k < 0. || ( Op<T>::u(MC._I) > 0.5*k && Op<T>::l(MC._I) < 0.5*k ) ){
    return exp( - k * inv( MC ) );
  }

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::arh( MC._I, k );

  if ( Op<T>::u(MC._I) <= 0.5*k ){
	 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = 1;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid );
		  MC2._cv[ipt] = std::exp( - k / vmid );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = k / ( vmid * vmid ) * MC2._cv[ipt] * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		}
		{ int imid = 2;
		  double r = 0.;
		  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
			r = ( mc::arh( Op<T>::u(MC._I),k ) - mc::arh( Op<T>::l(MC._I),k ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		  MC2._cc[ipt] = mc::arh( Op<T>::u(MC._I),k ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	 }
  }
  else if ( Op<T>::l(MC._I) >= 0.5*k ){
	 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = 1;
		  double r = 0.;
		  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
			r = ( mc::arh( Op<T>::u(MC._I),k ) - mc::arh( Op<T>::l(MC._I),k ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		  }
		  MC2._cv[ipt] = mc::arh( Op<T>::l(MC._I),k ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ) - Op<T>::l(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
		{ int imid = 2;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		  MC2._cc[ipt] = std::exp( - k / vmid );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = k / ( vmid * vmid ) * MC2._cc[ipt] * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		}
	 }
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "arh k =" + std::to_string(k);
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
	  // vMcCormick<T>::additionalLins.set_additional_values(&k,0);
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.ARH);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
log
( const vMcCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::LOG );
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::log( MC._I );
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imidcv = 1;
		double scal = 0.;
		if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
		  scal = ( std::log( Op<T>::u(MC._I) ) - std::log( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		MC2._cv[ipt] = std::log( Op<T>::l(MC._I) ) + scal * ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imidcv ) - Op<T>::l(MC._I) );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imidcv ) * scal;
	  }

	  { int imidcc = 2;
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imidcc );
		MC2._cc[ipt] = std::log( vmid );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imidcc ) / vmid;
	  }
  }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "log";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.LOG);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
xlog
( const vMcCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::LOG );
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::xlog( MC._I );
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), std::exp(-1.), imid );
		imid = -1;
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], zmin, imid );
		MC2._cv[ipt] = mc::xlog( vmid );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = (std::log( vmid ) + 1.) * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
	  }

	  { int imid = -1;
		double zmax = ( mc::xlog(Op<T>::u(MC._I))>=mc::xlog(Op<T>::l(MC._I))? Op<T>::u(MC._I): Op<T>::l(MC._I) );
		double r;
		double pt;
		if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
			r = 0.;
			pt = mc::xlog(Op<T>::l(MC._I))>mc::xlog(Op<T>::u(MC._I)) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
		}
		else {
			r = ( mc::xlog(Op<T>::u(MC._I)) - mc::xlog(Op<T>::l(MC._I)) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
			pt = Op<T>::l(MC._I);
		}
		imid = -1;
		MC2._cc[ipt] = mc::xlog(pt) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ) - pt);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
	  }
  }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "xlog";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.XLOG);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
fabsx_times_x
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::fabsx_times_x( MC._I );

  if(Op<T>::l(MC2._I)>=0){ // convex increasing
	  double r = 0;
	  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		r = (mc::fabsx_times_x(Op<T>::u(MC._I)) - mc::fabsx_times_x(Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I));
	  }
     for(unsigned int ipt = 0; ipt< MC2._npts; ipt++){
		  MC2._cv[ipt] = mc::fabsx_times_x(MC._cv[ipt]);
		  MC2._cc[ipt] = mc::fabsx_times_x(Op<T>::u(MC._I)) + r*(MC._cc[ipt] - Op<T>::u(MC._I));
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = MC._cvsub[ipt][i] * 2 * MC._cv[ipt];
			MC2._ccsub[ipt][i] = MC._ccsub[ipt][i] * r;
		  }
	 }
  }
  else if(Op<T>::u(MC2._I)<=0){ // concave increasing
      double r = 0;
	  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		r = (mc::fabsx_times_x(Op<T>::u(MC._I)) - mc::fabsx_times_x(Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I));
	  }
     for(unsigned int ipt = 0; ipt< MC2._npts; ipt++){
	  MC2._cv[ipt] = mc::fabsx_times_x(Op<T>::l(MC._I)) + r*(MC._cv[ipt] - Op<T>::l(MC._I));
	  MC2._cc[ipt] = mc::fabsx_times_x(MC._cc[ipt]);
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[ipt][i] = MC._cvsub[ipt][i] * r;
		MC2._ccsub[ipt][i] = MC._ccsub[ipt][i] * (-2) * MC._cc[ipt];
	  }
	 }
  }
  else{ // increasing
	 double ptCv = Op<T>::l(MC._I) - std::sqrt(2.0)* Op<T>::l(MC._I); // This is the solution point of (|x|*x - |xL|*xL)/(x-xL) = 2*x  <-- Note that we know that x is positive and xL is negative
	 double ptCc = Op<T>::u(MC._I) - std::sqrt(2.0)* Op<T>::u(MC._I); // This is the solution point of (|x|*x - |xU|*xU)/(x-xU) = -2*x <-- Note that we know that x is negative and xU is positive

     for(unsigned int ipt = 0; ipt< MC2._npts; ipt++){
		 // convex part
		 double subCv;
		 if( MC._cv[ipt] >= ptCv ){	 // convex part
			MC2._cv[ipt] = mc::fabsx_times_x(MC._cv[ipt]);
			subCv = 2*MC._cv[ipt];
		 }
		 else{
			subCv = ( isequal( Op<T>::l(MC._I), ptCv )? 0.: (mc::fabsx_times_x(ptCv)-mc::fabsx_times_x(Op<T>::l(MC._I)))/(ptCv-Op<T>::l(MC._I)) );
			MC2._cv[ipt] = mc::fabsx_times_x(Op<T>::l(MC._I)) + subCv*(MC._cv[ipt] - Op<T>::l(MC._I));
		 }
		 double subCc;
		 if( ptCc >= MC._cc[ipt]){
			MC2._cc[ipt] = mc::fabsx_times_x(MC._cc[ipt]);
			subCc = -2*MC._cc[ipt];
		 }
		 else{
			subCc = ( isequal( Op<T>::u(MC._I), ptCc )? 0.: (mc::fabsx_times_x(ptCc)-mc::fabsx_times_x(Op<T>::u(MC._I)))/(ptCc-Op<T>::u(MC._I)) );
			MC2._cc[ipt] = mc::fabsx_times_x(Op<T>::u(MC._I)) + subCc*(MC._cc[ipt] - Op<T>::u(MC._I));
		 }
		 for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = MC._cvsub[ipt][i] * subCv;
			MC2._ccsub[ipt][i] = MC._ccsub[ipt][i] * subCc;
		  }
	 }
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "fabsx_times_x";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}


template <typename T> inline vMcCormick<T>
xexpax
( const vMcCormick<T>&MC, const double a )
{
  if ( a == 0. )
    return MC;
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::xexpax( MC._I, a);
  // int currentCase= 0; // used for additional linearizations
  double zmin, zmax, r, pt;
  if(a>0){ // -1/a is the minimum of the function and -2/a is the turning point
	  //the function is convex (not necessarily monotonic)
	  if(Op<T>::l(MC._I) >= -2.0/a){
	    // currentCase = 0;
	    int imid = -1;
        zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), -1.0/a, imid );
        zmax = ( mc::xexpax(Op<T>::u(MC._I),a)>=mc::xexpax(Op<T>::l(MC._I),a)? Op<T>::u(MC._I): Op<T>::l(MC._I) );
	    pt = zmax;
	    if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
		    r = 0.;
	    }
	    else {
		 	r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
	    }
		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		 //convex relaxation
		 {
	       int imid = -1;
           double vmid = mid( MC._cv[ipt], MC._cc[ipt], zmin, imid );
           MC2._cv[ipt] = mc::xexpax( vmid,a );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[ipt][i] = (std::exp(a*vmid)*(1.0+a*vmid)) * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		   }
		 }
		 //concave relaxation
		 {
		   int imid = -1;
           MC2._cc[ipt] = mc::xexpax(pt,a) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ) - pt );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		   }
         }
		}// end of for ipt
	  }
	  //the function is concave and monotonically decreasing
	  else if(Op<T>::u(MC._I) <= -2.0/a){
	    // currentCase = 1;
		r = 0.;
        if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
             r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		}
		//convex relaxation
		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		 {
           MC2._cv[ipt] = mc::xexpax(Op<T>::u(MC._I),a) + r * ( MC._cc[ipt] - Op<T>::u(MC._I) );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[ipt][i] = MC._ccsub[ipt][i] * r;
		   }
         }
		 //concave relaxation
		 {
           MC2._cc[ipt] = mc::xexpax( MC._cv[ipt] ,a);
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[ipt][i] = (std::exp(a*MC._cv[ipt])*(1.0+a*MC._cv[ipt])) * MC._cvsub[ipt][i];
		   }
		 }
		}// end of for ipt
	  }
	  //the root of the second derivative (turning point) is within the interval bounds and the function is convex-concave
	  else{
		  //convex relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xL))/(x-xL) = 0 is in interval
		  double p1 = (Op<T>::l(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p1) + a*p1*std::exp(a*p1) - (p1*std::exp(a*p1) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(p1 - Op<T>::l(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::u(MC._I)) + a*Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - (Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I)))){
	         //there is a root
		     // currentCase = 2;
			  int imid = -1;
			  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				  const double* cvenv = vMcCormick<T>::_xexpaxcv( mid( MC._cv[ipt], MC._cc[ipt], -1.0/a, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
				  MC2._cv[ipt] = cvenv[0];
				  for( unsigned int i=0; i<MC2._nsub; i++ ){
					MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
				  }
			  }
		  }
		  //the root is not in the interval; in this case, xexpax is always decreasing within the interval
		  else{
		    // currentCase = 3;
			double r;
			if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
				r = 0.;
			} else {
				r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
			}
			for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				 MC2._cv[ipt] =  mc::xexpax(Op<T>::u(MC._I),a)+r*( MC._cc[ipt] - Op<T>::u(MC._I) );
				 for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._cvsub[ipt][i] = r*MC._ccsub[ipt][i];
				}
			 }
		  }
		  //concave relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xU))/(x-xU) = 0 is in interval
		  double p2 = (Op<T>::u(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p2) + a*p2*std::exp(a*p2) - (p2*std::exp(a*p2) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(p2 - Op<T>::u(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::l(MC._I)) + a*Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - (Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(Op<T>::l(MC._I) - Op<T>::u(MC._I)))){
		     // currentCase = currentCase*2;
			 double zmax = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
			 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				//there is a root
				int imid = -1;
				const double* ccenv = vMcCormick<T>::_xexpaxcc( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
				MC2._cc[ipt] = ccenv[0];
				for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
				}
			 }
          }
		  //the root is not in the interval; in this case, xexpax need not be monotonic
		  else{
		      // currentCase = currentCase*2+1;
			  double r;
			  double pt;
			  if (isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))) {
				  r = 0.;
				  pt = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
			  }
			  else {
				  r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
				  pt = Op<T>::l(MC._I);
			  }
			  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				 MC2._cc[ipt] =  mc::xexpax(pt,a)+r*( (r>=0?MC._cc[ipt]:MC._cv[ipt]) - pt );
				 for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._ccsub[ipt][i] = r*(r>=0?MC._ccsub[ipt][i]:MC._cvsub[ipt][i]);
				}
			 }
		  }
	  }
  }
  else{ // -1/a is the maximum of the function and -2/a is the turning point
	  //the function is concave (not necessarily monotonic)
	  if(Op<T>::u(MC._I) <= -2.0/a){
	     // currentCase = 0;
	     zmin = ( mc::xexpax(Op<T>::u(MC._I),a)>=mc::xexpax(Op<T>::l(MC._I),a)? Op<T>::l(MC._I): Op<T>::u(MC._I) );
		 pt = zmin;
         if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
		  r =0.;
		 }
		 else {
		  r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		 }
	     int imid = -1;
	     zmax = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), -1.0/a, imid );
	  	 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			 //convex relaxation
			 {
			   imid = -1;
			   MC2._cv[ipt] = mc::xexpax(pt,a) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ) - pt );
			   for( unsigned int i=0; i<MC2._nsub; i++ ){
				 MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
			   }
			 }
			 //concave relaxation
			 {
			   imid = -1;
			   double vmid = mid( MC._cv[ipt], MC._cc[ipt], zmax, imid );
			   MC2._cc[ipt] = mc::xexpax( vmid,a );
			   for( unsigned int i=0; i<MC2._nsub; i++ ){
				 MC2._ccsub[ipt][i] = (std::exp(a*vmid)*(1.0+a*vmid)) * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
			   }
			 }
		 }
	  }
	  //the function is convex and monotonically decreasing
	  else if(Op<T>::l(MC._I) >= -2.0/a){
	     // currentCase = 1;
		 double r = 0.;
		 if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
			 r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			 //concave relaxation
			 {
			   MC2._cc[ipt] = mc::xexpax(Op<T>::l(MC._I),a) + r * ( MC._cv[ipt] - Op<T>::l(MC._I) );
			   for( unsigned int i=0; i<MC2._nsub; i++ ){
				 MC2._ccsub[ipt][i] = MC._cvsub[ipt][i] * r;
			   }
			 }
			 //convex relaxation
			 {
			   MC2._cv[ipt] = mc::xexpax( MC._cc[ipt] ,a);
			   for( unsigned int i=0; i<MC2._nsub; i++ ){
				 MC2._cvsub[ipt][i] = (std::exp(a*MC._cc[ipt])*(1.0+a*MC._cc[ipt])) * MC._ccsub[ipt][i];
			   }
			 }
		 }
	  }
	  //the root of the second derivative is within the interval bounds and the function is convex-concave
	  else{
	      // currentCase = 3;
		  //convex relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xL))/(x-xL) = 0 is in interval
		  double p1 = (Op<T>::l(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p1) + a*p1*std::exp(a*p1) - (p1*std::exp(a*p1) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(p1 - Op<T>::l(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::u(MC._I)) + a*Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - (Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I)))){
			  //there is a root
		      // currentCase = 2;
			  double zmin = mc::xexpax(Op<T>::u(MC._I),a)<mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
		      for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				  int imid = -1;
				  const double* cvenv = vMcCormick<T>::_xexpaxcv( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
				  MC2._cv[ipt] = cvenv[0];
				  for( unsigned int i=0; i<MC2._nsub; i++ ){
					MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
				  }
			  }
		  }
		  //the root is not in the interval
		  else{
		     // currentCase = 3;
			 double r;
			 double pt;
			 if (isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))) {
				 pt = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
				 r = 0.;
			 }
			 else {
				 r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
				 pt = Op<T>::l(MC._I);
			 }
			 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				 MC2._cv[ipt] =  mc::xexpax(pt,a)+r*( (r>=0?MC._cv[ipt]:MC._cc[ipt]) - pt );
				 for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._cvsub[ipt][i] = r*(r>=0?MC._cvsub[ipt][i]:MC._ccsub[ipt][i]);
				}
			 }
		  }
		  //concave relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xU))/(x-xU) = 0 is in interval
		  double p2 = (Op<T>::u(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p2) + a*p2*std::exp(a*p2) - (p2*std::exp(a*p2) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(p2 - Op<T>::u(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::l(MC._I)) + a*Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - (Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(Op<T>::l(MC._I) - Op<T>::u(MC._I)))){
		    // currentCase = currentCase*2;
			for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				//there is a root
			    int imid = -1;
				const double* ccenv = vMcCormick<T>::_xexpaxcc( mid( MC._cv[ipt], MC._cc[ipt], -1/a, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
				MC2._cc[ipt] = ccenv[0];
				for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
				}
			}
          }
		  //the root is not in the interval
		  else{
		     // currentCase = currentCase*2+1;
			 double r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
			 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
				 MC2._cc[ipt] =  mc::xexpax(Op<T>::l(MC._I),a)+r*( (r>=0?MC._cc[ipt]:MC._cv[ipt]) - Op<T>::l(MC._I) );
				 for( unsigned int i=0; i<MC2._nsub; i++ ){
				  MC2._ccsub[ipt][i] = r*(r>=0?MC._ccsub[ipt][i]:MC._cvsub[ipt][i]);
				}
			 }
		  }
	  }
  }

#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "xexpax";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
	  // vMcCormick<T>::additionalLins.set_additional_values(&a,&currentCase);
      // vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.XEXPAX);
	  // vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
                                                                          // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
      // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }

  return MC2.cut();
}



template <typename T> inline vMcCormick<T>
centerline_deficit
( const vMcCormick<T>&MC, const double xLim, const double type )
{
      throw std::runtime_error("   mc::vMcCormick:\t centerline_deficit not implemented yet.");
      return MC;
}



template <typename T> inline vMcCormick<T>
wake_profile
( const vMcCormick<T>&MC, const double type )
{
      throw std::runtime_error("   mc::vMcCormick:\t wake_profile not implemented yet.");
      return MC;
}



template <typename T> inline vMcCormick<T>
wake_deficit
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2, const double a, const double alpha, const double rr, const double type1, const double type2 )
{
      throw std::runtime_error("   mc::vMcCormick:\t wake_deficit not implemented yet.");
      return MC1;
}



template <typename T> inline vMcCormick<T>
power_curve
( const vMcCormick<T>&MC, const double type )
{
      throw std::runtime_error("   mc::vMcCormick:\t power_curve not implemented yet.");
      return MC;
}


//added AVT.SVT 06.06.2017
template <typename T> inline vMcCormick<T>
lmtd
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{

  vMcCormick<T> MC3;
  if ( Op<T>::l(MC1._I) <= 0. || Op<T>::l(MC2._I) <= 0.)
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::LMTD );
  if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
	MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::lmtd( MC1._I, MC2._I );
  bool isthinMC1 = isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I));
  bool isthinMC2 = isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I));
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
	  //concave part
	  {
	    double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	    double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
		MC3._cc[ipt] = mc::lmtd( MC1_cc, MC2_cc );
		if(isequal(MC1_cc, MC2_cc)){
		   for( unsigned int i=0; i<MC3._nsub; i++ ){
				MC3._ccsub[ipt][i] = 0.5*(MC1._const? 0.:MC1._ccsub[ipt][i]) + 0.5*(MC2._const? 0.:MC2._ccsub[ipt][i]);
		   }
		}else{
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._ccsub[ipt][i] = (1./(std::log(MC1_cc)-std::log(MC2_cc))
									-(MC1_cc - MC2_cc)/(MC1_cc* mc::sqr(std::log(MC1_cc)-std::log(MC2_cc))))*(MC1._const? 0.:MC1._ccsub[ipt][i])
									+(-1./(std::log(MC1_cc)-std::log(MC2_cc))
									+(MC1_cc - MC2_cc)/(MC2_cc* mc::sqr(std::log(MC1_cc)-std::log(MC2_cc))))*(MC2._const? 0.:MC2._ccsub[ipt][i]);
			}
		}
	  }
	  //convex part
	  {
	    double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	    double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
		double l1 = mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I));
		double l2 = mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I));
		double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
		if( !isthinMC1){
			r11 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC1._I);
			l1 += (MC1_cv -Op<T>::l(MC1._I))*r11;
			r12 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) )/Op<T>::diam(MC1._I);
			l2 += (MC1_cv -Op<T>::u(MC1._I))*r12;
		}
		if(!isthinMC2){
			 r21 = ( mc::lmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
			 l1 += (MC2_cv - Op<T>::l(MC2._I))*r21;
			 r22 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
			 l2 += (MC2_cv - Op<T>::u(MC2._I))*r22;
		}

		double lambda1,lambda2;
		if( isthinMC1 || isthinMC2 ) {
			MC3._cv[ipt] = l1;
			lambda1 = 1.;
			lambda2 = 0.;
		} else {
			MC3._cv[ipt] = std::max(l1,l2);
			if(isequal(l1,l2 )){
			  lambda1 = 0.5;
			  lambda2 = 0.5;
			}
			else if( l1>l2 ){
			  lambda1 = 1.;
			  lambda2 = 0.;
			}
			else{
			  lambda1 = 0.;
			  lambda2 = 1.;
			}
		}
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  MC3._cvsub[ipt][i] = lambda1*(r11*(MC1._const? 0.:MC1._cvsub[ipt][i])+r21*(MC2._const? 0.:MC2._cvsub[ipt][i]))
							  +lambda2*(r12*(MC1._const? 0.:MC1._cvsub[ipt][i])+r22*(MC2._const? 0.:MC2._cvsub[ipt][i]));
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
		}
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "lmtd";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

//added AVT.SVT 08.06.2017
template <typename T> inline vMcCormick<T>
rlmtd
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  vMcCormick<T> MC3;
  if ( Op<T>::l(MC1._I) <= 0. || Op<T>::l(MC2._I) <= 0.)
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::RLMTD );
  if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
	MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::rlmtd( MC1._I, MC2._I );

  bool isthinMC1 = isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) );
  bool isthinMC2 = isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) );
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){

	  double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	  double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
	  //convex part
	  { MC3._cv[ipt] = mc::rlmtd( MC1_cc, MC2_cc);
		if(isequal(MC1_cc, MC2_cc )){
		   for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._cvsub[ipt][i] = -1./(2.*std::pow(MC1_cc,2))*(MC1._const? 0.:MC1._ccsub[ipt][i])
								   -1./(2.*std::pow(MC2_cc,2))*(MC2._const? 0.:MC2._ccsub[ipt][i]);
		   }
		}else{
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._cvsub[ipt][i] = (1./(MC1_cc*(MC1_cc-MC2_cc))
								  -(std::log(MC1_cc)-std::log(MC2_cc))/(mc::sqr(MC1_cc-MC2_cc)))*(MC1._const? 0.:MC1._ccsub[ipt][i])
								  +(-1./(MC2_cc*(MC1_cc-MC2_cc))
								  +(std::log(MC1_cc)-std::log(MC2_cc))/(mc::sqr(MC1_cc-MC2_cc)))*(MC2._const? 0.:MC2._ccsub[ipt][i]);
			}
		 }
	  }
	  //concave part
	  {
	    double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	    double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
		double l1 = mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I));
		double l2 = mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I));
		double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
		if( !isthinMC1){
			r11 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC1._I);
			l1 += (MC1_cv-Op<T>::u(MC1._I))*r11;
			r12 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) )/Op<T>::diam(MC1._I);
			l2 += (MC1_cv-Op<T>::l(MC1._I))*r12;
		}
		if(!isthinMC2){
			 r21 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
			 l1 += (MC2_cv-Op<T>::l(MC2._I))*r21;
			 r22 = ( mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
			 l2 += (MC2_cv-Op<T>::u(MC2._I))*r22;
		}

		double lambda1,lambda2;
		if(isthinMC1){
			if(isthinMC2){
				MC3._cc[ipt] = mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I));
				lambda1 = 0.;
				lambda2 = 0.;
			}
			else{
				MC3._cc[ipt] = 	l2;
				lambda1 = 0.;
				lambda2 = 1.;
			}
		}
		else if(isthinMC2){
			MC3._cc[ipt] = l1;
			lambda1 = 1.;
			lambda2 = 0.;
		}
		else {
			MC3._cc[ipt] = std::min(l1,l2);
			if(isequal(l1,l2 )){
			  lambda1 = 0.5;
			  lambda2 = 0.5;
			}
			else if( l1<l2 ){
			  lambda1 = 1.;
			  lambda2 = 0.;
			}
			else{
			  lambda1 = 0.;
			  lambda2 = 1.;
			}
		}
	    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  MC3._ccsub[ipt][i] = lambda1*(r11*(MC1._const? 0.:MC1._cvsub[ipt][i])+r21*(MC2._const? 0.:MC2._cvsub[ipt][i]))
								+lambda2*(r12*(MC1._const? 0.:MC1._cvsub[ipt][i])+r22*(MC2._const? 0.:MC2._cvsub[ipt][i]));
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
		}
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG

  	std::string str = "rlmtd";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);

#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

template <typename T> inline vMcCormick<T>
euclidean_norm_2d
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  vMcCormick<T> MC3;
  if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
	MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::euclidean_norm_2d( MC1._I, MC2._I );
  // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();

  // The function is convex
  int imidxCv = -1;
  int imidyCv = -1;
  // For concave we need to find out which hyperplane to use
  double xL = Op<T>::l(MC1._I);
  double xU = Op<T>::u(MC1._I);
  double yL = Op<T>::l(MC2._I);
  double yU = Op<T>::u(MC2._I);
  std::vector<double> slopes = {isequal(xL,xU) ? 0. : (mc::euclidean_norm_2d(xU,yL)-mc::euclidean_norm_2d(xL,yL))/(xU-xL),
								isequal(yL,yU) ? 0. : (mc::euclidean_norm_2d(xL,yU)-mc::euclidean_norm_2d(xL,yL))/(yU-yL),
								isequal(xL,xU) ? 0. : (mc::euclidean_norm_2d(xU,yU)-mc::euclidean_norm_2d(xL,yU))/(xU-xL),
								isequal(yL,yU) ? 0. : (mc::euclidean_norm_2d(xU,yU)-mc::euclidean_norm_2d(xU,yL))/(yU-yL)};
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
	  double xmin = mc::mid(MC1._cv[ipt],MC1._cc[ipt],0.,imidxCv);
	  double ymin = mc::mid(MC2._cv[ipt],MC2._cc[ipt],0.,imidyCv);
	  MC3._cv[ipt] = mc::euclidean_norm_2d(xmin,ymin);

	  bool useHyperplanes1 = (mc::euclidean_norm_2d(xL,yL)+mc::euclidean_norm_2d(xU,yU))/2. > (mc::euclidean_norm_2d(xL,yU)+mc::euclidean_norm_2d(xU,yL))/2.0 ? true : false;

      std::vector<double> hyperplanes;
      if(useHyperplanes1) {
		  hyperplanes = {mc::euclidean_norm_2d(xL,yL) + slopes[0]*( (slopes[0]>=0?MC1._cc[ipt]:MC1._cv[ipt]) -xL) + slopes[3]*((slopes[3]>=0?MC2._cc[ipt]:MC2._cv[ipt])-yL),
						 mc::euclidean_norm_2d(xU,yU) + slopes[2]*( (slopes[2]>=0?MC1._cc[ipt]:MC1._cv[ipt]) -xU) + slopes[1]*((slopes[1]>=0?MC2._cc[ipt]:MC2._cv[ipt])-yU)};
	  }
	  else{
		  hyperplanes = {mc::euclidean_norm_2d(xL,yU) + slopes[0]*( (slopes[0]>=0?MC1._cc[ipt]:MC1._cv[ipt]) -xL) + slopes[1]*((slopes[1]>=0?MC2._cc[ipt]:MC2._cv[ipt])-yU),
						 mc::euclidean_norm_2d(xU,yL) + slopes[2]*( (slopes[2]>=0?MC1._cc[ipt]:MC1._cv[ipt]) -xU) + slopes[3]*((slopes[3]>=0?MC2._cc[ipt]:MC2._cv[ipt])-yL)};
	  }

	  unsigned index = mc::argmin(2,hyperplanes.data());
	  double xDerCc, yDerCc;
	  unsigned imidxCc, imidyCc;
	  MC3._cc[ipt] = hyperplanes[index];

	  if(useHyperplanes1){
		  switch(index){
			case 0:
			  xDerCc = slopes[0];
			  yDerCc = slopes[3];
			  imidxCc = (slopes[0]>=0?2:1); // 2 is concave, 1 is convex --> see mcfunc.hpp mid function
			  imidyCc = (slopes[3]>=0?2:1);
			  break;
			case 1:
			  xDerCc = slopes[2];
			  yDerCc = slopes[1];
			  imidxCc = (slopes[2]>=0?2:1);
			  imidyCc = (slopes[1]>=0?2:1);
			  break;
			default:
			  xDerCc = 0; yDerCc = 0; imidxCc = 0; imidyCc = 0;
			  break;
		  }
	  }
	  else{
		  switch(index){
			case 0:
			  xDerCc = slopes[0];
			  yDerCc = slopes[1];
			  imidxCc = (slopes[0]>=0?2:1);
			  imidyCc = (slopes[1]>=0?2:1);
			  break;
			case 1:
			  xDerCc = slopes[2];
			  yDerCc = slopes[3];
			  imidxCc = (slopes[2]>=0?2:1);
			  imidyCc = (slopes[3]>=0?2:1);
			  break;
			default:
			  xDerCc = 0; yDerCc = 0; imidxCc = 0; imidyCc = 0;
			  break;
		  }
	  }

	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
	  // Subgradients
	  for(unsigned int i=0; i<MC3._nsub;i++){
		  MC3._cvsub[ipt][i] = (MC1._const ? 0. : mid(MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidxCv)) * mc::der_euclidean_norm_2d(xmin,ymin)
						      +(MC2._const ? 0. : mid(MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidyCv)) * mc::der_euclidean_norm_2d(ymin,xmin);
		  MC3._ccsub[ipt][i] = (MC1._const ? 0. : mid(MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidxCc)) * xDerCc
						      +(MC2._const ? 0. : mid(MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidyCc)) * yDerCc;
		  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  } // end of for npts
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "euclidean_norm_2d";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

//added AVT.SVT 01.03.2018
template <typename T> inline vMcCormick<T>
expx_times_y
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{

  if ( Op<T>::l(MC2._I) > 0. && !isequal(Op<T>::diam(MC2._I),0. )){

	vMcCormick<T> MC3;
	if( MC2._const )
	  MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
	else if( MC1._const )
	  MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
	else if( MC1._nsub != MC2._nsub )
	  throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
	else if( MC1._npts != MC2._npts )
	  throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
	else
	  MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

	MC3._I = Op<T>::expx_times_y( MC1._I, MC2._I );
    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();

	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	    double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
		double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	    double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
		//convex envelope
		//the convex envelope is monotonically increasing in every positive direction, i.e., direction point away from the corner (MC1._l,MC2._l). Thus we know that we have to use MC1._cv and MC2._cv
		{
			double alpha = std::log(Op<T>::u(MC2._I)/Op<T>::l(MC2._I));
			double lambda1 = (Op<T>::u(MC2._I)-MC2_cv)/(Op<T>::diam(MC2._I));
			double lambda2 = (MC2_cv-Op<T>::l(MC2._I))/(Op<T>::diam(MC2._I));

			if((Op<T>::l(MC1._I)+lambda1*alpha <= MC1_cv) && (MC1_cv <= Op<T>::u(MC1._I)-lambda2*alpha)){
				//convex relaxation
				MC3._cv[ipt] = std::exp(MC1_cv)*std::pow(Op<T>::l(MC2._I),lambda1)*std::pow(Op<T>::u(MC2._I),lambda2);
				//subgradient
				for( unsigned int i=0; i<MC3._nsub; i++ ){
				  MC3._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*( std::pow(Op<T>::l(MC2._I),lambda1)*std::exp(MC1_cv)/std::pow(Op<T>::u(MC2._I),-lambda2) )
										+(MC2._const? 0.:MC2._cvsub[ipt][i])*( std::pow(Op<T>::l(MC2._I),lambda1)*std::exp(MC1_cv)*(std::log(Op<T>::u(MC2._I))-std::log(Op<T>::l(MC2._I)))
										/(std::pow(Op<T>::u(MC2._I),-lambda2 )*Op<T>::diam(MC2._I) ) ) ;
				  }
			}
			else if(MC1_cv < lambda1*std::min(Op<T>::l(MC1._I)+alpha,Op<T>::u(MC1._I)) + lambda2*Op<T>::l(MC1._I) ){ //it always holds MC1._l <= MC1._cv
				//convex relaxation
				MC3._cv[ipt] = lambda1*std::exp( (MC1_cv - lambda2*Op<T>::l(MC1._I))/lambda1 )*Op<T>::l(MC2._I) + lambda2*std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I);
				//subgradient
				for( unsigned int i=0; i<MC3._nsub; i++ ){
				  double s1 = std::exp( (MC1_cv - Op<T>::l(MC1._I)*lambda2 )*1./lambda1  );
				  MC3._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*( Op<T>::l(MC2._I)*s1 )
									  +(MC2._const? 0.:MC2._cvsub[ipt][i])*( Op<T>::l(MC2._I)*s1/(-Op<T>::diam(MC2._I)) + Op<T>::u(MC2._I)*std::exp(Op<T>::l(MC1._I))/(Op<T>::diam(MC2._I))
									  + Op<T>::l(MC2._I)*s1*(MC1_cv-Op<T>::l(MC1._I))/(Op<T>::u(MC2._I)-MC2_cv)  ) ;
				}
			}
			else{
				//convex relaxation
				MC3._cv[ipt] = lambda1*std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) + lambda2*std::exp( (MC1_cv - lambda1*Op<T>::u(MC1._I))/lambda2 )*Op<T>::u(MC2._I);
				//subgradient
				for( unsigned int i=0; i<MC3._nsub; i++ ){
					double s1 = std::exp( (MC1_cv - Op<T>::u(MC1._I)*lambda1 )*1./lambda2  );
					MC3._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*( Op<T>::u(MC2._I)*s1 )
										 +(MC2._const? 0.:MC2._cvsub[ipt][i])*( Op<T>::u(MC2._I)*s1/(Op<T>::diam(MC2._I)) - Op<T>::l(MC2._I)*std::exp(Op<T>::u(MC1._I))/(Op<T>::diam(MC2._I))
										 +Op<T>::u(MC2._I)*s1*(Op<T>::u(MC1._I)-MC1_cv)/(MC2_cv-Op<T>::l(MC2._I))  ) ;
				}
			}
		}//end of convex
		//concave envelope
		{
			int imidcc1 = -1, imidcc2 = -1; // we can't know which relaxation to use
			double fmidcc1 = mid_ndiff(MC1_cv, MC1_cc, Op<T>::u(MC1._I), imidcc1);
			double fmidcc2 = mid_ndiff(MC2_cv, MC2_cc, Op<T>::u(MC2._I), imidcc2);
			double l1 = std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I);
			double l2 = std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I);
			double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
			if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				r11 = (std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC1._I));
				l1 += (fmidcc1-Op<T>::u(MC1._I))*r11;
				r12 = (std::exp(Op<T>::u(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I) )/(Op<T>::diam(MC1._I));
				l2 += (fmidcc1-Op<T>::l(MC1._I))*r12;
			}
			if(!isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
				r21 = (std::exp(Op<T>::u(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC2._I));
				l1 += (fmidcc2-Op<T>::l(MC2._I))*r21;
				r22 = (std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC2._I));
				l2 += (fmidcc2-Op<T>::u(MC2._I))*r22;
			}
			double lambda1,lambda2;
			if(isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
					MC3._cc[ipt] = mc::expx_times_y(Op<T>::u(MC1._I),Op<T>::u(MC2._I));
					lambda1 = 0.;
					lambda2 = 0.;
				}
				else{
					MC3._cc[ipt] = l1;
					lambda1 = 1.;
					lambda2 = 0.;
				}
			}
			else if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
				MC3._cc[ipt] = l2;
				lambda1 = 1.;
				lambda2 = 0.;
			}
			else {
				MC3._cc[ipt] = std::min(l1,l2);
				if(isequal(l1,l2)){
				  lambda1 = 0.5;
				  lambda2 = 0.5;
				}
				else if( l1<l2 ){
				  lambda1 = 1.;
				  lambda2 = 0.;
				}
				else{
				  lambda1 = 0.;
				  lambda2 = 1.;
				}
			}
	        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._ccsub[ipt][i] = lambda1*(r11*(MC1._const? 0.:MC1._ccsub[ipt][i])+r21*(MC2._const? 0.:MC2._ccsub[ipt][i]))
								  +lambda2*(r12*(MC1._const? 0.:MC1._ccsub[ipt][i])+r22*(MC2._const? 0.:MC2._ccsub[ipt][i]));
		      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
			}
		}//end of concave
        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
    }//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "expx_times_y";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
    if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
    return MC3.cut();
  }//end of if( Op<T>::l(MC2._I) > 0. && !isequal(Op<T>::diam(MC2._I),0.))

  return exp(MC1)*MC2;
}

//added AVT.SVT 22.08.2017
template <typename T> inline vMcCormick<T>
vapor_pressure
(const vMcCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4 = 0,
 const double p5 = 0, const double p6 = 0, const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::VAPOR_PRESSURE );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    MC2._I = Op<T>::vapor_pressure( MC1._I, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);

	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] =  mc::vapor_pressure(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
		double d = mc::der_vapor_pressure(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*d;
		}

		if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
			double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
			MC2._cc[ipt] =  Op<T>::l(MC2._I)+r*(MC1._cc[ipt]-Op<T>::l(MC1._I));
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
			}
		} else {
			MC2._cc[ipt] = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._ccsub[ipt][i] = 0.;
			}
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "vapor_pressure";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// std::vector<double> params = {type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
		// vMcCormick<T>::additionalLins.set_additional_values(params.data(), 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.VAPOR_PRESSURE);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																		    // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }

    return MC2.cut();

}

//added AVT.SVT 01.09.2017
template <typename T> inline vMcCormick<T>
ideal_gas_enthalpy
(const vMcCormick<T>&MC1, const double x0, const double type, const double p1, const double p2, const double p3, const double p4,
 const double p5, const double p6 = 0, const double p7 = 0 )
{
    if (( Op<T>::l(MC1._I) <= 0. ) || ( x0 <= 0 ))
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::IDEAL_GAS_ENTHALPY );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    MC2._I = Op<T>::ideal_gas_enthalpy( MC1._I, x0, type, p1, p2, p3, p4, p5, p6, p7);

	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] =  mc::ideal_gas_enthalpy(MC1._cv[ipt], x0, type, p1, p2, p3, p4, p5, p6, p7);
		double d = mc::der_ideal_gas_enthalpy(MC1._cv[ipt], x0, type, p1, p2, p3, p4, p5, p6, p7);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*d;
		}

		if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
			double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
			MC2._cc[ipt] =  Op<T>::l(MC2._I)+r*(MC1._cc[ipt]-Op<T>::l(MC1._I));
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
			}
		} else {
			MC2._cc[ipt] = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._ccsub[ipt][i] = 0.;
			}
		}
	}//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "ideal_gas_enthalpy";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// std::vector<double> params = {x0, type, p1, p2, p3, p4, p5, p6, p7};
		// vMcCormick<T>::additionalLins.set_additional_values(params.data(), 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.IDEAL_GAS_ENTHALPY);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																		    // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }

    return MC2.cut();

}

//added AVT.SVT 01.09.2017
template <typename T> inline vMcCormick<T>
saturation_temperature
(const vMcCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4 = 0,
 const double p5 = 0, const double p6 = 0, const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SATURATION_TEMPERATURE );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );
    MC2._I = Op<T>::saturation_temperature( MC1._I, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);

    for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
			double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
			MC2._cv[ipt] =  Op<T>::l(MC2._I)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[ipt][i] =  (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
			}
		} else {
			MC2._cv[ipt] = Op<T>::l(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[ipt][i] = 0.;
			}
		}

		MC2._cc[ipt] =  mc::saturation_temperature(MC1._cc[ipt], type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
		double d = mc::der_saturation_temperature(MC1._cc[ipt], type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*d;
		}
	}//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "saturation_temperature";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// std::vector<double> params = {type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
		// vMcCormick<T>::additionalLins.set_additional_values(params.data(), 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.SATURATION_TEMPERATURE);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																		    // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }

    return MC2.cut();

}

//added AVT.SVT 22.08.2017
template <typename T> inline vMcCormick<T>
enthalpy_of_vaporization
(const vMcCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4,
 const double p5, const double p6 = 0 )
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::ENTHALPY_OF_VAPORIZATION );

	vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );

	if ( Op<T>::l(MC1._I) >= p1 ) {	// completely in supercritical region, everything is zero

		MC2._I = T(0.0);
		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			MC2._cv[ipt] = 0;
			MC2._cc[ipt] = 0;

			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[ipt][i] = 0.;
			  MC2._ccsub[ipt][i] = 0.;
			}
		}
	}
	else if ( Op<T>::u(MC1._I) <= p1 ) {	// completely in subcritical region, use regular correlation

		MC2._I = Op<T>::enthalpy_of_vaporization( MC1._I, type, p1, p2, p3, p4, p5, p6);

		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			MC2._cc[ipt] =  mc::enthalpy_of_vaporization(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6);
			double d = mc::der_enthalpy_of_vaporization(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6);
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*d;
			}

			if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
				double r = -Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
				MC2._cv[ipt] =  Op<T>::u(MC2._I)+r*(MC1._cc[ipt]-Op<T>::l(MC1._I));
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
				}
			} else {
				MC2._cv[ipt] = Op<T>::l(MC2._I);
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._cvsub[ipt][i] = 0.;
				}
			}
		}
	}
	else {  // temperature interval contains critical point

		MC2._I = Op<T>::enthalpy_of_vaporization( MC1._I, type, p1, p2, p3, p4, p5, p6);

		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			// Convex relaxation
			if ( MC1._cc[ipt] >= p1 ) {
				MC2._cv[ipt] = 0.;
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._cvsub[ipt][i] = 0.;
				}
			} else {
				if (!isequal( Op<T>::l(MC1._I), p1 )) {
					double r = (0.-Op<T>::u(MC2._I))/(p1-Op<T>::l(MC1._I));
					MC2._cv[ipt] =  Op<T>::u(MC2._I)+r*(MC1._cc[ipt]-Op<T>::l(MC1._I));
					for( unsigned int i=0; i<MC2._nsub; i++ )
					{
					  MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
					}
				} else {
					MC2._cv[ipt] = Op<T>::l(MC2._I);
					for( unsigned int i=0; i<MC2._nsub; i++ )
					{
					  MC2._cvsub[ipt][i] = 0.;
					}
				}
			}

			// Concave relaxation
			if( !vMcCormick<T>::options.ENVEL_USE ){

				// simply use interval bounds
				MC2._cc[ipt] = Op<T>::u(MC2._I);
				for( unsigned int i=0; i<MC2._nsub; i++ ){
					MC2._ccsub[ipt][i] = 0.;
				}

			}
			else {

				double xj;
				double xu = Op<T>::u(MC1._I);
				std::vector<double> parameters = {p1,p2,p3,p4,p5,p6};
				int dummy = (int)type;
				int * ptr = &dummy;
				try{
					xj = vMcCormick<T>::_secant( Op<T>::l(MC1._I), (Op<T>::l(MC1._I)+p1)/2, Op<T>::l(MC1._I), p5 , vMcCormick<T>::_dhvapenv_func, &xu, ptr, parameters );
				}
				catch( typename vMcCormick<T>::Exceptions ){
					xj = vMcCormick<T>::_goldsect( Op<T>::l(MC1._I), p1 , vMcCormick<T>::_dhvapenv_func, &xu, ptr, parameters );
				}
				if (MC1._cv[ipt] <= xj) {
					MC2._cc[ipt] =  mc::enthalpy_of_vaporization(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6);
					double d = mc::der_enthalpy_of_vaporization(MC1._cv[ipt], type, p1, p2, p3, p4, p5, p6);
					for( unsigned int i=0; i<MC2._nsub; i++ )
					{
					  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*d;
					}
				} else {
					if (!isequal(xu,xj)) {
						double fxj = mc::enthalpy_of_vaporization(xj, type, p1, p2, p3, p4, p5, p6);
						double r = (0.-fxj)/(xu-xj);
						MC2._cc[ipt] =  fxj + r*(MC1._cv[ipt]-xj);
						for( unsigned int i=0; i<MC2._nsub; i++ )
						{
						  MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
						}
					} else {
						// simply use interval bounds
						MC2._cc[ipt] = Op<T>::u(MC2._I);
						for( unsigned int i=0; i<MC2._nsub; i++ ){
							MC2._ccsub[ipt][i] = 0.;
						}
					}
				}

			}
		}//end for-loop over _npts
	}

#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "enthalpy_of_vaporization";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// std::vector<double> params = {type, p1, p2, p3, p4, p5, p6};
		// vMcCormick<T>::additionalLins.set_additional_values(params.data(), 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.ENTHALPY_OF_VAPORIZATION);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																		    // vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
}

//added AVT.SVT 06.11.2017
template <typename T> inline vMcCormick<T>
cost_function
(const vMcCormick<T>&MC1, const double type, const double p1, const double p2, const double p3 )
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::COST_FUNCTION );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );

	switch((int)type){ // for Guthrie, we have specific calculations
		case 1:{ // Guthrie
			// Note that we can't precompute the values since most of them depend on interval bounds
			unsigned int monotonicity = vMcCormick<T>::MON_NONE;
			unsigned int convexity = vMcCormick<T>::CONV_NONE;
			// Sets monotonicity and convexity
			if(vMcCormick<T>::options.ENVEL_USE){
				vMcCormick<T>::_cost_Guthrie_mon_conv(monotonicity,convexity,p1,p2,p3,Op<T>::l(MC1._I),Op<T>::u(MC1._I));
			}else{
				return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
			}

			switch(monotonicity){
			  case vMcCormick<T>::MON_NONE:	// Not monotonic at all in the given interval but we can still compute exact range bounds
			  {
			    double min_point = 0.;
				double max_point = 0.;
				double extreme_point = 0.;

				if(p3>0.){
					extreme_point = std::exp(-p2*std::log(10.)/(2*p3));
					MC2._I = T(mc::cost_function(extreme_point,type,p1,p2,p3),std::max(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)));
					min_point = extreme_point;
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)>mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						max_point = Op<T>::l(MC1._I);
					}else{
						max_point = Op<T>::u(MC1._I);
					}
				}
				else if (p3<0.){
					extreme_point = std::exp(-p2*std::log(10.)/(2*p3));
					MC2._I = T(std::min(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)),mc::cost_function(extreme_point,type,p1,p2,p3));
					max_point = extreme_point;
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)<mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						min_point = Op<T>::l(MC1._I);
					}else{
						min_point = Op<T>::u(MC1._I);
					}
				}
				else { // case p3 = 0
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)>mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						max_point = Op<T>::l(MC1._I);
						min_point = Op<T>::u(MC1._I);
					}else{
						max_point = Op<T>::u(MC1._I);
						min_point = Op<T>::l(MC1._I);
					}

					MC2._I = T(mc::cost_function(min_point,type,p1,p2,p3),mc::cost_function(max_point,type,p1,p2,p3));
				}
				switch(convexity){
					case vMcCormick<T>::CONV_NONE: //not convex not concave
					    if(p3>0.){
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         mc::cost_function(extreme_point,type,p1,p2,p3),std::max(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)) );
						}
						else if (p3 < 0.){
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         std::min(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)),mc::cost_function(extreme_point,type,p1,p2,p3) );
						}
						else{
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         mc::cost_function(min_point,type,p1,p2,p3),mc::cost_function(max_point,type,p1,p2,p3) );
						}
						break;
					case vMcCormick<T>::CONVEX: //convex
						{
							for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
								//convex
								{int imid = -1;
									MC2._cv[ipt] = mc::cost_function( mid( MC1._cv[ipt], MC1._cc[ipt], min_point, imid ),type,p1,p2,p3);
									for( unsigned int i=0; i<MC2._nsub; i++ ){
									  MC2._cvsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * mc::der_cost_function(mid( MC1._cv[ipt], MC1._cc[ipt], min_point, imid ),type,p1,p2,p3);
									}
								}
								//concave
								{int imid = -1;
									double r = 0.;
									if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
										r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
									}
									MC2._cc[ipt] =  mc::cost_function(max_point, type,p1,p2,p3)+r*( mid( MC1._cv[ipt], MC1._cc[ipt], max_point, imid )-max_point);
									for( unsigned int i=0; i<MC2._nsub; i++ ){
										MC2._ccsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
									}
								}
							}
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "cost_function not monotonic convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3, min_point};
								// int convexity = 0; // convex not monotonic
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

							return MC2.cut();
							break;
						}
					case vMcCormick<T>::CONCAVE: //concave
						{
							for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
								//concave
								{int imid = -1;
									MC2._cc[ipt] = mc::cost_function( mid( MC1._cv[ipt], MC1._cc[ipt], max_point, imid ),type,p1,p2,p3);
									for( unsigned int i=0; i<MC2._nsub; i++ ){
									  MC2._ccsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * mc::der_cost_function(mid( MC1._cv[ipt], MC1._cc[ipt], max_point, imid ),type,p1,p2,p3);
									}
								}
								//convex
								{int imid = -1;
									double r = 0.;
									if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
										r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
									}
									MC2._cv[ipt] =  mc::cost_function(min_point, type,p1,p2,p3)+r*( mid( MC1._cv[ipt], MC1._cc[ipt], min_point, imid )-min_point);
									for( unsigned int i=0; i<MC2._nsub; i++ ){
										MC2._cvsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
									}
								}
							}//end for-loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "cost_function not monotonic concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3, max_point};
								// int convexity = 1; // concave not monotonic
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

							return MC2.cut();
							break;
						}
					default:
						return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
						break;
				}//end of switch(convexity) in case 0 of switch(monotonicity)
				break;
			  }//end of case 0
			  case vMcCormick<T>::MON_INCR: // increasing
			  {
				MC2._I = T(mc::cost_function( Op<T>::l(MC1._I), type,p1,p2,p3),mc::cost_function( Op<T>::u(MC1._I), type,p1,p2,p3));
				switch(convexity){
					case vMcCormick<T>::CONVEX: //convex
						{
						  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
							  //convex
							  MC2._cv[ipt] =  mc::cost_function(MC1._cv[ipt], type,p1,p2,p3);
							  //concave
							  double r = 0.;
							  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
								r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
							  }
							  MC2._cc[ipt] =  mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));
							  //subgradients
							  for( unsigned int i=0; i<MC2._nsub; i++ )
							  {
								MC2._cvsub[ipt][i] = mc::der_cost_function(MC1._cv[ipt], type,p1,p2,p3)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
								MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
							  }
						  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "cost_function increasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3};
								// int convexity = 2; // convex increasing
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

							return MC2.cut();
							break;
						}
					case vMcCormick<T>::CONCAVE: //concave
						{
						  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
							  //concave
							  MC2._cc[ipt] =  mc::cost_function(MC1._cc[ipt], type,p1,p2,p3);
							  //convex
							  double r = 0.;
							  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
							  }
							  MC2._cv[ipt] =  mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));
							  //subgradients
							  for( unsigned int i=0; i<MC2._nsub; i++ )
							  {
								MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
								MC2._ccsub[ipt][i] = mc::der_cost_function(MC1._cc[ipt], type,p1,p2,p3)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
							  }
						  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "cost_function increasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3};
								// int convexity = 3; // concave increasing
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

						    return MC2.cut();
							break;
						}
					case vMcCormick<T>::CONV_NONE: //not convex not concave
					default:
						return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) ),Op<T>::l(MC2._I),Op<T>::u(MC2._I));
						break;
				}//end of switch(convexity)	in case 1 of switch(monotonicity)
			  }//end of case 1
			  case vMcCormick<T>::MON_DECR: //decreasing
			  {
				MC2._I = T(mc::cost_function( Op<T>::u(MC1._I), type,p1,p2,p3),mc::cost_function( Op<T>::l(MC1._I), type,p1,p2,p3));
				switch(convexity){
					case vMcCormick<T>::CONVEX: //convex
						{
						  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
						      //convex
							  MC2._cv[ipt] =  mc::cost_function(MC1._cc[ipt], type,p1,p2,p3);
							  //concave
							  double r = 0.;
							  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
							  }
							  MC2._cc[ipt] =  mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));
							  //subgradients
							  for( unsigned int i=0; i<MC2._nsub; i++ )
							  {
								MC2._cvsub[ipt][i] = mc::der_cost_function(MC1._cc[ipt], type,p1,p2,p3)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
								MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
							  }
						  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "cost_function decreasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3};
								// int convexity = 4; // convex decreasing
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

							return MC2.cut();
							break;
						}
					case vMcCormick<T>::CONCAVE: //concave
						{
						  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
							  //concave
							  MC2._cc[ipt] =  mc::cost_function(MC1._cv[ipt], type,p1,p2,p3);
							  //convex
							  double r = 0.;
							  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
							  }
							  MC2._cv[ipt] =  mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));
							  //subgradient
							  for( unsigned int i=0; i<MC2._nsub; i++ )
							  {
								MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
								MC2._ccsub[ipt][i] = mc::der_cost_function(MC1._cv[ipt], type,p1,p2,p3)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
							  }
						  }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "cost_function decreasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
							// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
								// std::vector<double> params = {type, p1, p2, p3};
								// int convexity = 5; // concave decreasing
								// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
								// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.COST_FUNCTION);
								// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																									// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
							    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
							// }

							return MC2.cut();
							break;
						}
					case vMcCormick<T>::CONV_NONE: //not convex not concave
					default:
						return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) ),Op<T>::l(MC2._I),Op<T>::u(MC2._I));
						break;
				}//end of switch(convexity)	in case 2 of switch(monotonicity)
				default:
				//throw  vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::COST_FUNCTION_MON );
				return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
				break;
			  }//end of case 2
			}//end of switch(monotonicity)
		}//end of function type case 1 Guthrie
			break;
		default: // if something went wrong, just return the Guthrie formula
			return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
			break;
    }//end of switch type
}

//added AVT.SVT 23.11.2017
template <typename T> inline vMcCormick<T>
nrtl_tau
(const vMcCormick<T>&MC1, const double a, const double b, const double e, const double f)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::NRTL_TAU );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );

	//First, check monotonicity
	unsigned int monotonicity = vMcCormick<T>::MON_NONE; //0 means it is neither incr nor decr, 1 is incr, 2 is decr
	unsigned int convexity = vMcCormick<T>::CONV_NONE; //0 means it is neither convex nor concave, 1 is convex, 2 is concave
	double new_l = 1e51,new_u = -1e51,zmin = 0.,zmax = 1e51; //needed in the case of non-monotonicity
	if(vMcCormick<T>::options.ENVEL_USE){
		vMcCormick<T>::_nrtl_tau_mon_conv(monotonicity, convexity, a, b, e, f, Op<T>::l(MC1._I), Op<T>::u(MC1._I), new_l, new_u, zmin, zmax); //sets monotonicity and convexity parameters
	}else{
		return a + b/MC1 + e * log(MC1) + f*MC1;
	}

	switch(monotonicity){
	  case vMcCormick<T>::MON_NONE:	//not monotonic at all in the given interval
	  {
		  MC2._I = T(new_l,new_u);
		  switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
						//convex
					  { int imid = -1;
						double vmid = mid( MC1._cv[ipt], MC1._cc[ipt], zmin, imid );
						MC2._cv[ipt] = mc::nrtl_tau( vmid, a, b, e, f );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._cvsub[ipt][i] = mc::nrtl_dtau(vmid, b, e, f) * mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid );
					  }
						//concave
					  { int imid = -1;
						double r = 0.;
						if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						}
						MC2._cc[ipt] = mc::nrtl_tau(zmax, a, b, e, f) + r * ( mid( MC1._cv[ipt], MC1._cc[ipt], zmax, imid ) - zmax );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._ccsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau not monotonic convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f, zmin};
				  	// int convexity = 0; // convex not monotonic
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
						//convex
					 { int imid = -1;
						double r = 0.;
						if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						}
						MC2._cv[ipt] = mc::nrtl_tau(zmin, a, b, e, f) + r * ( mid( MC1._cv[ipt], MC1._cc[ipt], zmin, imid ) - zmin );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._cvsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
					  }
						//concave
					 { int imid = -1;
						double vmid = mid( MC1._cv[ipt], MC1._cc[ipt], zmax, imid );
						MC2._cc[ipt] = mc::nrtl_tau( vmid, a, b, e, f );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._ccsub[ipt][i] = mc::nrtl_dtau(vmid, b, e, f) * mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid );
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau not monotonic concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f, zmax};
				  	// int convexity = 1; // concave not monotonic
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1,Op<T>::l(MC2._I),Op<T>::u(MC2._I));
				break;
		  }
	  }
	  case vMcCormick<T>::MON_INCR:// increasing
	  {
		MC2._I = T(mc::nrtl_tau( Op<T>::l(MC1._I), a, b, e, f),mc::nrtl_tau( Op<T>::u(MC1._I), a, b, e, f));
		switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  //convex
					  MC2._cv[ipt] =  mc::nrtl_tau(MC1._cv[ipt], a, b, e, f);
					  //concave
					  MC2._cc[ipt] =  mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));
					  //subgradients
					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = mc::nrtl_dtau(MC1._cv[ipt], b, e, f)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
						MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
					  }

				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau increasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f};
				  	// int convexity = 2; // convex increasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  //concave
					  MC2._cc[ipt] =  mc::nrtl_tau(MC1._cc[ipt], a, b, e, f);
					  //convex
					  MC2._cv[ipt] =  mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));
					  //subgradients
					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
						MC2._ccsub[ipt][i] = mc::nrtl_dtau(MC1._cc[ipt], b, e, f)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau increasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f};
				  	// int convexity = 3; // concave increasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1,Op<T>::l(MC2._I),Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 1 of switch(monotonicity)
	  }//end of case 1
	  case vMcCormick<T>::MON_DECR ://decreasing
	  {
		MC2._I = T(mc::nrtl_tau( Op<T>::u(MC1._I), a, b, e, f),mc::nrtl_tau( Op<T>::l(MC1._I), a, b, e, f));
		switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				     r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  //convex
					  MC2._cv[ipt] =  mc::nrtl_tau(MC1._cc[ipt], a, b, e, f);
					  //concave
					  MC2._cc[ipt] =  mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));
					  //subgradients
					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = mc::nrtl_dtau(MC1._cc[ipt], b, e, f)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
						MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau decreasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f};
				  	// int convexity = 4; // convex decreasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				    r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  //concave
					  MC2._cc[ipt] =  mc::nrtl_tau(MC1._cv[ipt], a, b, e, f);
					  //convex
					  MC2._cv[ipt] =  mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));
					  //subgradients
					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
						MC2._ccsub[ipt][i] = mc::nrtl_dtau(MC1._cv[ipt], b, e, f)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
					  }

				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_tau decreasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {a, b, e, f};
				  	// int convexity = 5; // concave decreasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_TAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE : //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 2 of switch(monotonicity)
	  }//end of case 2
	   default:
		return a + b/MC1 + e * log(MC1) + f*MC1;
		break;

	}//end of switch(monotonicity)

}

//added AVT.SVT 10.01.2019
template <typename T> inline vMcCormick<T>
nrtl_dtau
(const vMcCormick<T>&MC1, const double b, const double e, const double f)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::NRTL_DTAU );

    vMcCormick<T> MC2;
    MC2._pts_sub( MC1._nsub, MC1._const, MC1._npts );

	//First, check monotonicity
	unsigned int monotonicity = vMcCormick<T>::MON_NONE; //0 means it is neither incr nor decr, 1 is incr, 2 is decr
	unsigned int convexity = vMcCormick<T>::CONV_NONE; //0 means it is neither convex nor concave, 1 is convex, 2 is concave
	double new_l = 1e51,new_u = -1e51,zmin = 0.,zmax = 1e51; //needed in the case of non-monotonicity
	if(vMcCormick<T>::options.ENVEL_USE){
		vMcCormick<T>::_nrtl_der_tau_mon_conv(monotonicity, convexity, b, e, f, Op<T>::l(MC1._I), Op<T>::u(MC1._I), new_l, new_u, zmin, zmax); //sets monotonicity and convexity parameters
	}else{
		return -b/sqr(MC1) + e/MC1 + f;
	}

	switch(monotonicity){
	  case vMcCormick<T>::MON_NONE:	//not monotonic at all in the given interval
	  {
		  MC2._I = T(new_l,new_u);
		  switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
						//convex
					  { int imid = -1;
						double vmid = mid( MC1._cv[ipt], MC1._cc[ipt], zmin, imid );
						MC2._cv[ipt] = mc::nrtl_dtau( vmid, b, e, f );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._cvsub[ipt][i] = mc::der2_nrtl_tau(vmid, b, e) * mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid );
					  }
						//concave
					  { int imid = -1;
						double r = 0.;
						if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						}
						MC2._cc[ipt] = mc::nrtl_dtau(zmax, b, e, f) + r * ( mid( MC1._cv[ipt], MC1._cc[ipt], zmax, imid ) - zmax );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._ccsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau not monotonic convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f, 0, zmin}; // the 0 is a dummy
				  	// int convexity = 0; // convex not monotonic
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
						//convex
					 { int imid = -1;
						double r = 0.;
						if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						}
						MC2._cv[ipt] = mc::nrtl_dtau(zmin, b, e, f) + r * ( mid( MC1._cv[ipt], MC1._cc[ipt], zmin, imid ) - zmin );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._cvsub[ipt][i] = mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid ) * r;
					  }
						//concave
					 { int imid = -1;
						double vmid = mid( MC1._cv[ipt], MC1._cc[ipt], zmax, imid );
						MC2._cc[ipt] = mc::nrtl_dtau( vmid, b, e, f );
						for( unsigned int i=0; i<MC2._nsub; i++ )
						  MC2._ccsub[ipt][i] = mc::der2_nrtl_tau(vmid, b, e) * mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imid );
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau not monotonic concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f, 0, zmax};
				  	// int convexity = 1; // concave not monotonic
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func( -b/sqr(MC1) + e/MC1 + f ,Op<T>::l(MC2._I),Op<T>::u(MC2._I));
				break;
		  }
	  }
	  case vMcCormick<T>::MON_INCR:// increasing
	  {
		MC2._I = T(mc::nrtl_dtau( Op<T>::l(MC1._I), b, e, f),mc::nrtl_dtau( Op<T>::u(MC1._I), b, e, f));
		switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  MC2._cv[ipt] =  mc::nrtl_dtau(MC1._cv[ipt], b, e, f);

					  MC2._cc[ipt] =  mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));

					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = mc::der2_nrtl_tau(MC1._cv[ipt], b, e)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
						MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau increasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f}; // the 0 is a dummy
				  	// int convexity = 2; // convex increasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  MC2._cc[ipt] =  mc::nrtl_dtau(MC1._cc[ipt], b, e, f);
					  MC2._cv[ipt] =  mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));

					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
						MC2._ccsub[ipt][i] = mc::der2_nrtl_tau(MC1._cc[ipt], b, e)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
					  }

				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau increasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f}; // the 0 is a dummy
				  	// int convexity = 3; // convex not monotonic
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func( -b/sqr(MC1) + e/MC1 + f, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 1 of switch(monotonicity)
	  }//end of case 1
	  case vMcCormick<T>::MON_DECR ://decreasing
	  {
		MC2._I = T(mc::nrtl_dtau( Op<T>::u(MC1._I), b, e, f),mc::nrtl_dtau( Op<T>::l(MC1._I), b, e, f));
		switch(convexity){
			case vMcCormick<T>::CONVEX ://convex
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				     r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  MC2._cv[ipt] =  mc::nrtl_dtau(MC1._cc[ipt], b, e, f);
					  MC2._cc[ipt] =  mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f)+r*(MC1._cv[ipt]-Op<T>::l(MC1._I));

					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = mc::der2_nrtl_tau(MC1._cc[ipt], b, e)*(MC1._const? 0.:MC1._ccsub[ipt][i]);
						MC2._ccsub[ipt][i] = (MC1._const? 0.:MC1._cvsub[ipt][i])*r;
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau decreasing convex";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f}; // the 0 is a dummy
				  	// int convexity = 4; // convex decreasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONCAVE ://concave
				{
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				     r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
					  MC2._cc[ipt] =  mc::nrtl_dtau(MC1._cv[ipt], b, e, f);
					  MC2._cv[ipt] =  mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)+r*(MC1._cc[ipt]-Op<T>::u(MC1._I));

					  for( unsigned int i=0; i<MC2._nsub; i++ )
					  {
						MC2._cvsub[ipt][i] = (MC1._const? 0.:MC1._ccsub[ipt][i])*r;
						MC2._ccsub[ipt][i] = mc::der2_nrtl_tau(MC1._cv[ipt], b, e)*(MC1._const? 0.:MC1._cvsub[ipt][i]);
					  }
				  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "nrtl_dtau decreasing concave";
	vMcCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
				  	// std::vector<double> params = {b, e, f}; // the 0 is a dummy
				  	// int convexity = 5; // concave decreasing
				  	// vMcCormick<T>::additionalLins.set_additional_values(params.data(), &convexity);
				  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC1._cv, MC1._cc, MC1._cvsub, MC1._ccsub, MC2._npts, Op<T>::l(MC1._I), Op<T>::u(MC1._I), vMcCormick<T>::additionalLins.NRTL_DTAU);
				  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
				  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
				    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
				  // }
				  return MC2.cut();
				  break;
				}
			case vMcCormick<T>::CONV_NONE : //not convex not concave
			default:
				return bounding_func(-b/sqr(MC1) + e/MC1 + f, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 2 of switch(monotonicity)
	  }//end of case 2
	  default:
		return -b/sqr(MC1) + e/MC1 + f;
		break;

	}//end of switch(monotonicity)
}

//added AVT.SVT 23.11.2017
template <typename T> inline vMcCormick<T>
nrtl_G
(const vMcCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha )
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::NRTL_G );

    return exp(-alpha*nrtl_tau(MC1,a,b,e,f));
}

//added AVT.SVT 01.03.2018
template <typename T> inline vMcCormick<T>
nrtl_Gtau
( const vMcCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::NRTL_GTAU);

  return xexpax(nrtl_tau(MC1,a,b,e,f),-alpha);
}

//added AVT.SVT 22.03.2018
template <typename T> inline vMcCormick<T>
nrtl_Gdtau
( const vMcCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::NRTL_GDTAU);

  return nrtl_G(MC1,a,b,e,f,alpha)*nrtl_dtau(MC1,b,e,f);
}

//added AVT.SVT 22.03.2018
template <typename T> inline vMcCormick<T>
nrtl_dGtau
( const vMcCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::NRTL_DGTAU);

  return -alpha*nrtl_Gtau(MC1,a,b,e,f,alpha)*nrtl_dtau(MC1,b,e,f);
}

//added AVT.SVT J. Luethe, A. Schweidtmann, W. Huster 18.12.2017
template <typename T> inline vMcCormick<T>
p_sat_ethanol_schroeder( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::p_sat_ethanol_schroeder( MC._I );

  if( Op<T>::l(MC._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::P_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::u(MC._I) > _T_c_K){
	const double _N_Tsat_1 = -8.94161;
	const double _N_Tsat_2 = 1.61761;
	const double _N_Tsat_3 = -51.1428;
	const double _N_Tsat_4 = 53.1360;
	const double _k_Tsat_1 = 1.0;
	const double _k_Tsat_2 = 1.5;
	const double _k_Tsat_3 = 3.4;
	const double _k_Tsat_4 = 3.7;
	const double _p_c = 62.68;

	return _p_c*(exp(_T_c_K/MC*(_N_Tsat_1*pow((1-MC/_T_c_K),_k_Tsat_1) + _N_Tsat_2*pow((1-MC/_T_c_K),_k_Tsat_2) + _N_Tsat_3*pow((1-MC/_T_c_K),_k_Tsat_3) + _N_Tsat_4*pow((1-MC/_T_c_K),_k_Tsat_4))));
  }

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  //convex
	  { int imid = -1;
		MC2._cv[ipt] = mc::p_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_p_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ));
		}
	  }
	  //concave
	  { int imid = -1;
		double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::p_sat_ethanol_schroeder(Op<T>::u(MC._I))-mc::p_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
		MC2._cc[ipt] = mc::p_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid )-Op<T>::u(MC._I));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "p_sat_ethanol_schroeder";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.P_SAT_ETHANOL_SCHROEDER);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
	return MC2.cut();
}

template <typename T> inline vMcCormick<T>
rho_vap_sat_ethanol_schroeder( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::rho_vap_sat_ethanol_schroeder( MC._I );

  if( Op<T>::l(MC._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::RHO_VAP_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::u(MC._I) > _T_c_K){
	const double _N_vap_1 = -1.75362;
	const double _N_vap_2 = -10.5323;
	const double _N_vap_3 = -37.6407;
	const double _N_vap_4 = -129.762;
	const double _k_vap_1 = 0.21;
	const double _k_vap_2 = 1.1;
	const double _k_vap_3 = 3.4;
	const double _k_vap_4 = 10;
	const double _rho_c = 273.195;

	return _rho_c*(exp(_N_vap_1*pow((1 - MC/_T_c_K),_k_vap_1) + _N_vap_2*pow((1 - MC/_T_c_K),_k_vap_2) + _N_vap_3*pow((1 - MC/_T_c_K),_k_vap_3) + _N_vap_4*pow((1 - MC/_T_c_K),_k_vap_4)));
 }

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	   //convex
	  { int imid = -1;
		MC2._cv[ipt] = mc::rho_vap_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_rho_vap_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ));
		}
	  }
	  //concave
	  { int imid = -1;
		double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:( mc::rho_vap_sat_ethanol_schroeder(Op<T>::u(MC._I))- mc::rho_vap_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
		MC2._cc[ipt] = mc::rho_vap_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid )-Op<T>::u(MC._I));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	  }
	}
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "rho_vap_sat_ethanol_schroeder";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.RHO_VAP_SAT_ETHANOL_SCHROEDER);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
}

// Only for T > 290.3
template <typename T> inline vMcCormick<T>
rho_liq_sat_ethanol_schroeder( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::rho_liq_sat_ethanol_schroeder( MC._I );

  if( Op<T>::l(MC._I)<= 0.)
    throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::RHO_LIQ_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::l(MC._I) <= 290.3 || Op<T>::u(MC._I) > _T_c_K){
	const double _N_liq_1=9.00921;
	const double _N_liq_2=-23.1668;
	const double _N_liq_3=30.9092;
	const double _N_liq_4=-16.5459;
	const double _N_liq_5=3.64294;
	const double _k_liq_1=0.5;
	const double _k_liq_2=0.8;
	const double _k_liq_3=1.1;
	const double _k_liq_4=1.5;
	const double _k_liq_5=3.3;
	const double _rho_c = 273.195;

	return _rho_c*(1 + _N_liq_1*pow((1 - MC/_T_c_K),_k_liq_1) + _N_liq_2*pow((1 - MC/_T_c_K),_k_liq_2) + _N_liq_3*pow((1 - MC/_T_c_K),_k_liq_3) + _N_liq_4*pow((1 - MC/_T_c_K),_k_liq_4) + _N_liq_5*pow((1 - MC/_T_c_K),_k_liq_5));
 }

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	//convex
	  { int imid = -1;
		double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::rho_liq_sat_ethanol_schroeder(Op<T>::u(MC._I))-mc::rho_liq_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
		MC2._cv[ipt] = mc::rho_liq_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid )-Op<T>::u(MC._I));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	  }
	  //concave
	  { int imid = -1;
		MC2._cc[ipt] = mc::rho_liq_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ));
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_rho_liq_sat_ethanol_schroeder(mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ));
		}
	  }
	}
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "rho_liq_sat_ethanol_schroeder";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.RHO_LIQ_SAT_ETHANOL_SCHROEDER);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
}

template <typename T> inline vMcCormick<T>
covariance_function
( const vMcCormick<T>&MC, const double type )
{
  if ( Op<T>::l(MC._I) < 0.  )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::COVARIANCE_FUNCTION );
  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::covariance_function( MC._I, type );

  // the covariance functions are all convex and decreasing
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
      // convex
      int imid = 2; // we use the cc relaxation for cv
      double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
      MC2._cv[ipt] = mc::covariance_function(vmid, type);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_covariance_function(vmid, type);
      }

      // concave
      imid = 1; // concave envelope of covariance is decreasing so we use the cv relaxation for cc
      double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::covariance_function(Op<T>::u(MC._I),type)-mc::covariance_function(Op<T>::l(MC._I),type))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
      MC2._cc[ipt] = mc::covariance_function(Op<T>::l(MC._I),type)+r*(mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid )-Op<T>::l(MC._I));
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) *r;
  }
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "covariance_function";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
acquisition_function
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2, const double type, const double fmin )
{

  if ( Op<T>::l(MC2._I) < 0.  )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::ACQUISITION_FUNCTION );

  vMcCormick<T> MC3;
  if( MC2._const )
      MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
      MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
	MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::acquisition_function( MC1._I, MC2._I, type, fmin );

  switch((int)type){
	  case 1: // lower confidence bound
	      return MC1 - fmin*MC2;
	      break;
      case 2: // expected improvement
	  {
          for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			  MC3._cv[ipt] = mc::acquisition_function(MC1._cc[ipt],MC2._cv[ipt], type, fmin);
			  for( unsigned int i=0; i<MC3._nsub; i++ ){
				  MC3._cvsub[ipt][i] =  mc::der_x_acquisition_function(MC1._cc[ipt], MC2._cv[ipt], type, fmin)*(MC1._const? 0.:MC1._ccsub[ipt][i])
								      + mc::der_y_acquisition_function(MC1._cc[ipt], MC2._cv[ipt], type, fmin)*(MC2._const? 0.:MC2._cvsub[ipt][i]);
			  }
		  }
		  double xL = Op<T>::l(MC1._I);
		  double xU = Op<T>::u(MC1._I);
		  double yL = Op<T>::l(MC2._I);
		  double yU = Op<T>::u(MC2._I);
		  double cornerLL = mc::acquisition_function(xL, yL, type, fmin);
		  double cornerUU = mc::acquisition_function(xU, yU, type, fmin);
		  double cornerLU = mc::acquisition_function(xL, yU, type, fmin);
		  double cornerUL = mc::acquisition_function(xU, yL, type, fmin);
		  double r11,r12,r21,r22,val1corner,val2corner,val1x,val1y,val2x,val2y=0;
          if(cornerLL+cornerUU< cornerLU+cornerUL){
			  r11 = isequal(xL,xU) ? 0 : (cornerLL - cornerUL)/(xL-xU);
			  r12 = isequal(yL,yU) ? 0 : (cornerLL - cornerLU)/(yL-yU);
			  val1corner = cornerLL;
			  val1x = xL;
			  val1y = yL;

			  r21 = isequal(xL,xU) ? 0 : (cornerLU - cornerUU)/(xL-xU);
			  r22 = isequal(yL,yU) ? 0 : (cornerUL - cornerUU)/(yL-yU);
			  val2corner = cornerUU;
			  val2x = xU;
			  val2y = yU;

		  }
		  else{
			  r11 = isequal(xL,xU) ? 0 : (cornerLU - cornerUU)/(xL-xL);
			  r12 = isequal(yL,yU) ? 0 : (cornerLL - cornerLU)/(yL-yU);
			  val1corner = cornerLU;
			  val1x = xL;
			  val1y = yU;

			  r21 = isequal(xL,xU) ? 0 : (cornerLL - cornerUL)/(xL-xU);
			  r22 = isequal(yL,yU) ? 0 : (cornerUL - cornerUU)/(yL-yU);
			  val2corner = cornerUL;
			  val2x = xU;
			  val2y = yL;
		  }
          for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			  double val1 = val1corner + r11*(MC1._cv[ipt] - val1x) + r12*(MC2._cc[ipt] - val1y);
			  double val2 = val2corner + r21*(MC1._cv[ipt] - val2x) + r22*(MC2._cc[ipt] - val2y);
			  if(val1<val2){
				  MC3._cc[ipt] = val1;
				  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
				  for( unsigned int i=0; i<MC3._nsub; i++ ){
					  MC3._ccsub[ipt][i] =  r11*(MC1._const? 0.:MC1._cvsub[ipt][i])
									      + r12*(MC2._const? 0.:MC2._ccsub[ipt][i]);
		              if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
				  }
			  }
			  else{
				  MC3._cc[ipt] = val2;
				  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
				  for( unsigned int i=0; i<MC3._nsub; i++ ){
					  MC3._ccsub[ipt][i] =  r21*(MC1._const? 0.:MC1._cvsub[ipt][i])
									      + r22*(MC2._const? 0.:MC2._ccsub[ipt][i]);
		              if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
				  }
			  }
              if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
		  }	// end of for loop over _npts
	  }
	      break;
      case 3: // probability of improvement
		  throw std::runtime_error("mc::McCormick\t Probability of improvement acquisition function currently not implemented.\n");
	      break;
      default:
		  throw std::runtime_error("mc::McCormick\t Probability of improvement acquisition called with unknown type.\n");
	      break;
  }

#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "acquisition_function";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif

  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

template <typename T> inline vMcCormick<T>
gaussian_probability_density_function
( const vMcCormick<T>&MC )
{
  // This function is monotonically increasing for xU <= 0 and monotonically decreasing for xL>=0
  // It is convex on xU <= -1 or xL >= 1 and concave on [-1,1]
  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::gaussian_probability_density_function(MC._I);

  double xL = Op<T>::l(MC._I);
  double xU = Op<T>::u(MC._I);

  if(xU <= -1){ // The function is convex and monotonically increasing
      for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		  // convex relaxation
		  int imid = 1; // we use the cv relaxation for cv
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], xL, imid );
		  MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }

		  // concave relaxation
		  imid = 2; // concave envelope is increasing so we use the cc relaxation for cc
		  double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
		  MC2._cc[ipt] = mc::gaussian_probability_density_function(xU)+r*(mid( MC._cv[ipt], MC._cc[ipt], xU, imid )-xU);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) *r;
		  }
	  }
  }
  else if(xL >= 1){ // The function is convex and monotonically decreasing
      for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	      // convex relaxation
          int imid = 2; // we use the cc relaxation for cv since it is decreasing
          double vmid = mid( MC._cv[ipt], MC._cc[ipt], xU, imid );
          MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
          }

          // concave relaxation
          imid = 1; // concave envelope is decreasing so we use the cv relaxation for cc
          double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
          MC2._cc[ipt] = mc::gaussian_probability_density_function(xL)+r*(mid( MC._cv[ipt], MC._cc[ipt], xL, imid )-xL);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) *r;
          }
	  }
  }
  else if(-1 <= xL && xU <= 1){
	  // The function is concave
	  double minPoint;
	  double maxPoint;
	  if(mc::gaussian_probability_density_function(xL) < mc::gaussian_probability_density_function(xU)){
		  minPoint = xL;
		  maxPoint = xU;
	  }
	  else{
		  minPoint = xU;
		  maxPoint = xL;
	  }
	  if(xL<= 0. && 0. <= xU){
		  maxPoint = 0.;
	  }
      for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	      // convex relaxation
          int imid = -1;
          double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
          MC2._cv[ipt] = mc::gaussian_probability_density_function(minPoint)+r*(mid( MC._cv[ipt], MC._cc[ipt], minPoint, imid )-minPoint);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
          }
          // concave relaxation
          imid = -1;
          double vmid = mid( MC._cv[ipt], MC._cc[ipt], maxPoint, imid );
          MC2._cc[ipt] = mc::gaussian_probability_density_function(vmid);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
          }
	  }
  }
  else if(xL <= -1 && -1 < xU && xU < 1){
	  // Left convex-concave part
	  // convex relaxation is increasing
	  double starting_point = (xL + std::min(xU,0.))/2.;
	  double solPointCv = vMcCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,std::min(xU,0.)/*right bound*/,xL/*starting point for Newton*/,xU/*fixed point in the equality*/);
	  double solPointCc = vMcCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,std::min(xU,0.)/*right bound*/,std::min(xU,0.)/*starting point for Newton*/,xL/*fixed point in the equality*/);
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	      if(MC._cv[ipt]<solPointCv){
		      int imid = 1; // we use the cv relaxation for cv
		      double vmid = mid( MC._cv[ipt], MC._cc[ipt], xL, imid );
		      MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
		    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		      }
	      }
	      else{
		      int imid = 1; // we use the cv relaxation for cv
		      double r = ( isequal( solPointCv, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCv))/(xU-solPointCv) );
              MC2._cv[ipt] = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cv[ipt] - solPointCv);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	      // concave relaxation is increasing (up to 0)
	      int imid = -1; // we cannot know which to use for the concave relaxation
	      double vmid = mid( MC._cv[ipt], MC._cc[ipt], 0, imid );
	      if( vmid>solPointCc){
		      MC2._cc[ipt] = mc::gaussian_probability_density_function(vmid);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
		    	MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		      }
	      }
	      else{
		      double r = ( isequal( solPointCc, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCc))/(xL-solPointCc) );
              MC2._cc[ipt] = mc::gaussian_probability_density_function(solPointCc)+r*( vmid - solPointCc);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	  }
  }
  else if(-1 < xL && xL < 1 && xU >= 1){
	  double solPointCv = vMcCormick<T>::_gpdf_compute_sol_point(std::max(xL,0.)/*left bound*/,xU/*right bound*/,xU/*starting point for Newton*/,xL/*fixed point in the equality*/);
	  double solPointCc = vMcCormick<T>::_gpdf_compute_sol_point(std::max(xL,0.)/*left bound*/,xU/*right bound*/,std::max(xL,0.)/*starting point for Newton*/,xU/*fixed point in the equality*/);
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	      // Right convex-concave part
	      // convex relaxation is decreasing
	      if(MC._cc[ipt]>solPointCv){
		      int imid = 2; // we use the cc relaxation for cv
		      double vmid = mid( MC._cv[ipt], MC._cc[ipt], xU, imid );
		      MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
		    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		      }
	      }
	      else{
		      int imid = 2; // we use the cc relaxation for cv
		      double r = ( isequal( solPointCv, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCv))/(xL-solPointCv) );
              MC2._cv[ipt] = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cc[ipt] - solPointCv);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	      // concave relaxation is decreasing
	      if(MC._cv[ipt]<solPointCc){
		      int imid = 1; // we use the cv relaxation for cc
		      double vmid = mid( MC._cv[ipt], MC._cc[ipt], xU, imid );
		      MC2._cc[ipt] = mc::gaussian_probability_density_function(vmid);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
		    	MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		      }
	      }
	      else{
		      int imid = 1; // we use the cv relaxation for cc
		      double r = ( isequal( solPointCc, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCc))/(xU-solPointCc) );
              MC2._cc[ipt] = mc::gaussian_probability_density_function(solPointCc)+r*( MC._cv[ipt] - solPointCc);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	  }
  }
  else{ // xL <= -1 && xU >= 1
      // convex relaxation may be increasing or decreasing
	  if(xL + xU < 0){
          // convex relaxation is increasing
		  double solPointCv = xL;
		  if(vMcCormick<T>::_gpdf_func(xL,&xU,0)*vMcCormick<T>::_gpdf_func(xU,&xU,0)>0){ // make sure there is a root to avoid errors in newton computations
			  solPointCv = vMcCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,std::min(xU,-1.)/*right bound*/,std::min(xU,-1.)/*starting point for Newton*/,xU/*fixed point in the equality*/);
		  }
		  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		      if(MC._cv[ipt]<solPointCv){
			      int imid = 1; // we use the cv relaxation for cv
			      double vmid = mid( MC._cv[ipt], MC._cc[ipt], xL, imid );
			      MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
			      for( unsigned int i=0; i<MC2._nsub; i++ ){
			    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
			      }
		      }
		      else{
			      int imid = 1; // we use the cv relaxation for cv
			      double r = ( isequal( solPointCv, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCv))/(xU-solPointCv) );
			      MC2._cv[ipt] = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cv[ipt] - solPointCv);
			      for( unsigned int i=0; i<MC2._nsub; i++ ){
			    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
			      }
		      }
		  }
	  }
	  else if(xL+xU>0){
          // convex relaxation is decreasing
		  double solPointCv = xU;
		  if(vMcCormick<T>::_gpdf_func(xL,&xL,0)*vMcCormick<T>::_gpdf_func(xU,&xL,0)>0){
		      solPointCv = vMcCormick<T>::_gpdf_compute_sol_point(std::max(xL,1.)/*left bound*/,xU/*right bound*/,std::max(xL,1.)/*starting point for Newton*/,xL/*fixed point in the equality*/);
		  }
		  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		      if(MC._cc[ipt]>solPointCv){
			      int imid = 2; // we use the cc relaxation for cv
			      double vmid = mid( MC._cv[ipt], MC._cc[ipt], xU, imid );
			      MC2._cv[ipt] = mc::gaussian_probability_density_function(vmid);
			      for( unsigned int i=0; i<MC2._nsub; i++ ){
			    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
			      }
		      }
		      else{
			      int imid = 2; // we use the cc relaxation for cv
			      double r = ( isequal( solPointCv, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCv))/(xL-solPointCv) );
			      MC2._cv[ipt] = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cc[ipt] - solPointCv);
			      for( unsigned int i=0; i<MC2._nsub; i++ ){
			    	MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
			      }
		      }
		  }
	  }
	  else{
		  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		      // we need this case to avoid numerical issues in newton
		      int imid = 2; // we use the cc relaxation for cv
		      double r = 0;
		      MC2._cv[ipt] = mc::gaussian_probability_density_function(xL);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
			    MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		      }
		  }
	  }
	  // concave relaxation
	  double solPointCc1 = vMcCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,0./*right bound*/,0./*starting point for Newton*/,xL/*fixed point in the equality*/);
	  double solPointCc2 = vMcCormick<T>::_gpdf_compute_sol_point(0./*left bound*/,xU/*right bound*/,0./*starting point for Newton*/,xU/*fixed point in the equality*/);
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	      int imid = -1; // we cannot know which relaxation to use
	      double vmid = mid( MC._cv[ipt], MC._cc[ipt], 0., imid );
	      if(vmid <= solPointCc1){
		      double r = ( isequal( solPointCc1, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCc1))/(xL-solPointCc1) );
              MC2._cc[ipt] = mc::gaussian_probability_density_function(solPointCc1)+r*( vmid - solPointCc1);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	      else if(vmid >= solPointCc2){
		      double r = ( isequal( solPointCc2, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCc2))/(xU-solPointCc2) );
              MC2._cc[ipt] = mc::gaussian_probability_density_function(solPointCc2)+r*( vmid - solPointCc2);
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
              }
	      }
	      else{
		      MC2._cc[ipt] = mc::gaussian_probability_density_function(vmid);
		      for( unsigned int i=0; i<MC2._nsub; i++ ){
		    	MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		      }
	      }
	  }
  }

#ifdef VMC__MCCORMICK_DEBUG
  	std::string str = "gaussian probability density function";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
regnormal
( const vMcCormick<T>&MC, const double a, const double b )
{
  if ( a <= 0. || b <= 0.  )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::REGNORMAL );

  vMcCormick<T> MC2;
  MC2._pts_sub(  MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::regnormal( MC._I, a, b );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
      // convex relaxation
      {int imid = -1;
        const double* cvenv = vMcCormick<T>::_regnormal_cv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), a, b, Op<T>::l(MC._I), Op<T>::u(MC._I) );
        MC2._cv[ipt] = cvenv[0];
        for( unsigned int i=0; i<MC2._nsub; i++ ){
          MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
        }
      }
      // concave relaxation
      { int imid = -1;
        const double* ccenv = vMcCormick<T>::_regnormal_cc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), a, b, Op<T>::l(MC._I), Op<T>::u(MC._I) );
        MC2._cc[ipt] = ccenv[0];
        for( unsigned int i=0; i<MC2._nsub; i++ ){
          MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
        }
      }
  }

#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "regnormal";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
sqrt
( const vMcCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) < 0. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SQRT );
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::sqrt( MC._I );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  //convex
	  { double r = 0.;
		if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
		  r = ( std::sqrt( Op<T>::u(MC._I) ) - std::sqrt( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		int imid = -1;
		double vmid = mid_ndiff( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid );
		MC2._cv[ipt] = std::sqrt( Op<T>::l(MC._I) ) + r * ( vmid - Op<T>::l(MC._I) );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;

	  }
	  //concave
	  { int imid = -1;
		double vmid = mid_ndiff( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		if(vmid > 0){
			MC2._cc[ipt] = std::sqrt( vmid );
			for( unsigned int i=0; i<MC2._nsub; i++ )
				MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) / (2.*MC2._cc[ipt]);
		}
		else{
			MC2._cc[ipt] = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ )
				MC2._ccsub[ipt][i] = 0.0;
		}

	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "sqrt";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.SQRT);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
}

template <typename T> inline vMcCormick<T>
erfc
( const vMcCormick<T> &MC )
{
  return ( 1. - erf( MC ) );
}

template <typename T> inline vMcCormick<T>
erf
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::erf( MC._I );

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  //convex
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_erfcv( mid( MC._cv[ipt],MC._cc[ipt], Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  //concave
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_erfcc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
#ifdef MC__VMCCORMICK_DEBUG
    std::string str = "erf";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.ERF);
  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	// if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
pow
( const vMcCormick<T>&MC, const int n )
{
  if( n == 0 ){
    return 1.;
  }

  if( n == 1 ){
    return MC;
  }

  if( n == 2){
	  return sqr(MC);
  }
  if( n >= 2 && !(n%2) ){
    vMcCormick<T> MC2;
    MC2._pts_sub( MC._nsub, MC._const, MC._npts );
    MC2._I = Op<T>::pow( MC._I, n );
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ int imid = -1;
		  double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
		  imid = -1;
		  MC2._cv[ipt] = std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ), n );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = n * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ), n-1 );
		}
		//concave
		{ int imid = -1;
		  double zmax = (std::pow( Op<T>::l(MC._I), n )>std::pow( Op<T>::u(MC._I), n )?
			Op<T>::l(MC._I): Op<T>::u(MC._I));
		  double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.: ( std::pow( Op<T>::u(MC._I),n ) - std::pow( Op<T>::l(MC._I), n ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) );
		  MC2._cc[ipt] = std::pow( zmax, n ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ) - zmax );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow1(MC, int)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.set_additional_values(0, &n);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.NPOW);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
  }

  if( n >= 3 && vMcCormick<T>::options.ENVEL_USE ){
    vMcCormick<T> MC2;
    MC2._pts_sub( MC._nsub, MC._const, MC._npts );
    MC2._I = Op<T>::pow( MC._I, n );
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ int imid = -1;
		  const double* cvenv = vMcCormick<T>::_oddpowcv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		  MC2._cv[ipt] = cvenv[0];
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		  }
		}
		//concave
		{ int imid = -1;
		  const double* ccenv = vMcCormick<T>::_oddpowcc( mid( MC._cv[ipt],	MC._cc[ipt], Op<T>::u(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		  MC2._cc[ipt] = ccenv[0];
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		  }
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow2(MC, int)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.set_additional_values(0, &n);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.NPOW);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();
  }

  if( n >= 3 ){
    return pow( MC, n-1 ) * MC;
  }

  if( n == -1 ){
    return inv( MC );
  }

  // without envelope
  if ( Op<T>::l(MC._I) <= 0. && Op<T>::u(MC._I) >= 0. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::INV );
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::pow( MC._I, n );

  if ( Op<T>::l(MC._I) > 0. ){
	 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ int imid = -1;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		  MC2._cv[ipt] = std::pow( vmid, n );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = n* mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::pow( vmid, n-1 );
		}
		//concave
		{ double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
		  for( int i=1; i<=-n-2; i++ )
			 r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
		  r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
		  int imid = -1;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid );
		  MC2._cc[ipt] = std::pow( Op<T>::l(MC._I), n ) + r * ( vmid - Op<T>::l(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = r * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		}
	 }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow3(MC, int)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
    return MC2.cut();
  }

  if( (-n)%2 ){
	 for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
		  for( int i=1; i<=-n-2; i++ )
			 r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
		  r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
		  int imid = -1;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		  MC2._cv[ipt] = std::pow( Op<T>::u(MC._I), n ) + r * ( vmid - Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = r * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		}
		//concave
		{ int imid = -1;
		  double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid );
		  MC2._cc[ipt] = std::pow( vmid, n );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = n* mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::pow( vmid, n-1 );
		}
	 }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow4(MC, int)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
    return MC2.cut();
  }

	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
	  { int imid = -1;
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid );
		MC2._cv[ipt] = std::pow( vmid, n );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = n* mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::pow( vmid, n-1 );
	  }
	  //concave
		{ double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
		  for( int i=1; i<=-n-2; i++ )
			 r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
		  r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
		int imid = -1;
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid );
		MC2._cc[ipt] = std::pow( Op<T>::u(MC._I), n ) + r * ( vmid - Op<T>::u(MC._I) );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = r * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
	  }
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow5(MC, int)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
pow
( const vMcCormick<T> &MC, const double a )
{
  if ( Op<T>::l(MC._I) < 0.)
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::DPOW ); // no negative values allowed

  if( a==0. ){
    return 1.;
  }

  if( a==1. ){
    return MC;
  }

  if( a<0. ){
    return inv( pow(MC,-a) ); // if the exponent is negative simply compute (MC^a)^-1
  }

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::pow(MC._I,a); // compute the correct interval

	if(isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))){ // in case of thin interval, simply use interval bounds
		for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
			MC2._cv[ipt] = Op<T>::l(MC2._I);
			MC2._cc[ipt] = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[ipt][i] = 0.;
				MC2._ccsub[ipt][i] = 0.;
			}
		}
		return MC2.cut();
	}

  if(a>1. && vMcCormick<T>::options.ENVEL_USE){ // for a > 1 the function is convex
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = -1; // convex
		  double zmin = Op<T>::l(MC._I);
		  MC2._cv[ipt] = std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ), a ); // convex relaxation
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = a * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ), a-1 );
		}
		{ int imid = -1; // concave
		  double zmax = Op<T>::u(MC._I);
		  double r = (std::pow( Op<T>::u(MC._I), a ) - std::pow( Op<T>::l(MC._I), a ) ) / // slope of concave relaxation
					 ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
		  MC2._cc[ipt] = std::pow( Op<T>::u(MC._I), a ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmax,imid ) - Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow(MC, double)";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.set_additional_values(&a, 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.DPOW);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();

  }

  if(a<1. && vMcCormick<T>::options.ENVEL_USE){ // for 0< a < 1 the function is concave
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = -1; //convex
		  double zmin = Op<T>::l(MC._I);
		  double r = (std::pow( Op<T>::u(MC._I), a ) - std::pow( Op<T>::l(MC._I), a ) ) / // slope of convex relaxation
					 ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
		  MC2._cv[ipt] = std::pow( Op<T>::l(MC._I), a ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], zmin,imid ) - Op<T>::l(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] =  mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
		}
		{ int imid = -1; //concave
		  double zmax = Op<T>::u(MC._I);
		  MC2._cc[ipt] = std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ), a );  // concave relaxation
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = a * mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid )* std::pow( mid( MC._cv[ipt], MC._cc[ipt], zmax, imid ), a-1 );
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pow(MC, double <1 )";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	// if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
		// vMcCormick<T>::additionalLins.set_additional_values(&a, 0);
		// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.DPOW);
		// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
																			// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
	    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
	// }
    return MC2.cut();

  }

  return exp( a * log( MC ) ); // if no envelope is required simply use exp(a*log(MC)) resulting in worse relaxations
                               // and not allowing 0. in interval since log(0.) is not defined
}

template <typename T> inline vMcCormick<T>
pow
( const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  return exp( MC2 * log( MC1 ) );
}

template <typename T> inline vMcCormick<T>
pow
( const double a, const vMcCormick<T> &MC )
{
  return exp( MC * std::log( a ) );
}

template <typename T> inline vMcCormick<T>
prod
( const unsigned int n, const vMcCormick<T>*MC )
{
  switch( n ){
   case 0:  return 1.;
   case 1:  return MC[0];
   default: return MC[0] * prod( n-1, MC+1 );
  }
}

template <typename T> inline vMcCormick<T>
monom
( const unsigned int n, const vMcCormick<T>*MC, const unsigned*k )
{
  switch( n ){
   case 0:  return 1.;
   case 1:  return pow( MC[0], (int)k[0] );
   default: return pow( MC[0], (int)k[0] ) * monom( n-1, MC+1, k+1 );
  }
}

template <typename T> inline vMcCormick<T>
cheb
( const vMcCormick<T> &MC, const unsigned n )
{
  if ( !isequal(Op<T>::l(MC._I),-1.) || !isequal(Op<T>::u(MC._I),1.) )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::CHEB );

  switch( n ){
    case 0:  return 1.;
    case 1:  return MC;
    case 2:  return 2*sqr(MC)-1;
    default: break;
  }

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::cheb( MC._I, n );
  if( !(n%2) ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ int imid = -1;
		  const double* cvenv = vMcCormick<T>::_evenchebcv( mid( MC._cv[ipt],MC._cc[ipt], Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		  MC2._cv[ipt] = cvenv[0];
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		  }
		}
		//concave
		{ MC2._cc[ipt] = 1.;
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[ipt][i] = 0.;
		}
	}
  }
  else{
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		//convex
		{ int imid = -1;
		  const double* cvenv = vMcCormick<T>::_oddchebcv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		  MC2._cv[ipt] = cvenv[0];
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		  }
		}
		//concave
		{ int imid = -1;
		  const double* ccenv = vMcCormick<T>::_oddchebcc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		  MC2._cc[ipt] = ccenv[0];
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		  }
		}
	}
  }
  //vMcCormick<T> MCcheb = 2.*MC*cheb(MC,n-1)-cheb(MC,n-2);
  //return( inter( MCcheb, MCcheb, vMcCormick<T>(T(-1.,1.)) )? MCcheb: vMcCormick<T>(T(-1.,1.))
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
fabs
( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::fabs( MC._I );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  //convex
	  { int imid = -1;
		double zmin = mid_ndiff( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
		imid = -1;
		double vmid = mid_ndiff( MC._cv[ipt], MC._cc[ipt], zmin, imid );
		MC2._cv[ipt] = std::fabs( vmid );
		if( vmid >= 0. )
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		else
		  for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._cvsub[ipt][i] = - mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
	  }
	  //concave
	  { int imid = -1;
		double zmax = (std::fabs( Op<T>::l(MC._I) )>std::fabs( Op<T>::u(MC._I) )? Op<T>::l(MC._I): Op<T>::u(MC._I));
		double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.: ( std::fabs( Op<T>::u(MC._I) )
		  - std::fabs( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) );
		MC2._cc[ipt] = std::fabs( zmax ) + r * ( mid_ndiff( MC._cv[ipt], MC._cc[ipt], zmax, imid ) - zmax );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
	  }
  }

  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.FABS);
  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
min
( const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  vMcCormick<T> MC3;
  if( MC2._const )
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
	  throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
    MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::min( MC1._I, MC2._I );

  if( Op<T>::u(MC1._I) <= Op<T>::l(MC2._I) ){
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cv[ipt] = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._cvsub[ipt][i] = (MC1._const? 0.: MC1._cvsub[ipt][i]);
	}
  }
  else if( Op<T>::u(MC2._I) <= Op<T>::l(MC1._I) ){
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cv[ipt] = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._cvsub[ipt][i] = (MC2._const? 0.: MC2._cvsub[ipt][i]);
	}
  }
  else if( vMcCormick<T>::options.MVCOMP_USE ){
     double minL1L2 = std::min( Op<T>::l(MC1._I), Op<T>::l(MC2._I) );
     double minL1U2 = std::min( Op<T>::l(MC1._I), Op<T>::u(MC2._I) );
     double minU1L2 = std::min( Op<T>::u(MC1._I), Op<T>::l(MC2._I) );
     double minU1U2 = std::min( Op<T>::u(MC1._I), Op<T>::u(MC2._I) );

     bool thin1 = isequal( Op<T>::diam(MC1._I), 0. );
     double r11 = ( thin1?  0.: ( minU1L2 - minL1L2 ) / Op<T>::diam(MC1._I) );
     double r21 = ( thin1?  0.: ( minL1U2 - minU1U2 ) / Op<T>::diam(MC1._I) );

     bool thin2 = isequal( Op<T>::diam(MC2._I), 0. );
     double r12 = ( thin2?  0.: ( minL1U2 - minL1L2 ) / Op<T>::diam(MC2._I) );
     double r22 = ( thin2?  0.: ( minU1L2 - minU1U2 ) / Op<T>::diam(MC2._I) );

	 for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		 double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
		 double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
		 double g1cv = minL1L2 + r11 * ( MC1_cv - Op<T>::l(MC1._I) )
							   + r12 * ( MC2_cv - Op<T>::l(MC2._I) );
		 double g2cv = minU1U2 - r21 * ( MC1_cv - Op<T>::u(MC1._I) )
							   - r22 * ( MC2_cv - Op<T>::u(MC2._I) );

		if(thin1 || thin2){
			MC3._cv[ipt] = g1cv;
			for( unsigned int i=0; i< MC3._nsub; i++ )
				MC3._cvsub[ipt][i] = (MC1._const? 0.: r11*MC1._cvsub[ipt][i])
								   + (MC2._const? 0.: r12*MC2._cvsub[ipt][i]);
		}
		else{
		 if( g1cv > g2cv ){
		   MC3._cv[ipt] = g1cv;
		   for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._cvsub[ipt][i] = (MC1._const? 0.: r11*MC1._cvsub[ipt][i])
						       + (MC2._const? 0.: r12*MC2._cvsub[ipt][i]);
		 }
		 else{
		   MC3._cv[ipt] = g2cv;
		   for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._cvsub[ipt][i] = - (MC1._const? 0.: r21*MC1._cvsub[ipt][i])
							     - (MC2._const? 0.: r22*MC2._cvsub[ipt][i]);
		 }
	   }
	} // end of for ipt
  }
  else{
    vMcCormick<T> MCMin = 0.5*( MC1 + MC2 - fabs( MC2 - MC1 ) );
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cv[ipt] = MCMin._cv[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._cvsub[ipt][i] = MCMin._cvsub[ipt][i];
	}
  }

  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
      double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	  double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
	  MC3._cc[ipt] = std::min( MC1_cc, MC2_cc );
	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt);  // It is ok to do it here, since at this point both relaxations are computed
	  for( unsigned int i=0; i< MC3._nsub; i++ ){
		MC3._ccsub[ipt][i] = ( (MC1_cc <= MC2_cc)  ? (MC1._const? 0.: MC1._ccsub[ipt][i]) : (MC2._const? 0.: MC2._ccsub[ipt][i]) );
		if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "min(MC,MC)";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return  MC3.cut();
}

template <typename T> inline vMcCormick<T>
max
( const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  vMcCormick<T> MC3;
  if( MC2._const )
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
	throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
    MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::max( MC1._I, MC2._I );

  if( Op<T>::u(MC1._I) <= Op<T>::l(MC2._I) ){
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cc[ipt] = MC2._const ? MC2._cc[0] : MC2._cc[ipt];				// Changed from ... = MC1._cc;, AVT.SVT on 04/04/16
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._ccsub[ipt][i] = (MC2._const? 0.: MC2._ccsub[ipt][i]);		// Changed from MC1. ..., AVT.SVT on 04/04/16
	}
  }
  else if( Op<T>::u(MC2._I) <= Op<T>::l(MC1._I) ){
	for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cc[ipt] = MC1._const ? MC1._cc[0] : MC1._cc[ipt];				// Changed from ... = MC2._cc;, AVT.SVT on 04/04/16
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._ccsub[ipt][i] = (MC1._const? 0.: MC1._ccsub[ipt][i]);		// Changed from MC2. ..., AVT.SVT on 04/04/16
	}
  }
  else if ( vMcCormick<T>::options.MVCOMP_USE ){
     double maxL1L2 = std::max( Op<T>::l(MC1._I), Op<T>::l(MC2._I) );
     double maxL1U2 = std::max( Op<T>::l(MC1._I), Op<T>::u(MC2._I) );
     double maxU1L2 = std::max( Op<T>::u(MC1._I), Op<T>::l(MC2._I) );
     double maxU1U2 = std::max( Op<T>::u(MC1._I), Op<T>::u(MC2._I) );

     bool thin1 = isequal( Op<T>::diam(MC1._I), 0., vMcCormick<T>::options.MVCOMP_TOL, vMcCormick<T>::options.MVCOMP_TOL);
     double r11 = ( thin1?  0.: ( maxU1L2 - maxL1L2 ) / Op<T>::diam(MC1._I) );
     double r21 = ( thin1?  0.: ( maxL1U2 - maxU1U2 ) / Op<T>::diam(MC1._I) );

     bool thin2 = isequal( Op<T>::diam(MC2._I), 0., vMcCormick<T>::options.MVCOMP_TOL, vMcCormick<T>::options.MVCOMP_TOL);
     double r12 = ( thin2?  0.: ( maxL1U2 - maxL1L2 ) / Op<T>::diam(MC2._I) );
     double r22 = ( thin2?  0.: ( maxU1L2 - maxU1U2 ) / Op<T>::diam(MC2._I) );

	 for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
	     double MC1_cc = MC1._const ? MC1._cc[0] : MC1._cc[ipt];
	     double MC2_cc = MC2._const ? MC2._cc[0] : MC2._cc[ipt];
		 double g1cc = maxL1L2 + r11 * ( MC1_cc - Op<T>::l(MC1._I) )
							   + r12 * ( MC2_cc - Op<T>::l(MC2._I) );
		 double g2cc = maxU1U2 - r21 * ( MC1_cc - Op<T>::u(MC1._I) )
							   - r22 * ( MC2_cc - Op<T>::u(MC2._I) );

		 if(thin1 || thin2){
			MC3._cc[ipt] = g2cc;
			for( unsigned int i=0; i< MC3._nsub; i++ )
			  MC3._ccsub[ipt][i] = - (MC1._const? 0.: r21*MC1._ccsub[ipt][i])
							       - (MC2._const? 0.: r22*MC2._ccsub[ipt][i]);
		 }
		 else{
			 if( g2cc > g1cc ){
			   MC3._cc[ipt] = g1cc;
			   for( unsigned int i=0; i< MC3._nsub; i++ )
				MC3._ccsub[ipt][i] = (MC1._const? 0.: r11*MC1._ccsub[ipt][i])
							       + (MC2._const? 0.: r12*MC2._ccsub[ipt][i]);
			 }
			 else{
			   MC3._cc[ipt] = g2cc;
			  for( unsigned int i=0; i< MC3._nsub; i++ )
				MC3._ccsub[ipt][i] = - (MC1._const? 0.: r21*MC1._ccsub[ipt][i])
								     - (MC2._const? 0.: r22*MC2._ccsub[ipt][i]);
			 }
		 }
	 } // end of for ipt
  }
  else{
    vMcCormick<T> MCMax = 0.5*( MC1 + MC2 + fabs( MC1 - MC2 ) );
    for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
		MC3._cc[ipt] = MCMax._cc[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._ccsub[ipt][i] = MCMax._ccsub[ipt][i];
	}
  }
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){

 	  double MC1_cv = MC1._const ? MC1._cv[0] : MC1._cv[ipt];
	  double MC2_cv = MC2._const ? MC2._cv[0] : MC2._cv[ipt];
	  MC3._cv[ipt] = std::max( MC1_cv, MC2_cv );
	  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt); // It is ok to do it here, since at this point both relaxations are computed
	  for( unsigned int i=0; i< MC3._nsub; i++ ){
		MC3._cvsub[ipt][i] = ( (MC1_cv >= MC2_cv) ? (MC1._const? 0.: MC1._cvsub[ipt][i]) : (MC2._const? 0.: MC2._cvsub[ipt][i]) );
		if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
	  }
      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
  }
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "max(MC,MC)";
	vMcCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return  MC3.cut();
}

///////////////////////////////////////////////////////////////////////////////////
// Added @ AVT.SVT, Aug 30, 2016
template <typename T> inline vMcCormick<T>
max
( const vMcCormick<T> &MC, const double a  ) {
	return max( a, MC );
}

template <typename T> inline vMcCormick<T>
max
( const double a, const vMcCormick<T> &MC ) {

	vMcCormick<T> MC2 = a;
	return max( MC, MC2 );
}

template <typename T> inline vMcCormick<T>
min
( const vMcCormick<T> &MC, const double a  ) {
	vMcCormick<T> MC2 = a;
	return min( MC, MC2 );
}

template <typename T> inline vMcCormick<T>
min
( const double a, const vMcCormick<T> &MC ) {
	vMcCormick<T> MC2 = a;
	return min( MC, MC2 );
}
///////////////////////////////////////////////////////////////////////////////////

template <typename T> inline vMcCormick<T>
min
( const unsigned int n, const vMcCormick<T>*MC )
{
  vMcCormick<T> MC2( n==0 || !MC ? 0.: MC[0] );
  for( unsigned int i=1; i<n; i++ ) MC2 = min( MC2, MC[i] );
  return MC2;
}

template <typename T> inline vMcCormick<T>
max
( const unsigned int n, const vMcCormick<T>*MC )
{
  vMcCormick<T> MC2( n==0 || !MC ? 0.: MC[0] );
  for( unsigned int i=1; i<n; i++ ) MC2 = max( MC2, MC[i] );
  return MC2;
}

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 27.06.2017
//Note that we only change the convex relaxation and the lower interval bound
template <typename T> inline vMcCormick<T>
pos
( const vMcCormick<T> &MC  ) {
	vMcCormick<T> MC2(MC);
	MC2._I = Op<T>::max(MC._I, vMcCormick<T>::options.MVCOMP_TOL);
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		if(MC._cc[ipt] < vMcCormick<T>::options.MVCOMP_TOL){
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::POS );
		}
		MC2._cv[ipt] = std::max(MC._cv[ipt], vMcCormick<T>::options.MVCOMP_TOL);
		for( unsigned int i=0; i< MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = ( MC._cv[ipt]>=vMcCormick<T>::options.MVCOMP_TOL? (MC._const? 0.: MC._cvsub[ipt][i]) : 0.);
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "pos";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	return MC2;
}
//////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 25.07.2017
//Note that we only change the concave relaxation and the upper interval bound
template <typename T> inline vMcCormick<T>
neg
( const vMcCormick<T> &MC  ) {
	vMcCormick<T> MC2(MC);
	MC2._I = Op<T>::min(MC._I, -vMcCormick<T>::options.MVCOMP_TOL);
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		if(MC._cv[ipt] > -vMcCormick<T>::options.MVCOMP_TOL){
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::NEGAT );
		}
		MC2._cc[ipt] = std::min(MC._cc[ipt], -vMcCormick<T>::options.MVCOMP_TOL);
		for( unsigned int i=0; i< MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = ( MC._cc[ipt]<=-vMcCormick<T>::options.MVCOMP_TOL? (MC._const? 0.: MC._ccsub[ipt][i]): 0. );
		}
	}
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "neg";
	vMcCormick<T>::_debug_check(MC, MC2, str);
#endif
	return MC2;
}
//////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 28.09.2017
//Note that we only change the convex relaxation and the upper interval bound
template <typename T> inline vMcCormick<T>
lb_func
( const vMcCormick<T> &MC1, const double lb  ) {

#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "lb_func";
	vMcCormick<T> dummy(lb,MC1._npts);
	vMcCormick<T>::_debug_check(MC1,dummy,str);
#endif
    vMcCormick<T> MC2(MC1);
    MC2._I = Op<T>::max( MC1._I, lb);
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		if(MC1._cc[ipt] < lb && !isequal(MC1._cc[ipt], lb, vMcCormick<T>::options.MVCOMP_TOL, vMcCormick<T>::options.MVCOMP_TOL)){
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::LB_FUNC );
		}
		MC2._cv[ipt] = std::max(MC1._cv[ipt],lb);
		for( unsigned int i=0; i< MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = ( MC1._cv[ipt]>lb? (MC1._const? 0.: MC1._cvsub[ipt][i]) : 0.);
		}
	}
	return MC2.cut();
}

//Note that we only change the concave relaxation and the upper interval bound
template <typename T> inline vMcCormick<T>
ub_func
( const vMcCormick<T> &MC1, const double ub  ) {
#ifdef MC__VMCCORMICK_DEBUG
	std::string str = "ub_func";
	vMcCormick<T> dummy(ub,MC1._npts);
	vMcCormick<T>::_debug_check(MC1,dummy,str);
#endif
    vMcCormick<T> MC2(MC1);
    MC2._I = Op<T>::min( MC1._I, ub);
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		if(MC1._cv[ipt] > ub && !isequal(MC1._cv[ipt], ub, vMcCormick<T>::options.MVCOMP_TOL, vMcCormick<T>::options.MVCOMP_TOL)){
			throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::UB_FUNC );
		}
		MC2._cc[ipt] = std::min(MC1._cc[ipt],ub);
		for( unsigned int i=0; i< MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = ( MC1._cc[ipt]<ub? (MC1._const? 0.: MC1._ccsub[ipt][i]) : 0.);
		}
	}
	return MC2.cut();
}

template <typename T> inline vMcCormick<T>
bounding_func
( const vMcCormick<T> &MC, const double lb, const double ub  ) {
    vMcCormick<T> MC2 = lb_func(MC,lb);
	return ub_func(MC2,ub);
}


//Note that we don't throw any exception here, since when using this function, the user has to make sure (through linear inequalities) that x is only feasible for [lb,ub]
template <typename T> inline vMcCormick<T>
squash_node
( const vMcCormick<T> &MC, const double lb, const double ub  ) {

    vMcCormick<T> MC2(MC);
    MC2._I = Op<T>::squash_node(MC._I, lb, ub);
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = MC2._cv[ipt]>ub ? lb : std::max(MC._cv[ipt],lb);
		MC2._cc[ipt] = MC2._cc[ipt]<lb ? ub : std::min(MC._cc[ipt],ub);
		for( unsigned int i=0; i< MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = ( MC._cv[ipt]>lb ? (MC._const? 0.: MC._cvsub[ipt][i]) : 0.);
			MC2._ccsub[ipt][i] = ( MC._cc[ipt]<ub ? (MC._const? 0.: MC._ccsub[ipt][i]) : 0.);
		}
	}
	return MC2;
}

// AVT.SVT 10.07.2018
template <typename T> inline vMcCormick<T>
sum_div
( const std::vector< vMcCormick<T> > &MC, const std::vector<double> &coeff)
{
	vMcCormick<T> MC3;
	// check for possible subgradient exceptions
	bool all_const = true;
	std::vector<unsigned int> not_const = {};
	for(unsigned int i=0; i<MC.size();i++){
	  if( !MC[i]._const ){
		not_const.push_back(i);
		all_const = false;
	  }
	}
	if(all_const){
		MC3._pts_sub(MC[0]._nsub, MC[0]._const, MC[0]._npts);
	}else{
		for(unsigned int i = 0; i<not_const.size()-1;i++){
			if(MC[not_const[i]]._nsub != MC[not_const[i+1]]._nsub){
				throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
			}
			if(MC[not_const[i]]._npts != MC[not_const[i+1]]._npts){
				throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
			}
		}
		MC3._pts_sub(MC[not_const[0]]._nsub, MC[not_const[0]]._const, MC[not_const[0]]._npts);
	}

	std::vector<T> MCI(MC.size());
	for(unsigned int i=0;i<MC.size();i++){
		// check positivity of each variable
		 if ( Op<T>::l(MC[i]._I) <= 0. ){
			 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUM_DIV_VAR );
		 }
		 // save intervals
		 MCI[i] = MC[i]._I;
		 // check positivity of coefficients
		 if ( coeff[i] <= 0. ){
			 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUM_DIV_COEFF );
		 }
	}
	// check last coefficient
	if ( coeff[coeff.size()-1] <= 0. ){
		throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUM_DIV_COEFF );
	}

	// compute interval extensions
	MC3._I=Op<T>::sum_div(MCI,coeff);
    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates();

	// auxiliary variables to avoid re-computations
	double xL=Op<T>::l(MC[0]._I);
	double xU=Op<T>::u(MC[0]._I);
	double s2 = std::pow(std::sqrt(xL) + std::sqrt(xU), 2); 		// (sqrt(xL) + sqrt(xU))^2
	std::vector<double> yL(MC.size()-1);
	std::vector<double> yU(MC.size()-1);
	for(unsigned int i =1;i<MC.size();i++){
		yL[i-1] = Op<T>::l(MC[i]._I);
		yU[i-1] = Op<T>::u(MC[i]._I);
	}
	bool is_cvenvelope = true;										// under certain conditions, we only get the convex envelope if we compute additional affine relaxation,
																	// this is expensive in higher dimensions and the gain is not high in these cases,
																	// since we still have the exact range bounds, so just stick to the classic McCormick
    double s1,s3,scv4,scv5,scc4,scc5;								// auxiliary variables
	double l1=coeff[1]*std::sqrt(xL)*std::sqrt(xU),l2=coeff[1]*std::sqrt(xL)*std::sqrt(xU);
	for(unsigned int i=0;i<MC.size()-1;i++){
		l1 -= 2*coeff[i+2]*yL[i];
		l2 -= 2*coeff[i+2]*yU[i];
	}
	if((l1/coeff[1]>xL && l1/coeff[1]<xU) || ( l2/coeff[2]>xL || l2/coeff[2]<xU) ){ is_cvenvelope = false;}

	if(is_cvenvelope){
		for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
			s1 = MC[0]._cv[ipt] + std::sqrt(xL)*std::sqrt(xU);			 // (xcv + sqrt(xL)*sqrt(xU))
			s3 = std::pow(MC[0]._cv[ipt]+std::sqrt(xL)*std::sqrt(xU), 2);// (x + sqrt(xL)*sqrt(xU))^2
			scv4 = coeff[1]*MC[0]._cv[ipt];								 // c*xcv
			for(unsigned int i =1;i<MC.size();i++){
				double MCi_cc = MC[i]._const ? MC[i]._cc[0] : MC[i]._cc[ipt];
				scv4 += coeff[i+1]*MCi_cc;						 // c*xcv + d1*y1cc + d2*y2cc + ...
			}
			scv5 = std::pow(scv4, 2);									 // (c*xcv + d1*y1cc + d2*y2cc + ...)^2
			// convex relaxation   a*(xcv + sqrt(xL)*sqrt(xU))^2 / ( (sqrt(xL)+sqrt(xU))^2 * (c*xcv + d1*y1cc + d2*y2cc + ... )
			MC3._cv[ipt] = (coeff[0]*std::pow(s1,2))/(s2*scv4);
			// subgradient convex
			for(unsigned int i = 0; i< MC3._nsub;i++){
				// subgradient convex first term
				MC3._cvsub[ipt][i] = (2*s1/(s2*scv4) - coeff[1]*s3/(s2*scv5) ) *(MC[0]._const? 0.:MC[0]._cvsub[ipt][i]); // for x we take cv
				// subgradient convex following terms
				for(unsigned int j = 1; j< MC.size();j++){
					MC3._cvsub[ipt][i] -= coeff[j+1]*s3/(s2*scv5)*(MC[j]._const? 0.:MC[j]._ccsub[ipt][i]); 			   // for everything else cc
				}
				MC3._cvsub[ipt][i] *= coeff[0];
			}
		}//end for-loop over _npts
	} // not the convex envelope
	else{
		vMcCormick<T> MC1(coeff[0]*MC[0]);
		vMcCormick<T> MC2(coeff[1]*MC[0]);
		for(unsigned int i=1; i< MC.size();i++){
			MC2 += coeff[i+1]*MC[i];
		}
        // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.reset_candidates(); // Have to reset, since we used + to compute MC2
		for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
			// use convex envelope for x/y
			int imidcv1 = 1, imidcv2 = 2; // We can easily solve the convex problem given in Theorem 2 in Tsoukalas & Mitsos 2014,
										  // since the minimum of the convex relaxation is at point f1cv, f2cc, which can be shown through directional derivatives
			double fmidcv1 = ( mid(MC1._cv[ipt], MC1._cc[ipt], Op<T>::l(MC1._I), imidcv1)+ std::sqrt(Op<T>::l(MC1._I) * Op<T>::u(MC1._I)) )/ ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) );
			double fmidcv2 = mid(MC2._cv[ipt], MC2._cc[ipt], Op<T>::u(MC2._I), imidcv2);
			MC3._cv[ipt] = mc::sqr(fmidcv1) / fmidcv2;
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._cvsub[ipt][i] = 2. * fmidcv1 / fmidcv2 / ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) )* (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcv1 ))
									- mc::sqr( fmidcv1 / fmidcv2 )* (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcv2 ));
			}
		}//end for-loop over _npts
	}

	bool is_ccenvelope = true;										// under certain conditions, we only get the concave envelope if we compute additional affine relaxation,
																	// this is expensive in higher dimensions (much more expensive than for convex) and the gain is not high in these cases,
																	// since we have exact range bounds, so just stick to the classic McCormick
	if(MC.size()==2){ // for 2-dim case, the formula differs slightly
		double p1 = (coeff[2]*std::sqrt(yL[0]*yU[0])-2*coeff[1]*xL)/coeff[2];
		double p2 = (coeff[2]*std::sqrt(yL[0]*yU[0])-2*coeff[1]*xU)/coeff[2];
		if((p1>yL[0] && p1<yU[0]) || ( p2>yL[0] && p2<yU[0]) ){ is_ccenvelope = false;}
	}
	else{	// check if the maximum of the concave relaxation is within bounds => then it's not the envelope anymore
		for(unsigned int i = 1; i<MC.size() && is_ccenvelope;i++){
			double l1 = coeff[1]*xU+coeff[i+1]*yL[i-1], l2 = coeff[1]*xL+coeff[i+1]*yU[i-1];
			double k1 = std::pow(coeff[1]*xU,2) + std::pow(coeff[i+1],2)*yL[i-1]*yU[i-1]-2*coeff[1]*coeff[i+1]*xU*std::sqrt(yL[i-1]*yU[i-1]);
			double k2 = std::pow(coeff[1]*xL,2) + std::pow(coeff[i+1],2)*yL[i-1]*yU[i-1]-2*coeff[1]*coeff[i+1]*xL*std::sqrt(yL[i-1]*yU[i-1]);
			double s1=0,s2=0;
			for(unsigned int j = 1; j<MC.size(); j++){
				if(j!=i){
					l1 += coeff[j+1]*yL[j-1]; l2 += coeff[j+1]*yU[j-1];
					s1 += std::pow(coeff[j+1]*yL[j-1],2) + (2*coeff[1]*xU*coeff[j+1] + coeff[i+1]*coeff[j+1]*(yL[i-1] +yU[i-1]))*yL[j-1];
					s2 += std::pow(coeff[j+1]*yU[j-1],2) + (2*coeff[1]*xL*coeff[j+1] + coeff[i+1]*coeff[j+1]*(yL[i-1] +yU[i-1]))*yU[j-1];
				}
				double mix1=0,mix2=0;
				for(unsigned int k=j+1;k<MC.size();k++){
					if(k!=i){
						mix1 += 2*coeff[j+1]*coeff[k+1]*yL[j-1]*yL[k-1];
						mix2 += 2*coeff[j+1]*coeff[k+1]*yU[j-1]*yU[k-1];
					}
				}
				s1 += mix1; s2 += mix2;
			}
			double res1 = -(l1-std::sqrt(s1))/coeff[i+1] ,res2 = -(l2-std::sqrt(s2))/coeff[i+1];
			if((res1>yL[i-1] && res1<yU[i-1]) || ( res2>yL[i-1] && res2<yU[i-1]) ){ is_ccenvelope = false;}
		}
	}
	if(is_ccenvelope){
		for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
			scc4 = coeff[1]*MC[0]._cc[ipt];									// c*xcc
			for(unsigned int i =1;i<MC.size();i++){
				double MCi_cv = MC[i]._const ? MC[i]._cv[0] : MC[i]._cv[ipt];
				scc4 += coeff[i+1]*MCi_cv;							// c*xcc + d1*y1cv + d2*y2cv + ...
			}
			scc5 = std::pow(scc4, 2);										// (c*xcc + d1*y1cv + d2*y2cv + ...)^2
			// concave relaxation a*(xcc/(c*xcc + d1*y1cv + ...) + d1*(yL1-y1cv)*(y1cv - yU1)/(c*(sqrt(yL1)+sqrt(yU1)^2 * (c*xcc + d1*y1cv + ...)) + d2*(yL2-y2cv)*(y2cv - yU2)/(c*(sqrt(yL2)+sqrt(yU2)^2 * (c*xcc + d1*y2cv + ...)) + ...
			MC3._cc[ipt]= MC[0]._cc[ipt] / scc4;
			for(unsigned int i = 1; i < MC.size(); i++){
				double MCi_cv = MC[i]._const ? MC[i]._cv[0] : MC[i]._cv[ipt];
				MC3._cc[ipt] += coeff[i+1]*(yL[i-1] - MCi_cv)*(MCi_cv - yU[i-1])/( coeff[1]*std::pow( std::sqrt(yL[i-1])+std::sqrt(yU[i-1]), 2) * scc4 );
			}
			MC3._cc[ipt] *= coeff[0];
	        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt);
			// subgradient concave
			for( unsigned int i=0; i<MC3._nsub; i++ ){
				double temp = 0;
				temp += 1/scc4 - coeff[1]*MC[0]._cc[ipt]/scc5;
				for(unsigned int j = 1; j< MC.size();j++){
					double MCj_cv = MC[j]._const ? MC[j]._cv[0] : MC[j]._cv[ipt];
					temp += coeff[j+1]*(MCj_cv - yL[j-1])*(MCj_cv - yU[j-1])/( std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2) * scc5 );
				}

				MC3._ccsub[ipt][i] = coeff[0]*temp*(MC[0]._const? 0.:MC[0]._ccsub[ipt][i]);		// for x we take cc
				for(unsigned int j = 1; j< MC.size();j++){
					temp = 0;
					double MCj_cv = MC[j]._const ? MC[j]._cv[0] : MC[j]._cv[ipt];
					temp += coeff[j+1]*(MCj_cv - yL[j-1])*(MCj_cv - yU[j-1])/( coeff[1]*std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2) * scc5 )
							- ((MCj_cv - yL[j-1]) + (MCj_cv - yU[j-1]))/(coeff[1]*std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2)*scc4)
							- MC[0]._cc[ipt]/scc5;
					for(unsigned int k = 1; k < MC.size();k++){
						if(j != k){
							double MCk_cv = MC[k]._const ? MC[k]._cv[0] : MC[k]._cv[ipt];
							temp += coeff[k+1]*(MCk_cv - yL[k-1])*(MCk_cv - yU[k-1])/( coeff[1]*std::pow( std::sqrt(yL[k-1])+std::sqrt(yU[k-1]), 2) * scc5 );
						}
					}
					MC3._ccsub[ipt][i] += coeff[0]*coeff[j+1]*temp*(MC[j]._const? 0.:MC[j]._cvsub[ipt][i]);	// for everything else cv
				}
		        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i],MC3._ccsub[ipt][i], ipt, i);
			} // end of for-loop over _nsub
            if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
		}//end for-loop over _npts
	} // not concave envelope
	else{
		vMcCormick<T> MC1(coeff[0]*MC[0]);
		vMcCormick<T> MC2(coeff[1]*MC[0]);
		for(unsigned int i=1; i< MC.size();i++){
			MC2 += coeff[i+1]*MC[i];
		}

		for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
			int imidcc1 = -1, imidcc2 = -1;
			// use concave envelope for x/y
			double fmidcc1 = mid_ndiff(MC1._cv[ipt], MC1._cc[ipt], Op<T>::u(MC1._I), imidcc1);
			double fmidcc2 = mid_ndiff(MC2._cv[ipt], MC2._cc[ipt], Op<T>::l(MC2._I), imidcc2);
			double gcc1 = Op<T>::u(MC2._I) * fmidcc1 - Op<T>::l(MC1._I) * fmidcc2 + Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
			double gcc2 = Op<T>::l(MC2._I) * fmidcc1 - Op<T>::u(MC1._I) * fmidcc2 + Op<T>::u(MC1._I) * Op<T>::u(MC2._I);

			if(  gcc1 <= gcc2  ){ //uses equation (31) in multivariate McCormick paper
			  MC3._cc[ipt] = gcc1 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
	          if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt],MC3._cc[ipt], ipt);
			  for( unsigned int i=0; i<MC3._nsub; i++ ){
				MC3._ccsub[ipt][i] = 1. / Op<T>::l(MC2._I) * (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcc1 ))
									- Op<T>::l(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) ) * (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcc2 ));
		        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i],MC3._ccsub[ipt][i], ipt, i);
			  }
			}
			else{
			  MC3._cc[ipt] = gcc2 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
	          if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt],MC3._cc[ipt], ipt);
			  for( unsigned int i=0; i<MC3._nsub; i++ ){
				MC3._ccsub[ipt][i] = 1. / Op<T>::u(MC2._I) * (MC1._const? 0.: mid( MC1._cvsub[ipt], MC1._ccsub[ipt], i, imidcc1 ))
									- Op<T>::u(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) ) * (MC2._const? 0.: mid( MC2._cvsub[ipt], MC2._ccsub[ipt], i, imidcc2 ));
		        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i],MC3._ccsub[ipt][i], ipt, i);
			  }
			}
            if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
		}//end for-loop over _npts
	}
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "sum_div";
	vMcCormick<T>::_debug_check(MC, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

// AVT.SVT 10.07.2018
template <typename T> inline vMcCormick<T>
xlog_sum
( const std::vector< vMcCormick<T> > &MC, const std::vector<double> &coeff)
{
	vMcCormick<T> MC3;
	// check for possible subgradient exceptions
	bool all_const = true;
	std::vector<unsigned int> not_const = {};
	for(unsigned int i=0; i<MC.size();i++){
	  if( !MC[i]._const ){
		not_const.push_back(i);
		all_const = false;
	  }
	}
	if(all_const){
		MC3._pts_sub(MC[0]._nsub, MC[0]._const, MC[0]._npts);
	}else{
		for(unsigned int i = 0; i<not_const.size()-1;i++){
			if(MC[not_const[i]]._nsub != MC[not_const[i+1]]._nsub){
				throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
			}
			if(MC[not_const[i]]._npts != MC[not_const[i+1]]._npts){
				throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
			}
		}
		MC3._pts_sub(MC[not_const[0]]._nsub, MC[not_const[0]]._const, MC[not_const[0]]._npts);
	}

	// Special case x*log(a*x), it is anologous to xlog, the only difference is in zmin and the derivative
	if(MC.size()==1){
		// check positivity of each variable
		if ( Op<T>::l(MC[0]._I) <= 0. ){
		 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::XLOG_SUM_VAR );
		}
		// check positivity of coefficients
		if ( coeff[0] <= 0. ){
		 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::XLOG_SUM_COEFF );
		}
		std::vector<T> interval = {MC[0]._I};
		MC3._I = Op<T>::xlog_sum(interval,coeff);

		for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
			int imid = -1;
			double zmin = mid( Op<T>::l(MC[0]._I), Op<T>::u(MC[0]._I), std::exp(-1.)/coeff[0], imid );
			imid = -1;
			double vmid = mid( MC[0]._cv[ipt], MC[0]._cc[ipt], zmin, imid );
			MC3._cv[ipt] = vmid * std::log(coeff[0]*vmid);
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._cvsub[ipt][i] = (std::log(coeff[0]* vmid ) + 1.) * mid( MC[0]._cvsub[ipt], MC[0]._ccsub[ipt], i, imid );
			}

			imid = -1;
			double valL = Op<T>::l(MC[0]._I)*std::log(coeff[0]*Op<T>::l(MC[0]._I));
			double valU = Op<T>::u(MC[0]._I)*std::log(coeff[0]*Op<T>::u(MC[0]._I));
			double zmax = ( valU >= valL)? Op<T>::u(MC[0]._I): Op<T>::l(MC[0]._I) ;
			double r, pt;
			if( isequal( Op<T>::l(MC[0]._I), Op<T>::u(MC[0]._I) )){
				r = 0.;
				pt = zmax;
			}
			else {
				r = ( valU - valL ) / ( Op<T>::u(MC[0]._I) - Op<T>::l(MC[0]._I) );
				pt = Op<T>::l(MC[0]._I);
			}
			imid = -1;
			MC3._cc[ipt] = pt*std::log(coeff[0]*pt) + r * ( mid( MC[0]._cv[ipt], MC[0]._cc[ipt], zmax, imid ) - pt );
			for( unsigned int i=0; i<MC3._nsub; i++ )
			  MC3._ccsub[ipt][i] = mid( MC[0]._cvsub[ipt], MC[0]._ccsub[ipt], i, imid ) * r;
		} // end of for loop over _npts
#ifdef MC__VMCCORMICK_DEBUG
			std::string str = "xlog_sum 1 dim";
			vMcCormick<T>::_debug_check(MC[0], MC3, str);
#endif
          // For univariate case, we do not need the heuristic
		  return MC3.cut();
	}

	// In the case of thin X interval, we simply return the normal McCormick relaxation. We do this to avoid numerical problems in _newton
	if(isequal(Op<T>::l(MC[0]._I),Op<T>::u(MC[0]._I))){
		MC3 = 0;
		for(size_t i = 0;i<MC.size();i++){
			MC3 += coeff[i]*MC[i];
		}
		MC3 = MC[0]*log(MC3);
		return MC3.cut();
	}

	// Dimension > 1
	std::vector<T> MCI(MC.size());
	std::vector<double> intervalLowerBounds(MC.size());
	std::vector<double> intervalUpperBounds(MC.size());
	std::vector<std::vector<double>> pointCv(MC3._npts,std::vector<double>(MC.size()));
	std::vector<std::vector<double>> pointCc(MC3._npts,std::vector<double>(MC.size()));
	std::vector<double> cvDerivativesFacet1(MC.size(),0); // Since the derivatives w.r.t. dimensions >=1 are constant, we can compute them only once instead of recomputing them for every point
	std::vector<double> cvDerivativesFacet2(MC.size(),0); // This is especially useful if we have many points and/or many subgradients
	std::vector<double> ccDerivativesFacet1(MC.size(),0);
	std::vector<double> ccDerivativesFacet2(MC.size(),0);
	for(unsigned int i=0;i<MC.size();i++){
		// check positivity of each variable
		 if ( Op<T>::l(MC[i]._I) <= 0. ){
			 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::XLOG_SUM_VAR );
		 }
		 // check positivity of coefficients
		 if ( coeff[i] <= 0. ){
			 throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::XLOG_SUM_COEFF );
		 }
		 // save intervals
		 MCI[i] = MC[i]._I;
		 intervalLowerBounds[i] = Op<T>::l(MC[i]._I);
		 intervalUpperBounds[i] = Op<T>::u(MC[i]._I);
		 for(unsigned int ipt = 0; ipt<MC3._npts;ipt++){
			 pointCv[ipt][i] = MC[i]._cv[ipt];
			 pointCc[ipt][i] = MC[i]._cc[ipt];
		 }
	}
	for(unsigned int i=1;i<MC.size();i++){
		cvDerivativesFacet1[i] = (MC.size()<3)?vMcCormick<T>::_xlog_sum_cv_partial_derivative(pointCv[0],coeff,intervalLowerBounds,intervalUpperBounds,1,i):0; // For dimensions >2 we currently don't know the correct facet
		cvDerivativesFacet2[i] = (MC.size()<3)?vMcCormick<T>::_xlog_sum_cv_partial_derivative(pointCv[0],coeff,intervalLowerBounds,intervalUpperBounds,2,i):0;
		ccDerivativesFacet1[i] = vMcCormick<T>::_xlog_sum_cc_partial_derivative(pointCc[0],coeff,intervalLowerBounds,intervalUpperBounds,1,i);
		ccDerivativesFacet2[i] = vMcCormick<T>::_xlog_sum_cc_partial_derivative(pointCc[0],coeff,intervalLowerBounds,intervalUpperBounds,2,i);
	}
	// compute interval extensions
	MC3._I=Op<T>::xlog_sum(MCI,coeff);
	// convex is currently not possible if dimension > 2, so we simply use the normal McCormick relaxations
	if(MC.size()>2){
		vMcCormick<T> MCtmp(0);
		for(size_t i = 0;i<MC.size();i++){
			MCtmp += coeff[i]*MC[i];
		}
		MCtmp = MC[0]*log(MCtmp);
		for(unsigned int ipt = 0; ipt<MC3._npts;ipt++){
			const double* ccrel = vMcCormick<T>::_xlog_sum_cc(pointCc[ipt],MC[0]._cv[ipt],coeff,intervalLowerBounds,intervalUpperBounds);
			MC3._cc[ipt] = ccrel[0];
			MC3._cv[ipt] = MCtmp._cv[ipt];
	        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt);
			// Subgradients
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  double subgradientCc = 0;
			  subgradientCc += vMcCormick<T>::_xlog_sum_cc_partial_derivative(pointCc[ipt],coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],0)
										   *(MC[0]._cv[ipt]==pointCc[ipt][0]?MC[0]._cvsub[ipt][i]: MC[0]._ccsub[ipt][i]);
			  for(unsigned int j = 1; j<MC.size();j++){
					subgradientCc += ((ccrel[1]==1)?ccDerivativesFacet1[j]:ccDerivativesFacet2[j])* (MC[j]._const? 0.:MC[j]._ccsub[ipt][i]); // in all y directions, the relaxation is monotonically decreasing
			  }
			  MC3._cvsub[ipt][i] = MCtmp._cvsub[ipt][i];
			  MC3._ccsub[ipt][i] = subgradientCc;
		      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
			}
            if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
		} // end of for-loop over _npts
	}
	else{ // MC.size() == 2
		for(unsigned int ipt = 0; ipt<MC3._npts;ipt++){
			// Compute convex and concave relaxations and get the facet id
			const double* cvrel = vMcCormick<T>::_xlog_sum_cv(pointCv[ipt],MC[0]._cc[ipt],coeff,intervalLowerBounds,intervalUpperBounds,pointCc[ipt]);
			MC3._cv[ipt] = cvrel[0];
			const double* ccrel = vMcCormick<T>::_xlog_sum_cc(pointCc[ipt],MC[0]._cv[ipt],coeff,intervalLowerBounds,intervalUpperBounds);
			MC3._cc[ipt] = ccrel[0];
	        if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.set_lower_upper_bound_value(MC3._cv[ipt], MC3._cc[ipt], ipt);

			// Subgradients
			for( unsigned int i=0; i<MC3._nsub; i++ ){
			  double subgradientCv = 0;
			  double subgradientCc = 0;
			  if(MC[0]._cv[ipt] < pointCv[ipt][0] && pointCv[ipt][0] < MC[0]._cc[ipt]){
				// we are at the minimum w.r.t. x so we would have to multiply with 0 meaning that we simply add nothing
			  }
			  else{
				subgradientCv += vMcCormick<T>::_xlog_sum_cv_partial_derivative(pointCv[ipt],coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)cvrel[1],0)
									*(MC[0]._cv[ipt]==pointCv[ipt][0]?MC[0]._cvsub[ipt][i]: MC[0]._ccsub[ipt][i]);
			  }

			  subgradientCc += vMcCormick<T>::_xlog_sum_cc_partial_derivative(pointCc[ipt],coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],0)
										   *(MC[0]._cv[ipt]==pointCc[ipt][0]?MC[0]._cvsub[ipt][i]: MC[0]._ccsub[ipt][i]);
			  for(unsigned int j = 1; j<MC.size();j++){
					subgradientCv += ((cvrel[1]==1)?cvDerivativesFacet1[j]:cvDerivativesFacet2[j])* (MC[j]._const? 0.:MC[j]._cvsub[ipt][i]); // in all y directions, the relaxation is monotonically increasing
					subgradientCc += ((ccrel[1]==1)?ccDerivativesFacet1[j]:ccDerivativesFacet2[j])* (MC[j]._const? 0.:MC[j]._ccsub[ipt][i]); // in all y directions, the relaxation is monotonically decreasing
			  }
			  MC3._cvsub[ipt][i] = subgradientCv;
			  MC3._ccsub[ipt][i] = subgradientCc;
		      if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.add_to_lower_upper_bound_values(MC3._cvsub[ipt][i], MC3._ccsub[ipt][i], ipt, i);
			}
            if(vMcCormick<T>::options.SUB_INT_HEUR_USE) vMcCormick<T>::subHeur.update_best_values(ipt);
		} // end of for loop over _npts
	} // end of if MC.size() > 2
#ifdef MC__VMCCORMICK_DEBUG
  	std::string str = "xlog_sum";
	vMcCormick<T>::_debug_check(MC, MC3, str);
#endif
  if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

template <typename T> inline vMcCormick<T>
mc_print
( const vMcCormick<T> &MC, const int number){
	std::cout << "vMcCormick relaxation #" << number << ": " << MC << std::endl;
	return MC;
}
//////////////////////////////////////////////////////////////////////////////////

template <typename T> inline vMcCormick<T>
fstep
( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  if( Op<T>::l( MC._I ) >= 0 )
    MC2._I = 1.;
  else if( Op<T>::u( MC._I ) < 0 )
    MC2._I = 0.;
  else
    MC2._I = Op<T>::zeroone();

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		double zmin = Op<T>::l(MC._I);
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], zmin, imid );
		const double* cvenv = vMcCormick<T>::_stepcv( vmid, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid )*cvenv[1];
	  }

	  { int imid = -1;
		double zmax = Op<T>::u(MC._I);
		double vmid = mid( MC._cv[ipt], MC._cc[ipt], zmax, imid );
		const double* ccenv = vMcCormick<T>::_stepcc( vmid, Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid )*ccenv[1];
	  }
  }
  return  MC2.cut();
}

template <typename T> inline vMcCormick<T>
bstep
( const vMcCormick<T> &MC )
{
  return fstep( -MC );
}

template <typename T> inline vMcCormick<T>
ltcond
( const T &I0, const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  if( Op<T>::u( I0 ) < 0. )       return MC1;
  else if( Op<T>::l( I0 ) >= 0. ) return MC2;

  vMcCormick<T> MC3;
  if( MC2._const )
    MC3._pts_sub( MC1._nsub, MC1._const, MC1._npts );
  else if( MC1._const )
    MC3._pts_sub( MC2._nsub, MC2._const, MC2._npts );
  else if( MC1._nsub != MC2._nsub )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  else if( MC1._npts != MC2._npts )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );
  else
    MC3._pts_sub( MC1._nsub, MC1._const||MC2._const, MC1._npts );

  MC3._I = Op<T>::hull( MC1._I, MC2._I );
  vMcCormick<T> MCMin  = 0.5*( MC1 + MC2 - fabs( MC2 - MC1 ) );
  vMcCormick<T> MCMax  = 0.5*( MC1 + MC2 + fabs( MC1 - MC2 ) );
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
	  MC3._cv[ipt] = MCMin._cv[ipt];
	  MC3._cc[ipt] = MCMax._cc[ipt];
	  for( unsigned int i=0; i< MC3._nsub; i++ ){
		MC3._cvsub[ipt][i] = MCMin._cvsub[ipt][i];
		MC3._ccsub[ipt][i] = MCMax._ccsub[ipt][i];
	  }
  }
  return  MC3.cut();
}

template <typename T> inline vMcCormick<T>
ltcond
( const vMcCormick<T> &MC0, const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  vMcCormick<T> MC3 = ltcond( MC0._I, MC1, MC2 );
  vMcCormick<T> MCStep = fstep(-MC0)*MC1 + fstep(MC0)*MC2;
  for(unsigned int ipt=0;ipt<MC3._npts;ipt++){
	  if( MCStep._cv[ipt] > MC3._cv[ipt] ){
		MC3._cv[ipt] = MCStep._cv[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._cvsub[ipt][i] = MCStep._cvsub[ipt][i];
	  }
	  if( MCStep._cc[ipt] < MC3._cc[ipt] ){
		MC3._cc[ipt] = MCStep._cc[ipt];
		for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._ccsub[ipt][i] = MCStep._ccsub[ipt][i];
	  }
  }
  return  MC3.cut();
}

template <typename T> inline vMcCormick<T>
gtcond
( const T &I0, const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  return ltcond( -I0, MC1, MC2 );
}

template <typename T> inline vMcCormick<T>
gtcond
( const vMcCormick<T> &MC0, const vMcCormick<T> &MC1, const vMcCormick<T> &MC2 )
{
  return ltcond( -MC0, MC1, MC2 );
}

template <typename T> inline vMcCormick<T>
cos
( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::cos( MC._I );

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

  double*argbnd = vMcCormick<T>::_cosarg( Op<T>::l(MC._I), Op<T>::u(MC._I) );
  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_coscv( mid( MC._cv[ipt], MC._cc[ipt], argbnd[0], imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i< MC2._nsub; i++ ){
		   MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_coscc( mid( MC._cv[ipt], MC._cc[ipt], argbnd[1], imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i< MC2._nsub; i++ ){
		   MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  return  MC2.cut();
}

template <typename T> inline vMcCormick<T>
sin
( const vMcCormick<T> &MC )
{
  return cos( MC - PI/2. );
}

template <typename T> inline vMcCormick<T>
asin
( const vMcCormick<T> &MC )
{
  if ( Op<T>::l(MC._I) <= -1. || Op<T>::u(MC._I) >= 1. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::ASIN );

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::asin( MC._I );

  if( !vMcCormick<T>::options.ENVEL_USE ){
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = -1;
		  MC2._cv[ipt] = Op<T>::l(MC2._I) + ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ) - Op<T>::l(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		  }
		}
		{ int imid = -1;
		  MC2._cc[ipt] = Op<T>::u(MC2._I) + ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		  }
		}
	  }
      return MC2.cut();
  }

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_asincv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_asincc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
acos
( const vMcCormick<T> &MC )
{
  return asin( -MC ) + PI/2.;
}

template <typename T> inline vMcCormick<T>
tan
( const vMcCormick<T> &MC )
{
  if ( Op<T>::diam(MC._I) >= PI )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::TAN );
  const double shift = PI*std::ceil(-Op<T>::l(MC._I)/PI-1./2.);
  const double xL1 = Op<T>::l(MC._I)+shift, xU1 = Op<T>::u(MC._I)+shift;
  if ( xL1 <= -PI/2. || xU1 >= PI/2. )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::TAN );

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::tan( MC._I );

  if( !vMcCormick<T>::options.ENVEL_USE ){
	  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		{ int imid = -1;
		  MC2._cv[ipt] = Op<T>::l(MC2._I) + ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ) - Op<T>::l(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		  }
		}
		{ int imid = -1;
		  MC2._cc[ipt] = Op<T>::u(MC2._I) + ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid );
		  }
		}
	  }
	  return MC2.cut();
  }

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_tancv( mid( MC._cv[ipt]+shift, MC._cc[ipt]+shift, Op<T>::l(MC._I)+shift, imid ), Op<T>::l(MC._I)+shift, Op<T>::u(MC._I)+shift );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_tancc( mid( MC._cv[ipt]+shift, MC._cc[ipt]+shift, Op<T>::u(MC._I)+shift, imid ), Op<T>::l(MC._I)+shift, Op<T>::u(MC._I)+shift );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
atan
( const vMcCormick<T> &MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::atan( MC._I );

  if( !vMcCormick<T>::options.ENVEL_USE ){
	for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
		MC2._cv[ipt] = Op<T>::l(MC2._I);
		MC2._cc[ipt] = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = MC2._ccsub[ipt][i] = 0.;
		}
	}
    return MC2;
  }

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_atancv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_atancc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
cosh
( const vMcCormick<T>&MC )
{
  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::cosh( MC._I );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
		imid = -1;
		MC2._cv[ipt] = std::cosh( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ));
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * std::sinh( mid( MC._cv[ipt], MC._cc[ipt], zmin, imid ));
	  }

	  { int imid = -1;
		double r = 0.;
		if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
		  r = ( std::cosh( Op<T>::u(MC._I) ) - std::cosh( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		MC2._cc[ipt] = std::cosh( Op<T>::u(MC._I) ) + r * ( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * r;
	  }
  }
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.COSH);
  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
sinh
( const vMcCormick<T>&MC )
{
  if( !vMcCormick<T>::options.ENVEL_USE )
    return( (exp(MC)-exp(-MC))/2. );

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::sinh( MC._I );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_sinhcv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }

	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_sinhcc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
tanh
( const vMcCormick<T> &MC )
{
  if( !vMcCormick<T>::options.ENVEL_USE )
    return( 1- 2./(exp(2*MC)+1) );

  vMcCormick<T> MC2;
  MC2._pts_sub( MC._nsub, MC._const, MC._npts );
  MC2._I = Op<T>::tanh( MC._I );

  for(unsigned int ipt=0;ipt<MC2._npts;ipt++){
	  { int imid = -1;
		const double* cvenv = vMcCormick<T>::_tanhcv( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cv[ipt] = cvenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._cvsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * cvenv[1];
		}
	  }
	  { int imid = -1;
		const double* ccenv = vMcCormick<T>::_tanhcc( mid( MC._cv[ipt], MC._cc[ipt], Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
		MC2._cc[ipt] = ccenv[0];
		for( unsigned int i=0; i<MC2._nsub; i++ ){
		  MC2._ccsub[ipt][i] = mid( MC._cvsub[ipt], MC._ccsub[ipt], i, imid ) * ccenv[1];
		}
	  }
  }
  // if(vMcCormick<T>::options.COMPUTE_ADDITIONAL_LINS){
  	// vMcCormick<T>::additionalLins.compute_additional_linearizations_univariate_operation(MC._cv, MC._cc, MC._cvsub, MC._ccsub, MC2._npts, Op<T>::l(MC._I), Op<T>::u(MC._I), vMcCormick<T>::additionalLins.TANH);
  	// vMcCormick<T>::additionalLins.choose_good_linearizations_univariate(MC2._cv,MC2._cc,MC2._cvsub,MC2._ccsub, vMcCormick<T>::subHeur.lowerBoundValues, vMcCormick<T>::subHeur.upperBoundValues,
  																		// vMcCormick<T>::subHeur.bestLowerBound, vMcCormick<T>::subHeur.bestUpperBound,  vMcCormick<T>::options.SUB_INT_HEUR_USE);
    // if(vMcCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
  // }
  return MC2.cut();
}

template <typename T> inline vMcCormick<T>
coth
( const vMcCormick<T> &MC )
{
	vMcCormick<T> MC2 = tanh(MC);
	if ( Op<T>::l(MC2._I) <= 0. && Op<T>::u(MC2._I) >= 0. ) throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::COTH );
	return inv( MC2 );
}


template <typename T> inline std::ostream&
operator<<
( std::ostream&out, const vMcCormick<T>&MC)
{
  // currently always printing the first point
  out << std::scientific << std::setprecision(vMcCormick<T>::options.DISPLAY_DIGITS) << std::right
      << "[ " << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.l() << " : "
              << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.u()
      << " ]";
	if(MC._npts>0){
	out <<	" [ "  << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.cv(0) << " : "
                   << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.cc(0) << " ]";
	   if( MC._nsub ){
		out << " [ (";
		for( unsigned int i=0; i<MC._nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.cvsub(0,i) << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.cvsub(0,MC._nsub-1) << ") : (";
		for( unsigned int i=0; i<MC._nsub-1; i++ )
			out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.ccsub(0,i) << ",";
		out << std::setw(vMcCormick<T>::options.DISPLAY_DIGITS+7) << MC.ccsub(0,MC._nsub-1) << ") ]";
		}
	}
  return out;
}


template <typename T> inline vMcCormick<T>
hull
( const vMcCormick<T>&X, const vMcCormick<T>&Y )
{
  if( !X._const && !Y._const && (X._nsub != Y._nsub) )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );
  if( !X._const && !Y._const && (X._npts != Y._npts) )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::PTS );

  vMcCormick<T> CV = min(X,Y);
  vMcCormick<T> CC = max(X,Y);
  std::vector<double> cv(CV.cv(), CV.cv()+X._npts);
  std::vector<double> cc(CC.cc(), CC.cc()+X._npts);
  vMcCormick<T> XUY( Op<T>::hull(X.I(),Y.I()), cv, cc );
  if( !X._const )
    XUY._pts_sub( X._nsub, X._const, X._npts );
  else
    XUY._pts_sub( Y._nsub, Y._const, Y._npts );
  for(unsigned int ipt=0;ipt<XUY._npts;ipt++){
	  for( unsigned int is=0; is<XUY._nsub; is++ ){
		XUY._cvsub[ipt][is] = CV.cvsub(ipt,is);
		XUY._ccsub[ipt][is] = CC.ccsub(ipt,is);
	  }
  }
  return XUY;
}

template <typename T> inline bool
inter
( vMcCormick<T>&XIY, const vMcCormick<T>&X, const vMcCormick<T>&Y )
{
  if( !X._const && !Y._const && (X._nsub != Y._nsub) )
    throw typename vMcCormick<T>::Exceptions( vMcCormick<T>::Exceptions::SUB );

  if( !Op<T>::inter( XIY._I, X._I, Y._I ) ) return false;
  vMcCormick<T> CV = max(X,Y);
  vMcCormick<T> CC = min(X,Y);
  for(unsigned int ipt=0;ipt<CC._npts;ipt++){
	if( CV.cv(ipt) > CC.cc(ipt) ) return false;
  }
  if( !X._const )
    XIY._pts_sub( X._nsub, X._const, X._npts );
  else
    XIY._pts_sub( Y._nsub, Y._const, X._npts );
  for(unsigned int ipt=0;ipt<XIY._npts;ipt++){
	XIY._cv[ipt] = CV.cv(ipt);
	XIY._cc[ipt] = CC.cc(ipt);
	for( unsigned int is=0; is<XIY._nsub; is++ ){
		XIY._cvsub[ipt][is] = CV.cvsub(ipt,is);
		XIY._ccsub[ipt][is] = CC.ccsub(ipt,is);
	}
  }
  return true;
}

template <typename T> inline bool
operator==
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) return false;
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]!=MC2._cv[ipt] || MC1._cc[ipt]!=MC2._cc[ipt]) return false;
  }
  return( Op<T>::eq(MC1._I,MC2._I));
}

template <typename T> inline bool
operator!=
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) return true;
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]!=MC2._cv[ipt] || MC1._cc[ipt]!=MC2._cc[ipt]) return true;
  }
  return( Op<T>::ne(MC1._I,MC2._I));
}

template <typename T> inline bool
operator<=
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS );
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]<MC2._cv[ipt] || MC1._cc[ipt]>MC2._cc[ipt]) return false;
  }
  return( Op<T>::le(MC1._I,MC2._I) );
}

template <typename T> inline bool
operator>=
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS );
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]>MC2._cv[ipt] || MC1._cc[ipt]<MC2._cc[ipt]) return false;
  }
  return( Op<T>::ge(MC1._I,MC2._I) );
}

template <typename T> inline bool
operator<
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS );
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]<=MC2._cv[ipt] || MC1._cc[ipt]>=MC2._cc[ipt]) return false;
  }
  return( Op<T>::lt(MC1._I,MC2._I) );
}

template <typename T> inline bool
operator>
( const vMcCormick<T>&MC1, const vMcCormick<T>&MC2 )
{
  if(MC1._npts != MC2._npts) throw typename vMcCormick<T>::Exceptions(vMcCormick<T>::Exceptions::PTS );
  for(unsigned int ipt=0;ipt<MC1._npts;ipt++){
	  if(MC1._cv[ipt]>=MC2._cv[ipt] || MC1._cc[ipt]<=MC2._cc[ipt]) return false;
  }
  return( Op<T>::gt(MC1._I,MC2._I) && MC1._cv < MC2._cv && MC1._cc > MC2._cc );
}

template <typename T> typename vMcCormick<T>::Options vMcCormick<T>::options;

template <typename T> thread_local typename vMcCormick<T>::SubHeur vMcCormick<T>::subHeur;

// template <typename T> typename mc::vMcCormick_Linearizations vMcCormick<T>::additionalLins;

} // namespace mc


#include "mcfadbad.hpp"
//#include "fadbad.h"

namespace fadbad
{

//! @brief Specialization of the structure fadbad::Op to allow usage of the type mc::McCormick of MC++ as a template parameter of the classes fadbad::F, fadbad::B and fadbad::T of FADBAD++
template<typename T> struct Op< mc::vMcCormick<T> >
{
  typedef mc::vMcCormick<T> vMC;
  typedef double Base;
  static Base myInteger( const int i ) { return Base(i); }
  static Base myZero() { return myInteger(0); }
  static Base myOne() { return myInteger(1);}
  static Base myTwo() { return myInteger(2); }
  static double myPI() { return mc::PI; }
  static vMC myPos( const vMC& x ) { return  x; }
  static vMC myNeg( const vMC& x ) { return -x; }
  template <typename U> static vMC& myCadd( vMC& x, const U& y ) { return x+=y; }
  template <typename U> static vMC& myCsub( vMC& x, const U& y ) { return x-=y; }
  template <typename U> static vMC& myCmul( vMC& x, const U& y ) { return x*=y; }
  template <typename U> static vMC& myCdiv( vMC& x, const U& y ) { return x/=y; }
  static vMC myInv( const vMC& x ) { return mc::inv( x ); }
  static vMC mySqr( const vMC& x ) { return mc::pow( x, 2 ); }
  template <typename X, typename Y> static vMC myPow( const X& x, const Y& y ) { return mc::pow( x, y ); }
  //static vMC myCheb( const vMC& x, const unsigned n ) { return mc::cheb( x, n ); }
  static vMC mySqrt( const vMC& x ) { return mc::sqrt( x ); }
  static vMC myLog( const vMC& x ) { return mc::log( x ); }
  static vMC myExp( const vMC& x ) { return mc::exp( x ); }
  static vMC mySin( const vMC& x ) { return mc::sin( x ); }
  static vMC myCos( const vMC& x ) { return mc::cos( x ); }
  static vMC myTan( const vMC& x ) { return mc::tan( x ); }
  static vMC myAsin( const vMC& x ) { return mc::asin( x ); }
  static vMC myAcos( const vMC& x ) { return mc::acos( x ); }
  static vMC myAtan( const vMC& x ) { return mc::atan( x ); }
  static vMC mySinh( const vMC& x ) { return mc::sinh( x ); }
  static vMC myCosh( const vMC& x ) { return mc::cosh( x ); }
  static vMC myTanh( const vMC& x ) { return mc::tanh( x ); }
  static bool myEq( const vMC& x, const vMC& y ) { return x==y; }
  static bool myNe( const vMC& x, const vMC& y ) { return x!=y; }
  static bool myLt( const vMC& x, const vMC& y ) { return x<y; }
  static bool myLe( const vMC& x, const vMC& y ) { return x<=y; }
  static bool myGt( const vMC& x, const vMC& y ) { return x>y; }
  static bool myGe( const vMC& x, const vMC& y ) { return x>=y; }
};

} // end namespace fadbad


//#include "mcop.hpp"

namespace mc
{

//! @brief Specialization of the structure mc::Op to allow usage of the type mc::vMcCormick as a template parameter in the classes mc::TModel, mc::TVar, and mc::SpecBnd
// template <> template<typename T> struct Op< mc::vMcCormick<T> >
template<typename T> struct Op< mc::vMcCormick<T> > // Modified at AVT.SVT in July 2016 to avoid problems with Intel C++ or MSVC++
{
  typedef mc::vMcCormick<T> vMC;
  static vMC point( const double c ) { return vMC(c); }
  static vMC zeroone() { return vMC( mc::Op<T>::zeroone() ); }
  static void I(vMC& x, const vMC&y) { x = y; }
  static double l(const vMC& x) { return x.l(); }
  static double u(const vMC& x) { return x.u(); }
  static double abs (const vMC& x) { return mc::Op<T>::abs(x.I());  }
  static double mid (const vMC& x) { return mc::Op<T>::mid(x.I());  }
  static double diam(const vMC& x) { return mc::Op<T>::diam(x.I()); }
  static vMC inv (const vMC& x) { return mc::inv(x);  }
  static vMC sqr (const vMC& x) { return mc::sqr(x);  }
  static vMC sqrt(const vMC& x) { return mc::sqrt(x); }
  static vMC exp (const vMC& x) { return mc::exp(x);  }
  static vMC log (const vMC& x) { return mc::log(x);  }
  static vMC xlog(const vMC& x) { return mc::xlog(x); }
  static vMC fabsx_times_x(const vMC& x) { return mc::fabsx_times_x(x); }
  static vMC xexpax(const vMC& x, const double a) { return mc::xexpax(x,a); }
  static vMC centerline_deficit(const vMC& x, const double xLim, const double type) { return mc::centerline_deficit(x,xLim,type); }
  static vMC wake_profile(const vMC& x, const double type) { return mc::wake_profile(x,type); }
  static vMC wake_deficit(const vMC& x, const vMC& r, const double a, const double alpha, const double rr, const double type1, const double type2) { return mc::wake_deficit(x,r,a,alpha,rr,type1,type2); }
  static vMC power_curve(const vMC& x, const double type) { return mc::power_curve(x,type); }
  static vMC lmtd(const vMC& x,const vMC& y) { return mc::lmtd(x,y); }
  static vMC rlmtd(const vMC& x,const vMC& y) { return mc::rlmtd(x,y); }
  static vMC mid(const vMC& x, const vMC& y, const double k) { throw std::runtime_error("vMcCormick::mid -- operation not implemented"); return x; } 
  static vMC pinch(const vMC& Th, const vMC& Tc, const vMC& Tp ) { throw std::runtime_error("vMcCormick::pinch -- operation not implemented"); return Th; } 
  static vMC euclidean_norm_2d(const vMC& x,const vMC& y) { return mc::euclidean_norm_2d(x,y); }
  static vMC expx_times_y(const vMC& x,const vMC& y) { return mc::expx_times_y(x,y); }
  static vMC vapor_pressure(const vMC& x, const double type, const double p1, const double p2, const double p3, const double p4 = 0, const double p5 = 0, const double p6 = 0,
							const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0) { return mc::vapor_pressure(x,type,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10);}
  static vMC ideal_gas_enthalpy(const vMC& x, const double x0, const double type, const double p1, const double p2, const double p3, const double p4, const double p5, const double p6 = 0,
							   const double p7 = 0) { return mc::ideal_gas_enthalpy(x,x0,type,p1,p2,p3,p4,p5,p6,p7);}
  static vMC saturation_temperature(const vMC& x, const double type, const double p1, const double p2, const double p3, const double p4 = 0, const double p5 = 0, const double p6 = 0,
									const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0) { return mc::saturation_temperature(x,type,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10);}
  static vMC enthalpy_of_vaporization(const vMC& x, const double type, const double p1, const double p2, const double p3, const double p4, const double p5, const double p6 = 0) { return mc::enthalpy_of_vaporization(x,type,p1,p2,p3,p4,p5,p6);}
  static vMC cost_function(const vMC& x, const double type, const double p1, const double p2, const double p3) { return mc::cost_function(x,type,p1,p2,p3);}
  static vMC sum_div(const std::vector< vMcCormick<T> > &x, const std::vector<double> &coeff) { return mc::sum_div(x,coeff);  }
  static vMC xlog_sum(const std::vector< vMcCormick<T> > &x, const std::vector<double> &coeff) { return mc::xlog_sum(x,coeff);  }
  static vMC nrtl_tau(const vMC& x, const double a, const double b, const double e, const double f) { return mc::nrtl_tau(x,a,b,e,f);}
  static vMC nrtl_dtau(const vMC& x, const double b, const double e, const double f) { return mc::nrtl_dtau(x,b,e,f);}
  static vMC nrtl_G(const vMC& x, const double a, const double b, const double e, const double f, const double alpha ) { return mc::nrtl_G(x,a,b,e,f,alpha);}
  static vMC nrtl_Gtau(const vMC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_Gtau(x,a,b,e,f,alpha);}
  static vMC nrtl_Gdtau(const vMC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_Gdtau(x,a,b,e,f,alpha);}
  static vMC nrtl_dGtau(const vMC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_dGtau(x,a,b,e,f,alpha);}
  static vMC iapws(const vMC& x, const double type) { throw std::runtime_error("vMcCormick::iapws -- operation not implemented"); return x; } //return mc::templates::iapws(x,type); }
  static vMC iapws(const vMC& x, const vMC& y, const double type) { throw std::runtime_error("vMcCormick::iapws -- operation not implemented"); return x; } //return mc::templates::iapws(x,y,type); }
  static vMC p_sat_ethanol_schroeder(const vMC& x) { return mc::p_sat_ethanol_schroeder(x); }
  static vMC rho_vap_sat_ethanol_schroeder(const vMC& x) { return mc::rho_vap_sat_ethanol_schroeder(x); }
  static vMC rho_liq_sat_ethanol_schroeder(const vMC& x) { return mc::rho_liq_sat_ethanol_schroeder(x); }
  static vMC covariance_function(const vMC& x, const double type) { return mc::covariance_function(x,type);}
  static vMC acquisition_function(const vMC& x, const vMC& y, const double type, const double fmin) { return mc::acquisition_function(x,y,type,fmin);}
  static vMC gaussian_probability_density_function(const vMC& x) { return mc::gaussian_probability_density_function(x);}
  static vMC regnormal(const vMC& x, const double a, const double b) { return mc::regnormal(x,a,b);}
  static vMC fabs(const vMC& x) { return mc::fabs(x); }
  static vMC sin (const vMC& x) { return mc::sin(x);  }
  static vMC cos (const vMC& x) { return mc::cos(x);  }
  static vMC tan (const vMC& x) { return mc::tan(x);  }
  static vMC asin(const vMC& x) { return mc::asin(x); }
  static vMC acos(const vMC& x) { return mc::acos(x); }
  static vMC atan(const vMC& x) { return mc::atan(x); }
  static vMC sinh(const vMC& x) { return mc::sinh(x); }
  static vMC cosh(const vMC& x) { return mc::cosh(x); }
  static vMC tanh(const vMC& x) { return mc::tanh(x); }
  static vMC coth(const vMC& x) { return mc::coth(x); }
  static vMC asinh(const vMC& x) { throw std::runtime_error("vMcCormick::asinh -- operation not implemented");  }
  static vMC acosh(const vMC& x) { throw std::runtime_error("vMcCormick::acosh -- operation not implemented"); }
  static vMC atanh(const vMC& x) { throw std::runtime_error("vMcCormick::atanh -- operation not implemented"); }
  static vMC acoth(const vMC& x) { throw std::runtime_error("vMcCormick::acoth -- operation not implemented"); }
  static vMC erf (const vMC& x) { return mc::erf(x);  }
  static vMC erfc(const vMC& x) { return mc::erfc(x); }
  static vMC fstep(const vMC& x) { return mc::fstep(x); }
  static vMC bstep(const vMC& x) { return mc::bstep(x); }
  static vMC hull(const vMC& x, const vMC& y) { return mc::Op<T>::hull(x.I(),y.I()); }
  static vMC min (const vMC& x, const vMC& y) { return mc::min(x,y);  }
  static vMC max (const vMC& x, const vMC& y) { return mc::max(x,y);  }
  static vMC pos (const vMC& x) { return mc::pos(x);  }
  static vMC neg (const vMC& x) { return mc::neg(x);  }
  static vMC lb_func (const vMC& x, const double lb) { return mc::lb_func(x,lb);  }
  static vMC ub_func (const vMC& x, const double ub) { return mc::ub_func(x,ub);  }
  static vMC bounding_func (const vMC& x, const double lb, const double ub) { return mc::bounding_func(x,lb,ub);  }
  static vMC squash_node (const vMC& x, const double lb, const double ub) { return mc::squash_node(x,lb,ub);  }
  static vMC mc_print ( const vMC& x, const int number) {return mc::mc_print(x,number); }
  static vMC arh (const vMC& x, const double k) { return mc::arh(x,k); }
  template <typename X, typename Y> static vMC pow(const X& x, const Y& y) { return mc::pow(x,y); }
  static vMC cheb (const vMC& x, const unsigned n) { return mc::cheb(x,n); }
  static vMC prod (const unsigned int n, const vMC* x) { return mc::prod(n,x); }
  static vMC monom (const unsigned int n, const vMC* x, const unsigned* k) { return mc::monom(n,x,k); }
  static bool inter(vMC& xIy, const vMC& x, const vMC& y) { return mc::inter(xIy,x,y); }
  static bool eq(const vMC& x, const vMC& y) { return x==y; }
  static bool ne(const vMC& x, const vMC& y) { return x!=y; }
  static bool lt(const vMC& x, const vMC& y) { return x<y;  }
  static bool le(const vMC& x, const vMC& y) { return x<=y; }
  static bool gt(const vMC& x, const vMC& y) { return x>y;  }
  static bool ge(const vMC& x, const vMC& y) { return x>=y; }
};

} // namespace mc

#endif
