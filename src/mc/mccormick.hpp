// Copyright (C) 2009-2017 Benoit Chachuat, Imperial College London.
// All Rights Reserved.

/*!
\page page_MCCORMICK McCormick Relaxation Arithmetic for Factorable Functions
\author Beno&icirc;t Chachuat

A convex relaxation \f$f^{\rm cv}\f$ of a function \f$f\f$ on the convex domain \f$D\f$ is a function that is (i) convex on \f$D\f$ and (ii) underestimates \f$f\f$  on \f$D\f$. Likewise, a concave relaxation \f$f^{\rm cc}\f$ of a function \f$f\f$ on the convex domain \f$D\f$ is a function that is (i) concave on \f$D\f$ and (ii) overestimates \f$f\f$  on \f$D\f$. McCormick's technique [McCormick, 1976] provides a means for computing pairs of convex/concave relaxations of a multivariate function on interval domains provided that this function is factorable and that the intrinsic univariate functions in its factored form have known convex/concave envelopes or, at least, relaxations.

<CENTER><TABLE BORDER=0>
<TR>
<TD>\image html McCormick_relax.png</TD>
</TR>
</TABLE></CENTER>


The class mc::McCormick provides an implementation of the McCormick relaxation technique and its recent extensions; see [McCormick, 1976; Scott <i>et al.</i>, 2011; Tsoukalas & Mitsos, 2012; Wechsung & Barton, 2013]. mc::McCormick also has the capability to propagate subgradients for these relaxations, which are guaranteed to exist in the interior of the domain of definition of any convex/concave function. This propagation is similar in essence to the forward mode of automatic differentiation; see [Mitsos <i>et al.</i>, 2009]. We note that mc::McCormick is <b>not a verified implementation</b> in the sense that rounding errors are not accounted for in computing convex/concave bounds and subgradients.

The implementation of mc::McCormick relies on the operator/function overloading mechanism of C++. This makes the computation of convex/concave relaxations both simple and intuitive, similar to computing function values in real arithmetics or function bounds in interval arithmetic (see \ref page_INTERVAL). Moreover, mc::McCormick can be used as the template parameter of other classes of MC++, for instance mc::TModel and mc::TVar. Likewise, mc::McCormick can be used as the template parameter of the classes fadbad::F, fadbad::B and fadbad::T of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A> for computing McCormick relaxations and subgradients of the partial derivatives or the Taylor coefficients of a factorable function (see \ref sec_MCCORMICK_fadbad).

mc::McCormick itself is templated in the type used to propagate the supporting interval bounds. By default, mc::McCormick can be used with the non-verified interval type mc::Interval of MC++. For reliability, however, it is strongly recommended to use verified interval arithmetic such as <A href="http://www.ti3.tu-harburg.de/Software/PROFILEnglisch.html">PROFIL</A> or <A href="http://www.math.uni-wuppertal.de/~xsc/software/filib.html">FILIB++</A>. We note that Taylor models as provided by the classes mc::TModel and mc::TVar can also be used as the template parameter (see \ref page_TAYLOR).

Examples of McCormick relaxations constructed with mc::McCormick are shown on the left plot of the figure below for the factorable function \f$f(x)=\cos(x^2)\,\sin(x^{-3})\f$ for \f$x\in [\frac{\pi}{6},\frac{\pi}{3}]\f$. Also shown on the right plot are the affine relaxations constructed from a subgradient at \f$\frac{\pi}{4}\f$ of the McCormick relaxations of \f$f\f$ on \f$[\frac{\pi}{6},\frac{\pi}{3}]\f$.

<CENTER><TABLE BORDER=0>
<TR>
<TD>\image html MC-1D_relax.png</TD>
<TD>\image html MC-1D_linearize.png</TD>
</TR>
</TABLE></CENTER>


\section sec_MCCORMICK_use How do I compute McCormick relaxations of a factorable function?

Suppose one wants to compute McCormick relaxation of the real-valued function \f$f(x,y)=x(\exp(x)-y)^2\f$ for \f$(x,y)\in [-2,1]\times[-1,2]\f$, at the point \f$(x,y)=(0,1)\f$. For simplicity, the supporting interval bounds are calculated using the default interval type mc::Interval here:

\code
      #include "interval.hpp"
      typedef mc::Interval I;
      typedef mc::McCormick<I> MC;
\endcode

First, the variables \f$x\f$ and \f$y\f$ are defined as follows:

\code
      MC X( I( -2., 1. ), 0. );
      MC Y( I( -1., 2. ), 1. );
\endcode

Essentially, the first line means that <tt>X</tt> is a variable of type mc::McCormick, belonging to the interval \f$[-2,1]\f$, and whose current value is \f$0\f$. The same holds for the McCormick variable <tt>Y</tt>, which belonging to the interval \f$[-1,2]\f$ and has a value of \f$1\f$.

Having defined the variables, McCormick relaxations of \f$f(x,y)=x(\exp(x)-y)^2\f$ on \f$[-2,1]\times[-1,2]\f$ at \f$(0,1)\f$ are simply computed as:

\code
      MC F = X*pow(exp(X)-Y,2);
\endcode

These relaxations can be displayed to the standard output as:

\code
      std::cout << "f relaxations at (0,1): " << F << std::endl;
\endcode

which produces the following output:

\verbatim
f relaxations at (0,1): [ -2.76512e+01 :  1.38256e+01 ] [ -1.38256e+01 :  8.52245e+00 ]
\endverbatim

Here, the first pair of bounds correspond to the supporting interval bounds, as obtained with mc::Interval, and which are valid over the entire domain \f$[-2,1]\times[-1,2]\f$. The second pair of bounds are the values of the convex and concave relaxations at the selected point \f$(0,1)\f$. In order to describe the convex and concave relaxations on the entire range \f$[-2,1]\times[-1,2]\f$, it would be necessary to repeat the computations at different points. The current point can be set/modified by using the method mc::McCormick::c, for instance at \f$(-1,0)\f$

\code
      X.c( -1. );
      Y.c( 0. );
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ]
\endverbatim

The values of the McCormick convex and concave relaxations of \f$f(x,y)\f$ can be retrieved, respectively, as:

\code
      double Fcv = F.cv();
      double Fcc = F.cc();
\endcode

Likewise, the lower and upper bounds of the supporting interval bounds can be retrieved as:

\code
      double Flb = F.l();
      double Fub = F.u();
\endcode


\section sec_MCCORMICK_sub How do I compute a subgradient of the McCormick relaxations?

Computing a subgradient of a McCormick relaxation requires specification of the independent variables via the .sub method, prior to evaluating the function in mc::McCormick type. Continuing the previous example, the function has two independent variables \f$x\f$ and \f$y\f$. Defining \f$x\f$ and \f$y\f$ as the subgradient components \f$0\f$ and \f$1\f$ (indexing in C/C++ start at 0 by convention!), respectively, is done as follows:

\code
      X.sub( 2, 0 );
      Y.sub( 2, 1 );
\endcode

Similar to above, the McCormick convex and concave relaxations of \f$f(x,y)\f$ at \f$(-1,0)\f$ along with subgradients of these relaxations are computed as:

\code
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations and subgradients at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
\endverbatim

The additional information displayed corresponds to, respectively, a subgradient of the McCormick convex underestimator at (-1,0) and a subgradient of the McCormick concave overestimator at (-1,0). In turn, these subgradients can be used to construct affine relaxations on the current range, or passed to a bundle solver to locate the actual minimum or maximum of the McCormick relaxations.

The subgradients of the McCormick relaxations of \f$f(x,y)\f$ at the current point can be retrieved as follows:

\code
      const double* Fcvsub = F.cvsub();
      const double* Fccsub = F.ccsub();
\endcode

or, alternatively, componentwise as:

\code
      double Fcvsub_X = F.cvsub(0);
      double Fcvsub_Y = F.cvsub(1);
      double Fccsub_X = F.ccsub(0);
      double Fccsub_Y = F.ccsub(1);
\endcode

Directional subgradients can be propagated too. In the case that subgradients are to computed along the direction (1,-1) for both the convex and concave relaxations, we define:

\code
      const double sub_dir[2] = { 1., -1 };
      X.sub( 1, &sub_dir[0], &sub_dir[0] );
      Y.sub( 1, &sub_dir[1], &sub_dir[1] );
      F = X*pow(exp(X)-Y,2);
      std::cout << "f relaxations and subgradients along direction (1,-1) at (-1,0): " << F << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients along direction (1,-1) at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-6.89910e+00) : ( 3.44955e+00) ]
\endverbatim


\section sec_MCCORMICK_fadbad How do I compute McCormick relaxations of the partial derivatives or the Taylor coefficients of a factorable function using FADBAD++?

Now suppose one wants to compute McCormick relaxation not only for a given factorable function, but also for its partial derivatives. Continuing the previous example, the partial derivatives of \f$f(x,y)=x(\exp(x)-y)^2\f$ with respect to its independent variables \f$x\f$ and \f$y\f$ can be obtained via automatic differentiation (AD), either forward or reverse AD. This can be done for instance using the classes fadbad::F, and fadbad::B of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A>.

Considering forward AD first, we include the following header files:

\code
      #include "mcfadbad.hpp" // available in MC++
      #include "fadiff.h"     // available in FADBAD++
      typedef fadbad::F<MC> FMC;
\endcode

The variables are initialized and the derivatives and subgradients are seeded as follows:

\code
      FMC FX = X;             // initialize FX with McCormick variable X
      FX.diff(0,2);           // differentiate with respect to x (index 0 of 2)
      FX.x().sub(2,0);        // seed subgradient of x (index 0 of 2)

      FMC FY = Y;             // initialize FY with McCormick variable Y
      FY.diff(1,2);           // differentiate with respect to y (index 1 of 2)
      FY.x().sub(2,1);        // seed subgradient of y (index 1 of 2)
\endcode

As previously, the McCormick convex and concave relaxations of \f$f\f$, \f$\frac{\partial f}{\partial x}\f$, and \f$\frac{\partial f}{\partial y}\f$ at \f$(-1,0)\f$ on the range \f$[-2,1]\times[-1,2]\f$, along with subgradients of these relaxations, are computed as:

\code
      FMC FF = FX*pow(exp(FX)-FY,2);
      std::cout << "f relaxations and subgradients at (-1,0): " << FF.x() << std::endl;
      std::cout << "df/dx relaxations and subgradients at (-1,0): " << FF.d(0) << std::endl;
      std::cout << "df/dy relaxations and subgradients at (-1,0): " << FF.d(1) << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
df/dx relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -2.33469e+01 :  2.90549e+01 ] [ (-2.31383e+01,-1.94418e-01) : ( 1.59593e+00,-1.85362e+00) ]
df/dy relaxations and subgradients at (-1,0): [ -7.45866e+00 :  1.48731e+01 ] [ -5.96505e+00 :  7.71460e+00 ] [ (-5.96505e+00,-4.00000e+00) : ( 7.17326e+00,-4.00000e+00) ]
\endverbatim

Relaxations of the partial derivatives can also be computed using the backward mode of AD, which requires the additional header file:

\code
      #include "badiff.h"     // available in FADBAD++
      typedef fadbad::B<MC> BMC;
\endcode

then initialize and seed new variables and compute the function as follows:

\code
      BMC BX = X;             // initialize FX with McCormick variable X
      BX.x().sub(2,0);        // seed subgradient as direction (1,0)
      BMC BY = Y;             // initialize FY with McCormick variable Y
      BY.x().sub(2,1);        // seed subgradient as direction (1,0)

      BMC BF = BX*pow(exp(BX)-BY,2);
      BF.diff(0,1);           // differentiate f (index 0 of 1)
      std::cout << "f relaxations and subgradients at (-1,0): " << BF.x() << std::endl;
      std::cout << "df/dx relaxations and subgradients at (-1,0): " << BX.d(0) << std::endl;
      std::cout << "df/dy relaxations and subgradients at (-1,0): " << BY.d(0) << std::endl;
\endcode

producing the output:

\verbatim
f relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
df/dx relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -1.37142e+01 :  1.60092e+01 ] [ (-1.35056e+01,-1.94418e-01) : ( 8.82498e+00,-1.31228e+00) ]
df/dy relaxations and subgradients at (-1,0): [ -7.45866e+00 :  1.48731e+01 ] [ -5.96505e+00 :  7.71460e+00 ] [ (-5.96505e+00,-4.00000e+00) : ( 7.17326e+00,-4.00000e+00) ]
\endverbatim

It is noteworthy that the bounds, McCormick relaxations and subgradients for the partial derivatives as computed with the forward and backward mode, although valid, may not be the same since the computations involve different sequences or even operations. In the previous examples, for instance, forward and backward AD produce identical interval bounds for \f$\frac{\partial f}{\partial x}\f$ and \f$\frac{\partial f}{\partial y}\f$ at \f$(-1,0)\f$, yet significantly tighter McCormick relaxations are obtained with backward AD for \f$\frac{\partial f}{\partial x}\f$ at \f$(-1,0)\f$.

Another use of <A href="http://www.fadbad.com/fadbad.html">FADBAD++</A> involves computing McCormick relaxations of the Taylor coefficients in the Taylor expansion of a factorable function in a given direction up to a certain order. Suppose we want to compute McCormick relaxation of the first 5 Taylor coefficients of \f$f(x,y)=x(\exp(x)-y)^2\f$ in the direction \f$(1,0)\f$, i.e. the direction of \f$x\f$. This information can be computed by using the classes fadbad::T, which requires the following header file:

\code
      #include "tadiff.h"     // available in FADBAD++
      typedef fadbad::T<MC> TMC;
\endcode

The variables are initialized and the derivatives and subgradients are seeded as follows:

\code
      TMC TX = X;             // initialize FX with McCormick variable X
      TX[0].sub(2,0);        // seed subgradient as direction (1,0)
      TMC TY = Y;             // initialize FY with McCormick variable Y
      TY[0].sub(2,1);        // seed subgradient as direction (1,0)
      TX[1] = 1.;             // Taylor-expand with respect to x

      TMC TF = TX*pow(exp(TX)-TY,2);
      TF.eval(5);            // Taylor-expand f to degree 5
      for( unsigned int i=0; i<=5; i++ )
        std::cout << "d^" << i << "f/dx^" << i << " relaxations and subgradients at (-1,0): " << TF[i] << std::endl;
\endcode

producing the output:

\verbatim
d^0f/dx^0 relaxations and subgradients at (-1,0): [ -2.76512e+01 :  1.38256e+01 ] [ -1.75603e+01 :  8.78014e+00 ] [ (-3.19186e+00, 3.70723e+00) : ( 1.59593e+00,-1.85362e+00) ]
d^1f/dx^1 relaxations and subgradients at (-1,0): [ -4.04294e+01 :  3.41004e+01 ] [ -2.33469e+01 :  2.90549e+01 ] [ (-2.31383e+01,-1.94418e-01) : ( 1.59593e+00,-1.85362e+00) ]
d^2f/dx^2 relaxations and subgradients at (-1,0): [ -4.51302e+01 :  3.77111e+01 ] [ -1.97846e+01 :  2.25846e+01 ] [ (-1.97113e+01, 0.00000e+00) : ( 7.36023e+00,-4.06006e-01) ]
d^3f/dx^3 relaxations and subgradients at (-1,0): [ -2.65667e+01 :  2.82546e+01 ] [ -1.02662e+01 :  1.27412e+01 ] [ (-1.00820e+01,-4.51118e-02) : ( 7.66644e+00,-1.80447e-01) ]
d^4f/dx^4 relaxations and subgradients at (-1,0): [ -1.19764e+01 :  1.59107e+01 ] [ -4.29280e+00 :  6.13261e+00 ] [ (-4.25007e+00,-2.25559e-02) : ( 4.86086e+00,-5.63897e-02) ]
d^5f/dx^5 relaxations and subgradients at (-1,0): [ -4.44315e+00 :  7.16828e+00 ] [ -1.49744e+00 :  2.55611e+00 ] [ (-1.44773e+00,-6.76676e-03) : ( 2.29932e+00,-1.35335e-02) ]
\endverbatim

The zeroth Taylor coefficient corresponds to the function \f$f\f$ itself. It can also be checked that the relaxations of the first Taylor coefficient of \f$f\f$ matches those obtained with forward AD for \f$\frac{\partial f}{\partial x}\f$.

Naturally, the classes fadbad::F, fadbad::B and fadbad::T can be nested to produce relaxations of higher-order derivative information.


\section sec_MCCORMICK_fct Which functions are overloaded in McCormick relaxation arithmetic?

As well as overloading the usual functions <tt>exp</tt>, <tt>log</tt>, <tt>sqr</tt>, <tt>sqrt</tt>, <tt>pow</tt>, <tt>inv</tt>, <tt>cos</tt>, <tt>sin</tt>, <tt>tan</tt>, <tt>acos</tt>, <tt>asin</tt>, <tt>atan</tt>, <tt>erf</tt>, <tt>erfc</tt>, <tt>min</tt>, <tt>max</tt>, <tt>fabs</tt>, mc::McCormick also defines the following functions:
- <tt>fstep(x)</tt> and <tt>bstep(x)</tt>, implementing the forward step function (switching value from 0 to 1 for x>=0) and the backward step function (switching value from 1 to 0 for x>=0). These functions can be used to model a variety of discontinuous functions as first proposed in [Wechsung & Barton, 2012].
- <tt>ltcond(x,y,z)</tt> and <tt>gtcond(x,y,z)</tt>, similar in essence to <tt>fstep(x)</tt> and <tt>bstep(x)</tt>, and implementing disjunctions of the form { y if x<=0; z otherwise } and { y if x>=0; z otherwise }, respectively.
- <tt>inter(x,y,z)</tt>, computing the intersection \f$x = y\cap z\f$ and returning true/false if the intersection is nonempty/empty.
- <tt>hull(x,y)</tt>, computing convex/concave relaxations of the union \f$x\cup y\f$.
.


\section sec_MCCORMICK_opt What are the options in mc::McCormick and how are they set?

The class mc::McCormick has a public static member called mc::McCormick::options that can be used to set/modify the options; e.g.,

\code
      MC::options.ENVEL_USE = true;
      MC::options.ENVEL_TOL = 1e-12;
      MC::options.ENVEL_MAXIT = 100;
      MC::options.MVCOMP_USE = true;
\endcode

The available options are the following:

<TABLE border="1">
<CAPTION><EM>Options in mc::McCormick::Options: name, type and description</EM></CAPTION>
     <TR><TH><b>Name</b>  <TD><b>Type</b><TD><b>Default</b>
         <TD><b>Description</b>
     <TR><TH><tt>ENVEL_USE</tt> <TD><tt>bool</tt> <TD>true
         <TD>Whether to compute convex/concave envelopes for the neither-convex-nor-concave univariate functions such as odd power terms, sin, cos, asin, acos, tan, atan, erf, erfc. This provides tighter McCormick relaxations, but it is more time consuming. Junction points are computed using the Newton or secant method first, then the more robust golden section search method if unsuccessful.
     <TR><TH><tt>ENVEL_TOL</tt> <TD><tt>double</tt> <TD>1e-10
         <TD>Termination tolerance for determination function points in convex/concave envelopes of univariate terms.
     <TR><TH><tt>ENVEL_MAXIT</tt> <TD><tt>int</tt> <TD>100
         <TD>Maximum number of iterations for determination function points in convex/concave envelopes of univariate terms.
     <TR><TH><tt>MVCOMP_USE</tt> <TD><tt>bool</tt> <TD>false
         <TD>Whether to use Tsoukalas & Mitsos's multivariate composition result for min/max, product, and division terms; see [Tsoukalas & Mitsos, 2012]. This provides tighter McCormick relaxations, but it is more time consuming.
     <TR><TH><tt>MVCOMP_TOL</tt> <TD><tt>double</tt> <TD>1e1*machprec()
         <TD>Tolerance for equality test in subgradient propagation for product terms with Tsoukalas & Mitsos's multivariate composition result; see [Tsoukalas & Mitsos, 2012].
     <TR><TH><tt>DISPLAY_DIGITS</tt> <TD><tt>unsigned int</tt> <TD>5
         <TD>Number of digits in output stream
</TABLE>


\section sec_MC_err What Errors Can Be Encountered during the Computation of Convex/Concave Bounds?

Errors are managed based on the exception handling mechanism of the C++ language. Each time an error is encountered, a class object of type mc::McCormick::Exceptions is thrown, which contains the type of error. It is the user's responsibility to test whether an exception was thrown during a McCormick relaxation, and then make the appropriate changes. Should an exception be thrown and not caught by the calling program, the execution will stop.

Possible errors encountered during the computation of a McCormick relaxation are:

<TABLE border="1">
<CAPTION><EM>Errors during Computation of a McCormick relaxation</EM></CAPTION>
     <TR><TH><b>Number</b> <TD><b>Description</b>
     <TR><TH><tt>1</tt> <TD>Division by zero
     <TR><TH><tt>2</tt> <TD>Inverse with zero in range
     <TR><TH><tt>3</tt> <TD>Log with negative values in range
     <TR><TH><tt>4</tt> <TD>Square-root with nonpositive values in range
     <TR><TH><tt>5</tt> <TD>Inverse sine or cosine with values outside of \f$[-1,1]\f$ range
     <TR><TH><tt>6</tt> <TD>Tangent with values outside of \f$[-\frac{\pi}{2}+k\pi,\frac{\pi}{2}+k\pi]\f$ range
     <TR><TH><tt>-1</tt> <TD>Inconsistent size of subgradient between two mc::McCormick variables
     <TR><TH><tt>-2</tt> <TD>Failed to compute the convex or concave envelope of a univariate term
     <TR><TH><tt>-3</tt> <TD>Failed to propagate subgradients for a product term with Tsoukalas & Mitsos's multivariable composition result
</TABLE>

\section sec_MC_refs References
- Bompadre, A., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-011-9685-2">Convergence rate of McCormick relaxations</A>, <I>Journal of Global Optimization</I> <B>52</B>(1):1-28, 2012
- Bongartz, D., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-017-0547-4">Deterministic global optimization of process flowsheets in a reduced space using McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>in press</b>, 2017
- McCormick, G. P., <A href="http://dx.doi.org/10.1007/BF01580665">Computability of global solutions to factorable nonconvex programs: Part I. Convex underestimating problems</A>, <i>Mathematical Programming</i>, <b>10</b>(2):147-175, 1976
- Mitsos, A., B. Chachuat, and P.I. Barton, <A href="http://dx.doi.org/10.1137/080717341">McCormick-based relaxations of algorithms</A>, <i>SIAM Journal on Optimization</i>, <b>20</b>(2):573-601, 2009
- Najman, J., D. Bongartz, A. Tsoukalas, A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-016-0470-0">Erratum to: Multivariate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>68</b>:219-225, 2017
- Najman, J., A. Mitsos, <A href="http://dx.doi.org/10.1007/s10898-016-0408-6">Convergence analysis of multivariate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>66</b>(4):597-, 2016
- Scott, J.K., M.D. Stuber, P.I. Barton, <A href="http://dx.doi.org/10.1007/s10898-011-9664-7">Generalized McCormick relaxations</A>. <i>Journal of Global Optimization</i>, <b>51</b>(4), 569-606, 2011
- Tsoukalas, A., and A. Mitsos, <A href="http://www.optimization-online.org/DB_HTML/2012/05/3473.html">Multi-variate McCormick relaxations</A>, <i>Journal of Global Optimization</i>, <b>59</b>, 633-662, 2014
- Wechsung, A., and P.I. Barton, <A href="http://dx.doi.org/10.1007/s10898-013-0060-3">Global optimization of bounded factorable functions with discontinuities</A>, <i>Journal of Global Optimization</i>, <b>58</b>(1), 1-30, 2013.
.
*/

#ifndef MC__MCCORMICK_H
#define MC__MCCORMICK_H

#include <iostream>
#include <iomanip>
#include <stdarg.h>
#include <cassert>
#include <string>
#include <limits>

#include "mcfunc.hpp"
#include "mcop.hpp"

// preprocessor variable for easier debugging
#undef  MC__MCCORMICK_DEBUG

namespace mc
{
//! @brief C++ class for McCormick relaxation arithmetic for factorable function
////////////////////////////////////////////////////////////////////////
//! mc::McCormick is a C++ class computing the McCormick
//! convex/concave relaxations of factorable functions on a box,
//! as well as doing subgradient propagation. The template parameter
//! corresponds to the type used in the underlying interval arithmetic
//! computations.
////////////////////////////////////////////////////////////////////////
template <typename T>
class McCormick
////////////////////////////////////////////////////////////////////////
{
  template <typename U> friend class McCormick;

  template <typename U> friend McCormick<U> operator+
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> operator+
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator+
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator+
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> operator-
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> operator-
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator-
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator-
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> operator*
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator*
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator*
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> operator/
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator/
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> operator/
    ( const McCormick<U>&, const double );
  template <typename U> friend std::ostream& operator<<
    ( std::ostream&, const McCormick<U>& );
  template <typename U> friend bool operator==
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool operator!=
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool operator<=
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool operator>=
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool operator<
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool operator>
    ( const McCormick<U>&, const McCormick<U>& );

  template <typename U> friend McCormick<U> inv
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> sqr
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> exp
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> log
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> cos
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> sin
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> tan
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> acos
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> asin
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> atan
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> cosh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> sinh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> tanh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> coth
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> acosh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> asinh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> atanh
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> acoth
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> fabs
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> sqrt
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> root  // root function for the calculation of the n-th root
    ( const McCormick<U>&, const int );
  template <typename U> friend McCormick<U> xlog
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> fabsx_times_x
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> xexpax
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> centerline_deficit
    ( const McCormick<U>&, const double, const double );
  template <typename U> friend McCormick<U> wake_profile
    ( const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> wake_deficit
    ( const McCormick<U>&, const McCormick<U>&, const double, const double, const double, const double, const double);
  template <typename U> friend McCormick<U> power_curve
    ( const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> lmtd
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> rlmtd
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> mid 
  (const McCormick<U>&, const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> pinch 
  (const McCormick<U>&, const McCormick<U>&, const McCormick<U>&);
  template <typename U> friend McCormick<U> euclidean_norm_2d
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> expx_times_y
    ( const McCormick<U>&, const McCormick<U>& );
 template <typename U> friend McCormick<U> vapor_pressure
    (const McCormick<U>&, const double, const double, const double, const double, const double,
	 const double, const double, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> ideal_gas_enthalpy
    (const McCormick<U>&, const double, const double, const double, const double, const double, const double,
	 const double, const double, const double );
  template <typename U> friend McCormick<U> saturation_temperature
    (const McCormick<U>&, const double, const double, const double, const double, const double,
	 const double, const double, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> enthalpy_of_vaporization
    (const McCormick<U>&, const double, const double, const double, const double, const double, const double,
	 const double );
  template <typename U> friend McCormick<U> cost_function
    (const McCormick<U>&, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> sum_div
    ( const std::vector< McCormick<U> >&, const std::vector<double>& );
  template <typename U> friend McCormick<U> xlog_sum
    ( const std::vector< McCormick<U> >&, const std::vector<double>& );
  template <typename U> friend McCormick<U> nrtl_tau
    (const McCormick<U>&, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> nrtl_dtau
    (const McCormick<U>&, const double, const double, const double );
  template <typename U> friend McCormick<U> nrtl_G
    (const McCormick<U>&, const double, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> nrtl_Gtau
    (const McCormick<U>&, const double, const double, const double, const double, const double );
  template <typename U> friend McCormick<U> nrtl_Gdtau
    (const McCormick<U>&, const double, const double, const double, const double, const double);
  template <typename U> friend McCormick<U> nrtl_dGtau
    (const McCormick<U>&, const double, const double, const double, const double, const double);
 template <typename U> friend McCormick<U> iapws
    (const McCormick<U>&, const double);
 template <typename U> friend McCormick<U> iapws
    (const McCormick<U>&, const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> p_sat_ethanol_schroeder
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> rho_vap_sat_ethanol_schroeder
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> rho_liq_sat_ethanol_schroeder
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> covariance_function
    ( const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> acquisition_function
    ( const McCormick<U>&, const McCormick<U>&, const double, const double);
  template <typename U> friend McCormick<U> gaussian_probability_density_function
    ( const McCormick<U>&);
  template <typename U> friend McCormick<U> regnormal
    ( const McCormick<U>&, const double, const double);
  template <typename U> friend McCormick<U> arh
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> erf
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> erfc
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> fstep
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> bstep
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> pow
    ( const McCormick<U>&, const int );
  template <typename U> friend McCormick<U> pow
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> pow
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> pow
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> prod
    ( const unsigned int, const McCormick<U>* );
  template <typename U> friend McCormick<U> monom
    ( const unsigned int, const McCormick<U>*, const unsigned* );
  template <typename U> friend McCormick<U> cheb
    ( const McCormick<U>&, const unsigned );
  template <typename U> friend McCormick<U> min
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> max
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> min
    ( const McCormick<U>&,const double );
  template <typename U> friend McCormick<U> max
    ( const McCormick<U>&, const double );
  template <typename U> friend McCormick<U> min
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> max
    ( const double, const McCormick<U>& );
  template <typename U> friend McCormick<U> min
    ( const unsigned int, const McCormick<U>* );
  template <typename U> friend McCormick<U> max
    ( const unsigned int, const McCormick<U>* );
  template <typename U> friend McCormick<U> pos
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> neg
    ( const McCormick<U>& );
  template <typename U> friend McCormick<U> lb_func
    ( const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> ub_func
    ( const McCormick<U>&, const double);
  template <typename U> friend McCormick<U> bounding_func
    ( const McCormick<U>&, const double, const double);
  template <typename U> friend McCormick<U> squash_node
    ( const McCormick<U>&, const double, const double);
  template <typename U> friend McCormick<U> mc_print
	( const McCormick<U>&, const int);
  template <typename U> friend McCormick<U> ltcond
    ( const McCormick<U>&, const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> ltcond
    ( const U&, const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> gtcond
    ( const McCormick<U>&, const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> gtcond
    ( const U&, const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend bool inter
    ( McCormick<U>&, const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> hull
    ( const McCormick<U>&, const McCormick<U>& );
  template <typename U> friend McCormick<U> cut
    ( const McCormick<U>& );

public:

  McCormick<T>& operator=
    ( const McCormick<T>& );
  McCormick<T>& operator=
    ( const T& );
  McCormick<T>& operator=
    ( const double );
  McCormick<T>& operator+=
    ( const McCormick<T>& );
  McCormick<T>& operator+=
    ( const double );
  McCormick<T>& operator-=
    ( const McCormick<T>& );
  McCormick<T>& operator-=
    ( const double );
  McCormick<T>& operator*=
    ( const McCormick<T>& );
  McCormick<T>& operator*=
    ( const double );
  McCormick<T>& operator/=
    ( const McCormick<T>& );
  McCormick<T>& operator/=
    ( const double );

  /** @defgroup MCCORMICK McCormick Relaxation Arithmetic for Factorable Functions
   *  @{
   */
  //! @brief Options of mc::McCormick
  static struct Options
  {
    //! @brief Constructor
    Options():
      ENVEL_USE(true), ENVEL_MAXIT(100), ENVEL_TOL(1e-9), MVCOMP_USE(false),
      MVCOMP_TOL(1e-9), DISPLAY_DIGITS(5), SUB_INT_HEUR_USE(false)
      {}
    //! @brief Whether to compute convex/concave envelopes for the neither-convex-nor-concave univariate functions such as odd power terms, sin, cos, asin, acos, tan, atan, erf, erfc. This provides tighter McCormick relaxations, but it is more time consuming. Junction points are computed using the Newton or secant method first, then the more robust golden section search method if unsuccessful.
    bool ENVEL_USE;
    //! @brief Maximum number of iterations for determination function points in convex/concave envelopes of univariate terms.
    unsigned int ENVEL_MAXIT;
    //! @brief Termination tolerance for determination function points in convex/concave envelopes of univariate terms.
    double ENVEL_TOL;
    //! @brief Whether to use Tsoukalas & Mitsos's multivariate composition result for min/max, product, and division terms; see [Tsoukalas & Mitsos, 2012]. This provides tighter McCormick relaxations, but it is more time consuming.
    bool MVCOMP_USE;
    //! @brief Tolerance for testing equality in subgradient propagation for product terms with Tsoukalas & Mitsos's multivariate composition result; see [Tsoukalas & Mitsos, 2012].
    double MVCOMP_TOL;
    //! @brief Number of digits displayed with << operator (default=5)
    unsigned int DISPLAY_DIGITS;
	//! @brief Whether to use Najman & Mitsos's subgradient interval heuristic; see [Najman & Mitsos, 2018]. This often provides tighter relaxations, but is more time consuming.
    bool SUB_INT_HEUR_USE;
  } options;

  ////////////////////////////////////////////////////////////////////////
  //! mc::subHeur is a C++ structure for storing precomputed intervals, e.g., by the subgradient interval heuristic [Najman&Mitsos 2018], which can be used in order to improve the final McCormick relaxation.
  ////////////////////////////////////////////////////////////////////////
  static thread_local struct SubHeur{

	SubHeur():
		intervals(std::vector<T>()), itIntervals(intervals.begin()), originalLowerBounds(0), originalUpperBounds(0), referencePoint(0), usePrecomputedIntervals(false)
		{}

  	std::vector<T> intervals;						/*<! vector holding precomputed intervals */
  	typename std::vector<T>::iterator itIntervals;	/*<! iterator for getting the correct interval */
  	const std::vector<double>* originalLowerBounds;	/*<! pointer to the vector of original lower bounds */
  	const std::vector<double>* originalUpperBounds;	/*<! pointer to the vector of original upper bounds */
  	const std::vector<double>* referencePoint;		/*<! pointer to the vector holding the reference point */
  	bool usePrecomputedIntervals;					/*<! boolean telling whether Intervals should be used */

  	void reserve_size(unsigned int size){
		intervals.resize(0);
  		intervals.reserve(size);
  	}

  	void clear(){
  		intervals.clear();
  		itIntervals = intervals.begin();
  		originalLowerBounds = 0;
  		originalUpperBounds = 0;
  		referencePoint = 0;
  		usePrecomputedIntervals = false;
  	}

  	void reset_iterator(){
  		itIntervals = intervals.begin();
  	}

  	void insert_interval(T interval){
  		intervals.push_back(interval);
  	}

  	T get_interval(){
  		return (*itIntervals);
  	}

	void increase_iterator(){
		itIntervals++;
	}

  } subHeur;

  //! @brief Exceptions of mc::McCormick
  class Exceptions
  {
  public:
    //! @brief Enumeration type for McCormick exception handling
    enum TYPE{
      DIV=1,	//!< Division by zero
      INV,	//!< Inverse with zero in range
      LOG,	//!< Log with negative values in range
      SQRT,	//!< Square-root with nonpositive values in range
	    DPOW, //!< Power with negative double value in exponent
      ASIN,	//!< Inverse sine or cosine with values outside of \f$[-1,1]\f$ range
      TAN,	//!< Tangent with values outside of \f$[-\frac{\pi}{2}+k\pi,\frac{\pi}{2}+k\pi]\f$ range
      COTH,	//!< Hyperbolic tangent with zero in range
      CHEB,	//!< Chebyshev basis function different from [-1,1] range
      LMTD, //!< LMTD function with non-positive values in range
      RLMTD, //!< RLMTD function with non-positive values in range
      VAPOR_PRESSURE, //!< Temperature <= 0 in range
      IDEAL_GAS_ENTHALPY, //!< Temperature <= 0 in range
      SATURATION_TEMPERATURE, //!< Pressure <= 0 in range
      ENTHALPY_OF_VAPORIZATION, //!< Temperature <= 0 in range
      COST_FUNCTION, //!< Cost function with input <= 0 in range
      COST_FUNCTION_MON, //!< Cost function is not monotonically increasing
      NRTL_TAU, //!< NRTL tau function with Temperature <= 0 in range
      NRTL_DTAU, //!< der NRTL tau function with Temperature <= 0 in range
      NRTL_G, //!< NRTL G function with Temperature <= 0 in range
      NRTL_GTAU, //!< NRTL Gtau function with Temperature <= 0 in range
      NRTL_GDTAU, //!< NRTL Gdtau function with Temperature <= 0 in range
      NRTL_DGTAU, //!< NRTL dGtau function with Temperature <= 0 in range
      IAPWS, //!< Domain violation in IAPWS models
      P_SAT_ETHANOL_SCHROEDER, //!< P_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
      RHO_VAP_SAT_ETHANOL_SCHROEDER, //!< RHO_VAP_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
      RHO_LIQ_SAT_ETHANOL_SCHROEDER, //!< RHO_LIQ_SAT_ETHANOL_SCHROEDER function with Temperature <= 0 in range
      COVARIANCE_FUNCTION, //!< COVARIANCE_FUNCTION with nonpositive values
      ACQUISITION_FUNCTION, //!< ACQUISITION_FUNCTION with nonpositive values for sigma
      REGNORMAL, //!< Regnormal function with nonpositive parameters
      POS, //!< Pos function called with concave relaxation < MVCOMP_TOL
      NEGAT, //!< Neg function called with convex relaxation > MVCOMP_TOL
      LB_FUNC, //!< LB_func function called with concave relaxation < LB
      UB_FUNC, //!< UB_func function called with convex relaxation > UB
      SUM_DIV_VAR, //!< SUM_DIV function called with non-positive variables
      SUM_DIV_COEFF, //!< SUM_DIV function called with non-positive coefficient
      XLOG_SUM_VAR, //!< XLOG_SUM function called with non-positive variables
      XLOG_SUM_COEFF, //!< XLOG_SUM function called with non-positive coefficient
      DEBUG,//!< Error in McCormick Debug mode
      MULTSUB=-3,	//!< Failed to propagate subgradients for a product term with Tsoukalas & Mitsos's multivariable composition result
      ENVEL, 	//!< Failed to compute the convex or concave envelope of a univariate term
      SUB	//!< Inconsistent subgradient dimension between two mc::McCormick variables
    };
    //! @brief Constructor for error <a>ierr</a>
    Exceptions( TYPE ierr ) : _ierr( ierr ){}
    //! @brief Inline function returning the error flag
    int ierr() const { return _ierr; }
    //! @brief Return error description
    std::string what() const {
      switch( _ierr ){
      case DIV:
        return "mc::McCormick\t Relaxation of Division with zero in range. Check if your denominators pass zero and use the pos or neg function.";
      case INV:
        return "mc::McCormick\t Relaxation of Inverse with zero in range. Check if your denominators pass zero and use the pos or neg function.";
      case LOG:
        return "mc::McCormick\t Relaxation of Log with negative values in range.";
      case SQRT:
        return "mc::McCormick\t Relaxation of Square-root with nonpositive values in range.";
      case DPOW:
        return "mc::McCormick\t Relaxation of power function with nonpositive values in range.";
      case ASIN:
        return "mc::McCormick\t Inverse sine with values outside of [-1,1] range.";
      case TAN:
        return "mc::McCormick\t Tangent with values pi/2+k*pi in range.";
      case COTH:
        return "mc::McCormick\t Hyperbolic tangent with zero in range.";
      case CHEB:
        return "mc::McCormick\t Chebyshev basis outside of [-1,1] range.";
      case MULTSUB:
        return "mc::McCormick\t Subgradient propagation failed.";
      case ENVEL:
        return "mc::McCormick\t Convex/concave envelope computation failed.";
      case SUB:
        return "mc::McCormick\t Inconsistent subgradient dimension.";
      case LMTD:
        return "mc::McCormick\t Relaxation of LMTD with non-positive values in range.";
      case RLMTD:
        return "mc::McCormick\t Relaxation of RLMTD with non-positive values in range.";
	    case VAPOR_PRESSURE:
		    return "mc::McCormick\t Relaxation of Vapor Pressure with Temperature <= 0 in range.";
	    case IDEAL_GAS_ENTHALPY:
		    return "mc::McCormick\t Relaxation of Ideal Gas Enthalpy with Temperature <= 0 in range.";
	    case SATURATION_TEMPERATURE:
		    return "mc::McCormick\t Relaxation of Saturation Temperature with pressure <= 0 in range.";
	    case ENTHALPY_OF_VAPORIZATION:
		    return "mc::McCormick\t Relaxation of Enthalpy of Vaporization with Temperature <= 0 in range.";
	    case COST_FUNCTION:
		    return "mc::McCormick\t Relaxation of Cost function with input <= 0 in range.";
	    case COST_FUNCTION_MON:
		    return "mc::McCormick\t Cost function is not monotonically increasing. Please check your model.";
	    case NRTL_TAU:
		    return "mc::McCormick\t Relaxation of NRTL tau function with Temperature <= 0 in range.";
	    case NRTL_DTAU:
		    return "mc::McCormick\t Relaxation of derivative of NRTL tau function with Temperature <= 0 in range.";
	    case NRTL_G:
		    return "mc::McCormick\t Relaxation of NRTL G function with Temperature <= 0 in range.";
      case NRTL_GTAU:
		    return "mc::McCormick\t NRTL G*Tau with temperature <= 0 in range.";
      case NRTL_GDTAU:
		    return "mc::McCormick\t NRTL G*dTau/dT with temperature <= 0 in range.";
      case NRTL_DGTAU:
		    return "mc::McCormick\t NRTL dG/dT*Tau with temperature <= 0 in range.";
      case IAPWS:
		    return "mc::McCormick\t Domain violation in IAPWS model.";
      case P_SAT_ETHANOL_SCHROEDER:
		    return "mc::McCormick\t p_sat_ethanol_schroeder with temperature <= 0 in range.";
      case RHO_LIQ_SAT_ETHANOL_SCHROEDER:
		    return "mc::McCormick\t rho_liq_sat_ethanol_schroeder with temperature <= 0 in range.";
      case RHO_VAP_SAT_ETHANOL_SCHROEDER:
		    return "mc::McCormick\t rho_vap_sat_ethanol_schroeder with temperature <= 0 in range.";
      case COVARIANCE_FUNCTION:
		    return "mc::McCormick\t Relaxation of Covariance function with input < 0 in range.";
	    case ACQUISITION_FUNCTION:
		    return "mc::McCormick\t Relaxation of Acquisition function with input < 0 in range of sigma.";
	    case REGNORMAL:
		    return "mc::McCormick\t Regnormal function with parameters <= 0.";
      case POS:
        {
          std::ostringstream errmsg;
          errmsg << "mc::McCormick\t Pos function called with concave relaxation < " << std::setprecision(16) << machprec() << ".";
          return errmsg.str();
        }
      case NEGAT:
        {
          std::ostringstream errmsg;
          errmsg << "mc::McCormick\t Neg function called with convex relaxation > -" << std::setprecision(16) << machprec() << ".";
          return errmsg.str();
        }
      case LB_FUNC:
        return "mc::McCormick\t LB_func function called with concave relaxation < user defined lower bound.";
      case UB_FUNC:
        return "mc::McCormick\t UB_func function called with convex relaxation > user defined upper bound.";
      case SUM_DIV_VAR:
        return "mc::McCormick\t SUM_DIV function called with non-positive variable.";
      case SUM_DIV_COEFF:
        return "mc::McCormick\t SUM_DIV function called with non-positive coefficient.";
      case XLOG_SUM_VAR:
        return "mc::McCormick\t XLOG_SUM function called with non-positive variable.";
      case XLOG_SUM_COEFF:
        return "mc::McCormick\t XLOG_SUM function called with non-positive coefficient.";
      case DEBUG:
        return "mc::McCormick\t Error in McCormick Debug Mode. An incorrect calculation has occurred.";
      }
      return "mc::McCormick\t Undocumented error.";
    }

  private:
    TYPE _ierr;
  };

  //! @brief Default constructor (needed to declare arrays of McCormick class)
  McCormick():
    _nsub(0), _cvsub(0), _ccsub(0), _const(true)
    {}
  //! @brief Constructor for a constant value <a>c</a>
  McCormick
    ( const double c ):
    _nsub(0), _cv(c), _cc(c), _cvsub(0), _ccsub(0), _const(true)
    {
      Op<T>::I(_I,c);
    }
  //! @brief Constructor for an interval I
  McCormick
    ( const T&I ):
    _nsub(0), _cvsub(0), _ccsub(0), _const(true)
    {
      Op<T>::I(_I,I);
      _cv = Op<T>::l(I); _cc = Op<T>::u(I);
    }
  //! @brief Constructor for a variable, whose range is <a>I</a> and value is <a>c</a>
  McCormick
    ( const T&I, const double c ):
    _nsub(0), _cv(c), _cc(c), _cvsub(0), _ccsub(0), _const(false)
    {
      Op<T>::I(_I,I);
    }
  //! @brief Constructor for a variable, whose range is <a>I</a> and convex and concave bounds are <a>cv</a> and <a>cc</a>
  McCormick
    ( const T&I, const double cv, const double cc ):
    _nsub(0), _cv(cv), _cc(cc), _cvsub(0), _ccsub(0), _const(false)
    {
      Op<T>::I(_I,I); cut();
    }
  //! @brief Copy constructor
  McCormick
    ( const McCormick<T>&MC ):
    _nsub(MC._nsub), _cv(MC._cv), _cc(MC._cc),
    _cvsub(_nsub>0?new double[_nsub]:0), _ccsub(_nsub>0?new double[_nsub]:0),
    _const(MC._const)
    {
      Op<T>::I(_I,MC._I);
      for ( unsigned int ip=0; ip<_nsub; ip++ ){
        _cvsub[ip] = MC._cvsub[ip];
        _ccsub[ip] = MC._ccsub[ip];
      }
    }
  //! @brief Copy constructor doing type conversion for underlying interval
  template <typename U> McCormick
    ( const McCormick<U>&MC ):
    _nsub(MC._nsub), _cv(MC._cv), _cc(MC._cc),
    _cvsub(_nsub>0?new double[_nsub]:0), _ccsub(_nsub>0?new double[_nsub]:0),
    _const(MC._const)
    {
      Op<T>::I(_I,MC._I);
      for ( unsigned int ip=0; ip<_nsub; ip++ ){
        _cvsub[ip] = MC._cvsub[ip];
        _ccsub[ip] = MC._ccsub[ip];
      }
    }


  //! @brief Destructor
  ~McCormick()
    {
      delete [] _cvsub;
      delete [] _ccsub;
    }

  //! @brief Number of subgradient components/directions
  unsigned int& nsub()
    {
      return _nsub;
    }
  unsigned int nsub() const
    {
      return _nsub;
    }
  //! @brief Interval bounds
  T& I()
    {
      return _I;
    }
  const T& I() const
    {
      return _I;
    }
  //! @brief Lower bound
  double l() const
    {
      return Op<T>::l(_I);
    }
  //! @brief Upper bound
  double u() const
    {
      return Op<T>::u(_I);
    }
  //! @brief Convex bound
  double& cv()
    {
      return _cv;
    }
  double cv() const
    {
      return _cv;
    }
  //! @brief Concave bound
  double& cc()
    {
      return _cc;
    }
  double cc() const
    {
      return _cc;
    }
  //! @brief Pointer to a subgradient of convex underestimator
  double*& cvsub()
    {
      return _cvsub;
    }
  const double* cvsub() const
    {
      return _cvsub;
    }
  //! @brief Pointer to a subgradient of concave overestimator
  double*& ccsub()
    {
      return _ccsub;
    }
  const double* ccsub() const
    {
      return _ccsub;
    }
  //! @brief <a>i</a>th component of a subgradient of convex underestimator
  double& cvsub
    ( const unsigned int i )
    {
      return _cvsub[i];
    }
  double cvsub
    ( const unsigned int i ) const
    {
      return _cvsub[i];
    }
  //! @brief <a>i</a>th component of a subgradient of concave overestimator
  double& ccsub
    ( const unsigned int i )
    {
      return _ccsub[i];
    }
  double ccsub
    ( const unsigned int i ) const
    {
      return _ccsub[i];
    }

  //! @brief Set interval bounds
  void I
    ( const T& I )
    {
      Op<T>::I(_I,I);
    }
  //! @brief Set convex bound to <a>cv</a>
  void cv
    ( const double& cv )
    {
      _cv = cv;
      _const = false;
    }
  //! @brief Set concave bound to <a>cc</a>
  void cc
    ( const double& cc )
    {
      _cc = cc;
      _const = false;
    }
  //! @brief Set both convex and concave bounds to <a>c</a>
  void c
    ( const double& c )
    {
      _cv = _cc = c;
      _const = false;
    }

  //! @brief Set dimension of subgradient to <a>nsub</a>
  McCormick<T>& sub
    ( const unsigned int nsub);
  //! @brief Set dimension of subgradient to <a>nsub</a> and variable index <a>isub</a> (starts at 0)
  McCormick<T>& sub
    ( const unsigned int nsub, const unsigned int isub );
  //! @brief Set dimension of subgradient to <a>nsub</a> and subgradient values for the convex and concave relaxations to <a>cvsub</a> and <a>ccsub</a>
  McCormick<T>& sub
    ( const unsigned int nsub, const double*cvsub, const double*ccsub );

  //! @brief Cut convex/concave relaxations at interval bound
  McCormick<T>& cut();
  //! @brief Apply the subgradient interval heuristic by [Najman&Mitsos 2018]
  McCormick<T>& apply_subgradient_interval_heuristic();
  //! @brief Compute affine underestimator at <a>p</a> based on a subgradient value of the convex underestimator at <a>pref</a>
  double laff
    ( const double*p, const double*pref ) const;
  //! @brief Compute lower bound on range <a>Ip</a> based on an affine underestimator of the convex underestimator at <a>pref</a>
  double laff
    ( const T*Ip, const double*pref ) const;
  //! @brief Compute affine overestimator at <a>p</a> based on a subgradient value of the concave overestimator at <a>pref</a>
  double uaff
    ( const double*p, const double*pref ) const;
  //! @brief Compute upper bound on range <a>Ip</a> based on an affine overestimator of the concave overestimator at <a>pref</a>
  double uaff
    ( const T*Ip, const double*pref ) const;
  /** @} */

private:

  //! @brief Number of subgradient components
  unsigned int _nsub;
  //! @brief Interval bounds
  T _I;
  //! @brief Convex bound
  double _cv;
  //! @brief Concave overestimator
  double _cc;
  //! @brief Subgradient of convex underestimator
  double *_cvsub;
  //! @brief Subgradient of concave overestimator
  double *_ccsub;
  //! @brief Whether the convex/concave bounds are constant
  bool _const;

  //! @brief Enum for easier understanding of heuristics
  enum CONVEXITY{
	  CONV_NONE = 0,	//!< not convex or concave
	  CONVEX,			//!< convex
	  CONCAVE			//!< concave
  };

  //! @brief Enum for easier understanding of heuristics
  enum MONOTONICITY{
	  MON_NONE = 0,		//!< not increasing or decreasing
	  MON_INCR,			//!< increasing
	  MON_DECR			//!< decreasing
  };

  //! @brief Set subgradient size to <a>nsub</a> and specify if convex/concave bounds are constant with <a>cst</a>
  void _sub
    ( const unsigned int nsub, const bool cst );
  //! @brief Reset subgradient arrays
  void _sub_reset();
  //! @brief Resize subgradient arrays
  void _sub_resize
    ( const unsigned int nsub );
  //! @brief Copy subgradient arrays
  void _sub_copy
    ( const McCormick<T>&MC );

  //! @brief Compute McCormick relaxation of summation term u1+u2 with u2 constant
  McCormick<T>& _sum1
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of summation term u1+u2 with neither u1 nor u2 constant
  McCormick<T>& _sum2
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );

  //! @brief Compute McCormick relaxation of subtraction term u1-u2 with u2 constant
  McCormick<T>& _sub1
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of subtraction term u1-u2 with u1 constant
  McCormick<T>& _sub2
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of subtraction term u1-u2 with neither u1 nor u2 constant
  McCormick<T>& _sub3
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );

  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1 >= 0 and u2l >= 0, with u2 constant
  McCormick<T>& _mul1_u1pos_u2pos
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1 >= 0 and u2l >= 0, with neither u1 nor u2 constant
  McCormick<T>& _mul2_u1pos_u2pos
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with u2 constant
  McCormick<T>& _mul1_u1pos_u2mix
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with u1 constant
  McCormick<T>& _mul2_u1pos_u2mix
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1l >= 0 and u2l <= 0 <= u2u, with neither u1 nor u2 constant
  McCormick<T>& _mul3_u1pos_u2mix
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1l <= 0 <= u1u and u2l <= 0 <= u2u, with u2 constant
  McCormick<T>& _mul1_u1mix_u2mix
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 in the case u1l <= 0 <= u1u and u2l <= 0 <= u2u, with neither u1 nor u2 constant
  McCormick<T>& _mul2_u1mix_u2mix
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );
  //! @brief Compute McCormick relaxation of product term u1*u2 using Tsoukalas & Mitsos multivariable composition result
  McCormick<T>& _mulMV
    ( const McCormick<T>&MC1, const McCormick<T>&MC2 );

  //! @brief Prototype function for finding junction points in convex/concave envelopes of univariate terms
  typedef double (puniv)
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr );
  //! @brief Newton method for root finding
  static double _newton
    ( const double x0, const double xL, const double xU, puniv f,
      puniv df, const double*rusr, const int*iusr=0, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Secant method for root finding
  static double _secant
    ( const double x0, const double x1, const double xL, const double xU,
      puniv f, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Golden section search method for root finding
  static double _goldsect
    ( const double xL, const double xU, puniv f, const double*rusr,
      const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Golden section search iterations
  static double _goldsect_iter
    ( const bool init, const double a, const double fa, const double b,
      const double fb, const double c, const double fc, puniv f,
      const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of odd power terms
  static double* _oddpowcv
    ( const double x, const int iexp, const double xL, const double xU );
  //! @brief Compute concave envelope of odd power terms
  static double* _oddpowcc
    ( const double x, const int iexp, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of odd power terms
  static double _oddpowenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of odd power terms
  static double _oddpowenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

 //! @brief Compute convex envelope of odd Chebyshev terms
  static double* _oddchebcv
    ( const double x, const int iord, const double xL, const double xU );
  //! @brief Compute concave envelope of odd Chebyshev terms
  static double* _oddchebcc
    ( const double x, const int iord, const double xL, const double xU );
  //! @brief Compute convex envelope of even Chebyshev terms
  static double* _evenchebcv
    ( const double x, const int iord, const double xL, const double xU );

  //! @brief Compute convex envelope of erf terms
  static double* _erfcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of erf terms
  static double* _erfcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of erf terms
  static double _erfenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of erf terms
  static double _erfenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of atan terms
  static double* _atancv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of atan terms
  static double* _atancc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of atan terms
  static double _atanenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of atan terms
  static double _atanenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of sinh terms
  static double* _sinhcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of sinh terms
  static double* _sinhcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of sinh terms
  static double _sinhenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of sinh terms
  static double _sinhenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of tanh terms
  static double* _tanhcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of tanh terms
  static double* _tanhcc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of tanh terms
  static double _tanhenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of tanh terms
  static double _tanhenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of xexpax = x*exp(a*x) terms
  static double* _xexpaxcv
    ( const double x, const double xL, const double xU, const double a );
  //! @brief Compute concave envelope of xexpax = x*exp(a*x) terms
  static double* _xexpaxcc
    ( const double x, const double xL, const double xU, const double a );
  //! @brief Compute residual value for junction points in the envelope of xexpax = x*exp(a*x) terms
  static double _xexpaxenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of xexpax = x*exp(a*x) terms
  static double _xexpaxenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute value of convex relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double _xlog_sum_cv_val
    ( const std::vector<double> &x, const std::vector<double> &coeffs, const std::vector<double> &borderPoint,
      const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds );
  //! @brief Compute value of concave relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double _xlog_sum_cc_val
    ( const std::vector<double> &x, const std::vector<double> &coeffs, const std::vector<double> &borderPoint,
      const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds );
  //! @brief Compute convex relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double* _xlog_sum_cv
    ( std::vector<double> &xcv, const double xcc, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, const std::vector<double> &xcc_vec);
  //! @brief Compute concave relaxation of xlog_sum = x*log(a1*x+b1*y1+...)
  static double* _xlog_sum_cc
    ( std::vector<double> &xcc, const double xcv, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds );
  //! @brief Compute residual derivative for junction points in the first convex relaxation of x*log(a1*x+b1*y1+...) terms
  static double _xlog_sum_cv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual value for junction points in the first convex relaxation of x*log(a1*x+b1*y1+...) terms
  static double _xlog_sum_cv_ddfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute derivative/subgradient value of the convex relaxation of xlog_sum w.r.t. some direction
  static double _xlog_sum_cv_partial_derivative
	( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, unsigned facet, unsigned direction );
  //! @brief Compute derivative/subgradient value of the concave relaxation of xlog_sum w.r.t. some direction
  static double _xlog_sum_cc_partial_derivative
	( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds,
      const std::vector<double> &upperIntervalBounds, unsigned facet, unsigned direction );

  //! @brief Compute convex envelope of nonlinear function 1
  static double* _regnormal_cv
    ( const double x, const double a, const double b, const double xL, const double xU );
  //! @brief Compute concave envelope of nonlinear function 1
  static double* _regnormal_cc
    ( const double x, const double a, const double b, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of nonlinear function 1
  static double _regnormal_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of nonlinear function 1
  static double _regnormal_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Provide solution point of the equality der_gpdf(xj) = (gpdf(xj)-gpdf(fixed_point))/(xj-fixed_point)
  static double _gpdf_compute_sol_point
    ( const double xL, const double xU, const double starting_point, const double fixed_point );
  //! @brief Compute residual value for junction points in the envelope of gaussian probability density function
  static double _gpdf_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of gaussian probability density function
  static double _gpdf_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of a step at 0
  static double* _stepcv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of a step at 0
  static double* _stepcc
    ( const double x, const double xL, const double xU );

  //! @brief Compute arg min & arg max for the cos envelope
  static double* _cosarg
    (  const double xL, const double xU );
  //! @brief Compute convex envelope of cos terms
  static double* _coscv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of cos terms
  static double* _coscc
    ( const double x, const double xL, const double xU );
  //! @brief Compute convex envelope of cos terms in [-PI,PI]
  static double* _coscv2
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of cos terms
  static double _cosenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of cos terms
  static double _cosenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of asin terms
  static double* _asincv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of asin terms
  static double* _asincc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of asin terms
  static double _asinenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of asin terms
  static double _asinenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );

  //! @brief Compute convex envelope of tan terms
  static double* _tancv
    ( const double x, const double xL, const double xU );
  //! @brief Compute concave envelope of tan terms
  static double* _tancc
    ( const double x, const double xL, const double xU );
  //! @brief Compute residual value for junction points in the envelope of tan terms
  static double _tanenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  //! @brief Compute residual derivative for junction points in the envelope of tan terms
  static double _tanenv_dfunc
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr=std::vector<double>() );
  // @AVT.SVT 01.09.2017 added for computation of convex envelope of enthalpy of vaporization models
  //! @brief Compute convex envelope of enthalpy of vaporization models
  static double _dhvapenv_func
    ( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr );
  /**
	* @brief Compute monotonicity and convexity of the Guthrie cost function
	*
	* added by AVT.SVT 06.11.2017
	*
	* @param[in,out] monotonicity	 defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity       defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] p1      			 parameter p1 of Guthrie function
	* @param[in] p2      			 parameter p2 of Guthrie function
	* @param[in] p3      			 parameter p3 of Guthrie function
	* @param[in] l     				 lower bound
	* @param[in] u      			 upper bound
	*/
  static void _cost_Guthrie_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double p1, const double p2, const double p3, const double l, const double u );
 /**
	* @brief Compute monotonicity and convexity of the tau function of an NRTL model
	*
	* added by AVT.SVT 23.11.2017
	*
	* @param[in,out] monotonicity	defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity      defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] a      			parameter a of tau function
	* @param[in] b      			parameter b of tau function
	* @param[in] e      			parameter e of tau function
	* @param[in] f      			parameter f of tau function
	* @param[in] l     				lower bound
	* @param[in] u      			upper bound
	* @param[in,out] new_l     		lower range bound computed in the case of non-monotonicity
	* @param[in,out] new_u      	upper range bound computed in the case of non-monotonicity
	* @param[in,out] zmin     		point where lower bound computed in the case of non-monotonicity is attained
	* @param[in,out] zmax      		point where upper bound computed in the case of non-monotonicity is attained
	*/
  static void _nrtl_tau_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double a, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax );
  /**
	* @brief Compute monotonicity and convexity of the derivative of the tau function of an NRTL model
	*        Theorems 1 and 2 of Najman, Bongartz & Mitsos 2019 Relaxations of thermodynamic property and costing models in process engineering
	*
	* added by AVT.SVT 23.11.2017
	*
	* @param[in,out] monotonicity	defines the monotonicity of the function 0 = not monotonic, 1 = incr, 2 = decr
	* @param[in,out] convexity      defines the convexity of the function 0 = not convex/concave, 1 = conv, 2 = conc
	* @param[in] b      			parameter b of tau function
	* @param[in] e      			parameter e of tau function
	* @param[in] f      			parameter f of tau function
	* @param[in] l     				lower bound
	* @param[in] u      			upper bound
	* @param[in,out] new_l     		lower range bound computed in the case of non-monotonicity
	* @param[in,out] new_u      	upper range bound computed in the case of non-monotonicity
	* @param[in,out] zmin     		point where lower bound computed in the case of non-monotonicity is attained
	* @param[in,out] zmax      		point where upper bound computed in the case of non-monotonicity is attained
	*/
  static void _nrtl_der_tau_mon_conv
	( unsigned int &monotonicity, unsigned int &convexity, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax );

#ifdef  MC__MCCORMICK_DEBUG
  /**
	* @brief Perform a simple check if the McCormick result of a univariate operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin			the variable inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const McCormick<T> &MCin, const McCormick<T> &MCout, std::string &operation);
   /**
	* @brief Perform a simple check if the McCormick result of a multivariate operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin1			the 1st variable inserted into the operation
	* @param[in] MCin2			the 2nd variable inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const McCormick<T> &MCin1, const McCormick<T> &MCin2, const McCormick<T> &MCout, std::string &operation);
   /**
	* @brief Perform a simple check if the McCormick result of a multivariate (>2D) operation is valid
	*
	* added by AVT.SVT 12.04.2018
	*
	* @param[in] MCin			the variables inserted into the operation
	* @param[in] MCout      	the result of the operation
	* @param[in] operation      string defining the name of the operation
	*/
  static void _debug_check
    ( const std::vector<McCormick<T>> &MCin, const McCormick<T> &MCout, std::string &operation);
#endif
  //! @brief  array containing precomputed roots of Q^k(x) up to power 2*k+1 for k=1,...,10 [Liberti & Pantelides (2002), "Convex envelopes of Monomial of Odd Degree]
  static double _Qroots[10];
};

////////////////////////////////////////////////////////////////////////
template <typename T>
double McCormick<T>::_Qroots[10] = { -0.5000000000, -0.6058295862, -0.6703320476, -0.7145377272, -0.7470540749,
                                     -0.7721416355, -0.7921778546, -0.8086048979, -0.8223534102, -0.8340533676 };


template <typename T> inline void
McCormick<T>::_sub_reset()
{
  delete [] _cvsub;
  delete [] _ccsub;
  _cvsub = _ccsub = 0;
}

template <typename T> inline void
McCormick<T>::_sub_resize
( const unsigned int nsub )
{
  if( _nsub != nsub ){
    delete [] _cvsub;
    delete [] _ccsub;
    _nsub = nsub;
    if( _nsub > 0 ){
      _cvsub = new double[_nsub];
      _ccsub = new double[_nsub];
    }
    else{
      _cvsub = _ccsub = 0;
      return;
    }
  }
}

template <typename T> inline void
McCormick<T>::_sub_copy
( const McCormick<T>&MC )
{
  _sub_resize( MC._nsub );
  for ( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = MC._cvsub[i];
    _ccsub[i] = MC._ccsub[i];
  }
  return;
}

template <typename T> inline void
McCormick<T>::_sub
( const unsigned int nsub, const bool cst )
{
  _sub_resize( nsub );
  for ( unsigned int i=0; i<nsub; i++ ){
    _cvsub[i] = _ccsub[i] = 0.;
  }
  _const = cst;
}

template <typename T> inline McCormick<T>&
McCormick<T>::sub
( const unsigned int nsub )
{
  _sub( nsub, false );
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::sub
( const unsigned int nsub, const unsigned int isub )
{
  if( isub >= nsub ) throw Exceptions( Exceptions::SUB );
  sub( nsub );
  _cvsub[isub] = _ccsub[isub] = 1.;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::sub
( const unsigned int nsub, const double*cvsub, const double*ccsub )
{
  if( nsub && !(cvsub && ccsub) ) throw Exceptions( Exceptions::SUB );
  sub( nsub );
  for ( unsigned int i=0; i<nsub; i++ ){
    _cvsub[i] = cvsub[i];
    _ccsub[i] = ccsub[i];
  }
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::cut()
{
  if( _cv < Op<T>::l(_I) ){
    _cv = Op<T>::l(_I);
    for( unsigned int i=0; i<_nsub; i++ ) _cvsub[i] = 0.;
  }
  if( _cc > Op<T>::u(_I) ){
    _cc = Op<T>::u(_I);
    for( unsigned int i=0; i<_nsub; i++ ) _ccsub[i] = 0.;
  }
  return *this;
}

/**
 * @brief Possibly improve the interval bounds through subgradients, for further info see [Najman&Mitsos2018]
 */
template <typename T> inline McCormick<T>&
McCormick<T>::apply_subgradient_interval_heuristic()
{

  if(!mc::McCormick<T>::subHeur.usePrecomputedIntervals){
	  double l = Op<T>::l(_I);
	  double u = Op<T>::u(_I);
	  // Only do something if the object is not constant
	  if(Op<T>::l(_I) < Op<T>::u(_I)){
		  double tempcv = _cv;
		  double tempcc = _cc;
		  for(unsigned int i = 0; i<_nsub; i++){
			// Check the sign of the subgradients to go in the correct direction on the linearization
			// Convex
			if(_cvsub[i]>0.){
				tempcv=tempcv+_cvsub[i]*( (*mc::McCormick<T>::subHeur.originalLowerBounds)[i] - (*mc::McCormick<T>::subHeur.referencePoint)[i]);
			}else{
				tempcv=tempcv+_cvsub[i]*( (*mc::McCormick<T>::subHeur.originalUpperBounds)[i] - (*mc::McCormick<T>::subHeur.referencePoint)[i]);
			}
			// Concave
			if(_ccsub[i]>0.){
				tempcc=tempcc+_ccsub[i]*( (*mc::McCormick<T>::subHeur.originalUpperBounds)[i] - (*mc::McCormick<T>::subHeur.referencePoint)[i]);
			}else{
				tempcc=tempcc+_ccsub[i]*( (*mc::McCormick<T>::subHeur.originalLowerBounds)[i] - (*mc::McCormick<T>::subHeur.referencePoint)[i]);
			}
		  }// End of for i
		  if(tempcv <= tempcc){ // We don't want to violate the interval consistency
			  // If we improved, replace the appropriate bound
			  if(tempcv > Op<T>::l(_I) && !isequal(tempcv, Op<T>::l(_I), mc::McCormick<T>::options.MVCOMP_TOL, mc::McCormick<T>::options.MVCOMP_TOL)){
				l = tempcv;
			  }
			  if(tempcc < Op<T>::u(_I) && !isequal(tempcc, Op<T>::u(_I), mc::McCormick<T>::options.MVCOMP_TOL, mc::McCormick<T>::options.MVCOMP_TOL)){
				u = tempcc;
			  }
		  }
#ifdef MC__MCCORMICK_DEBUG
		  else{
			 if(!isequal(tempcv,tempcc, mc::McCormick<T>::options.MVCOMP_TOL, mc::McCormick<T>::options.MVCOMP_TOL)){
				 std::cout << "Bound crossing in subgradient interval heuristic! " << std::endl;
				 std::cout << "tempcv = " << std::setprecision(16) << tempcv << " > " << std::setprecision(16) << tempcc << " = tempcc " << std::endl;
                 throw std::runtime_error("mc::McCormick\t Error in subgradient heuristic.");
			 }
		  }
#endif
		  _I = T(l,u);
	  }// End of if l < u
	  mc::McCormick<T>::subHeur.insert_interval(_I);
  }else{
	  _I = mc::McCormick<T>::subHeur.get_interval();
	  mc::McCormick<T>::subHeur.increase_iterator();
  }
  return *this;
}

template <typename T> inline double
McCormick<T>::laff
( const double*p, const double*pref ) const
{
  double _laff = _cv;
  for( unsigned int i=0; i<_nsub; i++ ){
    _laff += _cvsub[i]*(p[i]-pref[i]);
  }
  return _laff;
}

template <typename T> inline double
McCormick<T>::laff
( const T*Ip, const double*pref ) const
{
  double _laff = _cv;
  for( unsigned int i=0; i<_nsub; i++ ){
    _laff += Op<T>::l(_cvsub[i]*(Ip[i]-pref[i]));
  }
  return _laff;
}

template <typename T> inline double
McCormick<T>::uaff
( const double*p, const double*pref ) const
{
  double _uaff = _cc;
  for( unsigned int i=0; i<_nsub; i++ ){
    _uaff += _ccsub[i]*(p[i]-pref[i]);
  }
  return _uaff;
}

template <typename T> inline double
McCormick<T>::uaff
( const T*Ip, const double*pref ) const
{
  double _uaff = _cc;
  for( unsigned int i=0; i<_nsub; i++ ){
    _uaff += Op<T>::u(_ccsub[i]*(Ip[i]-pref[i]));
  }
  return _uaff;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator=
( const double c )
{
  _I = c;
  _cv = _cc = c;
  _sub_reset();
  _nsub = 0;
  _const = true;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator=
( const T&I )
{
  _I = I;
  _cv = Op<T>::l(I);
  _cc = Op<T>::u(I);
  _sub_reset();
  _nsub = 0;
  _const = true;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator=
( const McCormick<T>&MC )
{
  if( this == &MC ) return *this;
  _I = MC._I;
  _cv = MC._cv;
  _cc = MC._cc;
  _sub_copy( MC );
  _const = MC._const;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator+=
( const double a )
{
  _I += a;
  _cv += a;
  _cc += a;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator+=
( const McCormick<T> &MC )
{
  McCormick<T> MC2 = *this + MC;
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "+=";
	McCormick<T>::_debug_check(MC,MC2, *this, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_sum1
( const McCormick<T>&MC1, const McCormick<T>&MC2 ) // MC2 is const
{
  _I = MC1._I + MC2._I;
  _cv = MC1._cv + MC2._cv;
  _cc = MC1._cc + MC2._cc;
  for( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = MC1._cvsub[i];
    _ccsub[i] = MC1._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "sum1";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_sum2
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  _I = MC1._I + MC2._I;
  _cv = MC1._cv + MC2._cv;
  _cc = MC1._cc + MC2._cc;
  for( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = MC1._cvsub[i] + MC2._cvsub[i];
    _ccsub[i] = MC1._ccsub[i] + MC2._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "sum2";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator-=
( const double a )
{
  _I -= a;
  _cv -= a;
  _cc -= a;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator-=
( const McCormick<T> &MC )
{
   McCormick<T> MC2 = *this -MC;
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "-=";
	McCormick<T>::_debug_check(*this, MC, MC2,  str);
#endif
   *this = MC2;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_sub1
( const McCormick<T>&MC1, const McCormick<T>&MC2 ) // MC2 is const
{
  _I = MC1._I - MC2._I;
  _cv = MC1._cv - MC2._cc;
  _cc = MC1._cc - MC2._cv;
  for( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = MC1._cvsub[i];
    _ccsub[i] = MC1._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "sub1";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_sub2
( const McCormick<T>&MC1, const McCormick<T>&MC2 ) // MC1 is const
{
  _I = MC1._I - MC2._I;
  _cv = MC1._cv - MC2._cc;
  _cc = MC1._cc - MC2._cv;
  for( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = -MC2._ccsub[i];
    _ccsub[i] = -MC2._cvsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "sub2";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_sub3
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  _I = MC1._I - MC2._I;
  _cv = MC1._cv - MC2._cc;
  _cc = MC1._cc - MC2._cv;
  for( unsigned int i=0; i<_nsub; i++ ){
    _cvsub[i] = MC1._cvsub[i] - MC2._ccsub[i];
    _ccsub[i] = MC1._ccsub[i] - MC2._cvsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
    std::string str = "sub3";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator*=
( const double a )
{
  McCormick<T> MC2 = a * (*this);
  *this = MC2;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator*=
( const McCormick<T>&MC )
{
  if( _const && !MC._const ) sub( MC._nsub );
  McCormick<T> MC2 = MC * (*this);
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "*=";
	McCormick<T>::_debug_check(MC,*this,MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul1_u1pos_u2pos
( const McCormick<T>&MC1, const McCormick<T>&MC2 )  // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
 //@AVT.SVT 04.08.2017 changed from cv1>cv2 since DAG representation returns minimally different results (diference is <ENVEL_TOL)
 //                    it has to do with saving results and calling for results in INTEL processors
  if ( cv1 > cv2 ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
//@AVT.SVT 04.08.2017 changed from cc1 < cc2 since DAG representation returns minimally different results (diference is <ENVEL_TOL)
//  				  it has to do with saving results and calling for results in INTEL processors
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul1_u1pos_u2pos";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  // if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul2_u1pos_u2pos
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);

  if ( cv1 > cv2 ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i] + Op<T>::u(MC1._I) * MC2._cvsub[i];
  }else                            // else here was missing before, added on 08.07.2016 at the AVT.SVT
  {
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i] + Op<T>::l(MC1._I) * MC2._cvsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i] + Op<T>::u(MC1._I) * MC2._ccsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i] + Op<T>::l(MC1._I) * MC2._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul2_u1pos_u2pos";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul1_u1pos_u2mix
( const McCormick<T>&MC1, const McCormick<T>&MC2 )  // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
  if ( cv1 > cv2 ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul1_u1pos_u2mix";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  // if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul2_u1pos_u2mix
( const McCormick<T>&MC1, const McCormick<T>&MC2 )  // MC1 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
  if ( cv1 > cv2  ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC1._I) * MC2._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC1._I) * MC2._cvsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC1._I) * MC2._ccsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC1._I) * MC2._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG

	std::string str = "mul2_u1pos_u2mix";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);

#endif
  // if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul3_u1pos_u2mix
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
  if ( cv1 > cv2 ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i] + Op<T>::u(MC1._I) * MC2._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i] + Op<T>::l(MC1._I) * MC2._cvsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i] + Op<T>::u(MC1._I) * MC2._ccsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i] + Op<T>::l(MC1._I) * MC2._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul3_u1pos_u2mix";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul1_u1mix_u2mix
( const McCormick<T>&MC1, const McCormick<T>&MC2 ) // MC2 is const so no subgradient heuristic needed
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
  if ( cv1 > cv2  ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul1_u1mix_u2mix";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  // if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mul2_u1mix_u2mix
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  _I = MC1._I * MC2._I;

  double cv1 = Op<T>::u(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cv
    - Op<T>::u(MC1._I) * Op<T>::u(MC2._I);
  double cv2 = Op<T>::l(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cc
    - Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
  if ( cv1 > cv2 ){
    _cv = cv1;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::u(MC2._I) * MC1._cvsub[i] + Op<T>::u(MC1._I) * MC2._cvsub[i];
  }
  else{
    _cv = cv2;
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = Op<T>::l(MC2._I) * MC1._ccsub[i] + Op<T>::l(MC1._I) * MC2._ccsub[i];
  }

  double cc1 = Op<T>::l(MC2._I) * MC1._cv + Op<T>::u(MC1._I) * MC2._cc
    - Op<T>::u(MC1._I) * Op<T>::l(MC2._I);
  double cc2 = Op<T>::u(MC2._I) * MC1._cc + Op<T>::l(MC1._I) * MC2._cv
    - Op<T>::l(MC1._I) * Op<T>::u(MC2._I);
  if ( cc1 < cc2 ){
    _cc = cc1;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::l(MC2._I) * MC1._cvsub[i] + Op<T>::u(MC1._I) * MC2._ccsub[i];
  }
  else{
    _cc = cc2;
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = Op<T>::u(MC2._I) * MC1._ccsub[i] + Op<T>::l(MC1._I) * MC2._cvsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "mul2_u1mix_u2mix";
	McCormick<T>::_debug_check(MC1,MC2, *this, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::_mulMV
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
 // Convex underestimator part
 {const double k = - Op<T>::diam(MC2._I) / Op<T>::diam(MC1._I);
  const double z = ( Op<T>::u(MC1._I)*Op<T>::u(MC2._I)
                   - Op<T>::l(MC1._I)*Op<T>::l(MC2._I) )
                   / Op<T>::diam(MC1._I);
  struct fct{
    static double t1
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return Op<T>::u(MC2._I) * x1 + Op<T>::u(MC1._I) * x2
	     - Op<T>::u(MC2._I) * Op<T>::u(MC1._I); }
    static double t2
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return Op<T>::l(MC2._I) * x1 + Op<T>::l(MC1._I) * x2
	     - Op<T>::l(MC2._I) * Op<T>::l(MC1._I); }
    static double t
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return std::max( t1(x1,x2,MC1,MC2), t2(x1,x2,MC1,MC2) ); }
  };

  // 23.08.2016
  // modified by AVT.SVT after realizing that the closed form for multiplication given in
  // Tsoukalas & Mitsos 2014 is not correct
  int imid[4] = { -1, -1, -1, -1 };
  const double x1t[6] = { MC1._cv, MC1._cc,
                          mid_ndiff( MC1._cv, MC1._cc, (MC2._cv-z)/k, imid[0] ),
						  mid_ndiff( MC1._cv, MC1._cc, (MC2._cc-z)/k, imid[1] ),
						  // added:
                          MC1._cv, MC1._cc
						};
  const double x2t[6] = { mid_ndiff( MC2._cv, MC2._cc, k*MC1._cv+z, imid[2] ),
						  mid_ndiff( MC2._cv, MC2._cc, k*MC1._cc+z, imid[3] ),
                          MC2._cv, MC2._cc,
						  // added:
                          MC2._cv, MC2._cc
						};

  const double v[6] = { fct::t( x1t[0], x2t[0], MC1, MC2 ), fct::t( x1t[1], x2t[1], MC1, MC2 ),
                        fct::t( x1t[2], x2t[2], MC1, MC2 ), fct::t( x1t[3], x2t[3], MC1, MC2 ),
// added the two corners (MC1._cv,MC2._cv),(MC1._cc,MC2._cc) for the convex relaxation specifically, since
// they can be excluded by the mid() if the envelope of the multiplication is monotone
                        fct::t( x1t[4], x2t[4], MC1, MC2 ),
                        fct::t( x1t[5], x2t[5], MC1, MC2 )
                        };

  const unsigned int ndx = argmin( 6, v ); // 6 elements now
  _cv = v[ndx];

  if( _nsub ){
    double myalpha;
    if( isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ),
                 fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),
		 options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 ) ){
      std::pair<double,double> alpha( 0., 1. );
     bool MC1thin = isequal( MC1._cv, MC1._cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
        true: false;
      if( !MC1thin && x1t[ndx] > MC1._cv ){
                // 29.08.2016
                // modified by AVT.SVT
                // we had to add an additional if-statement where we question if the two values x[ndx] and MC._cv
                // are equal since we only work with a given tolerance (MVCOMP_TOL)
                // the if-statement is added 4 times in the convex subgradient and 4 times in the concave subgradient
        if(!isequal(x1t[ndx], MC1._cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.second = std::min( alpha.second, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
                }
      }
      if( !MC1thin && x1t[ndx] < MC1._cc ){
       if(!isequal(x1t[ndx], MC1._cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                alpha.first = std::max( alpha.first, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
       }
      }
      bool MC2thin = isequal( MC2._cv, MC2._cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
        true: false;


      if( !MC2thin && x2t[ndx] > MC2._cv ){
        if(!isequal(x2t[ndx], MC2._cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.second = std::min( alpha.second, -Op<T>::l(MC1._I)/Op<T>::diam(MC1._I) );
                }
      }
      if( !MC2thin && x2t[ndx] < MC2._cc ){
        if(!isequal(x2t[ndx], MC2._cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.first = std::max( alpha.first, -Op<T>::l(MC1._I)/Op<T>::diam(MC1._I) );
                }
      }
      bool alphathin = isequal( alpha.first, alpha.second, options.MVCOMP_TOL, options.MVCOMP_TOL )?
        true: false;
      if( !alphathin && alpha.first > alpha.second ){
#ifdef MC__MCCORMICK_DEBUG
		std::cout << "WARNING1: alphaL= " << std::setprecision(std::numeric_limits<double>::digits10+1)  << alpha.first << "  alphaU= " << alpha.second << std::setprecision(6)
				  << std::endl;
		std::cout << "x1t: " << std::setprecision(std::numeric_limits<double>::digits10+1) << x1t[ndx] << " x2t: " << x2t[ndx] << std::endl;
		std::cout << "f(x1t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) << " f(x2t): " << fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) << std::endl;
		std::cout << "f(x1t)-f(x2t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) - fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) << std::endl;
		std::cout << "MC1thin: " << MC1thin << " MC2thin: " << MC2thin <<std::endl;
		std::cout << "x1t[ndx] > MC1._cv: " << (x1t[ndx] > MC1._cv) << " x1t[ndx] < MC1._cc " << (x1t[ndx] < MC1._cc) << std::endl;
		std::cout << "isequal(x1t, MC1._cv): " << isequal(x1t[ndx], MC1._cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x1t, MC1._cc): " << isequal(x1t[ndx], MC1._cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
		std::cout << "x2t[ndx] > MC2._cv: " << (x2t[ndx] > MC2._cv) << " x2t[ndx] < MC2._cc " << (x2t[ndx] < MC2._cc) <<std::endl;
		std::cout << "isequal(x2t, MC2._cv): " << isequal(x2t[ndx], MC2._cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x2t, MC2._cc): " << isequal(x2t[ndx], MC2._cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
		std::cout << "MC1.cv: "<< std::setprecision(std::numeric_limits<double>::digits10+1) << MC1._cv << " MC1.cc:" << std::setprecision(std::numeric_limits<double>::digits10+1) << MC1._cc <<std::endl;
		std::cout << "MC2.cv: " << MC2._cv << " MC2.cc:" << MC2._cc <<std::endl;
		std::cout << "MC1.l: " << Op<T>::l(MC1._I) << " MC1.u:" << Op<T>::u(MC1._I) << " MC1.diam: " << Op<T>::diam(MC1._I) << std::endl;
		std::cout << "MC2.l: " << Op<T>::l(MC2._I) << " MC2.u:" << Op<T>::u(MC2._I) << " MC2.diam: " << Op<T>::diam(MC2._I)<< std::endl;
		std::cout << "_cv: " << _cv << " _cc: " << _cc <<std::endl;
		std::cout << "_nsub: " << _nsub <<std::endl;
		std::cout << "subcv1: " << MC1._cvsub[0] << " subcc1: " << MC1._ccsub[0] <<std::endl;
		std::cout << "subcv2: " << MC2._cvsub[0] << " subcc2: " << MC2._ccsub[0] <<std::endl;
		std::cout << "isequal: " <<  isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ), fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),options.MVCOMP_TOL, options.MVCOMP_TOL ) <<std::endl;
		std::cout << "MVCOMP_TOL " << options.MVCOMP_TOL <<std::endl;
		std::cout << "Exception in multivariate binary product: Try increasing MC_mvcomp_tol and MC_envel_tol in the MAiNGO Settings file." <<std::endl;
        throw Exceptions( Exceptions::MULTSUB );
#endif
		// currently testing
		alpha.first  = 1;
		alpha.second = 0;
      }
      myalpha = 0.5*( alpha.first + alpha.second );
    }
    else if( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) > fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) )
      myalpha = 1.;
    else
      myalpha = 0.;
    double sigma1cv = Op<T>::l(MC2._I) + myalpha * Op<T>::diam(MC2._I),
           sigma2cv = Op<T>::l(MC1._I) + myalpha * Op<T>::diam(MC1._I);
    for( unsigned int i=0; i<_nsub; i++ )
      _cvsub[i] = ( sigma1cv>=0? (MC1._const? 0.:MC1._cvsub[i]):
                                 (MC1._const? 0.:MC1._ccsub[i]) ) * sigma1cv
                + ( sigma2cv>=0? (MC2._const? 0.:MC2._cvsub[i]):
		                 (MC2._const? 0.:MC2._ccsub[i]) ) * sigma2cv;
  }

 }

 // Concave overestimator part
 {const double k = Op<T>::diam(MC2._I) / Op<T>::diam(MC1._I);
  const double z = ( Op<T>::u(MC1._I)*Op<T>::l(MC2._I)
                   - Op<T>::l(MC1._I)*Op<T>::u(MC2._I) )
                   / Op<T>::diam(MC1._I);
  struct fct{
    static double t1
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return Op<T>::l(MC2._I) * x1 + Op<T>::u(MC1._I) * x2
	     - Op<T>::l(MC2._I) * Op<T>::u(MC1._I); }
    static double t2
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return Op<T>::u(MC2._I) * x1 + Op<T>::l(MC1._I) * x2
	     - Op<T>::u(MC2._I) * Op<T>::l(MC1._I); }
    static double t
      ( const double x1, const double x2, const McCormick<T>&MC1,
        const McCormick<T>&MC2 )
      { return std::min( t1(x1,x2,MC1,MC2), t2(x1,x2,MC1,MC2) ); }
  };

  // 23.08.2016
  // modified by AVT.SVT after realizing that the closed form for multiplication given in
  // Tsoukalas & Mitsos 2014 is not correct
  int imid[4] = { -1, -1, -1, -1 };
  const double x1t[6] = { MC1._cv, MC1._cc,
                          mid_ndiff( MC1._cv, MC1._cc, (MC2._cv-z)/k, imid[0] ),
                          mid_ndiff( MC1._cv, MC1._cc, (MC2._cc-z)/k, imid[1] ),
						  // Added:
                          MC1._cv, MC1._cc
						};
  const double x2t[6] = { mid_ndiff( MC2._cv, MC2._cc, k*MC1._cv+z, imid[2] ),
                          mid_ndiff( MC2._cv, MC2._cc, k*MC1._cc+z, imid[3] ),
						  // Added:
                          MC2._cv, MC2._cc,
                          MC2._cc,MC2._cv
						};
  const double v[6] = { fct::t( x1t[0], x2t[0], MC1, MC2 ), fct::t( x1t[1], x2t[1], MC1, MC2 ),
                        fct::t( x1t[2], x2t[2], MC1, MC2 ), fct::t( x1t[3], x2t[3], MC1, MC2 ),
   // added the two corners (MC1._cv,MC2._cc),(MC1._cc,MC2._cv) for the concave relaxation specifically, since
   // they can be excluded by the mid() if the envelope of the multiplication is monotone
                        fct::t( x1t[4], x2t[4], MC1, MC2 ),
                        fct::t( x1t[5], x2t[5], MC1, MC2 )
                        };

  const unsigned int ndx = argmax( 6, v );	 // 6 elements now
  _cc = v[ndx];

  if( _nsub ){
    double myalpha;
    if( isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ),
                 fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ),
		 options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 ) ){
      std::pair<double,double> alpha( 0., 1. );
      bool MC1thin = isequal( MC1._cv, MC1._cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
        true: false;
      if( !MC1thin && x1t[ndx] > MC1._cv ){
                // 29.08.2016
                // modified by AVT.SVT
                // we had to add an additional if-statement where we question if the two values x[ndx] and MC._cv
                // are equal since we only work with a given tolerance (MVCOMP_TOL)
                // the if-statement is added 4 times in the convex subgradient and 4 times in the concave subgradient
                if(!isequal(x1t[ndx], MC1._cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.first = std::max( alpha.first, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
                }
      }
      if( !MC1thin && x1t[ndx] < MC1._cc ){
                if(!isequal(x1t[ndx], MC1._cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.second = std::min( alpha.second, -Op<T>::l(MC2._I)/Op<T>::diam(MC2._I) );
                }
      }
      bool MC2thin = isequal( MC2._cv, MC2._cc, options.MVCOMP_TOL*1E-1, options.MVCOMP_TOL*1E-1 )?
        true: false;
      if( !MC2thin && x2t[ndx] > MC2._cv ){
                if(!isequal(x2t[ndx], MC2._cv, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.second = std::min( alpha.second, Op<T>::u(MC1._I)/Op<T>::diam(MC1._I) );
                }
      }
      if( !MC2thin && x2t[ndx] < MC2._cc ){
                if(!isequal(x2t[ndx], MC2._cc, options.MVCOMP_TOL, options.MVCOMP_TOL)){
                  alpha.first = std::max( alpha.first, Op<T>::u(MC1._I)/Op<T>::diam(MC1._I) );
                }
      }

      bool alphathin = isequal( alpha.first, alpha.second, options.MVCOMP_TOL, options.MVCOMP_TOL )?
        true: false;
      if( !alphathin && alpha.first > alpha.second ){
#ifdef MC__MCCORMICK_DEBUG
		std::cout << "WARNING2: alphaL= " <<  std::setprecision(std::numeric_limits<double>::digits10+1) << alpha.first << "  alphaU= " << alpha.second << std::setprecision(6)
				  << std::endl;
		std::cout << "x1t: " << std::setprecision(std::numeric_limits<double>::digits10+1) << x1t[ndx] << " x2t: " << x2t[ndx] <<std::endl;
		std::cout << "f(x1t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) << " f(x2t): " << fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) <<std::endl;
		std::cout << "f(x1t)-f(x2t): " << fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) - fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) <<std::endl;
		std::cout << "MC1thin: " << MC1thin << " MC2thin: " << MC2thin <<std::endl;
		std::cout << "x1t[ndx] > MC1._cv: " << (x1t[ndx] > MC1._cv) << " x1t[ndx] < MC1._cc " << (x1t[ndx] < MC1._cc) <<std::endl;
		std::cout << "isequal(x1t, MC1._cv): " << isequal(x1t[ndx], MC1._cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x1t, MC1._cc): " << isequal(x1t[ndx], MC1._cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
		std::cout << "x2t[ndx] > MC2._cv: " << (x2t[ndx] > MC2._cv) << " x2t[ndx] < MC2._cc " << (x2t[ndx] < MC2._cc) <<std::endl;
		std::cout << "isequal(x2t, MC2._cv): " << isequal(x2t[ndx], MC2._cv, options.MVCOMP_TOL, options.MVCOMP_TOL) << " isequal(x2t, MC2._cc): " << isequal(x2t[ndx], MC2._cc, options.MVCOMP_TOL, options.MVCOMP_TOL) <<std::endl;
		std::cout << "MC1.cv: "<< std::setprecision(std::numeric_limits<double>::digits10+1) << MC1._cv << " MC1.cc:" << std::setprecision(std::numeric_limits<double>::digits10+1) << MC1._cc <<std::endl;
		std::cout << "MC2.cv: " << MC2._cv << " MC2.cc:" << MC2._cc <<std::endl;
		std::cout << "MC1.l: " << Op<T>::l(MC1._I) << " MC1.u:" << Op<T>::u(MC1._I) << " MC1.diam: " << Op<T>::diam(MC1._I) <<std::endl;
		std::cout << "MC2.l: " << Op<T>::l(MC2._I) << " MC2.u:" << Op<T>::u(MC2._I) << " MC2.diam: " << Op<T>::diam(MC2._I)<<std::endl;
		std::cout << "_cv: " << _cv << " _cc: " << _cc <<std::endl;
		std::cout << "_nsub: " << _nsub <<std::endl;
		std::cout << "subcv1: " << MC1._cvsub[0] << " subcc1: " << MC1._ccsub[0] <<std::endl;
		std::cout << "subcv2: " << MC2._cvsub[0] << " subcc2: " << MC2._ccsub[0] <<std::endl;
		std::cout << "isequal: " <<  isequal( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ), fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ), options.MVCOMP_TOL, options.MVCOMP_TOL ) <<std::endl;
		std::cout << "MVCOMP_TOL " << options.MVCOMP_TOL <<std::endl;
		std::cout << "Exception in multivariate binary product: Try increasing MC_mvcomp_tol and MC_envel_tol in the MAiNGO Settings file." <<std::endl;
		throw Exceptions( Exceptions::MULTSUB );
#endif
		// currently testing
		alpha.first  = 1;
		alpha.second = 0;
      }
      myalpha = 0.5*( alpha.first + alpha.second );
    }
    else if( fct::t1( x1t[ndx], x2t[ndx], MC1, MC2 ) < fct::t2( x1t[ndx], x2t[ndx], MC1, MC2 ) )
      myalpha = 0.;
    else
      myalpha = 1.;
    double sigma1cc = Op<T>::l(MC2._I) + myalpha * Op<T>::diam(MC2._I),
           sigma2cc = Op<T>::u(MC1._I) - myalpha * Op<T>::diam(MC1._I);
    for( unsigned int i=0; i<_nsub; i++ )
      _ccsub[i] = ( sigma1cc>=0? (MC1._const? 0.:MC1._ccsub[i]):
                                 (MC1._const? 0.:MC1._cvsub[i]) ) * sigma1cc
                + ( sigma2cc>=0? (MC2._const? 0.:MC2._ccsub[i]):
		                 (MC2._const? 0.:MC2._cvsub[i]) ) * sigma2cc;
  }
 }
#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "multivariate mult";
	McCormick<T>::_debug_check(MC1, MC2, *this, str);

#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return apply_subgradient_interval_heuristic();
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator/=
( const double a )
{
  McCormick<T> MC2 = (*this) / a;
 #ifdef MC__MCCORMICK_DEBUG
   	std::string str = "/= double a =" + std::to_string(a);
	McCormick<T>::_debug_check(*this, MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline McCormick<T>&
McCormick<T>::operator/=
( const McCormick<T>&MC )
{
  if( _const && !MC._const ) sub( MC._nsub );
  McCormick<T> MC2 = (*this) / MC;
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "/=";
	McCormick<T>::_debug_check( *this,MC,MC2, str);
#endif
  *this = MC2;
  return *this;
}

template <typename T> inline double*
McCormick<T>::_erfcv
( const double x, const double xL, const double xU )
{
//AVT.SVT changed ::erf to std::erf since we don't have erf in the global or std namespace
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::erf(x), cv[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::erf(xU)-std::erf(xL))/(xU-xL) );
    cv[0] = std::erf(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _erfenv_func, _erfenv_dfunc, &xU );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _erfenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::erf(x), cv[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::erf(xU)-std::erf(xj))/(xU-xj) );
  cv[0] = std::erf(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_erfcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::erf(xU)-std::erf(xL))/(xU-xL) );
    cc[0] = std::erf(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::erf(x), cc[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _erfenv_func, _erfenv_dfunc, &xL );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _erfenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::erf(x), cc[1] = 2./std::sqrt(PI)*std::exp(-mc::sqr(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::erf(xL)-std::erf(xj))/(xL-xj) );
  cc[0] = std::erf(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_erfenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*exp(-z^2)-sqrt(pi)/2.*(erf(z)-erf(a)) = 0
  return (x-*rusr)*std::exp(-mc::sqr(x))-std::sqrt(PI)/2.*(std::erf(x)-std::erf(*rusr));
}

template <typename T> inline double
McCormick<T>::_erfenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*z*(z-a)*exp(-z^2)
  return -2.*x*(x-*rusr)*std::exp(-2.*mc::sqr(x));
}

template <typename T> inline double*
McCormick<T>::_atancv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::atan(x), cv[1] = 1./(1.+mc::sqr(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::atan(xU)-std::atan(xL))/(xU-xL) );
    cv[0] = std::atan(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _atanenv_func, _atanenv_dfunc, &xU, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _atanenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::atan(x), cv[1] = 1./(1.+mc::sqr(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::atan(xU)-std::atan(xj))/(xU-xj) );
  cv[0] = std::atan(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_atancc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::atan(xU)-std::atan(xL))/(xU-xL) );
    cc[0] = std::atan(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::atan(x), cc[1] = 1./(1.+mc::sqr(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _atanenv_func, _atanenv_dfunc, &xL, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _atanenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::atan(x), cc[1] = 1./(1.+mc::sqr(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::atan(xL)-std::atan(xj))/(xL-xj) );
  cc[0] = std::atan(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_atanenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = z-a-(1+z^2)*(atan(z)-atan(a)) = 0
  return (x-*rusr)-(1.+mc::sqr(x))*(std::atan(x)-std::atan(*rusr));
}

template <typename T> inline double
McCormick<T>::_atanenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*z*(atan(z)-atan(a))
  return -2.*x*(std::atan(x)-std::atan(*rusr));
}

template <typename T> inline double*
McCormick<T>::_sinhcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::sinh(x), cv[1] = std::cosh(x);
    return cv;
  }

  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:
      (std::sinh(xU)-std::sinh(xL))/(xU-xL) );
    cv[0] = std::sinh(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _sinhenv_func, _sinhenv_dfunc, &xL, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _sinhenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::sinh(x), cv[1] = std::cosh(x);
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.:
    (std::sinh(xj)-std::sinh(xL))/(xj-xL) );
  cv[0] = std::sinh(xL)+r*(x-xL), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_sinhcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:
      (std::sinh(xU)-std::sinh(xL))/(xU-xL) );
    cc[0] = std::sinh(xL)+r*(x-xL), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::sinh(x), cc[1] = std::cosh(x);
    return cc;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _sinhenv_func, _sinhenv_dfunc, &xU, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _oddpowenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::sinh(x), cc[1] = std::cosh(x);
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.:
    (std::sinh(xj)-std::sinh(xU))/(xj-xU) );
  cc[0] = std::sinh(xU)+r*(x-xU), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_sinhenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr  )
{
  // f(z) = (z-a)*cosh(z)-(sinh(z)-sinh(a)) = 0
  return (x-*rusr)*std::cosh(x)-(std::sinh(x)-std::sinh(*rusr));
}

template <typename T> inline double
McCormick<T>::_sinhenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr  )
{
  // f'(z) = (z-a)*sinh(z)
  return (x-*rusr)*std::sinh(x);
}

template <typename T> inline double*
McCormick<T>::_tanhcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = std::tanh(x), cv[1] = 1.-mc::sqr(std::tanh(x));
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(std::tanh(xU)-std::tanh(xL))/(xU-xL) );
    cv[0] = std::tanh(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _newton( xL, xL, 0., _tanhenv_func, _tanhenv_dfunc, &xU, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _tanhenv_func, &xU, 0 );
  }
  if( x < xj ){	 // convex part
    cv[0] = std::tanh(x), cv[1] = 1.-mc::sqr(std::tanh(x));
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(std::tanh(xU)-std::tanh(xj))/(xU-xj) );
  cv[0] = std::tanh(xj)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_tanhcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(std::tanh(xU)-std::tanh(xL))/(xU-xL) );
    cc[0] = std::tanh(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = std::tanh(x), cc[1] = 1.-mc::sqr(std::tanh(x));
    return cc;
  }

  double xj;
  try{
    xj = _newton( xU, 0., xU, _tanhenv_func, _tanhenv_dfunc, &xL, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _tanhenv_func, &xL, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = std::tanh(x), cc[1] = 1.-mc::sqr(std::tanh(x));
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(std::tanh(xL)-std::tanh(xj))/(xL-xj) );
  cc[0] = std::tanh(xj)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_tanhenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*(1-tanh(z)^2)-(tanh(z)-tanh(a)) = 0
  return (x-*rusr)*(1.-mc::sqr(std::tanh(x)))-(std::tanh(x)-std::tanh(*rusr));
}

template <typename T> inline double
McCormick<T>::_tanhenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = -2*(z-a)*tanh(z)*(1-tanh(z)^2)
  return -2.*(x-*rusr)*std::tanh(x)*(1.-mc::sqr(std::tanh(x)));
}

template <typename T> inline double*
McCormick<T>::_xexpaxcv
( const double x, const double xL, const double xU, const double a )
{
  static thread_local double cv[2];
  double dummy[2] = {xL,a};
  double p = -2.0/a; // turning point
  double xj;
    try{
      xj = _newton( xU, p, xU, _xexpaxenv_func, _xexpaxenv_dfunc, dummy );
    }
    catch( McCormick<T>::Exceptions ){
      xj = _goldsect(p, xU, _xexpaxenv_func, dummy, 0 );
    }
    if( x > xj ){	 // convex part
      cv[0] = mc::xexpax(x,a), cv[1] = std::exp(x*a)*(1.0+a*x);
      return cv;
    }
	double r,pt;
	if(isequal( xj, xL )){
		r = 0.;
		pt = mc::xexpax(xL,a)<mc::xexpax(xU,a) ? xL : xU;
	}
	else{
		r = (mc::xexpax(xL,a)-mc::xexpax(xj,a))/(xL-xj) ;
		pt = xL;
	}
    cv[0] = mc::xexpax(pt,a)+r*(x-pt), cv[1] = r;

  return cv;
}

template <typename T> inline double*
McCormick<T>::_xexpaxcc
( const double x, const double xL, const double xU, const double a )
{
  static thread_local double cc[2];
  double dummy[2] = {xU,a};
  double p = -2.0/a; // turning point
  double xj;
  try{
    xj = _newton( xL, xL, p, _xexpaxenv_func, _xexpaxenv_dfunc, dummy );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, p, _xexpaxenv_func, dummy, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = mc::xexpax(x,a), cc[1] = std::exp(x*a)*(1.0+a*x);
    return cc;
  }
  double r,pt;
	if(isequal( xj, xL )){
		r = 0.;
		pt = mc::xexpax(xL,a)>mc::xexpax(xU,a) ? xL : xU;
	}
	else{
		r = (mc::xexpax(xU,a)-mc::xexpax(xj,a))/(xU-xj);
		pt = xU;
	}
  cc[0] = mc::xexpax(pt,a)+r*(x-pt), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_xexpaxenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = exp(a*x)*(x - xL)*(1+a*x) - (x*exp(a*x) - xL*exp(a* xL)) = 0
  return std::exp(rusr[1]*x)*(x- rusr[0])*(1+rusr[1]*x) - (mc::xexpax(x,rusr[1]) - mc::xexpax(rusr[0],rusr[1]));
}

template <typename T> inline double
McCormick<T>::_xexpaxenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = a*exp(a*x)*(x - xL)*(2 + a*x)
  return rusr[1]*std::exp(rusr[1]*x)*(x - rusr[0])*(2.0+rusr[1]*x);
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cv_val
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &borderPoint,
  const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds  )
{
	//    fcv1(x) = f(xL,y1L,...,y(n-1)L,yn) + f(xL,y1L,...,y(n-1),ynL) + .. + f(x,y1L,...y(n-1)L,ynL) - n*f(xL,y1L,...ynL)
	// or fcv2(x) = f(xU,y1U,...,y(n-1)U,yn) + f(xU,y1U,...,y(n-1),ynU) + .. + f(x,y1U,...y(n-1)U,ynU) - n*f(xU,y1U,...ynU)
	// borderPoint holds xL, y1L,... or xU, y1U,... depending on which cv we want
	std::vector<std::vector<double>> points(x.size(),borderPoint);
	for(size_t i = 0;i<x.size();i++){
		points[i][i] = x[i] ;
	}
	unsigned n = x.size()-1;
	double res = -(n*mc::xlog_sum_componentwise_convex(borderPoint,coeff,intervalLowerBounds,intervalUpperBounds,n));
	for(size_t i = 0;i<points.size();i++){
		res += mc::xlog_sum_componentwise_convex(points[i],coeff,intervalLowerBounds,intervalUpperBounds,n);
	}
	return res;
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr ){
	// f'(x) = log(a1*x+b1*y1L+b2*y2L+...) + a1*x/(a1*x+b1*y1L+b2*y2L+...)
	// vusr holds coefficients a1,b1,...
	// rusr holds xL,y1L,...ynL
	double sum = vusr[0]*x;
	for(size_t i = 1;i<vusr.size();i++){
		sum += vusr[i]*rusr[i];
	}
	return std::log(sum) + vusr[0]*x/sum;
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cv_ddfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr ){
	// f''(x) = a1*(a1*x + 2*b1*y1L + 2*b2*y2L + ...)/(a1*x + b1*y1L + b2*y2L + ...)^2
	// vusr holds coefficients a1,b1,...
	// rusr holds xL,y1L,...ynL or xU,y1U,...ynU
	unsigned int size = *iusr;
	double sum1 = vusr[0]*x;
	double sum2 = vusr[0]*x;
	for(unsigned int i=1;i< size - 1;i++){
	  sum1 += 2*rusr[i]*vusr[i];
	  sum2 += rusr[i]*vusr[i];
	}
	return vusr[0]*sum1/std::pow(sum2,2);
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cc_val
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &borderPoint,
  const std::vector<double> &intervalLowerBounds, const std::vector<double> &intervalUpperBounds  )
{
	//    fcv1(x) = f(xL,y1L,...,y(n-1)L,yn) + f(xL,y1L,...,y(n-1),ynL) + .. + f(x,y1L,...y(n-1)L,ynL) - n*f(xL,y1L,...ynL)
	// or fcv2(x) = f(xU,y1U,...,y(n-1)U,yn) + f(xU,y1U,...,y(n-1),ynU) + .. + f(x,y1U,...y(n-1)U,ynU) - n*f(xU,y1U,...ynU)
	// borderPoint holds xU, y1L,... or xL, y1U,... depending on which cc we want
	std::vector<std::vector<double>> points(x.size(),borderPoint);
	for(size_t i = 0;i<x.size();i++){
		points[i][i] = x[i] ;
	}
	unsigned n = x.size()-1;
	double res = -(n*mc::xlog_sum_componentwise_concave(borderPoint,coeff,intervalLowerBounds,intervalUpperBounds));
	for(size_t i = 0;i<points.size();i++){
		res += mc::xlog_sum_componentwise_concave(points[i],coeff,intervalLowerBounds,intervalUpperBounds);
	}
	return res;
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cv_partial_derivative
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds, const std::vector<double> &upperIntervalBounds,
  unsigned facet, unsigned direction ){

	double val = 0;
	int dummy = 0.;
	switch(facet){
		case 1:
		{
			switch(direction){
				case 0:
				{
					val = _xlog_sum_cv_dfunc(x[0],lowerIntervalBounds.data(),&dummy,coeff);
					break;
				}
				default:
				{
					if(isequal(upperIntervalBounds[direction],lowerIntervalBounds[direction])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<x.size();i++){
							sum += lowerIntervalBounds[i]*coeff[i];
						}
						val = (lowerIntervalBounds[0]*std::log(sum-lowerIntervalBounds[direction]*coeff[direction]+upperIntervalBounds[direction]*coeff[direction])
							 - lowerIntervalBounds[0]*std::log(sum) )
							  /(upperIntervalBounds[direction]-lowerIntervalBounds[direction]);
						}
					break;
				}
			}
			break;
		}
		case 2:
		{
			switch(direction){
				case 0:
				{
					val = _xlog_sum_cv_dfunc(x[0],upperIntervalBounds.data(),&dummy,coeff);
					break;
				}
				default:
				{
					if(isequal(upperIntervalBounds[direction],lowerIntervalBounds[direction])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<x.size();i++){
							sum += upperIntervalBounds[i]*coeff[i];
						}
						val = (upperIntervalBounds[0]*std::log(sum)
							 - upperIntervalBounds[0]*std::log(sum-upperIntervalBounds[direction]*coeff[direction]+lowerIntervalBounds[direction]*coeff[direction]) )
							  /(upperIntervalBounds[direction]-lowerIntervalBounds[direction]);
					}
					break;
				}
			}
			break;
		}
		default:
		  break;
	}
	return val;
}

template <typename T> inline double
McCormick<T>::_xlog_sum_cc_partial_derivative
( const std::vector<double> &x, const std::vector<double> &coeff, const std::vector<double> &lowerIntervalBounds, const std::vector<double> &upperIntervalBounds,
  unsigned facet, unsigned direction ){

	double val = 0;
	int dummy = 0.;
	switch(facet){
		case 1:
		{
			switch(direction){
				case 0:
				{
					if(isequal(upperIntervalBounds[0],lowerIntervalBounds[0])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<lowerIntervalBounds.size();i++){
							sum += lowerIntervalBounds[i]*coeff[i]; // a1*xL+b1*y1L+...
						}
						val = (upperIntervalBounds[0]*std::log(sum-lowerIntervalBounds[0]*coeff[0]+upperIntervalBounds[0]*coeff[0])
							 - lowerIntervalBounds[0]*std::log(sum) )
							 /(upperIntervalBounds[0]-lowerIntervalBounds[0]);
					}
					break;
				}
				default:
				{
					double sum = coeff[0]*upperIntervalBounds[0];
					for(size_t i = 1;i<x.size();i++){
						if(i==direction){
							sum += coeff[direction]*x[direction];
						}
						else{
							sum += lowerIntervalBounds[i]*coeff[i];
						}
					}
					val = coeff[direction]*upperIntervalBounds[0]/(sum);
					break;
				}
			}
			break;
		}
		case 2:
		{
			switch(direction){
				case 0:
				{
					if(isequal(upperIntervalBounds[0],lowerIntervalBounds[0])){
						val = 0;
					}else{
						double sum = 0.;
						for(size_t i = 0;i<lowerIntervalBounds.size();i++){
							sum += upperIntervalBounds[i]*coeff[i]; // a1*xU+b1*y1U+...
						}
						val = (upperIntervalBounds[0]*std::log(sum)
							 - lowerIntervalBounds[0]*std::log(sum-upperIntervalBounds[0]*coeff[0]+lowerIntervalBounds[0]*coeff[0]) )
							 /(upperIntervalBounds[0]-lowerIntervalBounds[0]);
					}
					break;
				}
				default:
				{
					double sum = coeff[0]*lowerIntervalBounds[0];
					for(size_t i = 1;i<x.size();i++){
						if(i==direction){
							sum += coeff[direction]*x[direction];
						}
						else{
							sum += upperIntervalBounds[i]*coeff[i];
						}
					}
					val = coeff[direction]*lowerIntervalBounds[0]/(sum);
					break;
				}
			break;
			}
		}
		default:
		  break;
	}
	return val;
}

template <typename T> inline double*
McCormick<T>::_xlog_sum_cv
( std::vector<double> &xcv, const double xcc, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
  const std::vector<double> &upperIntervalBounds, const std::vector<double> &xcc_vec ){

  static thread_local double cv[2]; // first is value of convex relaxation, second is which of the two convex relaxations has been used,
                       // and third is the point xj which is later used for subgradient computation
  int c = coeffs.size()-1;
  double xj1,xj2;
    try{
      xj1 = _newton( xcv[0], xcv[0], xcc, _xlog_sum_cv_dfunc, _xlog_sum_cv_ddfunc, lowerIntervalBounds.data(), &c, coeffs );
    }
    catch( McCormick<T>::Exceptions ){
      xj1 = _goldsect(xcv[0], xcc, _xlog_sum_cv_dfunc, lowerIntervalBounds.data(), &c, coeffs );
    }
	try{
      xj2 = _newton( xcv[0], xcv[0], xcc, _xlog_sum_cv_dfunc, _xlog_sum_cv_ddfunc, upperIntervalBounds.data(), &c, coeffs );
    }
    catch( McCormick<T>::Exceptions ){
      xj2 = _goldsect( xcv[0], xcc, _xlog_sum_cv_dfunc, upperIntervalBounds.data(), &c, coeffs );
    }
	xcv[0] = xj1;
	double val1 = _xlog_sum_cv_val(xcv,coeffs,lowerIntervalBounds,lowerIntervalBounds,upperIntervalBounds); // ^L relaxation
	xcv[0] = xj2;
	double val2 = _xlog_sum_cv_val(xcv,coeffs,upperIntervalBounds,lowerIntervalBounds,upperIntervalBounds); // ^U relaxation

    if( val1 >= val2 ){
	  cv[0] =val1;
	  cv[1] = 1;
	  xcv[0] = xj1;
    }
	else{
	  cv[0] =val2;
	  cv[1] = 2;
	  xcv[0] = xj2;
	}

  return cv;
}

template <typename T> inline double*
McCormick<T>::_xlog_sum_cc
( std::vector<double> &xcc, double xcv, const std::vector<double>& coeffs, const std::vector<double> &lowerIntervalBounds,
  const std::vector<double> &upperIntervalBounds ){

	static thread_local double cc[2]; // first is value of convex relaxation, second is which of the two convex relaxations has been used

	// Check whether the relaxation function is increasing or decreasing w.r.t. x
	double sum = 0.;
	for(size_t i = 0;i<lowerIntervalBounds.size();i++){
		sum += lowerIntervalBounds[i]*coeffs[i]; // a1*xL+b1*y1L+...
	}
	double val = (upperIntervalBounds[0]*std::log(sum-lowerIntervalBounds[0]*coeffs[0]+upperIntervalBounds[0]*coeffs[0])
	     - lowerIntervalBounds[0]*std::log(sum) )
	     /(upperIntervalBounds[0]-lowerIntervalBounds[0]);

	// The function is decreasing w.r.t. x
	if(val<0){
		xcc[0] = xcv;
	}

	std::vector<double> borderPoint1(lowerIntervalBounds);
	std::vector<double> borderPoint2(upperIntervalBounds);
	borderPoint1[0] = upperIntervalBounds[0];
	borderPoint2[0] = lowerIntervalBounds[0];

	double val1 = _xlog_sum_cc_val(xcc,coeffs,borderPoint1,lowerIntervalBounds,upperIntervalBounds); // xL,yiU, relaxation
	double val2 = _xlog_sum_cc_val(xcc,coeffs,borderPoint2,lowerIntervalBounds,upperIntervalBounds); // xU,yiL, relaxation

	if( val1 <= val2 ){
		cc[0] =val1;
		cc[1] = 1;
	}
	else{
		cc[0] =val2;
		cc[1] = 2;
	}

	return cc;
}

template <typename T> inline double*
McCormick<T>::_regnormal_cv
( const double x, const double a, const double b, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xU <= 0. ){	 // convex part
    cv[0] = mc::regnormal(x,a,b), cv[1] = mc::der_regnormal(x,a,b);
    return cv;
  }

  if( xL >= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xL,a,b))/(xU-xL) );
    cv[0] = mc::regnormal(xL,a,b)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  double params [3] = {a,b,xU};
  try{
    xj = _newton( xL, xL, 0., _regnormal_func, _regnormal_dfunc, params, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _regnormal_func, params, 0 );
  }

  if( x < xj ){	 // convex part
    cv[0] = mc::regnormal(x,a,b), cv[1] = mc::der_regnormal(x,a,b);
    return cv;
  }
  double r = ( isequal( xj, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xj,a,b))/(xU-xj) );
  cv[0] = mc::regnormal(xj,a,b)+r*(x-xj), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_regnormal_cc
( const double x, const double a, const double b, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xU <= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:(mc::regnormal(xU,a,b)-mc::regnormal(xL,a,b))/(xU-xL) );
    cc[0] = mc::regnormal(xU,a,b)+r*(x-xU), cc[1] = r;
    return cc;
  }

  if( xL >= 0. ){	 // concave part
    cc[0] = mc::regnormal(x,a,b), cc[1] = mc::der_regnormal(x,a,b);
    return cc;
  }

  double xj;
  double params [3] = {a,b,xL};
  try{
    xj = _newton( xU, 0., xU, _regnormal_func, _regnormal_dfunc, params, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _regnormal_func, params, 0 );
  }
  if( x > xj ){	 // concave part
    cc[0] = mc::regnormal(x,a,b), cc[1] = mc::der_regnormal(x,a,b);
    return cc;
  }
  double r = ( isequal( xj, xL )? 0.:(mc::regnormal(xj,a,b)-mc::regnormal(xL,a,b))/(xj-xL) );
  cc[0] = mc::regnormal(xj,a,b)+r*(x-xj), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_regnormal_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(x) = (x-w)*der_regnormal(x,a,b) - (regnormal(x,a,b)-regnormal(w,a,b)) = 0
  return (x-rusr[2])*mc::der_regnormal(x,rusr[0],rusr[1])-(mc::regnormal(x,rusr[0],rusr[1])-mc::regnormal(rusr[2],rusr[0],rusr[1]));
}

template <typename T> inline double
McCormick<T>::_regnormal_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(x) = 3*a*b*x*(w-x) / (a + b*x^2)^(5/2)
  return 3*rusr[0]*rusr[1]*x*(rusr[2]-x) / std::pow(rusr[0] + rusr[1]*std::pow(x,2),5./2.);
}

template <typename T> inline double
McCormick<T>::_gpdf_compute_sol_point
( const double xL, const double xU, const double starting_point, const double fixed_point )
{
  double xj;
  try{
    xj = _newton( starting_point, xL, xU, _gpdf_func, _gpdf_dfunc, &fixed_point, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, xU, _gpdf_func, &fixed_point, 0 );
  }
  return xj;
}

template <typename T> inline double
McCormick<T>::_gpdf_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(x) = (gaussian_probability_density_function(x)-gaussian_probability_density_function(xJ))/(x-xJ) - der_gaussian_probability_density_function(x)
  return (mc::gaussian_probability_density_function(x)- mc::gaussian_probability_density_function(*rusr)) - mc::der_gaussian_probability_density_function(x)*(x - (*rusr));
}

template <typename T> inline double
McCormick<T>::_gpdf_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(x) = mc::gaussian_probability_density_function(x)*(1-x^2)*(x-xJ)
  return mc::gaussian_probability_density_function(x) * (1 - std::pow(x,2))*(x - (*rusr));
}

template <typename T> inline double*
McCormick<T>::_oddpowcv
( const double x, const int iexp, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    double v = std::pow(x,iexp-1);
    cv[0] = x*v, cv[1] = iexp*v;
    return cv;
  }

  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.:
      (std::pow(xU,iexp)-std::pow(xL,iexp))/(xU-xL) );
    cv[0] = std::pow(xL,iexp)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  if( iexp > 21 ){ // if the odd exponent is larger than 21, we use newton to compute the envelope
    try{
      xj = _newton( xU, 0., xU, _oddpowenv_func, _oddpowenv_dfunc, &xL, &iexp );
    }
    catch( McCormick<T>::Exceptions ){
      xj = _goldsect( 0., xU, _oddpowenv_func, &xL, &iexp );
    }
  }
  else{// if the odd exponent is lesser or equal than 21, we use the tangent points given in Table 1
       // of [Liberti & Pantelides 2002, "Convex Envelopes of Monomials of Odd Degree"]
    xj = _Qroots[ (iexp-1)/2-1 ] * xL;
  }

  if( x > xj ){	 // convex part
    double v = std::pow(x,iexp-1);
    cv[0] = x*v, cv[1] = iexp*v;
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.:
    (std::pow(xj,iexp)-std::pow(xL,iexp))/(xj-xL) );
  cv[0] = std::pow(xL,iexp)+r*(x-xL), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_oddpowcc
( const double x, const int iexp, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.:
      (std::pow(xU,iexp)-std::pow(xL,iexp))/(xU-xL) );
    cc[0] = std::pow(xU,iexp)+r*(x-xU), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    double v = std::pow(x,iexp-1);
    cc[0] = x*v, cc[1] = iexp*v;
    return cc;
  }

  double xj;
  if( iexp > 21 ){ // if the odd exponent is larger than 21, we use newton to compute the envelope
    try{
      xj = _newton( xL, xL, 0., _oddpowenv_func, _oddpowenv_dfunc, &xU, &iexp );
    }
    catch( McCormick<T>::Exceptions ){
      xj = _goldsect( xL, 0., _oddpowenv_func, &xU, &iexp );
    }
  }
  else{// if the odd exponent is lesser or equal than 21, we use the tagnent points given in Table 1
       // of [Liberti & Pantelides 2002, "Convex Envelopes of Monomials of Odd Degree"]
    xj = _Qroots[ (iexp-1)/2-1 ] * xU;
  }

  if( x < xj ){	 // concave part
    double v = std::pow(x,iexp-1);
    cc[0] = x*v, cc[1] = iexp*v;
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.:
    (std::pow(xj,iexp)-std::pow(xU,iexp))/(xj-xU) );
  cc[0] = std::pow(xU,iexp)+r*(x-xU), cc[1] = r;
  return cc;
}

template <typename T> inline double
McCormick<T>::_oddpowenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (p-1)*z^p - a*p*z^{p-1} + a^p = 0
  return ((*iusr-1)*x-(*rusr)*(*iusr))*std::pow(x,*iusr-1)
    + std::pow(*rusr,*iusr);
}

template <typename T> inline double
McCormick<T>::_oddpowenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = p*(p-1)*z^{p-1} - a*p*(p-1)*z^{p-2}
  return ((*iusr)*(*iusr-1)*x-(*rusr)*(*iusr)*(*iusr-1))*std::pow(x,*iusr-2);
}

template <typename T> inline double*
McCormick<T>::_evenchebcv
( const double x, const int iord, const double xL, const double xU )
{
  double xjL = std::cos(PI-PI/(double)iord);
  double xjU = std::cos(PI/(double)iord);
  static thread_local double cv[2];
  if( x <= xjL || x >= xjU )
    cv[0] = mc::cheb(x,iord), cv[1] = iord*mc::cheb2(x,iord-1);
  else
    cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_oddchebcv
( const double x, const int iord, const double xL, const double xU )
{
  static thread_local double cv[2];
  double xj = std::cos(PI/(double)iord);
  if( x >= xj ){	 // convex part
    cv[0] = mc::cheb(x,iord), cv[1] = iord*mc::cheb2(x,iord-1);
    return cv;
  }
  cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_oddchebcc
( const double x, const int iord, const double xL, const double xU )
{
  static thread_local double cc[2];
  double xj = std::cos(PI-PI/(double)iord);
  if( x <= xj ){	 // concave part
    cc[0] = mc::cheb(x,iord), cc[1] = iord*mc::cheb2(x,iord-1);
    return cc;
  }
  cc[0] = 1., cc[1] = 0.;
  return cc;
}

template <typename T> inline double*
McCormick<T>::_stepcv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];

  if( x < 0. ){
    cv[0] = cv[1] = 0.;
    return cv;
  }

  if( xL >= 0. ){
    cv[0] = 1., cv[1] = 0.;
    return cv;
  }

  cv[0] = x/xU, cv[1] = 1./xU;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_stepcc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];

  if( x >= 0. ){
    cc[0] = 1., cc[1] = 0.;
    return cc;
  }

  else if( xU < 0. ){
    cc[0] = 0., cc[1] = 0.;
    return cc;
  }

  cc[0] = 1.-x/xL, cc[1] = -1./xL;
  return cc;
}

template <typename T> inline double*
McCormick<T>::_cosarg
( const double xL, const double xU )
{
  static thread_local double arg[2];
  const int kL = std::ceil(-(1.+xL/PI)/2.);
  const double xL1 = xL+2.*PI*kL, xU1 = xU+2.*PI*kL;
  assert( xL1 >= -PI && xL1 <= PI );
  if( xL1 <= 0 ){
    if( xU1 <= 0 ) arg[0] = xL, arg[1] = xU;
    else if( xU1 >= PI ) arg[0] = PI*(1.-2.*kL), arg[1] = -PI*2.*kL;
    else arg[0] = std::cos(xL1)<=std::cos(xU1)?xL:xU, arg[1] = -PI*2.*kL;
    return arg;
  }
  if( xU1 <= PI ) arg[0] = xU, arg[1] = xL;
  else if( xU1 >= 2.*PI ) arg[0] = PI*(1-2.*kL), arg[1] = 2.*PI*(1.-kL);
  else arg[0] = PI*(1.-2.*kL), arg[1] = std::cos(xL1)>=std::cos(xU1)?xL:xU;
  return arg;
}

template <typename T> inline double*
McCormick<T>::_coscv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  const int kL = std::ceil(-(1.+xL/PI)/2.);
  if( x <= PI*(1-2*kL) ){
    const double xL1 = xL+2.*PI*kL;
    if( xL1 >= 0.5*PI ){
      cv[0] = std::cos(x), cv[1] = -std::sin(x);
      return cv;
    }
    const double xU1 = std::min(xU+2.*PI*kL,PI);
    if( xL1 >= -0.5*PI && xU1 <= 0.5*PI ){
	  double r, pt;
	  if(isequal( xL, xU )){
		  r = 0.;
		  pt = std::cos(xL)<std::cos(xU) ? xL : xU;
	  }
	  else{
		  r =  (std::cos(xU)-std::cos(xL))/(xU-xL);
		  pt = xL;
	  }
      cv[0] = std::cos(pt)+r*(x-pt), cv[1] = r;
      return cv;
    }
    return _coscv2( x+2.*PI*kL, xL1, xU1 );
  }

  const int kU = std::floor((1.-xU/PI)/2.);
  if( x >= PI*(-1-2*kU) ){
    const double xU2 = xU+2.*PI*kU;
    if( xU2 <= -0.5*PI ){
      cv[0] = std::cos(x), cv[1] = -std::sin(x);
      return cv;
    }
    return _coscv2( x+2.*PI*kU, std::max(xL+2.*PI*kU,-PI), xU2 );
  }

  cv[0] = -1., cv[1] = 0.;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_coscv2
( const double x, const double xL, const double xU )
{
  bool left;
  double x0, xm;
  if( std::fabs(xL)<=std::fabs(xU) )
    left = false, x0 = xU, xm = xL;
  else
    left = true, x0 = xL, xm = xU;

  double xj;
  try{
    xj = _newton( x0, xL, xU, _cosenv_func, _cosenv_dfunc, &xm, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, xU, _cosenv_func, &xm, 0 );
  }
  static thread_local double cv[2];
  if(( left && x<xj ) || ( !left && x>xj )){
    cv[0] = std::cos(x), cv[1] = -std::sin(x);
    return cv;
  }
  double r, pt;
  if(isequal( xm, xj )){
	  r = 0.;
	  pt = std::cos(xm)<std::cos(xj) ? xm : xj;
  }
  else{
	  r = (std::cos(xm)-std::cos(xj))/(xm-xj);
	  pt = xm;
  }
  cv[0] = std::cos(pt)+r*(x-pt), cv[1] = r;
  return cv;
}

template <typename T> inline double*
McCormick<T>::_coscc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  const double*cvenv = _coscv( x-PI, xL-PI, xU-PI );
  cc[0] = -cvenv[0], cc[1] = -cvenv[1];
  return cc;
}

template <typename T> inline double
McCormick<T>::_cosenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)*sin(z)+cos(z)-cos(a) = 0
  return ((x-*rusr)*std::sin(x)+std::cos(x)-std::cos(*rusr));
}

template <typename T> inline double
McCormick<T>::_cosenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = (z-a)*cos(z)
  return ((x-*rusr)*std::cos(x));
}

template <typename T> inline double*
McCormick<T>::_asincv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::asin(x), cv[1] = 1./std::sqrt(1-x*x);
    return cv;
  }
  if( xU <= 0. ){	 // concave part
    double r,pt;
	if(isequal( xL, xU )){
		r = 0.;
		pt = std::asin(xL)<std::asin(xU) ? xL : xU;
	}
	else{
		r = (std::asin(xU)-std::asin(xL))/(xU-xL);
		pt = xL;
	}
    cv[0] = std::asin(pt)+r*(x-pt), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _secant( 0., xU, 0., xU, _asinenv_func, &xL, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _asinenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::asin(x), cv[1] = 1./std::sqrt(1-x*x);
    return cv;
  }
  double r,pt;
  if(isequal( xL, xj )){
  	r = 0.;
  	pt = std::asin(xL)<std::asin(xj) ? xL : xj;
  }
  else{
  	r = (std::asin(xU)-std::asin(xj))/(xU-xj);
  	pt = xL;
  }
  cv[0] = std::asin(pt)+r*(x-pt), cv[1] = r;	// linear part
  return cv;
}

template <typename T> inline double*
McCormick<T>::_asincc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r,pt;
	if(isequal( xL, xU )){
		r = 0.;
		pt = std::asin(xL)>std::asin(xU) ? xL : xU;
	}
	else{
		r = (std::asin(xU)-std::asin(xL))/(xU-xL);
		pt = xU;
	}
    cc[0] = std::asin(pt)+r*(x-pt), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::asin(x), cc[1] = 1./std::sqrt(1-x*x);
    return cc;
  }

  double xj;
  try{
    xj = _secant( 0., xL, xL, 0., _asinenv_func, &xU, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _asinenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::asin(x), cc[1] = 1./std::sqrt(1-x*x);
    return cc;
  }
  double r,pt;
  if(isequal( xU, xj )){
  	r = 0.;
  	pt = std::asin(xU)>std::asin(xj) ? xU : xj;
  }
  else{
  	r = (std::asin(xU)-std::asin(xj))/(xU-xj);
  	pt = xU;
  }
  cc[0] = std::asin(pt)+r*(x-pt), cc[1] = r;	// secant part
  return cc;
}

template <typename T> inline double
McCormick<T>::_asinenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = z-a-sqrt(1-z^2)*(asin(z)-asin(a)) = 0
  return x-(*rusr)-std::sqrt(1.-x*x)*(std::asin(x)-std::asin(*rusr));
}

template <typename T> inline double
McCormick<T>::_asinenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = z/sqrt(1-z^2)*(asin(z)-asin(a))
  return x/std::sqrt(1.-x*x)*(std::asin(x)-std::asin(*rusr));
}

template <typename T> inline double*
McCormick<T>::_tancv
( const double x, const double xL, const double xU )
{
  static thread_local double cv[2];
  if( xL >= 0. ){	 // convex part
    cv[0] = std::tan(x), cv[1] = 1.+mc::sqr(std::tan(x));
    return cv;
  }
  if( xU <= 0. ){	 // concave part
    double r = ( isequal( xL, xU )? 0.: (std::tan(xU)-std::tan(xL))/(xU-xL));
    cv[0] = std::tan(xL)+r*(x-xL), cv[1] = r;
    return cv;
  }

  double xj;
  try{
    xj = _secant( 0., xU, 0., xU, _tanenv_func, &xL, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( 0., xU, _tanenv_func, &xL, 0 );
  }
  if( x > xj ){	 // convex part
    cv[0] = std::tan(x), cv[1] = 1.+mc::sqr(std::tan(x));
    return cv;
  }
  double r = ( isequal( xL, xj )? 0.: (std::tan(xj)-std::tan(xL))/(xj-xL));
  cv[0] = std::tan(xL)+r*(x-xL), cv[1] = r;	// secant part
  return cv;
}

template <typename T> inline double*
McCormick<T>::_tancc
( const double x, const double xL, const double xU )
{
  static thread_local double cc[2];
  if( xL >= 0. ){	 // convex part
    double r = ( isequal( xL, xU )? 0.: (std::tan(xU)-std::tan(xL))/(xU-xL));
    cc[0] = std::tan(xU)+r*(x-xU), cc[1] = r;
    return cc;
  }
  if( xU <= 0. ){	 // concave part
    cc[0] = std::tan(x), cc[1] = 1.+mc::sqr(std::tan(x));
    return cc;
  }

  double xj;
  try{
    xj = _secant( 0., xL, xL, 0., _tanenv_func, &xU, 0 );
  }
  catch( McCormick<T>::Exceptions ){
    xj = _goldsect( xL, 0., _tanenv_func, &xU, 0 );
  }
  if( x < xj ){	 // concave part
    cc[0] = std::tan(x), cc[1] = 1.+mc::sqr(std::tan(x));
    return cc;
  }
  double r = ( isequal( xU, xj )? 0.: (std::tan(xj)-std::tan(xU))/(xj-xU));
  cc[0] = std::tan(xU)+r*(x-xU), cc[1] = r;	// secant part
  return cc;
}

template <typename T> inline double
McCormick<T>::_tanenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f(z) = (z-a)-(tan(z)-tan(a))/(1+tan(z)^2) = 0
  return (x-(*rusr))-(std::tan(x)-std::tan(*rusr))/(1.+mc::sqr(std::tan(x)));
}

template <typename T> inline double
McCormick<T>::_tanenv_dfunc
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  // f'(z) = (tan(z)-tan(a))/(1+tan(z)^2)*2*tan(z)
  return 2.*std::tan(x)/(1.+mc::sqr(std::tan(x)))*(std::tan(x)-std::tan(*rusr));
}

template <typename T> inline double
McCormick<T>::_newton
( const double x0, const double xL, const double xU, puniv f,
  puniv df, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
  double xk = std::max(xL,std::min(xU,x0));
  double fk = f(xk,rusr,iusr,vusr);

  for( unsigned int it=0; it<options.ENVEL_MAXIT; it++ ){
    if( std::fabs(fk) < options.ENVEL_TOL ) return xk;
    double dfk = df(xk,rusr,iusr,vusr);
    if( dfk == 0 ) throw Exceptions( Exceptions::ENVEL );
    if( isequal(xk,xL) && fk/dfk>0 ) return xk;
    if( isequal(xk,xU) && fk/dfk<0 ) return xk;
    xk = std::max(xL,std::min(xU,xk-fk/dfk));
    fk = f(xk,rusr,iusr,vusr);
  }

  throw Exceptions( Exceptions::ENVEL );
}
/////////////////////////////////////////////////////////////////////////////////////////////////
// @AVT.SVT added 01.09.2017
template <typename T> inline double
McCormick<T>::_dhvapenv_func
( const double x, const double*rusr, const int*iusr, const std::vector<double>&vusr )
{
	switch(*iusr){
		case 1:
			return mc::enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4])
					+ mc::der_enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4])*(*rusr - x);
		case 2:
			return mc::enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4],vusr[5])
				+ mc::der_enthalpy_of_vaporization(x,*iusr,vusr[0],vusr[1],vusr[2],vusr[3],vusr[4],vusr[5])*(*rusr - x);
		default:
			throw std::runtime_error("mc::McCormick\t dhvapenv_func called with an unknown type.");
			break;
	}

}

template <typename T> inline void
McCormick<T>::_cost_Guthrie_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double p1, const double p2, const double p3, const double l, const double u ){
	// Test w.r.t. parameter p3
	if(p3 == 0.){ // there is no root of the first derivative so simply check corners
		// monotonicity first
		monotonicity = (p2 >= 0. )? MON_INCR : MON_DECR;
		// Second, check convexity/concavity
		if(std::pow(p2,2)-p2 >= 0.){
			convexity = CONVEX;
		}
		else{
			convexity = CONCAVE;
		}
	}
	else{
		// monotonicity first
        //we have exactly one root
		double root = std::exp(-p2*std::log(10.)/(2.0*p3));
		if( root <= l){ // left of the interval
			monotonicity = (p3 >= 0. )? MON_INCR : MON_DECR;//if p3 >=0 then it's a minimum
		}
		else if( root >= u){ // right of the interval
			monotonicity = (p3 >= 0. )? MON_DECR : MON_INCR; //if p3 >=0 then it's a minimum
		}
		else{
			monotonicity = MON_NONE;
		}
		// Second, check convexity/concavity
		const double min = std::exp((std::log(10.) - 2*p2*std::log(10.))/(4*p3));	// it's always a min
		if( l < min && min < u ){ // check if it lies within the interval bounds

			if( std::pow(p2,2) + (4*p2*p3*std::log(min))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(min),2))/(std::pow(std::log(10.),2))
				+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(min))/std::log(10.) >= 0. ){ // if the sub_part of the second derivative of Guthrie at the minimum is >= 0, we can deduce that the function is convex
				convexity = CONVEX;
			}else{	// else we have to check the corners of the sub_part function of the second derivative of Guthrie
				const double r1 = std::pow(p2,2) + (4*p2*p3*std::log(l))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(l),2))/(std::pow(std::log(10.),2))
							+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(l))/std::log(10.);
				const double r2 = std::pow(p2,2) + (4*p2*p3*std::log(u))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(u),2))/(std::pow(std::log(10.),2))
							+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(u))/std::log(10.);

				if( r1 <= 0. && r2 <= 0. ){
					convexity = CONCAVE;
				}
			}
		}
		else{ //end of if {l < min && min < u} // if the minimum is not within the interval bounds, we can simply check corners to get convexity information

			const double r1 = std::pow(p2,2) + (4*p2*p3*std::log(l))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(l),2))/(std::pow(std::log(10.),2))
						+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(l))/std::log(10.);
			const double r2 = std::pow(p2,2) + (4*p2*p3*std::log(u))/(std::log(10.)) + (4*std::pow(p3,2)*std::pow(std::log(u),2))/(std::pow(std::log(10.),2))
						+ (2*p3)/std::log(10.) - p2 - (2*p3*std::log(u))/std::log(10.);

			if( r1 <= 0. && r2 <= 0.){
				convexity = CONCAVE;
			}
			else if( r1 >= 0. && r2 >= 0.){
				convexity = CONVEX;
			}

		}//end of else {l < min && min < u}

		// We can still compute the envelope with Newton, for this we need the root of the second derivative
		if(convexity == CONV_NONE){
			// Check if we have a real root
			if(8.0*p3-std::log(10)<=0){
				const double r1 = std::exp(-(std::sqrt(-std::log(10)*(8*p3 - std::log(10))) - std::log(10) + 2*p2*std::log(10))/(4*p3));
				const double r2 = std::exp( (std::log(10) + std::sqrt(-std::log(10)*(8*p3 - std::log(10))) - 2*p2*std::log(10))/(4*p3));
				bool firstRootInInterval = false;
				bool secondRootInInterval = false;
				if(l< r1 && r1 < u) {
					firstRootInInterval = true;
				}
				if(l< r2 && r2 <u){
					secondRootInInterval = true;
				}
				if(!firstRootInInterval && !secondRootInInterval){
					// Root not in the interval, check sign of second derivative at any point, here lower bound of interval
				    convexity = ((mc::sqr(p2)*mc::sqr(std::log(10)) + (4*p2*p3*std::log(l))*std::log(10) - p2*mc::sqr(std::log(10)) + (4*mc::sqr(p3)*mc::sqr(std::log(l))) - (2*p3*std::log(l))*std::log(10) + (2*p3)*std::log(10)) <= 0) ? CONCAVE : CONVEX;
				}
			}
			else{
				// No real root, check sign of second derivative at any point, here lower bound of interval
				convexity = ((mc::sqr(p2)*mc::sqr(std::log(10)) + (4*p2*p3*std::log(l))*std::log(10) - p2*mc::sqr(std::log(10)) + (4*mc::sqr(p3)*mc::sqr(std::log(l))) - (2*p3*std::log(l))*std::log(10) + (2*p3)*std::log(10)) <= 0) ? CONCAVE : CONVEX;
			}
		}
	}

}


template <typename T> inline void
McCormick<T>::_nrtl_tau_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double a, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax ){
	// for further details, please refer to Najman, Bongartz & Mitsos 2019 Relaxations of thermodynamic property and costing models in process engineering
	// monotonicity
	if(f == 0 && e == 0){ // there is no root of derivative so simply parameter b
		monotonicity = (b <= 0 )? MON_INCR : MON_DECR;

	}
	else if(f == 0){
		//the root of derivative is b/e
		double root = b/e;
		if(root <= 0){ // if it is not > 0, then do a simple corner check
			monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
		}
		else if(root <= l ){ // left of the interval
			monotonicity = (std::pow(e,3)/std::pow(b,2)>0 )? MON_INCR : MON_DECR; //check if its a minimum or maximum
		}
		else if(root >= u ){ // left of the interval
			monotonicity = (std::pow(e,3)/std::pow(b,2)>0 )? MON_DECR : MON_INCR; //check if its a minimum or maximum
		}
		else{ // the root lies in the interval and the function is not monotonic but we can still get tight interval bounds
			monotonicity = MON_NONE;
			if(std::pow(e,3)/std::pow(b,2)>0 ){ //minimum
				new_l = mc::nrtl_tau(b/e,a,b,e,f);
				zmin = b/e;
				if(mc::nrtl_tau(l,a,b,e,f)<=mc::nrtl_tau(u,a,b,e,f)){
					new_u =  mc::nrtl_tau(u,a,b,e,f);
					zmax = u;
				}
				else{
					new_u = mc::nrtl_tau(l,a,b,e,f);
					zmax = l;
				}
			}
			else{	//maximum
				new_u = mc::nrtl_tau(b/e,a,b,e,f);
				zmax = b/e;
				if(mc::nrtl_tau(l,a,b,e,f)<=mc::nrtl_tau(u,a,b,e,f)){
					new_l =  mc::nrtl_tau(l,a,b,e,f);
					zmin = l;
				}
				else{
					new_l = mc::nrtl_tau(u,a,b,e,f);
					zmin = u;
				}
			}
		}
	}
	else{

		double val = std::pow(e,2) +4.0*b*f;
        if(val < 0.){ // check of Assumption 1 we have no roots, so simply check corners
			monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
		}
		else{
			//in this case, we have two roots with r1 < r2
			double r1 = std::min(-(e+std::sqrt(val ))/(2.0*f),-(e-std::sqrt(val ))/(2.0*f));
			double r2 = std::max(-(e+std::sqrt(val ))/(2.0*f),-(e-std::sqrt(val ))/(2.0*f));

			// if the right root does not lie in the valid domain T>0 then its second derivative is invalid for the following checks
			if(r2 <= 0. ){ // if it is not > 0, then do a simple corner check
				monotonicity = (mc::nrtl_tau(l,a,b,e,f) < mc::nrtl_tau(u,a,b,e,f) )? MON_INCR : MON_DECR;
			}
			else{
				// note that checking only der2_nrtl_tau is cheaper than checking nrtl_tau twice when evaluating corner points
				// the right root does not lie in the interval and is left of the interval
				if(r2 <= l){
					monotonicity = (mc::der2_nrtl_tau(r2,b,e) > 0. )? MON_INCR : MON_DECR; //check if its a minimum or maximum
				}
				// the left root does not lie in the interval and is right of the interval
				else if(r1 >= u){
					monotonicity = (mc::der2_nrtl_tau(r1,b,e) > 0. )? MON_DECR : MON_INCR; //check if its a minimum or maximum
				}
				// both roots are outside the interval
				else if (r1 <= l && u <= r2){
					if(r1 <= 0.){		 // the smaller root can still be invalid
						monotonicity = (mc::der2_nrtl_tau(r2,b,e) > 0. )? MON_DECR : MON_INCR; //check if its a minimum or maximum
					}
					else{
						monotonicity = (mc::der2_nrtl_tau(r1,b,e) > 0. )? MON_INCR : MON_DECR; //check if its a minimum or maximum
					}
				}
				// at least one root lies within the interval bounds
				else{ // we can still get tight interval bounds
					monotonicity = MON_NONE;
					// check if left root lies in the interval bounds
					if(l<r1){
						// save value at root and root
						if(mc::der2_nrtl_tau(r1,b,e) > 0.){	// minimum
							new_l = mc::nrtl_tau(r1,a,b,e,f);
							zmin = r1;
						}
						else{	// maximum
							new_u = mc::nrtl_tau(r1,a,b,e,f);
							zmax = r1;
						}
					}
					// check if right root lies in the interval bounds
					if(r2<u){
						if(mc::der2_nrtl_tau(r2,b,e) > 0.){	// minimum
							new_l = mc::nrtl_tau(r2,a,b,e,f);
							zmin = r2;
						}
						else{	// maximum
							new_u = mc::nrtl_tau(r2,a,b,e,f);
							zmax = r2;
						}
					}

					if(mc::nrtl_tau(l,a,b,e,f) < new_l){ new_l = mc::nrtl_tau(l,a,b,e,f); zmin = l; }
					if(mc::nrtl_tau(u,a,b,e,f) < new_l){ new_l = mc::nrtl_tau(u,a,b,e,f); zmin = u; }
					if(mc::nrtl_tau(l,a,b,e,f) > new_u){ new_u = mc::nrtl_tau(l,a,b,e,f); zmax = l; }
					if(mc::nrtl_tau(u,a,b,e,f) > new_u){ new_u = mc::nrtl_tau(u,a,b,e,f); zmax = u; }
				}
			}
		}
	}

	//convexity
	if(b == 0 || e == 0 ){
		convexity = (mc::der2_nrtl_tau((l+u)/2,b,e)>=0)? CONVEX:CONCAVE;
	}
	else{
		//we have only one root of the second derivative so we simply check whether it lies within the interval
		double root = 2.0*b/e;
		if(root <= l || root >= u ){
			convexity = (mc::der2_nrtl_tau((l+u)/2,b,e)>=0)? CONVEX:CONCAVE;
		}else{
			convexity = CONV_NONE;
		}
	}
}


template <typename T> inline void
McCormick<T>::_nrtl_der_tau_mon_conv
( unsigned int &monotonicity, unsigned int &convexity, const double b, const double e, const double f, const double l, const double u, double &new_l, double &new_u, double &zmin, double &zmax ){

    if(e == 0){
		//the function is then simply f - b/T^2  ---> first derivative = 2*b/T^3  ---> second derivative -6*b/T^4, T>0
		if(b>=0){
			monotonicity = MON_INCR;
			convexity = CONCAVE;
		}else{
			monotonicity = MON_DECR;
			convexity = CONVEX;
		}
	}
	else{
	    //monotonicity
	    //in this case, we have one root
		double root = 2.0*b/e;
		if(root <= l){
			monotonicity = ( b<=0 )? MON_INCR : MON_DECR; //check if its a minimum or maximum, if b <= 0 then it's a minimum
		}
		else if(root >= u){
			monotonicity = ( b<=0 )? MON_DECR : MON_INCR; //check if its a minimum or maximum, if b <= 0 then it's a minimum
		}
		else{ // we can still get tight interval bounds
			monotonicity = MON_NONE;
			if(b<=0){ // it's a minimum
				//lower bound and min point
				new_l = mc::nrtl_dtau(root,b,e,f);
				zmin = root;
				//upper bound and max point
				if(mc::nrtl_dtau(l,b,e,f) <= mc::nrtl_dtau(u,b,e,f)){
					new_u = mc::nrtl_dtau(u,b,e,f);
					zmax = u;
				}else{
					new_u = mc::nrtl_dtau(l,b,e,f);
					zmax = u;
				}
			}else{ // it is a maximum
				//lower bound and min point
				if(mc::nrtl_dtau(l,b,e,f) <= mc::nrtl_dtau(u,b,e,f)){
					new_l = mc::nrtl_dtau(l,b,e,f);
					zmin = l;
				}else{
					new_l = mc::nrtl_dtau(u,b,e,f);
					zmin = u;
				}
				//upper bound and max point
				new_u = mc::nrtl_dtau(root,b,e,f);
				zmax = root;
			}
		}

		//convexity
		root = 3.0*b/e;
		if( root <= l || u <= root){ //if the root of second derivative is not within the interval, then just check any point of the second derivative
				convexity = ( 2.0*e/std::pow((l+u)/2,2) - 6.0*b/std::pow((l+u)/2,2) >0 )? CONVEX : CONCAVE;
		}
		else{ //the root lies within the interval and the function is convex-concave
			convexity = CONV_NONE;
		}
	}

}


#ifdef MC__MCCORMICK_DEBUG
template <typename T> inline void
McCormick<T>::_debug_check
( const McCormick<T> &MCin, const McCormick<T> &MCout, std::string &operation) {

  bool error = false;
  // check if any of the subgradients is a NaN
  for(unsigned int i = 0; i < MCout._nsub; i++){
		if(!(MCout._cvsub[i]==MCout._cvsub[i]) || !(MCout._ccsub[i]==MCout._ccsub[i])){
				error = true;
				std::cout << "convex subgradient[" << i << "]:" << MCout._cvsub[i] << " concave subgradient[" << i << "]:" << MCout._ccsub[i] << std::endl;
		}
  }
  // check if lower interval bound > upper interval bound
  // note that we do the isequal checks with a tolerance which is one order of magnitude higher than the MVCOMP_TOL. This is due to the fact, that MVCOMP_TOL may be the first decimal which is not exact
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if lower interval bound > cc
  if(Op<T>::l(MCout._I)>MCout._cc && !isequal(Op<T>::l(MCout._I),MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc << "=CC" << std::endl;
  }
   // check if upper interval bound < cv
  if(MCout._cv > Op<T>::u(MCout._I) && !isequal(MCout._cv,Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if convex >= concave
  if(MCout._cv>MCout._cc && !isequal(MCout._cv,MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << MCout._cc << "=CC" << std::endl;
  }
  // check for NaNs in intervals
  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) || Op<T>::l(MCout._I) != Op<T>::l(MCout._I) || Op<T>::u(MCin._I) != Op<T>::u(MCin._I) || Op<T>::l(MCin._I) != Op<T>::l(MCin._I)){
	  error = true;
  }

  // check if any is
  if(MCout._cv == std::numeric_limits<double>::infinity( ) || MCout._cc == std::numeric_limits<double>::infinity( )
		|| MCout._cv == -std::numeric_limits<double>::infinity( ) || MCout._cc == -std::numeric_limits<double>::infinity( ) || error){
		std::cout << "operation: " << operation << std::endl;
		std::cout << "MCin.l: " << std::setprecision(16) << Op<T>::l(MCin._I) << " MCin.cv: " << std::setprecision(16) <<  MCin._cv << " MCin.cc: " << std::setprecision(16) <<  MCin._cc << " MCin.u_ " << std::setprecision(16) <<  Op<T>::u(MCin._I) << std::endl;
		std::cout << "MCout.l: " << std::setprecision(16) <<  Op<T>::l(MCout._I) << " MCout.cv: " << std::setprecision(16) <<  MCout._cv << " MCout.cc: " << std::setprecision(16) <<  MCout._cc << " MCout.u_ " << std::setprecision(16) <<  Op<T>::u(MCout._I) << std::endl;
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::DEBUG );
	}
}

template <typename T> inline void
McCormick<T>::_debug_check
( const McCormick<T> &MCin1, const McCormick<T> &MCin2, const McCormick<T> &MCout, std::string &operation){

  bool error = false;
  // check if any of the subgradients is a NaN
  for(unsigned int i = 0; i < MCout._nsub; i++){
		if(!(MCout._cvsub[i]==MCout._cvsub[i]) || !(MCout._ccsub[i]==MCout._ccsub[i])){
				error = true;
				std::cout << "MCout._cvsub[" << i << "]: " << MCout._cvsub[i] << " MCout._ccsub[" << i << "]: " << MCout._ccsub[i] << std::endl;
		}
  }
  // check if lower interval bound > upper interval bound
  // note that we do the isequal checks with a tolerance which is one order of magnitude higher than the MVCOMP_TOL. This is due to the fact, that MVCOMP_TOL may be the first decimal which is not exact
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if lower interval bound > cc
  if(Op<T>::l(MCout._I)>MCout._cc && !isequal(Op<T>::l(MCout._I),MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc << "=CC" << std::endl;
  }
  // check if upper interval bound < _cv
  if(MCout._cv > Op<T>::u(MCout._I) && !isequal(MCout._cv,Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if convex > concave
  if(MCout._cv>MCout._cc && !isequal(MCout._cv,MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << MCout._cc << "=CC" << std::endl;
  }
  // check for NaNs in intervals
  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) ||Op<T>::l(MCout._I) != Op<T>::l(MCout._I)  || Op<T>::u(MCin2._I) != Op<T>::u(MCin2._I)
   || Op<T>::l(MCin1._I) != Op<T>::l(MCin1._I) || Op<T>::u(MCin1._I) != Op<T>::u(MCin1._I) || Op<T>::l(MCin2._I) != Op<T>::l(MCin2._I)){
	  error = true;
  }

  // check if any is
  if(MCout._cv == std::numeric_limits<double>::infinity( ) || MCout._cc == std::numeric_limits<double>::infinity( )
		|| MCout._cv == -std::numeric_limits<double>::infinity( ) || MCout._cc == -std::numeric_limits<double>::infinity( ) || error){
		std::cout << "operation: " << operation << std::endl;
		std::cout << "MCin1.l: " << std::setprecision(16) << Op<T>::l(MCin1._I) << " MCin1.cv: " << std::setprecision(16) <<  MCin1._cv << " MCin1.cc: " << std::setprecision(16) <<  MCin1._cc << " MCin1.u_ " << std::setprecision(16) <<  Op<T>::u(MCin1._I) << std::endl;
		std::cout << "MCin2.l: " << std::setprecision(16) <<  Op<T>::l(MCin2._I) << " MCin2.cv: " << std::setprecision(16) <<  MCin2._cv << " MCin2.cc: " << std::setprecision(16) <<  MCin2._cc << " MCin2.u_ " << std::setprecision(16) <<  Op<T>::u(MCin2._I) << std::endl;
		std::cout << "MCout.l: " << std::setprecision(16) <<  Op<T>::l(MCout._I) << " MCout.cv: " << std::setprecision(16) <<  MCout._cv << " MCout.cc: " << std::setprecision(16) <<  MCout._cc << " MCout.u_ " << std::setprecision(16) <<  Op<T>::u(MCout._I) << std::endl;
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::DEBUG );
	}
}

template <typename T> inline void
McCormick<T>::_debug_check
( const std::vector<McCormick<T>> &MCin, const McCormick<T> &MCout, std::string &operation) {

  bool error = false;
  // check if any of the subgradients is a NaN
  for(unsigned int i = 0; i < MCout._nsub; i++){
		if(!(MCout._cvsub[i]==MCout._cvsub[i]) || !(MCout._ccsub[i]==MCout._ccsub[i])){
				error = true;
				std::cout << "convex subgradient[" << i << "]:" << MCout._cvsub[i] << " concave subgradient[" << i << " ]:" << MCout._ccsub[i] << std::endl;
		}
  }
  // check if lower interval bound > upper interval bound
  // note that we do the isequal checks with a tolerance which is one order of magnitude higher than the MVCOMP_TOL. This is due to the fact, that MVCOMP_TOL may be the first decimal which is not exact
  if(Op<T>::l(MCout._I)>Op<T>::u(MCout._I) && !isequal(Op<T>::l(MCout._I),Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if lower interval bound > cc
  if(Op<T>::l(MCout._I)>MCout._cc && !isequal(Op<T>::l(MCout._I),MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "L=" << Op<T>::l(MCout._I) << ">" << MCout._cc << "=CC" << std::endl;
  }
   // check if upper interval bound < cv
  if(MCout._cv > Op<T>::u(MCout._I) && !isequal(MCout._cv,Op<T>::u(MCout._I),McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << Op<T>::u(MCout._I) << "=U" << std::endl;
  }
  // check if convex >= concave
  if(MCout._cv>MCout._cc && !isequal(MCout._cv,MCout._cc,McCormick<T>::options.MVCOMP_TOL*1e1,McCormick<T>::options.MVCOMP_TOL*1e1)){
	  error=true;
	  std::cout << "CV=" << MCout._cv << ">" << MCout._cc << "=CC" << std::endl;
  }
  // check for NaNs in intervals
  if(Op<T>::u(MCout._I) != Op<T>::u(MCout._I) || Op<T>::l(MCout._I) != Op<T>::l(MCout._I)){
	  error = true;
  }
  for(size_t i = 0;i<MCin.size();i++){
	  if(Op<T>::u(MCin[i]._I) != Op<T>::u(MCin[i]._I) || Op<T>::l(MCin[i]._I) != Op<T>::l(MCin[i]._I)){
		  error = true;
	  }
  }

  // check if any is
  if(MCout._cv == std::numeric_limits<double>::infinity( ) || MCout._cc == std::numeric_limits<double>::infinity( )
		|| MCout._cv == -std::numeric_limits<double>::infinity( ) || MCout._cc == -std::numeric_limits<double>::infinity( ) || error){
		std::cout << "operation: " << operation << std::endl;
		for(size_t i = 0; i<MCin.size(); i++){
			std::cout << "MCin[" << i << "].l: " << std::setprecision(16) << Op<T>::l(MCin[i]._I) << " MCin[" << i << "].cv: " << std::setprecision(16) <<  MCin[i]._cv
			          << " MCin[" << i << "].cc: " << std::setprecision(16) <<  MCin[i]._cc << " MCin[" << i << "].u_ " << std::setprecision(16) <<  Op<T>::u(MCin[i]._I) << std::endl;
		}
		std::cout << "MCout.l: " << std::setprecision(16) <<  Op<T>::l(MCout._I) << " MCout.cv: " << std::setprecision(16) <<  MCout._cv << " MCout.cc: " << std::setprecision(16) <<  MCout._cc << " MCout.u_ " << std::setprecision(16) <<  Op<T>::u(MCout._I) << std::endl;
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::DEBUG );
	}
}
#endif

template <typename T> inline double
McCormick<T>::_secant
( const double x0, const double x1, const double xL, const double xU,
  puniv f, const double*rusr, const int*iusr, const std::vector<double>&vusr)
{
  double xkm = std::max(xL,std::min(xU,x0));
  double fkm = f(xkm,rusr,iusr,vusr);
  double xk = std::max(xL,std::min(xU,x1));

  for( unsigned int it=0; it<options.ENVEL_MAXIT; it++ ){
    double fk = f(xk,rusr,iusr,vusr);
    if( std::fabs(fk) < options.ENVEL_TOL ) return xk;
    double Bk = (fk-fkm)/(xk-xkm);
    if( Bk == 0 ) throw Exceptions( Exceptions::ENVEL );
    if( isequal(xk,xL) && fk/Bk>0 ) return xk;
    if( isequal(xk,xU) && fk/Bk<0 ) return xk;
    xkm = xk;
    fkm = fk;
    xk = std::max(xL,std::min(xU,xk-fk/Bk));
  }

  throw Exceptions( Exceptions::ENVEL );
}

template <typename T> inline double
McCormick<T>::_goldsect
( const double xL, const double xU, puniv f, const double*rusr,
  const int*iusr, const std::vector<double>&vusr)
{
  const double phi = 2.-(1.+std::sqrt(5.))/2.;
  const double fL = f(xL,rusr,iusr,vusr), fU = f(xU,rusr,iusr,vusr);
  if( fL*fU > 0 ) throw Exceptions( Exceptions::ENVEL );
  const double xm = xU-phi*(xU-xL), fm = f(xm,rusr,iusr,vusr);
  return _goldsect_iter( true, xL, fL, xm, fm, xU, fU, f, rusr, iusr, vusr );
}

template <typename T> inline double
McCormick<T>::_goldsect_iter
( const bool init, const double a, const double fa, const double b,
  const double fb, const double c, const double fc, puniv f,
  const double*rusr, const int*iusr, const std::vector<double>&vusr )
// a and c are the current bounds; the minimum is between them.
// b is a center point
{
  static thread_local unsigned int iter;
  iter = ( init? 1: iter+1 );
  const double phi = 2.-(1.+std::sqrt(5.))/2.;
  bool b_then_x = ( c-b > b-a );
  double x = ( b_then_x? b+phi*(c-b): b-phi*(b-a) );
  if( std::fabs(c-a) < options.ENVEL_TOL*(std::fabs(b)+std::fabs(x))
   || iter > options.ENVEL_MAXIT ) return (c+a)/2.;
  double fx = f(x,rusr,iusr,vusr);
  if( b_then_x )
    return( fa*fx<0? _goldsect_iter( false, a, fa, b, fb, x, fx, f, rusr, iusr, vusr ):
                      _goldsect_iter( false, b, fb, x, fx, c, fc, f, rusr, iusr, vusr ) );
  return( fa*fb<0? _goldsect_iter( false, a, fa, x, fx, b, fb, f, rusr, iusr, vusr ):
                    _goldsect_iter( false, x, fx, b, fb, c, fc, f, rusr, iusr, vusr ) );
}

////////////////////////////////////////////////////////////////////////

template <typename T> inline McCormick<T>
cut
( const McCormick<T>&MC )
{
  McCormick<T> MC2( MC );
  return MC2.cut();
}

template <typename T> inline McCormick<T>
operator+
( const McCormick<T>&MC )
{
  McCormick<T> MC2( MC );
  return MC2;
}

template <typename T> inline McCormick<T>
operator+
( const double a, const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = a + MC._I;
  MC2._cv = a + MC._cv;
  MC2._cc = a + MC._cc;
  for( unsigned int i=0; i<MC2._nsub; i++ ){
    MC2._cvsub[i] = MC._cvsub[i];
    MC2._ccsub[i] = MC._ccsub[i];
  }
#ifdef MC__MCCORMICK_DEBUG
   	std::string str = "addition double+MC a =" + std::to_string(a);
	McCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2;
}

template <typename T> inline McCormick<T>
operator+
( const McCormick<T>&MC, const double a )
{
  return a + MC;
}

template <typename T> inline McCormick<T>
operator+
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  if( MC2._const ){
    McCormick<T> MC3;
    MC3._sub( MC1._nsub, MC1._const );
    return MC3._sum1( MC1, MC2 );
  }
  if( MC1._const ){
    McCormick<T> MC3;
    MC3._sub( MC2._nsub, MC2._const );
    return MC3._sum1( MC2, MC1 );
  }
  if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  McCormick<T> MC3;
  MC3._sub( MC1._nsub, MC1._const||MC2._const );
  return MC3._sum2( MC1, MC2 );
}

template <typename T> inline McCormick<T>
operator-
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = -MC._I;
  MC2._cv = -MC._cc;
  MC2._cc = -MC._cv;
  for( unsigned int i=0; i<MC2._nsub; i++ ){
    MC2._cvsub[i] = -MC._ccsub[i];
    MC2._ccsub[i] = -MC._cvsub[i];
  }
  return MC2;
}

template <typename T> inline McCormick<T>
operator-
( const McCormick<T>&MC, const double a )
{
  return MC + (-a);
}

template <typename T> inline McCormick<T>
operator-
( const double a, const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = a - MC._I;
  MC2._cv = a - MC._cc;
  MC2._cc = a - MC._cv;
  for( unsigned int i=0; i<MC2._nsub; i++ ){
    MC2._cvsub[i] = -MC._ccsub[i];
    MC2._ccsub[i] = -MC._cvsub[i];
  }
  return MC2;
}

template <typename T> inline McCormick<T>
operator-
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  if( &MC1 == &MC2 ) return 0;

  if( MC2._const ){
    McCormick<T> MC3;
    MC3._sub( MC1._nsub, MC1._const );
    return MC3._sub1( MC1, MC2 );
  }
  if( MC1._const ){
    McCormick<T> MC3;
    MC3._sub( MC2._nsub, MC2._const );
    return MC3._sub2( MC1, MC2 );
  }
  if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  McCormick<T> MC3;
  MC3._sub( MC1._nsub, MC1._const||MC2._const );
  return MC3._sub3( MC1, MC2 );
}

template <typename T> inline McCormick<T>
operator*
( const double a, const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = a * MC._I;
  if ( a >= 0 ){
    MC2._cv = a * MC._cv;
    MC2._cc = a * MC._cc;
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = a * MC._cvsub[i];
      MC2._ccsub[i] = a * MC._ccsub[i];
    }
  }
  else{
    MC2._cv = a * MC._cc;
    MC2._cc = a * MC._cv;
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = a * MC._ccsub[i];
      MC2._ccsub[i] = a * MC._cvsub[i];
    }
  }

#ifdef MC__MCCORMICK_DEBUG
   	std::string str = "mult double*MC a =" + std::to_string(a);
	McCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2;
}

template <typename T> inline McCormick<T>
operator*
( const McCormick<T>&MC, const double a )
{
  return a * MC;
}

template <typename T> inline McCormick<T>
operator*
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{

  if( &MC1 == &MC2 ) return sqr(MC1);


  bool thin1 = isequal( Op<T>::diam(MC1._I), 0. );
  bool thin2 = isequal( Op<T>::diam(MC2._I), 0. );

  if ( McCormick<T>::options.MVCOMP_USE && !(thin1||thin2) ){
    McCormick<T> MC3;
    if( MC2._const )
      MC3._sub( MC1._nsub, MC1._const );
    else if( MC1._const )
      MC3._sub( MC2._nsub, MC2._const );
    else if( MC1._nsub != MC2._nsub )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
    else
      MC3._sub( MC1._nsub, MC1._const||MC2._const );

    MC3._I = MC1._I * MC2._I;
    return MC3._mulMV( MC1, MC2 ).cut();
  }

  if ( Op<T>::l(MC1._I) >= 0. ){
    if ( Op<T>::l(MC2._I) >= 0. ){
      if( MC2._const ){
        McCormick<T> MC3;
        MC3._sub( MC1._nsub, MC1._const );
        return MC3._mul1_u1pos_u2pos( MC1, MC2 ).cut();
      }
      if( MC1._const ){
        McCormick<T> MC3;
        MC3._sub( MC2._nsub, MC2._const );
        return MC3._mul1_u1pos_u2pos( MC2, MC1 ).cut();
      }
      if( MC1._nsub != MC2._nsub )
        throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
      McCormick<T> MC3;
      MC3._sub( MC1._nsub, MC1._const||MC2._const );
      return MC3._mul2_u1pos_u2pos( MC1, MC2 ).cut();
    }
    if ( Op<T>::u(MC2._I) <= 0. ){
      return -( MC1 * (-MC2) );
    }
    if( MC2._const ){
      McCormick<T> MC3;
      MC3._sub( MC1._nsub, MC1._const );
      return MC3._mul1_u1pos_u2mix( MC1, MC2 ).cut();
    }
    if( MC1._const ){
      McCormick<T> MC3;
      MC3._sub( MC2._nsub, MC2._const );
      return MC3._mul2_u1pos_u2mix( MC1, MC2 ).cut();
    }
    if( MC1._nsub != MC2._nsub )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
    McCormick<T> MC3;
    MC3._sub( MC1._nsub, MC1._const||MC2._const );
    return MC3._mul3_u1pos_u2mix( MC1, MC2 ).cut();
  }

  if ( Op<T>::u(MC1._I) <= 0. ){
    if ( Op<T>::l(MC2._I) >= 0. ){
      return -( (-MC1) * MC2);
    }
    if ( Op<T>::u(MC2._I) <= 0. ){
      return (-MC1) * (-MC2);
    }
    return -( MC2 * (-MC1) );
  }

  if ( Op<T>::l(MC2._I) >= 0. ){
    return MC2 * MC1;
  }
  if ( Op<T>::u(MC2._I) <= 0. ){
    return -( (-MC2) * MC1 );
  }
  if( MC2._const ){
    McCormick<T> MC3;
    MC3._sub( MC1._nsub, MC1._const );
    return MC3._mul1_u1mix_u2mix( MC1, MC2 ).cut();
  }
  if( MC1._const ){
    McCormick<T> MC3;
    MC3._sub( MC2._nsub, MC2._const );
    return MC3._mul1_u1mix_u2mix( MC2, MC1 ).cut();
  }
  if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  McCormick<T> MC3;
  MC3._sub( MC1._nsub, MC1._const||MC2._const );
  return MC3._mul2_u1mix_u2mix( MC1, MC2 ).cut();
}

template <typename T> inline McCormick<T>
operator/
( const McCormick<T>&MC, const double a )
{
  if ( isequal( a, 0. ))
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::DIV );
  return (1./a) * MC;
}

template <typename T> inline McCormick<T>
operator/
( const double a, const McCormick<T>&MC )
{
  if( a == 0.) {return 0.;}
  return a * inv( MC );
}

template <typename T> inline McCormick<T>
operator/
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{

  if( &MC1 == &MC2 ) return 1.;

  // Added @ AVT.SVT, Aug 30, 2016: When not checking for constant denominator, multivariate gives unnecessarily lose relaxations for linear function
  if( (MC2._const) && (Op<T>::l(MC2._I)==Op<T>::u(MC2._I)) ) {
	if ( Op<T>::l(MC2._I) == 0. ) throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::INV );
	return (1./Op<T>::l(MC2._I)) * MC1;
  }

  bool posorthant = ( Op<T>::l(MC1._I) > 0. && Op<T>::l(MC2._I) > 0. );

  if ( McCormick<T>::options.MVCOMP_USE && posorthant){
    McCormick<T> MC3;
	if( MC2._const )
      MC3._sub( MC1._nsub, MC1._const );
    else if( MC1._const )
		// Removed @ AVT.SVT, Apr 07, 2017: Otherwise subgradient dimension can get lost!
	// if( MC1._const )
      MC3._sub( MC2._nsub, MC2._const );
    else if( MC1._nsub != MC2._nsub )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
    else
      MC3._sub( MC1._nsub, MC1._const||MC2._const );

    MC3._I = MC1._I / MC2._I;

    int imidcv1 = 1, imidcv2 = 2; // We can easily solve the convex problem given in Theorem 2 in Tsoukalas & Mitsos 2014,
								  // since the minimum of the convex relaxation is at point f1cv, f2cc, which can be shown through directional derivatives
    double fmidcv1 = ( mid(MC1._cv, MC1._cc, Op<T>::l(MC1._I), imidcv1)
      + std::sqrt(Op<T>::l(MC1._I) * Op<T>::u(MC1._I)) )
      / ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) );
    double fmidcv2 = mid(MC2._cv, MC2._cc, Op<T>::u(MC2._I), imidcv2);
    MC3._cv = mc::sqr(fmidcv1) / fmidcv2;
    for( unsigned int i=0; i<MC3._nsub; i++ )
      MC3._cvsub[i] = 2. * fmidcv1 / fmidcv2
        / ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) )
        * (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcv1 ))
        - mc::sqr( fmidcv1 / fmidcv2 )
        * (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcv2 ));

    int imidcc1 = -1, imidcc2 = -1; // we can't know which relaxation to use
	// Fixed @ AVT.SVT, Aug 24, 2016; Division was broken (e.g. log(x2)/x1 on[10,10.1]x[20,50])
    //double fmidcc1 = mid(MC1._cv, MC1._cc, Op<T>::l(MC1._I), imidcc1);
    //double fmidcc2 = mid(MC2._cv, MC2._cc, Op<T>::u(MC2._I), imidcc2);
    double fmidcc1 = mid_ndiff(MC1._cv, MC1._cc, Op<T>::u(MC1._I), imidcc1);
    double fmidcc2 = mid_ndiff(MC2._cv, MC2._cc, Op<T>::l(MC2._I), imidcc2);
    double gcc1 = Op<T>::u(MC2._I) * fmidcc1 - Op<T>::l(MC1._I) * fmidcc2
                 + Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
    double gcc2 = Op<T>::l(MC2._I) * fmidcc1 - Op<T>::u(MC1._I) * fmidcc2
                 + Op<T>::u(MC1._I) * Op<T>::u(MC2._I);

    if(  gcc1 <= gcc2 ){ //uses equation (31) in multivariate McCormick paper
      MC3._cc = gcc1 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
      for( unsigned int i=0; i<MC3._nsub; i++ ){
        MC3._ccsub[i] = 1. / Op<T>::l(MC2._I)
          * (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcc1 ))
          - Op<T>::l(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) )
          * (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcc2 ));
      }
    }
    else{
      MC3._cc = gcc2 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
      for( unsigned int i=0; i<MC3._nsub; i++ )
        MC3._ccsub[i] = 1. / Op<T>::u(MC2._I)
          * (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcc1 ))
          - Op<T>::u(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) )
          * (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcc2 ));
    }
#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "MC1/MC2";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);

#endif
	if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
    return MC3.cut();
  }

  return MC1 * inv( MC2 );
}

template <typename T> inline McCormick<T>
inv
( const McCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. && Op<T>::u(MC._I) >= 0. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::INV );
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::inv( MC._I );

  if ( Op<T>::l(MC._I) > 0. ){
    { int imid = 2; // convex envelope of 1/x is decreasing for x^L>0 so we use the cc relaxation for cv
      double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
      MC2._cv = 1./vmid;
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = - mid( MC._cvsub, MC._ccsub, i, imid )
          / ( vmid * vmid );
      }
    }
    { int imid = 1; // concave envelope of 1/x is decreasing for x^L>0 so we use the cv relaxation for cc
      MC2._cc = 1. / Op<T>::l(MC._I) + 1. / Op<T>::u(MC._I) - mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = - mid( MC._cvsub, MC._ccsub, i, imid )
          / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
    }

  }

  else{
    { int imid = 2; // convex envelope of 1/x is decreasing for x^L<0 so we use the cc relaxation for cv
      MC2._cv = 1. / Op<T>::l(MC._I) + 1. / Op<T>::u(MC._I) - mid( MC._cv, MC._cc,
        Op<T>::u(MC._I), imid ) / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = - mid( MC._cvsub, MC._ccsub, i, imid )
          / ( Op<T>::l(MC._I) * Op<T>::u(MC._I) );
    }
    { int imid = 1; // concave envelope of 1/x is decreasing for x^L>0 so we use the cv relaxation for cc
      double vmid = mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid);
      MC2._cc = 1. / vmid;
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = - mid( MC._cvsub, MC._ccsub, i, imid )
          / ( vmid * vmid );
    }
  }

#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "inv";
	McCormick<T>::_debug_check(MC, MC2, str);

#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
sqr
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::sqr( MC._I );
  { int imid = -1;
    double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
    imid = -1;
    MC2._cv = mc::sqr( mid( MC._cv, MC._cc, zmin, imid ) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = 2 * mid( MC._cvsub, MC._ccsub, i, imid )
        * mid( MC._cv, MC._cc, zmin, imid );
  }

  { int imid = -1;
    double zmax = (mc::sqr( Op<T>::l(MC._I) )>mc::sqr( Op<T>::u(MC._I) ) ? Op<T>::l(MC._I) : Op<T>::u(MC._I));
	  double r;
	  double pt;
	  if(isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		  r = 0.;
		  pt = mc::sqr(Op<T>::l(MC._I))>mc::sqr(Op<T>::u(MC._I)) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
	  }
	  else{
		  r = ( mc::sqr( Op<T>::u(MC._I) ) - mc::sqr( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
		  pt = Op<T>::l(MC._I);
	  }
    MC2._cc = mc::sqr( pt ) + r * ( mid( MC._cv, MC._cc, zmax, imid ) - pt );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }

#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "sqr";
	McCormick<T>::_debug_check(MC, MC2, str);

#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
exp
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::exp( MC._I );

  { int imid = 1;
    MC2._cv = std::exp( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ));
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * MC2._cv;
  }

  { int imid = 2;
    double r = 0.;
    if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
      r = ( std::exp( Op<T>::u(MC._I) ) - std::exp( Op<T>::l(MC._I) ) )
        / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
    MC2._cc = std::exp( Op<T>::u(MC._I) ) + r * ( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "exp";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
arh
( const McCormick<T>&MC, const double k )
{
  if( Op<T>::l(MC._I) <= 0. || k < 0. || ( Op<T>::u(MC._I) > 0.5*k && Op<T>::l(MC._I) >= 0.5*k ) ){
    return exp( - k * inv( MC ) );
  }

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::arh( MC._I, k );

  if ( Op<T>::u(MC._I) <= 0.5*k ){
    { int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid );
      MC2._cv = std::exp( - k / vmid );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = k / ( vmid * vmid ) * MC2._cv
          * mid( MC._cvsub, MC._ccsub, i, imid );
    }
    { int imid = -1;
      double r = 0.;
      if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
        r = ( mc::arh( Op<T>::u(MC._I),k ) - mc::arh( Op<T>::l(MC._I),k ) )
          / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
      MC2._cc = mc::arh( Op<T>::u(MC._I),k ) + r * ( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ) - Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
  }
  else if ( Op<T>::l(MC._I) >= 0.5*k ){
    { int imid = -1;
      double r = 0.;
      if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
        r = ( mc::arh( Op<T>::u(MC._I),k ) - mc::arh( Op<T>::l(MC._I),k ) )
          / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
      MC2._cv = mc::arh( Op<T>::l(MC._I),k ) + r * ( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ) - Op<T>::l(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
    { int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
      MC2._cc = std::exp( - k / vmid );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = k / ( vmid * vmid ) * MC2._cc
          * mid( MC._cvsub, MC._ccsub, i, imid );
    }
  }
#ifdef MC__MCCORMICK_DEBUG
   	std::string str = "arh k =" + std::to_string(k);
	McCormick<T>::_debug_check(MC, MC2, str);
#endif
    return MC2.cut();
}

template <typename T> inline McCormick<T>
log
( const McCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::LOG );
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::log( MC._I );

  { int imidcv = 1;
    double scal = 0.;
    if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
      scal = ( std::log( Op<T>::u(MC._I) ) - std::log( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
    MC2._cv = std::log( Op<T>::l(MC._I) ) + scal * ( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imidcv )
      - Op<T>::l(MC._I) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imidcv ) * scal;
  }

  { int imidcc = 2;
    double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imidcc );
    MC2._cc = std::log( vmid );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imidcc ) / vmid;
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "log";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
xlog
( const McCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) <= 0. ) // 0 is not allowed in McCormick, since the convex subgradient at 0 does not exist
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::LOG );
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::xlog( MC._I );

  { int imid = -1;
    double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), std::exp(-1.), imid );
    imid = -1;
    double vmid = mid( MC._cv, MC._cc, zmin, imid );
    MC2._cv = mc::xlog( vmid );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = (std::log( vmid ) + 1.) * mid( MC._cvsub, MC._ccsub, i, imid );
  }

  { int imid = -1;
    double zmax = ( mc::xlog(Op<T>::u(MC._I))>=mc::xlog(Op<T>::l(MC._I))? Op<T>::u(MC._I): Op<T>::l(MC._I) );
    double r;
	double pt;
    if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		r = 0.;
		pt = mc::xlog(Op<T>::l(MC._I))>mc::xlog(Op<T>::u(MC._I)) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
	}
	else {
		r = ( mc::xlog(Op<T>::u(MC._I)) - mc::xlog(Op<T>::l(MC._I)) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		pt = Op<T>::l(MC._I);
	}
    imid = -1;
    MC2._cc = mc::xlog(pt) + r * ( mid( MC._cv, MC._cc, zmax, imid ) - pt );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "xlog";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
fabsx_times_x
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::fabsx_times_x( MC._I );

  if(Op<T>::l(MC2._I)>=0){ // convex increasing
	  MC2._cv = mc::fabsx_times_x(MC._cv);
	  double r = 0;
	  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		r = (mc::fabsx_times_x(Op<T>::u(MC._I)) - mc::fabsx_times_x(Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I));
	  }
	  MC2._cc = mc::fabsx_times_x(Op<T>::u(MC._I)) + r*(MC._cc - Op<T>::u(MC._I));
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[i] = MC._cvsub[i] * 2 * MC._cv;
		MC2._ccsub[i] = MC._ccsub[i] * r;
	  }
  }
  else if(Op<T>::u(MC2._I)<=0){ // concave increasing
      double r = 0;
	  if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )){
		r = (mc::fabsx_times_x(Op<T>::u(MC._I)) - mc::fabsx_times_x(Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I));
	  }
	  MC2._cv = mc::fabsx_times_x(Op<T>::l(MC._I)) + r*(MC._cv - Op<T>::l(MC._I));
	  MC2._cc = mc::fabsx_times_x(MC._cc);
	  for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[i] = MC._cvsub[i] * r;
		MC2._ccsub[i] = MC._ccsub[i] * (-2) * MC._cc;
	  }
  }
  else{ // increasing
	 double ptCv = Op<T>::l(MC._I) - std::sqrt(2.0)* Op<T>::l(MC._I); // This is the solution point of (|x|*x - |xL|*xL)/(x-xL) = 2*x  <-- Note that we know that x is positive and xL is negative
	 double ptCc = Op<T>::u(MC._I) - std::sqrt(2.0)* Op<T>::u(MC._I); // This is the solution point of (|x|*x - |xU|*xU)/(x-xU) = -2*x <-- Note that we know that x is negative and xU is positive

	 // convex part
	 double subCv;
	 if( MC._cv >= ptCv ){	 // convex part
	    MC2._cv = mc::fabsx_times_x(MC._cv);
		subCv = 2*MC._cv;
	 }
	 else{
	    subCv = ( isequal( Op<T>::l(MC._I), ptCv )? 0.: (mc::fabsx_times_x(ptCv)-mc::fabsx_times_x(Op<T>::l(MC._I)))/(ptCv-Op<T>::l(MC._I)) );
		MC2._cv = mc::fabsx_times_x(Op<T>::l(MC._I)) + subCv*(MC._cv - Op<T>::l(MC._I));
	 }
	 double subCc;
	 if( ptCc >= MC._cc){
		MC2._cc = mc::fabsx_times_x(MC._cc);
        subCc = -2*MC._cc;
	 }
	 else{
	    subCc = ( isequal( Op<T>::u(MC._I), ptCc )? 0.: (mc::fabsx_times_x(ptCc)-mc::fabsx_times_x(Op<T>::u(MC._I)))/(ptCc-Op<T>::u(MC._I)) );
		MC2._cc = mc::fabsx_times_x(Op<T>::u(MC._I)) + subCc*(MC._cc - Op<T>::u(MC._I));
	 }
	 for( unsigned int i=0; i<MC2._nsub; i++ ){
		MC2._cvsub[i] = MC._cvsub[i] * subCv;
		MC2._ccsub[i] = MC._ccsub[i] * subCc;
	  }
  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "fabsx_times_x";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
xexpax
( const McCormick<T>&MC, const double a )
{
  if ( a == 0. )
    return MC;
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::xexpax( MC._I, a);

  if(a>0){ // -1/a is the minimum of the function
	  //the function is convex (not necessarily monotonic)
	  if(Op<T>::l(MC._I) >= -2.0/a){
		 //convex relaxation
		 {
	       int imid = -1;
           double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), -1.0/a, imid );
           imid = -1;
           double vmid = mid( MC._cv, MC._cc, zmin, imid );
           MC2._cv = mc::xexpax( vmid,a );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[i] = (std::exp(a*vmid)*(1.0+a*vmid)) * mid( MC._cvsub, MC._ccsub, i, imid );
		   }
		 }
		 //concave relaxation
		 {
		   int imid = -1;
           double zmax = ( mc::xexpax(Op<T>::u(MC._I),a)>=mc::xexpax(Op<T>::l(MC._I),a)? Op<T>::u(MC._I): Op<T>::l(MC._I) );
           double r;
		   double pt = zmax;
           if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
			   r = 0.;
		   }
		   else {
				r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		   }
           imid = -1;
           MC2._cc = mc::xexpax(pt,a) + r * ( mid( MC._cv, MC._cc, zmax, imid ) - pt );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
		   }
         }
	  }
	  //the function is concave and monotonically decreasing
	  else if(Op<T>::u(MC._I) <= -2.0/a){
		  //convex relaxation
		 {
           double r = 0.;
           if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
             r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
           MC2._cv = mc::xexpax(Op<T>::u(MC._I),a) + r * ( MC._cc - Op<T>::u(MC._I) );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[i] = MC._ccsub[i] * r;
		   }
         }
		 //concave relaxation
		 {
           MC2._cc = mc::xexpax( MC._cv ,a);
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[i] = (std::exp(a*MC._cv)*(1.0+a*MC._cv)) * MC._cvsub[i];
		   }
		 }
	  }
	  //the root of the second derivative is within the interval bounds and the function is convex-concave
	  else{
		  //convex relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xL))/(x-xL) = 0 is in interval,
		  //if it is not, newton may not converge to a bound in the given amount of iterations and ghen goldsect throws an error
		  double p1 = (Op<T>::l(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p1) + a*p1*std::exp(a*p1) - (p1*std::exp(a*p1) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(p1 - Op<T>::l(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::u(MC._I)) + a*Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - (Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I)))){
			  //there is a root
			  int imid = -1;
              const double* cvenv = McCormick<T>::_xexpaxcv( mid( MC._cv, MC._cc, -1.0/a, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
              MC2._cv = cvenv[0];
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
              }
		  }
		  //the root is not in the interval; in this case, xexpax is always decreasing within the interval
		  else{
			double r;
			if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
				r = 0.;
			} else {
				r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
			}
			 MC2._cv =  mc::xexpax(Op<T>::u(MC._I),a)+r*( MC._cc - Op<T>::u(MC._I) );
			 for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._cvsub[i] = r*MC._ccsub[i];
            }
		  }
		  //concave relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xU))/(x-xU) = 0 is in interval
		  double p2 = (Op<T>::u(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p2) + a*p2*std::exp(a*p2) - (p2*std::exp(a*p2) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(p2 - Op<T>::u(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::l(MC._I)) + a*Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - (Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(Op<T>::l(MC._I) - Op<T>::u(MC._I)))){
		    //there is a root
            int imid = -1;
			   double zmax = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
            const double* ccenv = McCormick<T>::_xexpaxcc( mid( MC._cv, MC._cc, zmax, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
            MC2._cc = ccenv[0];
            for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
            }
          }
		  //the root is not in the interval; in this case, xexpax need not be monotonic
		  else{
			  double r;
			  double pt;
			  if (isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))) {
				  r = 0.;
				  pt = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
			  }
			  else {
				  r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
				  pt = Op<T>::l(MC._I);
			  }
			 MC2._cc =  mc::xexpax(pt,a)+r*( (r>=0?MC._cc:MC._cv) - pt );
			 for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._ccsub[i] = r*(r>=0?MC._ccsub[i]:MC._cvsub[i]);
            }
		  }
	  }
  }
  else{ // -1/a is the maximum of the function
	  //the function is concave (not necessarily monotonic)
	  if(Op<T>::u(MC._I) <= -2.0/a){
	     //convex relaxation
		 {
		   int imid = -1;
           double zmin = ( mc::xexpax(Op<T>::u(MC._I),a)>=mc::xexpax(Op<T>::l(MC._I),a)? Op<T>::l(MC._I): Op<T>::u(MC._I) );
           double r;
		   double pt = zmin;
           if( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )) {
			   r =0.;
		   }
		   else {
			   r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
		   }
           imid = -1;
           MC2._cv = mc::xexpax(pt,a) + r * ( mid( MC._cv, MC._cc, zmin, imid ) - pt );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
		   }
         }
		 //concave relaxation
		 {
	       int imid = -1;
           double zmax = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), -1.0/a, imid );
           imid = -1;
           double vmid = mid( MC._cv, MC._cc, zmax, imid );
           MC2._cc = mc::xexpax( vmid,a );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[i] = (std::exp(a*vmid)*(1.0+a*vmid)) * mid( MC._cvsub, MC._ccsub, i, imid );
		   }
		 }
	  }
	  //the function is convex and monotonically decreasing
	  else if(Op<T>::l(MC._I) >= -2.0/a){
		 //concave relaxation
		 {
           double r = 0.;
           if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
             r = ( mc::xexpax(Op<T>::u(MC._I),a) - mc::xexpax(Op<T>::l(MC._I),a) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
           MC2._cc = mc::xexpax(Op<T>::l(MC._I),a) + r * ( MC._cv - Op<T>::l(MC._I) );
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._ccsub[i] = MC._cvsub[i] * r;
		   }
         }
		 //convex relaxation
		 {
           MC2._cv = mc::xexpax( MC._cc ,a);
           for( unsigned int i=0; i<MC2._nsub; i++ ){
             MC2._cvsub[i] = (std::exp(a*MC._cc)*(1.0+a*MC._cc)) * MC._ccsub[i];
		   }
		 }
	  }
	  //the root of the second derivative is within the interval bounds and the function is convex-concave
	  else{
		  //convex relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xL))/(x-xL) = 0 is in interval
		  double p1 = (Op<T>::l(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p1) + a*p1*std::exp(a*p1) - (p1*std::exp(a*p1) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(p1 - Op<T>::l(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::u(MC._I)) + a*Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - (Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)) - Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)))/(Op<T>::u(MC._I) - Op<T>::l(MC._I)))){
			  //there is a root
			     int imid = -1;
			     double zmin = mc::xexpax(Op<T>::u(MC._I),a)<mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::u(MC._I) : Op<T>::l(MC._I);
              const double* cvenv = McCormick<T>::_xexpaxcv( mid( MC._cv, MC._cc, zmin, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
              MC2._cv = cvenv[0];
              for( unsigned int i=0; i<MC2._nsub; i++ ){
                MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
              }
		  }
		  //the root is not in the interval; in this case, xexpax may not be monotonic
		  else{
			 double r;
			 double pt;
			 if (isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))) {
				 pt = mc::xexpax(Op<T>::u(MC._I),a)>mc::xexpax(Op<T>::l(MC._I),a) ? Op<T>::l(MC._I) : Op<T>::u(MC._I);
				 r = 0.;
			 }
			 else {
				 r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
				 pt = Op<T>::l(MC._I);
			 }
			 MC2._cv =  mc::xexpax(pt,a)+r*( (r>=0?MC._cv:MC._cc) - pt );
			 for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._cvsub[i] = r*(r>=0?MC._cvsub[i]:MC._ccsub[i]);
            }
		  }
		  //concave relaxation
		  //check if root of  derivative(xexpax(x)) - (xexpax(x)-xexpax(xU))/(x-xU) = 0 is in interval
		  double p2 = (Op<T>::u(MC._I)+(-2.0/a))/2.0;
		  if( mc::sign(std::exp(a*p2) + a*p2*std::exp(a*p2) - (p2*std::exp(a*p2) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(p2 - Op<T>::u(MC._I)))
			  != mc::sign(std::exp(a*Op<T>::l(MC._I)) + a*Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - (Op<T>::l(MC._I)*std::exp(a*Op<T>::l(MC._I)) - Op<T>::u(MC._I)*std::exp(a*Op<T>::u(MC._I)))/(Op<T>::l(MC._I) - Op<T>::u(MC._I)))){
		    //there is a root
            int imid = -1;
            const double* ccenv = McCormick<T>::_xexpaxcc( mid( MC._cv, MC._cc, -1./a, imid ), Op<T>::l(MC._I), Op<T>::u(MC._I), a );
            MC2._cc = ccenv[0];
            for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
            }
          }
		  //the root is not in the interval; in this case, xexpax is decreasing
		  else{
			  double r = 0.;
			  if (!isequal(Op<T>::u(MC._I),Op<T>::l(MC._I))) {
				r = (mc::xexpax(Op<T>::u(MC._I),a)-mc::xexpax(Op<T>::l(MC._I),a))/(Op<T>::u(MC._I)-Op<T>::l(MC._I));
			  }
			 MC2._cc =  mc::xexpax(Op<T>::l(MC._I),a)+r*( (r>=0?MC._cc:MC._cv) - Op<T>::l(MC._I) );
			 for( unsigned int i=0; i<MC2._nsub; i++ ){
              MC2._ccsub[i] = r*(r>=0?MC._ccsub[i]:MC._cvsub[i]);
            }
		  }
	  }
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "xexpax";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}


template <typename T> inline McCormick<T>
centerline_deficit
( const McCormick<T>&MC, const double xLim, const double type )
{
      McCormick<T> MC2;
      MC2._sub( MC._nsub, MC._const );
      MC2._I = Op<T>::centerline_deficit( MC._I, xLim, type);

      if (mc::isequal(Op<T>::diam(MC._I),0.)) {
            MC2._cv = Op<T>::l(MC2._I);
            MC2._cc = Op<T>::u(MC2._I);
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
            return MC2;
      }


      switch((int)type) {
            case 1:
            {
                  if (Op<T>::u(MC._I)<1.) {    // trivial case, function is zero
                        MC2._cv = 0.;
                        MC2._cc = 0.;
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                  } else if (Op<T>::l(MC._I)>=1.) {   // function is decreasing and convex

                        const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                        MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }

                        const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                        const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                        const double ccSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                        MC2._cc = fLeft + ccSlope*(MC._cv-Op<T>::l(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }

                  } else {

                        {     // convex relaxation
                              if (MC._cv<=1.) {
                                    MC2._cv = 0.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                              } else {
                                    const double cvSlope = isequal(Op<T>::u(MC._I),1.) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-0.)/(Op<T>::u(MC._I)-1.);
                                    MC2._cv = 0. + cvSlope*(MC._cv-1.);     
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }                               
                              }
                        }
                        {     // concave relaxation
                              if (MC._cv>1.) {
                                    const double ccSlope = isequal(1.,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-1.)/(Op<T>::u(MC._I)-1.);
                                    MC2._cc = 1. + ccSlope*(MC._cv-1.);
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                              } else if (MC._cc<1) {
                                    const double ccSlope = isequal(1.,Op<T>::l(MC._I)) ? 0. : 1./(1.-Op<T>::l(MC._I));
                                    MC2._cc = 1. + ccSlope*(MC._cc-1.);
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                              } else {
                                    MC2._cc = 1.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                              }
                        }

                  }
                  break;
            }
            case 2:
            {
                  if (Op<T>::u(MC._I)<xLim) {    // trivial case, function is zero

                        MC2._cv = 0.;
                        MC2._cc = 0.;
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }

                  } else if (Op<T>::u(MC._I)<=1.) {   // function is increasing and convex

                        const double cvSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                        MC2._cv = mc::centerline_deficit(MC._cv,xLim,type);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                        const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                        const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                        const double ccSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                        MC2._cc = fRight + ccSlope*(MC._cc-Op<T>::u(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }

                  } else if (Op<T>::l(MC._I)>=1.) {   // function is decreasing and convex

                        const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                        MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }

                        const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                        const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                        const double ccSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                        MC2._cc = fLeft + ccSlope*(MC._cv-Op<T>::l(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }

                  } else {

                        if (Op<T>::l(MC._I)<=xLim) {
                              {     // convex relaxation
                                    if (MC._cv<=xLim) {
                                          MC2._cv = 0.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                                    } else {
                                          const double cvSlope = isequal(Op<T>::u(MC._I),xLim) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-0.)/(Op<T>::u(MC._I)-xLim);
                                          MC2._cv = 0. + cvSlope*(MC._cv-xLim);     
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }                               
                                    }
                              }
                              {     // concave relaxation
                                    if (MC._cv>1.) {
                                          const double ccSlope = isequal(1.,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-1.)/(Op<T>::u(MC._I)-1.);
                                          MC2._cc = 1. + ccSlope*(MC._cv-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                    } else if (MC._cc<1) {
                                          const double ccSlope = isequal(1.,Op<T>::l(MC._I)) ? 0. : (1.-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(1.-Op<T>::l(MC._I));
                                          MC2._cc = 1. + ccSlope*(MC._cc-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    } else {
                                          MC2._cc = 1.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                                    }
                              }
                        } else {
                              {     // convex relaxation
                                    const double fL = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                                    const double fU = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                                    if (fU<=fL) {
                                          // In this case, could potentially have tangent at right end
                                          double rusr[4];
                                          rusr[0] = xLim;
                                          rusr[1] = type;
                                          rusr[2] = Op<T>::l(MC._I);
                                          rusr[3] = fL;
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::u(MC._I), 1.+1e-6*(Op<T>::u(MC._I)-1.), Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( 1., Op<T>::u(MC._I), fPtr, rusr );
                                          }
                                          if (MC._cc>=xj) { // we are still on the function itself
                                                const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                                MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::centerline_deficit(xj,xLim,type);
                                                const double cvSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(xj-Op<T>::l(MC._I));
                                                MC2._cv = fj + cvSlope*(MC._cc-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          } 
                                    } else {
                                          // In this case, it can only be the secant
                                          const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                                          MC2._cv = fL + cvSlope*(MC._cv-Op<T>::l(MC._I));
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                                    }
                              }
                              {     // concave relaxation
                                    if (MC._cv>1.) {
                                          const double ccSlope = isequal(1.,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-1.)/(Op<T>::u(MC._I)-1.);
                                          MC2._cc = 1. + ccSlope*(MC._cv-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                    } else if (MC._cc<1) {
                                          const double ccSlope = 1./xLim;
                                          MC2._cc = 1. + ccSlope*(MC._cc-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    } else {
                                          MC2._cc = 1.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                                    }
                              }

                        }

                  }
                  break;
            }
            case 3:
            {
                  
                  const double tmp = std::sqrt((9.*std::pow(xLim,3) - 69.*mc::sqr(xLim) + 175.*xLim - 175.)/std::pow(xLim - 1.,7));
                  const double xmax = ( tmp*( 5.*xLim - 1. - 10.*mc::sqr(xLim) + 10.*std::pow(xLim,3) - 5.*std::pow(xLim,4) + std::pow(xLim,5) ) - 47.*xLim + 4.*mc::sqr(xLim) + 3.*std::pow(xLim,3) + 70.)
                                                / (15.*(mc::sqr(xLim) - 4.*xLim + 5.));

                  if (Op<T>::l(MC._I)>=xmax) {    // for x>=xmax, function is decreasing
                        
                        const double tmp = std::sqrt(6.*std::pow(xLim,4) - 52.*std::pow(xLim,3) + 176.*sqr(xLim) - 280.*xLim + 175.); 
                        const double xInflection = -( tmp*(xLim-1.) + 11.*xLim + 8.*sqr(xLim) - 4.*std::pow(xLim,3) - 35.)/(10.*(sqr(xLim) - 4.*xLim + 5.));

                        if (Op<T>::l(MC._I)>=xInflection) { // in this area, function is convex

                              const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                              MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }

                              const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                              const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                              const double ccSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                              MC2._cc = fLeft + ccSlope*(MC._cv-Op<T>::l(MC._I));
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }

                        } else if (Op<T>::u(MC._I)<=xInflection) { // in this area, function is concave

                              const double ccSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                              MC2._cc = mc::centerline_deficit(MC._cv,xLim,type);
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }

                              const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                              const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                              const double cvSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                              MC2._cv = fLeft + cvSlope*(MC._cc-Op<T>::l(MC._I));
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }

                        } else { // in this area, the function is concave-convex

                              {     // concave part
                                    double rusr[4];
                                    rusr[0] = xLim;
                                    rusr[1] = type;
                                    rusr[2] = Op<T>::u(MC._I);
                                    rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                    double xj;
                                    try{
                                          xj = numerics::newton( Op<T>::l(MC._I), Op<T>::l(MC._I), xInflection, fPtr, dfPtr, rusr, NULL );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( Op<T>::l(MC._I), xInflection, fPtr, rusr, NULL );
                                    }
                                    if (MC._cv<=xj) { // we are still on the function itself
                                          const double ccSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                                          MC2._cc = mc::centerline_deficit(MC._cv,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::centerline_deficit(xj,xLim,type);
                                          const double ccSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-fj)/(Op<T>::u(MC._I)-xj);
                                          MC2._cc = fj + ccSlope*(MC._cv-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                    }
                              }
                              {     // convex part
                                    double rusr[4];
                                    rusr[0] = xLim;
                                    rusr[1] = type;
                                    rusr[2] = Op<T>::l(MC._I);
                                    rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                    double xj;
                                    try{
                                          xj = numerics::newton( Op<T>::u(MC._I), xInflection, Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xInflection, Op<T>::u(MC._I), fPtr, rusr );
                                    }
                                    if (MC._cc>=xj) { // we are still on the function itself
                                          const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                          MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::centerline_deficit(xj,xLim,type);
                                          const double cvSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(xj-Op<T>::l(MC._I));
                                          MC2._cv = fj + cvSlope*(MC._cc-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                    }
                              }

                        }

                  } else if (Op<T>::u(MC._I)<=xmax) {    // for x<=xmax, function is increasing

                        const double tmp = std::sqrt(175. + xLim*(-280. + xLim*(176. + xLim*(-52. + xLim*6.))));
                        const double xInflection = -(  tmp*(1.- xLim) + - 35. + xLim*(11. + xLim*(8. + xLim*(-4.))))/(10.*(5. + xLim*(-4. + xLim)));
                        

                        if (Op<T>::u(MC._I)<=xInflection) { // in this area, function is convex

                              const double cvSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                              MC2._cv = mc::centerline_deficit(MC._cv,xLim,type);
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                              const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                              const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                              const double ccSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                              MC2._cc = fRight + ccSlope*(MC._cc-Op<T>::u(MC._I));
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }

                        } else if (Op<T>::l(MC._I)>=xInflection) { // in this area, function is concave

                              const double ccSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                              MC2._cc = mc::centerline_deficit(MC._cc,xLim,type);
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }

                              const double fLeft = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                              const double fRight = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                              const double cvSlope = (fRight-fLeft)/Op<T>::diam(MC._I);
                              MC2._cv = fLeft + cvSlope*(MC._cv-Op<T>::l(MC._I));
                              for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                        } else { // in this area, the function is convex-concave

                              {     // convex part
                                    double rusr[4];
                                    rusr[0] = xLim;
                                    rusr[1] = type;
                                    rusr[2] = Op<T>::u(MC._I);
                                    rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                    double xj;
                                    const double xL = std::max(Op<T>::l(MC._I),xLim);
                                    try{
                                          xj = numerics::newton( xL+0.5*(xInflection-xL), xL, xInflection, fPtr, dfPtr, rusr, NULL );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xL, xInflection, fPtr, rusr, NULL );
                                    }
                                    if (MC._cv<=xj) { // we are still on the function itself
                                          const double cvSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                                          MC2._cv = mc::centerline_deficit(MC._cv,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::centerline_deficit(xj,xLim,type);
                                          const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-fj)/(Op<T>::u(MC._I)-xj);
                                          MC2._cv = fj + cvSlope*(MC._cv-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    }
                              }
                              {     // concave part
                                    double rusr[4];
                                    rusr[0] = xLim;
                                    rusr[1] = type;
                                    rusr[2] = Op<T>::l(MC._I);
                                    rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                    double xj;
                                    try{
                                          xj = numerics::newton( Op<T>::u(MC._I), xInflection, Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xInflection, Op<T>::u(MC._I), fPtr, rusr );
                                    }
                                    if (MC._cc>=xj) { // we are still on the function itself
                                          const double ccSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                          MC2._cc = mc::centerline_deficit(MC._cc,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::centerline_deficit(xj,xLim,type);
                                          const double ccSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(xj-Op<T>::l(MC._I));
                                          MC2._cc = fj + ccSlope*(MC._cc-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    }
                              }
                        }

                  } else {    // function has a maximum at xmax

                        {     // concave part

                              // we know that the concave envelope is anchored at (xmax,f(xmax))
                              if (MC._cv > xmax) {
                                    
                                    const double tmp = std::sqrt(6.*std::pow(xLim,4) - 52.*std::pow(xLim,3) + 176.*sqr(xLim) - 280.*xLim + 175.); 
                                    const double xInflection = -( tmp*(xLim-1.) + 11.*xLim + 8.*sqr(xLim) - 4.*std::pow(xLim,3) - 35.)/(10.*(sqr(xLim) - 4.*xLim + 5.));

                                    if (Op<T>::u(MC._I)<=xInflection) { // in this area, function is concave
                                          const double ccSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                                          MC2._cc = mc::centerline_deficit(MC._cv,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                    } else { // in this area, the function is concave-convex
                                          double rusr[4];
                                          rusr[0] = xLim;
                                          rusr[1] = type;
                                          rusr[2] = Op<T>::u(MC._I);
                                          rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( xmax, xmax, xInflection, fPtr, dfPtr, rusr, NULL );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xmax, xInflection, fPtr, rusr, NULL );
                                          }
                                          if (MC._cv<=xj) { // we are still on the function itself
                                                const double ccSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                                                MC2._cc = mc::centerline_deficit(MC._cv,xLim,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::centerline_deficit(xj,xLim,type);
                                                const double ccSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-fj)/(Op<T>::u(MC._I)-xj);
                                                MC2._cc = fj + ccSlope*(MC._cv-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                          }
                                    }
                              } else if (MC._cc < xmax) {
                                    
                                    const double tmp = std::sqrt(6.*std::pow(xLim,4) - 52.*std::pow(xLim,3) + 176.*sqr(xLim) - 280.*xLim + 175.); 
                                    const double xInflection = -(  tmp*(1.- xLim) + 11.*xLim + 8.*sqr(xLim) - 4.*std::pow(xLim,3) - 35.)/(10.*(sqr(xLim) - 4.*xLim + 5.));

                                    if (Op<T>::l(MC._I)>=xInflection) { // in this area, function is concave
                                          const double ccSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                          MC2._cc = mc::centerline_deficit(MC._cc,xLim,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    } else { // in this area, the function is convex-concave
                                          double rusr[4];
                                          rusr[0] = xLim;
                                          rusr[1] = type;
                                          rusr[2] = Op<T>::l(MC._I);
                                          rusr[3] = mc::centerline_deficit(rusr[2],xLim,type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( xmax, xInflection, xmax, fPtr, dfPtr, rusr );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xInflection, xmax, fPtr, rusr );
                                          }
                                          if (MC._cc>=xj) { // we are still on the function itself
                                                const double ccSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                                MC2._cc = mc::centerline_deficit(MC._cc,xLim,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::centerline_deficit(xj,xLim,type);
                                                const double ccSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(xj-Op<T>::l(MC._I));
                                                MC2._cc = fj + ccSlope*(MC._cc-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                          }
                                    }
                              } else {
                                    MC2._cc = mc::centerline_deficit(xmax,xLim,type);
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                              }

                        }
                        {     // convex part
                              // since the function is increasing for x<xmax and decreasing for x>xmax, there cannot be a common tangent on both sides.
                              // therefore, the convex envelope is either only a tangent at one side or the other, or a secant between both endpoints
                              // if it is a tangent at some side, it needs to be at the side with the lower function value.
                              // also, a tangent can only occur if the inflection point on the respective side is included in the interval
                              const double fL = mc::centerline_deficit(Op<T>::l(MC._I),xLim,type);
                              const double fU = mc::centerline_deficit(Op<T>::u(MC._I),xLim,type);
                              if (fL <= fU) {
                                    const double tmp = std::sqrt(6.*std::pow(xLim,4) - 52.*std::pow(xLim,3) + 176.*sqr(xLim) - 280.*xLim + 175.); 
                                    const double xInflection = -(  tmp*(1.- xLim) + 11.*xLim + 8.*sqr(xLim) - 4.*std::pow(xLim,3) - 35.)/(10.*(sqr(xLim) - 4.*xLim + 5.));
                                    if (Op<T>::l(MC._I)>=xInflection) {      // inflection point is not included --> secant
                                          const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                                          MC2._cv = fL + cvSlope*(MC._cv-Op<T>::l(MC._I));
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    } else {
                                          double rusr[4];
                                          rusr[0] = xLim;
                                          rusr[1] = type;
                                          rusr[2] = Op<T>::u(MC._I);
                                          rusr[3] = fU;
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                          double xj;
                                          const double xL = std::max(Op<T>::l(MC._I),xLim);
                                          try{
                                                xj = numerics::newton( xL+0.5*(xInflection-xL), xL, xInflection, fPtr, dfPtr, rusr, NULL );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xL, xInflection, fPtr, rusr, NULL );
                                          }
                                          if (MC._cv<=xj) { // we are still on the function itself
                                                const double cvSlope = mc::der_centerline_deficit(MC._cv,xLim,type);
                                                MC2._cv = mc::centerline_deficit(MC._cv,xLim,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::centerline_deficit(xj,xLim,type);
                                                const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::centerline_deficit(Op<T>::u(MC._I),xLim,type)-fj)/(Op<T>::u(MC._I)-xj);
                                                MC2._cv = fj + cvSlope*(MC._cv-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                          }
                                    }
                              } else {    // right side is lower, tangent can only be there
                                    const double tmp = std::sqrt(6.*std::pow(xLim,4) - 52.*std::pow(xLim,3) + 176.*sqr(xLim) - 280.*xLim + 175.); 
                                    const double xInflection = -( tmp*(xLim-1.) + 11.*xLim + 8.*sqr(xLim) - 4.*std::pow(xLim,3) - 35.)/(10.*(sqr(xLim) - 4.*xLim + 5.));
                                    if (Op<T>::u(MC._I)<=xInflection) {      // inflection point is not included --> secant 
                                          const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                                          MC2._cv = fU + cvSlope*(MC._cc-Op<T>::u(MC._I));
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                    } else {
                                          double rusr[4];
                                          rusr[0] = xLim;
                                          rusr[1] = type;
                                          rusr[2] = Op<T>::l(MC._I);
                                          rusr[3] = fL;
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::centerline_deficit(x,rusr[0],rusr[1]) + mc::der_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x) - rusr[3]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_centerline_deficit(x,rusr[0],rusr[1])*(rusr[2]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::u(MC._I), xInflection, Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xInflection, Op<T>::u(MC._I), fPtr, rusr );
                                          }
                                          if (MC._cc>=xj) { // we are still on the function itself
                                                const double cvSlope = mc::der_centerline_deficit(MC._cc,xLim,type);
                                                MC2._cv = mc::centerline_deficit(MC._cc,xLim,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::centerline_deficit(xj,xLim,type);
                                                const double cvSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::centerline_deficit(Op<T>::l(MC._I),xLim,type))/(xj-Op<T>::l(MC._I));
                                                MC2._cv = fj + cvSlope*(MC._cc-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          }
                                    }
                              }
                        }
                  
                  }
                  break;
            }
            default:
                  throw std::runtime_error("mc::McCormick\t centerline_deficit called with unkonw type.\n");
      }


      

#ifdef MC__MCCORMICK_DEBUG
      std::string str = "centerline_deficit";
      McCormick<T>::_debug_check(MC, MC2, str);
#endif
      if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
      return MC2.cut();
}



template <typename T> inline McCormick<T>
wake_profile
( const McCormick<T>&MC, const double type )
{
      McCormick<T> MC2;
      MC2._sub( MC._nsub, MC._const );
      MC2._I = Op<T>::wake_profile( MC._I, type);

      if (mc::isequal(Op<T>::diam(MC._I),0.)) {
            MC2._cv = Op<T>::l(MC2._I);
            MC2._cc = Op<T>::u(MC2._I);
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
            return MC2;
      }


      switch((int)type) {
            case 1: // Jensen top hat
            {
                  if ( ( Op<T>::l(MC._I) > 1. ) || ( Op<T>::u(MC._I) < -1. ) ) {
                        MC2._cv = 0.;
                        MC2._cc = 0.;
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                  } else if ( ( -1. < Op<T>::l(MC._I) ) && ( Op<T>::u(MC._I) < 1. ) ) {
                        MC2._cv = 1.;
                        MC2._cc = 1.;
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                  } else {
                        {     // convex part
                              if ( ( Op<T>::l(MC._I) <= -1. ) && ( Op<T>::u(MC._I) >= 1. ) ) {
                                    MC2._cv = 0.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                              } else if ( Op<T>::l(MC._I) > -1. ) {   // decreasing part of the convex envelope
                                    if (MC._cc>=1.) {
                                          MC2._cv = 0.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                                    } else {
                                          const double cvSlope = (isequal(Op<T>::l(MC._I),1.)) ? 0. : (-1./(1.-Op<T>::l(MC._I)));
                                          MC2._cv = 0. + cvSlope*(MC._cc-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._ccsub[i]*cvSlope); }
                                    }
                              } else {  // increasing part of the convex envelope
                                    if (MC._cv<=-1.) {
                                          MC2._cv = 0.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                                    } else {
                                          const double cvSlope = (isequal(Op<T>::u(MC._I),-1.)) ? 0. : (1./(Op<T>::u(MC._I)+1.));
                                          MC2._cv = 0. + cvSlope*(MC._cv+1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._cvsub[i]*cvSlope); }
                                    }
                              }
                        }
                        {     // concave part
                              if (MC._cv>=-1.) {    // we are maximizing over the decreasing part of the concave envelope
                                    if (MC._cv<=1.) {
                                          MC2._cc = 1.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                                    } else {
                                          const double ccSlope = (isequal(Op<T>::u(MC._I),1.)) ? 0. : (-1./(Op<T>::u(MC._I)-1.));
                                          MC2._cc = 1. + ccSlope*(MC._cv-1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._cvsub[i]*ccSlope); }
                                    }
                              } else if (MC._cc<=1.) {    // we are maximizing over the increasing part of the concave envelope
                                    if (MC._cc>=-1.) {
                                          MC2._cc = 1.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                                    } else {
                                          const double ccSlope = (isequal(Op<T>::l(MC._I),-1.)) ? 0. : (1./(-1.-Op<T>::l(MC._I)));
                                          MC2._cc = 1. + ccSlope*(MC._cc+1.);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._ccsub[i]*ccSlope); }
                                    }
                              } else {
                                    MC2._cc = 1.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                              }
                        }
                  }
                  break;
            }
            case 2: // Park Gauss profile
            {
                        if ( Op<T>::l(MC._I) >= 0.) { // function is decreasing
                              const double xInflextion = 1./std::sqrt(2.);
                              if ( Op<T>::l(MC._I) >= xInflextion ) { // function is convex

                                    const double cvSlope = mc::der_wake_profile(MC._cc,type);
                                    MC2._cv = mc::wake_profile(MC._cc,type);   
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._ccsub[i]*cvSlope); }                                 

                                    const double fAtXl = mc::wake_profile(Op<T>::l(MC._I),type);
                                    const double ccSlope = (mc::wake_profile(Op<T>::u(MC._I),type)-fAtXl)/Op<T>::diam(MC._I);
                                    MC2._cc = fAtXl + ccSlope*(MC._cv-Op<T>::l(MC._I));
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._cvsub[i]*ccSlope); }  

                              } else if ( Op<T>::u(MC._I) <= xInflextion ) {    // function is concave

                                    const double ccSlope = mc::der_wake_profile(MC._cv,type);
                                    MC2._cc = mc::wake_profile(MC._cv,type);   
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._cvsub[i]*ccSlope); }                                 

                                    const double fAtXu = mc::wake_profile(Op<T>::u(MC._I),type);
                                    const double cvSlope = (fAtXu-mc::wake_profile(Op<T>::l(MC._I),type))/Op<T>::diam(MC._I);
                                    MC2._cv = fAtXu + cvSlope*(MC._cc-Op<T>::u(MC._I));
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._ccsub[i]*cvSlope); }  

                              } else {    // function is concave-convex

                                    {     // concave relaxation
                                          double rusr[3];
                                          rusr[0] = type;
                                          rusr[1] = Op<T>::u(MC._I);
                                          rusr[2] = mc::wake_profile(rusr[1],type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::l(MC._I), Op<T>::l(MC._I), xInflextion-1e-6*(xInflextion-Op<T>::l(MC._I)), fPtr, dfPtr, rusr, NULL );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( Op<T>::l(MC._I), xInflextion, fPtr, rusr, NULL );
                                          }
                                          if (MC._cv<=xj) { // we are still on the function itself
                                                const double ccSlope = mc::der_wake_profile(MC._cv,type);
                                                MC2._cc = mc::wake_profile(MC._cv,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::wake_profile(xj,type);
                                                const double ccSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::wake_profile(Op<T>::u(MC._I),type)-fj)/(Op<T>::u(MC._I)-xj);
                                                MC2._cc = fj + ccSlope*(MC._cv-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                          }
                                    }
                                    {     // convex relaxation
                                          double rusr[3];
                                          rusr[0] = type;
                                          rusr[1] = Op<T>::l(MC._I);
                                          rusr[2] = mc::wake_profile(rusr[1],type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::u(MC._I), xInflextion+1e-6*(Op<T>::u(MC._I)-xInflextion), Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xInflextion, Op<T>::u(MC._I), fPtr, rusr );
                                          }
                                          if (MC._cc>=xj) { // we are still on the function itself
                                                const double cvSlope = mc::der_wake_profile(MC._cc,type);
                                                MC2._cv = mc::wake_profile(MC._cc,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::wake_profile(xj,type);
                                                const double cvSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::wake_profile(Op<T>::l(MC._I),type))/(xj-Op<T>::l(MC._I));
                                                MC2._cv = fj + cvSlope*(MC._cc-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          }
                                    }

                              }
                        } else if (Op<T>::u(MC._I) <= 0.) { // increasing part

                              const double xInflextion = -1./std::sqrt(2.);
                              if ( Op<T>::u(MC._I) <= xInflextion ) { // function is convex

                                    const double cvSlope = mc::der_wake_profile(MC._cv,type);
                                    MC2._cv = mc::wake_profile(MC._cv,type);   
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._cvsub[i]*cvSlope); }                                 

                                    const double fAtXu = mc::wake_profile(Op<T>::u(MC._I),type);
                                    const double ccSlope = (fAtXu-mc::wake_profile(Op<T>::l(MC._I),type))/Op<T>::diam(MC._I);
                                    MC2._cc = fAtXu + ccSlope*(MC._cc-Op<T>::u(MC._I));
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._ccsub[i]*ccSlope); }  

                              } else if ( Op<T>::l(MC._I) >= xInflextion ) {    // function is concave

                                    const double ccSlope = mc::der_wake_profile(MC._cc,type);
                                    MC2._cc = mc::wake_profile(MC._cc,type);   
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const ? 0. : (MC._ccsub[i]*ccSlope); }                                 

                                    const double fAtXl = mc::wake_profile(Op<T>::l(MC._I),type);
                                    const double cvSlope = (mc::wake_profile(Op<T>::u(MC._I),type)-fAtXl)/Op<T>::diam(MC._I);
                                    MC2._cv = fAtXl + cvSlope*(MC._cv-Op<T>::l(MC._I));
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const ? 0. : (MC._cvsub[i]*cvSlope); }  

                              } else {    // function is concave-convex

                                    {     // concave relaxation
                                          double rusr[3];
                                          rusr[0] = type;
                                          rusr[1] = Op<T>::l(MC._I);
                                          rusr[2] = mc::wake_profile(rusr[1],type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::u(MC._I), xInflextion+1e-6*(Op<T>::u(MC._I)-xInflextion), Op<T>::u(MC._I), fPtr, dfPtr, rusr, NULL );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( xInflextion, Op<T>::u(MC._I), fPtr, rusr, NULL );
                                          }
                                          if (MC._cc>=xj) { // we are still on the function itself
                                                const double ccSlope = mc::der_wake_profile(MC._cc,type);
                                                MC2._cc = mc::wake_profile(MC._cc,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::wake_profile(xj,type);
                                                const double ccSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::wake_profile(Op<T>::l(MC._I),type))/(xj-Op<T>::l(MC._I));
                                                MC2._cc = fj + ccSlope*(MC._cc-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                          }
                                    }
                                    {     // convex relaxation
                                          double rusr[3];
                                          rusr[0] = type;
                                          rusr[1] = Op<T>::u(MC._I);
                                          rusr[2] = mc::wake_profile(rusr[1],type);
                                          double (*fPtr)(const double,const double*,const int*);
                                          double (*dfPtr)(const double,const double*,const int*);
                                          fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                          dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                          double xj;
                                          try{
                                                xj = numerics::newton( Op<T>::l(MC._I), Op<T>::l(MC._I), xInflextion-1e-6*(xInflextion-Op<T>::l(MC._I)), fPtr, dfPtr, rusr );
                                          }
                                          catch( ... ){
                                                xj = numerics::goldsect( Op<T>::l(MC._I), xInflextion, fPtr, rusr );
                                          }
                                          if (MC._cv<=xj) { // we are still on the function itself
                                                const double cvSlope = mc::der_wake_profile(MC._cv,type);
                                                MC2._cv = mc::wake_profile(MC._cv,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                          } else {    // we are on the secant part
                                                const double fj = mc::wake_profile(xj,type);
                                                const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::wake_profile(Op<T>::u(MC._I),type)-fj)/(Op<T>::u(MC._I)-xj);
                                                MC2._cv = fj + cvSlope*(MC._cv-xj);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                          }
                                    }

                              }
                        } else {    // interval contains maximum f(0)=1
                              const double absXinflextion = 1./std::sqrt(2.);
                              {     // concave relaxation
                                    // we know that the concave envelope is anchored at (0,2*a)
                                    if (MC._cv > 0.) {
                                          if (Op<T>::u(MC._I)<=absXinflextion) { // in this area, function is concave
                                                const double ccSlope = mc::der_wake_profile(MC._cv,type);
                                                MC2._cc = mc::wake_profile(MC._cv,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                          } else { // in this area, the function is concave-convex
                                                double rusr[3];
                                                rusr[0] = type;
                                                rusr[1] = Op<T>::u(MC._I);
                                                rusr[2] = mc::wake_profile(rusr[1],type);
                                                double (*fPtr)(const double,const double*,const int*);
                                                double (*dfPtr)(const double,const double*,const int*);
                                                fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                                dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                                double xj;
                                                try{
                                                      xj = numerics::newton( 0., 0., absXinflextion*(1-1e-6), fPtr, dfPtr, rusr, NULL );
                                                }
                                                catch( ... ){
                                                      xj = numerics::goldsect( 0., absXinflextion, fPtr, rusr, NULL );
                                                }
                                                if (MC._cv<=xj) { // we are still on the function itself
                                                      const double ccSlope = mc::der_wake_profile(MC._cv,type);
                                                      MC2._cc = mc::wake_profile(MC._cv,type);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                                } else {    // we are on the secant part
                                                      const double fj = mc::wake_profile(xj,type);
                                                      const double ccSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::wake_profile(Op<T>::u(MC._I),type)-fj)/(Op<T>::u(MC._I)-xj);
                                                      MC2._cc = fj + ccSlope*(MC._cv-xj);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._cvsub[i]*ccSlope); }
                                                }
                                          }
                                    } else if (MC._cc < 0) {
                                          if (Op<T>::l(MC._I)>=-absXinflextion) { // in this area, function is concave
                                                const double ccSlope = mc::der_wake_profile(MC._cc,type);
                                                MC2._cc = mc::wake_profile(MC._cc,type);
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                          } else { // in this area, the function is convex-concave
                                                double rusr[3];
                                                rusr[0] = type;
                                                rusr[1] = Op<T>::l(MC._I);
                                                rusr[2] = mc::wake_profile(rusr[1],type);
                                                double (*fPtr)(const double,const double*,const int*);
                                                double (*dfPtr)(const double,const double*,const int*);
                                                fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                                dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                                double xj;
                                                try{
                                                      xj = numerics::newton( 0., -absXinflextion*(1.-1e-6), 0., fPtr, dfPtr, rusr );
                                                }
                                                catch( ... ){
                                                      xj = numerics::goldsect( -absXinflextion, 0., fPtr, rusr );
                                                }
                                                if (MC._cc>=xj) { // we are still on the function itself
                                                      const double ccSlope = mc::der_wake_profile(MC._cc,type);
                                                      MC2._cc = mc::wake_profile(MC._cc,type);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                                } else {    // we are on the secant part
                                                      const double fj = mc::wake_profile(xj,type);
                                                      const double ccSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::wake_profile(Op<T>::l(MC._I),type))/(xj-Op<T>::l(MC._I));
                                                      MC2._cc = fj + ccSlope*(MC._cc-xj);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                                }
                                          }
                                    } else {
                                          MC2._cc = 1.;
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                                    }
                              }
                              {     // convex relaxation
                                    // since the function is increasing for x<0 and decreasing for x>0, there cannot be a common tangent on both sides.
                                    // therefore, the convex envelope is either only a tangent at one side or the other, or a secant between both endpoints
                                    // if it is a tangent at some side, it needs to be at the side with the lower function value.
                                    // also, a tangent can only occur if the inflection point on the respective side is included in the interval
                                    const double fL = mc::wake_profile(Op<T>::l(MC._I),type);
                                    const double fU = mc::wake_profile(Op<T>::u(MC._I),type);
                                    if (fL <= fU) {
                                          if (Op<T>::l(MC._I)>=-absXinflextion) {      // inflection point is not included --> secant
                                                const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                                                MC2._cv = fL + cvSlope*(MC._cv-Op<T>::l(MC._I));
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                          } else {
                                                double rusr[3];
                                                rusr[0] = type;
                                                rusr[1] = Op<T>::u(MC._I);
                                                rusr[2] = fU;
                                                double (*fPtr)(const double,const double*,const int*);
                                                double (*dfPtr)(const double,const double*,const int*);
                                                fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                                dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                                double xj;
                                                try{
                                                      xj = numerics::newton( Op<T>::l(MC._I)+1e-6*(-absXinflextion-Op<T>::l(MC._I)), Op<T>::l(MC._I)+1e-6*(-absXinflextion-Op<T>::l(MC._I)), -absXinflextion-1e-6*(-absXinflextion-Op<T>::l(MC._I)), fPtr, dfPtr, rusr, NULL );
                                                }
                                                catch( ... ){
                                                      xj = numerics::goldsect( Op<T>::l(MC._I), -absXinflextion, fPtr, rusr, NULL );
                                                }
                                                if (MC._cv<=xj) { // we are still on the function itself
                                                      const double cvSlope = mc::der_wake_profile(MC._cv,type);
                                                      MC2._cv = mc::wake_profile(MC._cv,type);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                                } else {    // we are on the secant part
                                                      const double fj = mc::wake_profile(xj,type);
                                                      const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (mc::wake_profile(Op<T>::u(MC._I),type)-fj)/(Op<T>::u(MC._I)-xj);
                                                      MC2._cv = fj + cvSlope*(MC._cv-xj);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                                }
                                          }
                                    } else {    // right side is lower, tangent can only be there
                                          if (Op<T>::u(MC._I)<=absXinflextion) {      // inflection point is not included --> secant 
                                                const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                                                MC2._cv = fU + cvSlope*(MC._cc-Op<T>::u(MC._I));
                                                for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                          } else {
                                                double rusr[3];
                                                rusr[0] = type;
                                                rusr[1] = Op<T>::l(MC._I);
                                                rusr[2] = fL;
                                                double (*fPtr)(const double,const double*,const int*);
                                                double (*dfPtr)(const double,const double*,const int*);
                                                fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::wake_profile(x,rusr[0]) + mc::der_wake_profile(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                                dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_wake_profile(x,rusr[0])*(rusr[1]-x); };
                                                double xj;
                                                try{
                                                      xj = numerics::newton( Op<T>::u(MC._I), absXinflextion+1e-6*(Op<T>::u(MC._I)-absXinflextion), Op<T>::u(MC._I), fPtr, dfPtr, rusr );
                                                }
                                                catch( ... ){
                                                      xj = numerics::goldsect( absXinflextion, Op<T>::u(MC._I), fPtr, rusr );
                                                }
                                                if (MC._cc>=xj) { // we are still on the function itself
                                                      const double cvSlope = mc::der_wake_profile(MC._cc,type);
                                                      MC2._cv = mc::wake_profile(MC._cc,type);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                                } else {    // we are on the secant part
                                                      const double fj = mc::wake_profile(xj,type);
                                                      const double cvSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::wake_profile(Op<T>::l(MC._I),type))/(xj-Op<T>::l(MC._I));
                                                      MC2._cv = fj + cvSlope*(MC._cc-xj);
                                                      for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._ccsub[i]*cvSlope); }
                                                }
                                          }
                                    }
                              }                          
                        }
                  break;
            }
            default:
                  throw std::runtime_error("mc::McCormick:\twake_profile called with unknown type.\n");
      }

#ifdef MC__MCCORMICK_DEBUG
      std::string str = "wake_profile";
      McCormick<T>::_debug_check(MC, MC2, str);
#endif
      if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
      return MC2.cut();
}



template <typename T> inline McCormick<T>
wake_deficit
( const McCormick<T>&MC1, const McCormick<T>&MC2, const double a, const double alpha, const double rr, const double type1, const double type2 )
{
      
      const double r0 = rr*std::sqrt((1.-a)/(1.-2.*a));
      const McCormick<T> Rwake = r0 + alpha*MC1;
      McCormick<T> MC3 = 2.*a*centerline_deficit(Rwake/r0,1.-alpha*rr/r0,type1)*wake_profile(MC2/max(Rwake,rr),type2);
      MC3._I = Op<T>::wake_deficit( MC1._I, MC2._I, a, alpha, rr, type1, type2 );

#ifdef MC__MCCORMICK_DEBUG
      std::string str = "wake_deficit";
      McCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
      if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
      return MC3.cut();
}


template <typename T> inline McCormick<T>
power_curve
( const McCormick<T>&MC, const double type )
{
      McCormick<T> MC2;
      MC2._sub( MC._nsub, MC._const );
      MC2._I = Op<T>::power_curve( MC._I, type);

      if ((mc::isequal(Op<T>::diam(MC._I),0.))||(Op<T>::u(MC._I)<=0.)||(Op<T>::l(MC._I)>=1.)) {
            MC2._cv = Op<T>::l(MC2._I);
            MC2._cc = Op<T>::u(MC2._I);
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
            for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
            return MC2;
      }

      switch((int)type) {
            case 1: // classical cubic power curve
                  if (Op<T>::u(MC._I)<=1.) {      // increasing and convex
                        const double cvSlope = (MC._cv<=0)? 0. : 3.*mc::sqr(MC._cv);
                        MC2._cv = (MC._cv<=0)? 0. : std::pow(MC._cv,3);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                        const double fL = (Op<T>::l(MC._I)<=0.) ? 0. : std::pow(Op<T>::l(MC._I),3);
                        const double fU = std::pow(Op<T>::u(MC._I),3);
                        const double ccSlope = (fU-fL)/Op<T>::diam(MC._I);
                        MC2._cc = fU + ccSlope*(MC._cc-Op<T>::u(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                  } else {    // increasing and convex-concave
                        {     // convex relaxation
                              if (MC._cv<=0.) {
                                    MC2._cv = 0.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                              } else {
                                    double rusr[3];
                                    rusr[0] = type;
                                    rusr[1] = Op<T>::u(MC._I);
                                    rusr[2] = 1.;
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::power_curve(x,rusr[0]) + mc::der_power_curve(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_power_curve(x,rusr[0])*(rusr[1]-x); };
                                    double xj;
                                    const double xLow = std::max(0.,Op<T>::l(MC._I));
                                    try{
                                          xj = numerics::newton( xLow+0.5*(1.-xLow), xLow+1e-6*(1.-xLow), 1.-1e-6*(1.-xLow), fPtr, dfPtr, rusr );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xLow, 1., fPtr, rusr );
                                    }
                                    if (MC._cv<=xj) { // we are still on the function itself
                                          const double cvSlope = (MC._cv<=0)? 0. : 3.*mc::sqr(MC._cv);
                                          MC2._cv = (MC._cv<=0)? 0. : std::pow(MC._cv,3);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::power_curve(xj,type);
                                          const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (fj-mc::power_curve(Op<T>::u(MC._I),type))/(xj-Op<T>::u(MC._I));
                                          MC2._cv = fj + cvSlope*(MC._cv-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    }

                              }
                        }
                        {     // concave relaxation
                              if (MC._cc>=1.) {
                                    MC2._cc = 1.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                              } else {
                                    const double fL = (Op<T>::l(MC._I)<=0.) ? 0. : std::pow(Op<T>::l(MC._I),3);
                                    const double ccSlope = isequal(1.,Op<T>::l(MC._I)) ? 0. : (1.-fL)/(1.-Op<T>::l(MC._I));
                                    MC2._cc = 1. + ccSlope*(MC._cc-1.);
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                              }
                        }
                  }
                  break;
            case 2: // generalized power curve based on Enercon E-70 E4 according to Hau
            {
                  const double xInflection = 1342074484852144./1998999927019571.;
                  if (Op<T>::u(MC._I)<=xInflection) {      // increasing and convex
                        const double cvSlope = der_power_curve(MC._cv,type);
                        MC2._cv = power_curve(MC._cv,type);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }

                        const double fL = power_curve(Op<T>::l(MC._I),type);
                        const double fU = power_curve(Op<T>::u(MC._I),type);
                        const double ccSlope = (fU-fL)/Op<T>::diam(MC._I);
                        MC2._cc = fU + ccSlope*(MC._cc-Op<T>::u(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                  } else if (Op<T>::l(MC._I)>=xInflection) {      // increasing and concave
                        const double ccSlope = der_power_curve(MC._cc,type);
                        MC2._cc = power_curve(MC._cc,type);
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }

                        const double fL = power_curve(Op<T>::l(MC._I),type);
                        const double fU = power_curve(Op<T>::u(MC._I),type);
                        const double cvSlope = (fU-fL)/Op<T>::diam(MC._I);
                        MC2._cv = fL + cvSlope*(MC._cv-Op<T>::l(MC._I));
                        for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                  } else {    // increasing and convex-concave
                        {     // convex relaxation
                              if (MC._cv<=0.) {
                                    MC2._cv = 0.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = 0.; }
                              } else {
                                    double rusr[3];
                                    rusr[0] = type;
                                    rusr[1] = Op<T>::u(MC._I);
                                    rusr[2] = power_curve(Op<T>::u(MC._I),type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::power_curve(x,rusr[0]) + mc::der_power_curve(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_power_curve(x,rusr[0])*(rusr[1]-x); };
                                    double xj;
                                    const double xLow = std::max(0.,Op<T>::l(MC._I));
                                    try{
                                          xj = numerics::newton( 0.5*(xInflection+xLow), xLow, xInflection, fPtr, dfPtr, rusr );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xLow, xInflection, fPtr, rusr );
                                    }
                                    if (MC._cv<=xj) { // we are still on the function itself
                                          const double cvSlope = der_power_curve(MC._cv,type);
                                          MC2._cv = power_curve(MC._cv,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::power_curve(xj,type);
                                          const double cvSlope = mc::isequal(xj,Op<T>::u(MC._I)) ? 0. : (fj-mc::power_curve(Op<T>::u(MC._I),type))/(xj-Op<T>::u(MC._I));
                                          MC2._cv = fj + cvSlope*(MC._cv-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._cvsub[i] = MC._const? 0. : (MC._cvsub[i]*cvSlope); }
                                    }

                              }
                        }
                        {     // concave relaxation
                              if (MC._cc>=1.) {
                                    MC2._cc = 1.;
                                    for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = 0.; }
                              } else {
                                    double rusr[3];
                                    rusr[0] = type;
                                    rusr[1] = Op<T>::l(MC._I);
                                    rusr[2] = power_curve(Op<T>::l(MC._I),type);
                                    double (*fPtr)(const double,const double*,const int*);
                                    double (*dfPtr)(const double,const double*,const int*);
                                    fPtr = [](const double x, const double*rusr, const int*iusr) { return mc::power_curve(x,rusr[0]) + mc::der_power_curve(x,rusr[0])*(rusr[1]-x) - rusr[2]; };
                                    dfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der2_power_curve(x,rusr[0])*(rusr[1]-x); };
                                    double xj;
                                    const double xUp = std::min(1.,Op<T>::u(MC._I));
                                    try{
                                          xj = numerics::newton( 0.5*(xUp+xInflection), xInflection, xUp, fPtr, dfPtr, rusr );
                                    }
                                    catch( ... ){
                                          xj = numerics::goldsect( xInflection, xUp, fPtr, rusr );
                                    }
                                    if (MC._cc>=xj) { // we are still on the function itself
                                          const double ccSlope = der_power_curve(MC._cc,type);
                                          MC2._cc = power_curve(MC._cc,type);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    } else {    // we are on the secant part
                                          const double fj = mc::power_curve(xj,type);
                                          const double ccSlope = mc::isequal(xj,Op<T>::l(MC._I)) ? 0. : (fj-mc::power_curve(Op<T>::l(MC._I),type))/(xj-Op<T>::l(MC._I));
                                          MC2._cc = fj + ccSlope*(MC._cc-xj);
                                          for ( unsigned int i=0; i<MC2._nsub; i++ ) { MC2._ccsub[i] = MC._const? 0. : (MC._ccsub[i]*ccSlope); }
                                    }
                              }
                        }
                  }
                  break;
            }
            default:
                  throw std::runtime_error("mc::McCormick:\tpower_curve called with unknown type.\n");
      }
#ifdef MC__MCCORMICK_DEBUG
      std::string str = "power_curve";
      McCormick<T>::_debug_check(MC, MC2, str);
#endif
      if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC2.cut().apply_subgradient_interval_heuristic();
      return MC2.cut();
}



//added AVT.SVT 06.06.2017
template <typename T> inline McCormick<T>
lmtd
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  if ( Op<T>::l(MC1._I) <= 0. || Op<T>::l(MC2._I) <= 0.){
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::LMTD );
  }

 McCormick<T> MC3;
 if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );

  MC3._I = Op<T>::lmtd( MC1._I, MC2._I );
  //concave part
  { MC3._cc = mc::lmtd( MC1._cc, MC2._cc);
    if(isequal(MC1._cc,MC2._cc)){
       for( unsigned int i=0; i<MC3._nsub; i++ ){
	  MC3._ccsub[i] = 0.5*(MC1._const? 0.:MC1._ccsub[i]) + 0.5*(MC2._const? 0.:MC2._ccsub[i]);
       }
    }else{
    for( unsigned int i=0; i<MC3._nsub; i++ )
      MC3._ccsub[i] = (1./(std::log(MC1._cc)-std::log(MC2._cc))
		      -(MC1._cc - MC2._cc)/(MC1._cc* mc::sqr(std::log(MC1._cc)-std::log(MC2._cc))))*(MC1._const? 0.:MC1._ccsub[i])
                     +(-1./(std::log(MC1._cc)-std::log(MC2._cc))
		      +(MC1._cc - MC2._cc)/(MC2._cc* mc::sqr(std::log(MC1._cc)-std::log(MC2._cc))))*(MC2._const? 0.:MC2._ccsub[i]);
      }
  }
  //convex part
  {
    double l1 = mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I));
    double l2 = mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I));
    double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
    if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
        r11 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC1._I);
		l1 += (MC1._cv-Op<T>::l(MC1._I))*r11;
		r12 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) )/Op<T>::diam(MC1._I);
		l2 += (MC1._cv-Op<T>::u(MC1._I))*r12;
    }
    if(!isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
         r21 = ( mc::lmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
		 l1 += (MC2._cv-Op<T>::l(MC2._I))*r21;
		 r22 = ( mc::lmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::lmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
		 l2 += (MC2._cv-Op<T>::u(MC2._I))*r22;
    }

    double lambda1,lambda2;
	if( isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I)) || isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I)) ) {
		MC3._cv = l1;
		lambda1 = 1.;
		lambda2 = 0.;
	} else {
		MC3._cv = std::max(l1,l2);
		if(isequal(l1,l2)){
		  lambda1 = 0.5;
		  lambda2 = 0.5;
		}
		else if( l1>l2 ){
		  lambda1 = 1.;
		  lambda2 = 0.;
		}
		else{
		  lambda1 = 0.;
		  lambda2 = 1.;
		}
	}
    for( unsigned int i=0; i<MC3._nsub; i++ ){
      MC3._cvsub[i] = lambda1*(r11*(MC1._const? 0.:MC1._cvsub[i])+r21*(MC2._const? 0.:MC2._cvsub[i]))
                     +lambda2*(r12*(MC1._const? 0.:MC1._cvsub[i])+r22*(MC2._const? 0.:MC2._cvsub[i]));
    }
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "lmtd";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);

#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

//added AVT.SVT 08.06.2017
template <typename T> inline McCormick<T>
rlmtd
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  if ( Op<T>::l(MC1._I) <= 0. || Op<T>::l(MC2._I) <= 0.)
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::RLMTD );

  McCormick<T> MC3;
  if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );


  MC3._I = Op<T>::rlmtd( MC1._I, MC2._I );
  //convex part
  { MC3._cv = mc::rlmtd( MC1._cc, MC2._cc);
    if(isequal(MC1._cc,MC2._cc)){
       for( unsigned int i=0; i<MC3._nsub; i++ ){
	  MC3._cvsub[i] = -1./(2.*std::pow(MC1._cc,2))*(MC1._const? 0.:MC1._ccsub[i])
	                 - 1./(2.*std::pow(MC2._cc,2))*(MC2._const? 0.:MC2._ccsub[i]);
       }
    }else{
    for( unsigned int i=0; i<MC3._nsub; i++ )
      MC3._cvsub[i] = (1./(MC1._cc*(MC1._cc-MC2._cc))
		     -(std::log(MC1._cc)-std::log(MC2._cc))/(mc::sqr(MC1._cc-MC2._cc)))*(MC1._const? 0.:MC1._ccsub[i])
                     +(-1./(MC2._cc*(MC1._cc-MC2._cc))
		     +(std::log(MC1._cc)-std::log(MC2._cc))/(mc::sqr(MC1._cc-MC2._cc)))*(MC2._const? 0.:MC2._ccsub[i]);
     }
  }
  //concave part
  {
    double l1 = mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I));
    double l2 = mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I));
    double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
    if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
        r11 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC1._I);
	    l1 += (MC1._cv-Op<T>::u(MC1._I))*r11;
	    r12 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) )/Op<T>::diam(MC1._I);
	    l2 += (MC1._cv-Op<T>::l(MC1._I))*r12;
    }
    if(!isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
        r21 = ( mc::rlmtd(Op<T>::u(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::u(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
	    l1 += (MC2._cv-Op<T>::l(MC2._I))*r21;
	    r22 = ( mc::rlmtd(Op<T>::l(MC1._I),Op<T>::u(MC2._I)) - mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I)) )/Op<T>::diam(MC2._I);
	    l2 += (MC2._cv-Op<T>::u(MC2._I))*r22;
    }

    double lambda1,lambda2;
	if(isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
		if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
			MC3._cc = mc::rlmtd(Op<T>::l(MC1._I),Op<T>::l(MC2._I));
			lambda1 = 0.;
			lambda2 = 0.;
		}
		else{
			MC3._cc = 	l2;
			lambda1 = 0.;
			lambda2 = 1.;
		}
	}
	else if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
		MC3._cc = l1;
		lambda1 = 1.;
		lambda2 = 0.;
	}
	else {
		MC3._cc = std::min(l1,l2);
		if(isequal(l1,l2)){
		  lambda1 = 0.5;
		  lambda2 = 0.5;
		}
		else if( l1<l2 ){
		  lambda1 = 1.;
		  lambda2 = 0.;
		}
		else{
		  lambda1 = 0.;
		  lambda2 = 1.;
		}
	}

    for( unsigned int i=0; i<MC3._nsub; i++ ){
      MC3._ccsub[i] = lambda1*(r11*(MC1._const? 0.:MC1._cvsub[i])+r21*(MC2._const? 0.:MC2._cvsub[i]))
                     +lambda2*(r12*(MC1._const? 0.:MC1._cvsub[i])+r22*(MC2._const? 0.:MC2._cvsub[i]));
    }
  }
#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "rlmtd";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);

#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

//added AVT.SVT 09/2021
template <typename T> inline McCormick<T>
	mid
	(const McCormick<T>&MC1, const McCormick<T>&MC2, const double k)
	{

		McCormick<T> MC3;
		if (MC2._const)
			MC3._sub(MC1._nsub, MC1._const);
		else if (MC1._const)
			MC3._sub(MC2._nsub, MC2._const);
		else if (MC1._nsub != MC2._nsub)
			throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::SUB);
		else
			MC3._sub(MC1._nsub, MC1._const && MC2._const);

		MC3._I = Op<T>::mid(MC1._I, MC2._I, k);

		// auxiliary variables
		double x_1_L = Op<T>::l(MC1._I);
		double x_1_U = Op<T>::u(MC1._I);
		double x_2_L = Op<T>::l(MC2._I);
		double x_2_U = Op<T>::u(MC2._I);
		double z_max = mc::mid(x_1_U, x_2_U, k);
		double z_min = mc::mid(x_1_L, x_2_L, k);

		//convex part
		{
			//monotony: increasing in x1 and x2
			double x_1 = MC1._cv;
			double x_2 = MC2._cv;

			//coordinates of kink points
			double z_mid_right = std::max(std::min(x_1_U, k), std::min(x_2_L, z_max));
			double z_mid_left = std::max(std::min(x_2_U, k), std::min(x_1_L, z_max));
			double x_1_bot = std::max(std::min(z_min, x_1_U), x_1_L);
			double x_1_mid_left = std::max(std::min(k, std::min(x_1_U, x_2_U)), x_1_L);
			double x_2_bot = std::max(std::min(z_min, x_2_U), x_2_L);
			double x_2_mid_right = std::max(std::min(k, std::min(x_1_U, x_2_U)), x_2_L);

			const unsigned int num_smplx = 4;
			double numer_x1[num_smplx], denom_x1[num_smplx], numer_x2[num_smplx], denom_x2[num_smplx], x_1_0[num_smplx], x_2_0[num_smplx], z_0[num_smplx], cv[num_smplx];
			//left
			numer_x1[0] = 0;
			denom_x1[0] = 1;
			numer_x2[0] = z_mid_left - z_min;
			denom_x2[0] = x_2_U - x_2_bot;
			x_1_0[0] = 0;
			x_2_0[0] = x_2_bot;
			z_0[0] = z_min;
			//right
			numer_x1[1] = z_mid_right - z_min;
			denom_x1[1] = x_1_U - x_1_bot;
			numer_x2[1] = 0;
			denom_x2[1] = 1;
			x_1_0[1] = x_1_bot;
			x_2_0[1] = 0;
			z_0[1] = z_min;
			//top
			numer_x1[2] = z_mid_left - z_max;
			denom_x1[2] = x_1_mid_left - x_1_U;
			numer_x2[2] = z_mid_right - z_max;
			denom_x2[2] = x_2_mid_right - x_2_U;
			x_1_0[2] = x_1_U;
			x_2_0[2] = x_2_U;
			z_0[2] = z_max;
			//bottom
			numer_x1[3] = (z_mid_left - z_min) * (x_2_mid_right - x_2_bot) - (z_mid_right - z_min) * (x_2_U - x_2_bot);
			denom_x1[3] = (x_1_mid_left - x_1_bot) * (x_2_mid_right - x_2_bot) - (x_1_U - x_1_bot) * (x_2_U - x_2_bot);
			numer_x2[3] = (z_mid_left - z_min) * (x_1_U - x_1_bot) - (z_mid_right - z_min) * (x_1_mid_left - x_1_bot);
			denom_x2[3] = (x_2_U - x_2_bot) * (x_1_U - x_1_bot) - (x_2_mid_right - x_2_bot) * (x_1_mid_left - x_1_bot);
			x_1_0[3] = x_1_bot;
			x_2_0[3] = x_2_bot;
			z_0[3] = z_min;


			for (unsigned int i = 0; i < num_smplx; i++) {
				if (!(isequal(denom_x1[i], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL) || isequal(denom_x2[i], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL))) {
					cv[i] = numer_x1[i] / denom_x1[i] * (x_1 - x_1_0[i]) + numer_x2[i] / denom_x2[i] * (x_2 - x_2_0[i]) + z_0[i];
				}
				else {
					cv[i] = z_min;
				}
			}
			// Store the best relaxation with the corresponding subgradient
			const unsigned irelax = argmax(num_smplx, cv);
			double x_1_slope = 0;
			double x_2_slope = 0;
			MC3._cv = cv[irelax];
			if (!(isequal(denom_x1[irelax], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL) || isequal(denom_x2[irelax], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL))) {
				x_1_slope = numer_x1[irelax] / denom_x1[irelax];
				x_2_slope = numer_x2[irelax] / denom_x2[irelax];
			}
			for (unsigned int i = 0; i < MC3._nsub; i++) {
				MC3._cvsub[i] = x_1_slope * (MC1._const ? 0. : MC1._cvsub[i]) + x_2_slope * (MC2._const ? 0. : MC2._cvsub[i]);
			}
		}
		//concave part
		{
			//monotony: increasing in x1 and x2
			double x_1 = MC1._cc;
			double x_2 = MC2._cc;

			//coordinates of kink points
			double z_mid_right = std::min(std::max(x_2_L, k), std::max(x_1_U, z_min));
			double z_mid_left = std::min(std::max(x_1_L, k), std::max(x_2_U, z_min));
			double x_1_top = std::min(std::max(z_max, x_1_L), x_1_U);
			double x_1_mid_right = std::min(std::max(k, std::max(x_1_L, x_2_L)), x_1_U);
			double x_2_top = std::min(std::max(z_max, x_2_L), x_2_U);
			double x_2_mid_left = std::min(std::max(k, std::max(x_2_L, x_1_L)), x_2_U);

			const unsigned int num_smplx = 4;
			double numer_x1[num_smplx], denom_x1[num_smplx], numer_x2[num_smplx], denom_x2[num_smplx], x_1_0[num_smplx], x_2_0[num_smplx], z_0[num_smplx], cc[num_smplx];
			//left
			numer_x1[0] = z_mid_left - z_max;
			denom_x1[0] = x_1_L - x_1_top;
			numer_x2[0] = 0;
			denom_x2[0] = 1;
			x_1_0[0] = x_1_top;
			x_2_0[0] = 0;
			z_0[0] = z_max;
			//right
			numer_x1[1] = 0;
			denom_x1[1] = 1;
			numer_x2[1] = z_mid_right - z_max;
			denom_x2[1] = x_2_L - x_2_top;
			x_1_0[1] = 0;
			x_2_0[1] = x_2_top;
			z_0[1] = z_max;
			//bottom
			numer_x1[2] = z_mid_right - z_min;
			denom_x1[2] = x_1_mid_right - x_1_L;
			numer_x2[2] = z_mid_left - z_min;
			denom_x2[2] = x_2_mid_left - x_2_L;
			x_1_0[2] = x_1_L;
			x_2_0[2] = x_2_L;
			z_0[2] = z_min;
			//top
			numer_x1[3] = (z_mid_left - z_max) * (x_2_L - x_2_top) - (z_mid_right - z_max) * (x_2_mid_left - x_2_top);
			denom_x1[3] = (x_1_L - x_1_top) * (x_2_L - x_2_top) - (x_1_mid_right - x_1_top) * (x_2_mid_left - x_2_top);
			numer_x2[3] = (z_mid_left - z_max) * (x_1_mid_right - x_1_top) - (z_mid_right - z_max) * (x_1_L - x_1_top);
			denom_x2[3] = (x_2_mid_left - x_2_top) * (x_1_mid_right - x_1_top) - (x_2_L - x_2_top) * (x_1_L - x_1_top);
			x_1_0[3] = x_1_top;
			x_2_0[3] = x_2_top;
			z_0[3] = z_max;


			for (unsigned int i = 0; i < num_smplx; i++) {
				if (!(isequal(denom_x1[i], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL) || isequal(denom_x2[i], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL))) {
					cc[i] = numer_x1[i] / denom_x1[i] * (x_1 - x_1_0[i]) + numer_x2[i] / denom_x2[i] * (x_2 - x_2_0[i]) + z_0[i];
				}
				else {
					cc[i] = z_max;
				}
			}
			// Store the best relaxation with the corresponding subgradient
			const unsigned irelax = argmin(num_smplx, cc);
			MC3._cc = cc[irelax];
			double x_1_slope = 0;
			double x_2_slope = 0;
			if (!(isequal(denom_x1[irelax], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL) || isequal(denom_x2[irelax], 0, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL))) {
				x_1_slope = numer_x1[irelax] / denom_x1[irelax];
				x_2_slope = numer_x2[irelax] / denom_x2[irelax];
			}
			for (unsigned int i = 0; i < MC3._nsub; i++) {
				MC3._ccsub[i] = x_1_slope * (MC1._const ? 0. : MC1._ccsub[i]) + x_2_slope * (MC2._const ? 0. : MC2._ccsub[i]);
			}
		}

		//////////////////////////////////////////////////////////////////////////////////
#ifdef MC__MCCORMICK_DEBUG

		std::string str = "mid";
		McCormick<T>::_debug_check(MC1, MC2, MC3, k, str);

#endif
		if (McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
		return MC3.cut();

	}



	//added AVT.SVT 09/2021
template <typename T> inline McCormick<T>
	pinch
	(const McCormick<T>&Th, const McCormick<T>&Tc, const McCormick<T>&Tp)
	{
		McCormick<T> dugro_max = max(Th - Tp, 0.) - max(Tc - Tp, 0.);
		McCormick<T> alt_max = max(Th, Tp) - max(Tc, Tp);
		McCormick<T> output = dugro_max;

		double cc[2] = { dugro_max._cc, alt_max._cc };
		double cv[2] = { dugro_max._cv, alt_max._cv };
		const unsigned irelax_cc = argmin(2, cc);
		const unsigned irelax_cv = argmax(2, cv);


		output._I = Op<T>::pinch(Th._I, Tc._I, Tp._I);
		output._cc = cc[irelax_cc];
		output._cv = cv[irelax_cv];

		if (irelax_cc == 0) {
			for (unsigned int i = 0; i < output._nsub; i++) {
				output._ccsub[i] = dugro_max._ccsub[i];
			}
		}
		else {
			for (unsigned int i = 0; i < output._nsub; i++) {
				output._ccsub[i] = alt_max._ccsub[i];
			}
		}
		if (irelax_cv == 0) {
			for (unsigned int i = 0; i < output._nsub; i++) {
				output._cvsub[i] = dugro_max._cvsub[i];
			}
		}
		else {
			for (unsigned int i = 0; i < output._nsub; i++) {
				output._cvsub[i] = alt_max._cvsub[i];
			}
		}


#ifdef MC__MCCORMICK_DEBUG

		std::string str = "pinch";
		McCormick<T>::_debug_check(Th, Tc, Tp, output, str);

#endif
		if (McCormick<T>::options.SUB_INT_HEUR_USE) return output.cut().apply_subgradient_interval_heuristic();
		return output.cut();
	}


template <typename T> inline McCormick<T>
euclidean_norm_2d
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
 McCormick<T> MC3;
 if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );

  MC3._I = Op<T>::euclidean_norm_2d( MC1._I, MC2._I );

  // The function is convex
  int imidxCv = -1;
  int imidyCv = -1;
  double xmin = mc::mid(MC1._cv,MC1._cc,0.,imidxCv);
  double ymin = mc::mid(MC2._cv,MC2._cc,0.,imidyCv);
  MC3._cv = mc::euclidean_norm_2d(xmin,ymin);

  // For concave we need to find out which hyperplane to use
  double xL = Op<T>::l(MC1._I);
  double xU = Op<T>::u(MC1._I);
  double yL = Op<T>::l(MC2._I);
  double yU = Op<T>::u(MC2._I);
  std::vector<double> slopes = {isequal(xL,xU) ? 0. : (mc::euclidean_norm_2d(xU,yL)-mc::euclidean_norm_2d(xL,yL))/(xU-xL),
                                isequal(yL,yU) ? 0. : (mc::euclidean_norm_2d(xL,yU)-mc::euclidean_norm_2d(xL,yL))/(yU-yL),
                                isequal(xL,xU) ? 0. : (mc::euclidean_norm_2d(xU,yU)-mc::euclidean_norm_2d(xL,yU))/(xU-xL),
                                isequal(yL,yU) ? 0. : (mc::euclidean_norm_2d(xU,yU)-mc::euclidean_norm_2d(xU,yL))/(yU-yL)};

  bool useHyperplanes1 = (mc::euclidean_norm_2d(xL,yL)+mc::euclidean_norm_2d(xU,yU))/2.0 > (mc::euclidean_norm_2d(xL,yU)+mc::euclidean_norm_2d(xU,yL))/2.0 ? true : false;

  std::vector<double> hyperplanes;
  if(useHyperplanes1) {
      hyperplanes = {mc::euclidean_norm_2d(xL,yL) + slopes[0]*( (slopes[0]>=0?MC1._cc:MC1._cv) -xL) + slopes[3]*((slopes[3]>=0?MC2._cc:MC2._cv)-yL),
                     mc::euclidean_norm_2d(xU,yU) + slopes[2]*( (slopes[2]>=0?MC1._cc:MC1._cv) -xU) + slopes[1]*((slopes[1]>=0?MC2._cc:MC2._cv)-yU)};
  }
  else{
      hyperplanes = {mc::euclidean_norm_2d(xL,yU) + slopes[0]*( (slopes[0]>=0?MC1._cc:MC1._cv) -xL) + slopes[1]*((slopes[1]>=0?MC2._cc:MC2._cv)-yU),
                     mc::euclidean_norm_2d(xU,yL) + slopes[2]*( (slopes[2]>=0?MC1._cc:MC1._cv) -xU) + slopes[3]*((slopes[3]>=0?MC2._cc:MC2._cv)-yL)};
  }

  unsigned index = mc::argmin(2,hyperplanes.data());
  double xDerCc, yDerCc;
  unsigned imidxCc, imidyCc;
  MC3._cc = hyperplanes[index];

  if(useHyperplanes1){
	  switch(index){
	    case 0:
		  xDerCc = slopes[0];
		  yDerCc = slopes[3];
		  imidxCc = (slopes[0]>=0?2:1); // 2 is concave, 1 is convex --> see mcfunc.hpp mid function
		  imidyCc = (slopes[3]>=0?2:1);
		  break;
		case 1:
		  xDerCc = slopes[2];
		  yDerCc = slopes[1];
		  imidxCc = (slopes[2]>=0?2:1);
		  imidyCc = (slopes[1]>=0?2:1);
		  break;
		default:
		  xDerCc = 0; yDerCc = 0; imidxCc = 0; imidyCc = 0;
		  break;
      }
  }
  else{
	  switch(index){
	    case 0:
		  xDerCc = slopes[0];
		  yDerCc = slopes[1];
		  imidxCc = (slopes[0]>=0?2:1);
		  imidyCc = (slopes[1]>=0?2:1);
		  break;
		case 1:
		  xDerCc = slopes[2];
		  yDerCc = slopes[3];
		  imidxCc = (slopes[2]>=0?2:1);
		  imidyCc = (slopes[3]>=0?2:1);
		  break;
		default:
		  xDerCc = 0; yDerCc = 0; imidxCc = 0; imidyCc = 0;
		  break;
      }
  }


  // Subgradients
  for(unsigned int i=0; i<MC3._nsub;i++){
	  MC3._cvsub[i] = (MC1._const ? 0. : mid(MC1._cvsub, MC1._ccsub, i, imidxCv)) * mc::der_euclidean_norm_2d(xmin,ymin)
	                 +(MC2._const ? 0. : mid(MC2._cvsub, MC2._ccsub, i, imidyCv)) * mc::der_euclidean_norm_2d(ymin,xmin);
	  MC3._ccsub[i] = (MC1._const ? 0. : mid(MC1._cvsub, MC1._ccsub, i, imidxCc)) * xDerCc
	                 +(MC2._const ? 0. : mid(MC2._cvsub, MC2._ccsub, i, imidyCc)) * yDerCc;
  }

#ifdef MC__MCCORMICK_DEBUG
	std::string str = "euclidean_norm_2d";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

//added AVT.SVT 01.03.2018
template <typename T> inline McCormick<T>
expx_times_y
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );

  if ( Op<T>::l(MC2._I) > 0. && !isequal(Op<T>::diam(MC2._I),0.)){
    McCormick<T> MC3;
    MC3._sub(MC1._nsub, MC1._const || MC2._const);
    MC3._I = Op<T>::expx_times_y( MC1._I, MC2._I );

    //convex envelope
    //the convex envelope is monotonically increasing in every positive direction, i.e., directions pointing away from the corner (MC1._l,MC2._l). Thus we know that we have to use MC1._cv and MC2._cv
    {
      double alpha = std::log(Op<T>::u(MC2._I)/Op<T>::l(MC2._I));
      double lambda1 = (Op<T>::u(MC2._I)-MC2._cv)/(Op<T>::diam(MC2._I));
      double lambda2 = (MC2._cv-Op<T>::l(MC2._I))/(Op<T>::diam(MC2._I));

      if((Op<T>::l(MC1._I)+lambda1*alpha <= MC1._cv) && (MC1._cv <= Op<T>::u(MC1._I)-lambda2*alpha)){
		//convex relaxation
		MC3._cv = std::exp(MC1._cv)*std::pow(Op<T>::l(MC2._I),lambda1)*std::pow(Op<T>::u(MC2._I),lambda2);
		//subgradient
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  MC3._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*( std::pow(Op<T>::l(MC2._I),lambda1)*std::exp(MC1._cv)/std::pow(Op<T>::u(MC2._I),-lambda2) )
						 +(MC2._const? 0.:MC2._cvsub[i])*( std::pow(Op<T>::l(MC2._I),lambda1)*std::exp(MC1._cv)*(std::log(Op<T>::u(MC2._I))-std::log(Op<T>::l(MC2._I)))
								/(std::pow(Op<T>::u(MC2._I),-lambda2 )*Op<T>::diam(MC2._I) ) ) ;
		  }
      }
      else if(MC1._cv < lambda1*std::min(Op<T>::l(MC1._I)+alpha,Op<T>::u(MC1._I)) + lambda2*Op<T>::l(MC1._I) ){ //it always holds MC1._l <= MC1._cv
		//convex relaxation
		MC3._cv = lambda1*std::exp( (MC1._cv - lambda2*Op<T>::l(MC1._I))/lambda1 )*Op<T>::l(MC2._I) + lambda2*std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I);
		//subgradient
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  double s1 = std::exp( (MC1._cv - Op<T>::l(MC1._I)*lambda2 )*1./lambda1  );
		  MC3._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*( Op<T>::l(MC2._I)*s1 )
				+(MC2._const? 0.:MC2._cvsub[i])*( Op<T>::l(MC2._I)*s1/(-Op<T>::diam(MC2._I)) + Op<T>::u(MC2._I)*std::exp(Op<T>::l(MC1._I))/(Op<T>::diam(MC2._I))
								  + Op<T>::l(MC2._I)*s1*(MC1._cv-Op<T>::l(MC1._I))/(Op<T>::u(MC2._I)-MC2._cv)  ) ;
		}
      }
      else{
		  //convex relaxation
		  MC3._cv = lambda1*std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) + lambda2*std::exp( (MC1._cv - lambda1*Op<T>::u(MC1._I))/lambda2 )*Op<T>::u(MC2._I);
		  //subgradient
		  for( unsigned int i=0; i<MC3._nsub; i++ ){
			double s1 = std::exp( (MC1._cv - Op<T>::u(MC1._I)*lambda1 )*1./lambda2  );
			MC3._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*( Op<T>::u(MC2._I)*s1 )
						   +(MC2._const? 0.:MC2._cvsub[i])*( Op<T>::u(MC2._I)*s1/(Op<T>::diam(MC2._I)) - Op<T>::l(MC2._I)*std::exp(Op<T>::u(MC1._I))/(Op<T>::diam(MC2._I))
								  + Op<T>::u(MC2._I)*s1*(Op<T>::u(MC1._I)-MC1._cv)/(MC2._cv-Op<T>::l(MC2._I))  ) ;
		  }
		}
    }//end of convex
    //concave envelope
    {
	int imidcc1 = -1, imidcc2 = -1; // we can't know which relaxation to use
    double fmidcc1 = mid_ndiff(MC1._cv, MC1._cc, Op<T>::u(MC1._I), imidcc1);
    double fmidcc2 = mid_ndiff(MC2._cv, MC2._cc, Op<T>::u(MC2._I), imidcc2);
	double l1 = std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I);
	double l2 = std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I);
	double r11 =0., r12 = 0., r21 = 0., r22 = 0.;
	if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
	    r11 = (std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC1._I));
	    l1 += (fmidcc1-Op<T>::u(MC1._I))*r11;
	    r12 = (std::exp(Op<T>::u(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I) )/(Op<T>::diam(MC1._I));
	    l2 += (fmidcc1-Op<T>::l(MC1._I))*r12;
	}
	if(!isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
	    r21 = (std::exp(Op<T>::u(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::u(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC2._I));
	    l1 += (fmidcc2-Op<T>::l(MC2._I))*r21;
	    r22 = (std::exp(Op<T>::l(MC1._I))*Op<T>::u(MC2._I) - std::exp(Op<T>::l(MC1._I))*Op<T>::l(MC2._I) )/(Op<T>::diam(MC2._I));
	    l2 += (fmidcc2-Op<T>::u(MC2._I))*r22;
	}

	double lambda1,lambda2;
	if(isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
		if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
			MC3._cc = mc::expx_times_y(Op<T>::u(MC1._I),Op<T>::u(MC2._I));
			lambda1 = 0.;
			lambda2 = 0.;
		}
		else{
			MC3._cc = l1;
			lambda1 = 1.;
			lambda2 = 0.;
		}
	}
	else if(isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) )){
		MC3._cc = l2;
		lambda1 = 1.;
		lambda2 = 0.;
	}
	else {
		MC3._cc = std::min(l1,l2);
		if(isequal(l1,l2)){
		  lambda1 = 0.5;
		  lambda2 = 0.5;
		}
		else if( l1<l2 ){
		  lambda1 = 1.;
		  lambda2 = 0.;
		}
		else{
		  lambda1 = 0.;
		  lambda2 = 1.;
		}
	}

	for( unsigned int i=0; i<MC3._nsub; i++ ){
	  MC3._ccsub[i] = lambda1*(r11*(MC1._const? 0.:MC1._ccsub[i])+r21*(MC2._const? 0.:MC2._ccsub[i]))
			         +lambda2*(r12*(MC1._const? 0.:MC1._ccsub[i])+r22*(MC2._const? 0.:MC2._ccsub[i]));
	}
     }//end of concave
#ifdef MC__MCCORMICK_DEBUG

  	std::string str = "expx_times_y";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);

#endif
    if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
    return MC3.cut();
  }//end of if( Op<T>::l(MC2._I) > 0. && !isequal(Op<T>::diam(MC2._I),0.))

  return exp(MC1)*MC2;
}


//added AVT.SVT 22.08.2017
template <typename T> inline McCormick<T>
vapor_pressure
(const McCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4 = 0,
 const double p5 = 0, const double p6 = 0, const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::VAPOR_PRESSURE );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );
    MC2._I = Op<T>::vapor_pressure( MC1._I, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);

	MC2._cv =  mc::vapor_pressure(MC1._cv, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
	double d = mc::der_vapor_pressure(MC1._cv, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
    for( unsigned int i=0; i<MC2._nsub; i++ )
    {
      MC2._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*d;
    }

	if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
		double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
		MC2._cc =  Op<T>::l(MC2._I)+r*(MC1._cc-Op<T>::l(MC1._I));
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
		}
	} else {
		MC2._cc = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[i] = 0.;
		}
    }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "vapor_pressure";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

    return MC2.cut();
}

//added AVT.SVT 01.09.2017
template <typename T> inline McCormick<T>
ideal_gas_enthalpy
(const McCormick<T>&MC1, const double x0, const double type, const double p1, const double p2, const double p3, const double p4,
 const double p5, const double p6 = 0, const double p7 = 0 )
{
    if (( Op<T>::l(MC1._I) <= 0. ) || ( x0 <= 0 ))
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::IDEAL_GAS_ENTHALPY );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );
    MC2._I = Op<T>::ideal_gas_enthalpy( MC1._I, x0, type, p1, p2, p3, p4, p5, p6, p7);

    MC2._cv =  mc::ideal_gas_enthalpy(MC1._cv, x0, type, p1, p2, p3, p4, p5, p6, p7);
	double d = mc::der_ideal_gas_enthalpy(MC1._cv, x0, type, p1, p2, p3, p4, p5, p6, p7);
	for( unsigned int i=0; i<MC2._nsub; i++ )
    {
      MC2._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*d;
    }

	if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
		double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
		MC2._cc =  Op<T>::l(MC2._I)+r*(MC1._cc-Op<T>::l(MC1._I));
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
		}
	} else {
		MC2._cc = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[i] = 0.;
		}
	}
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "ideal_gas_enthalpy";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

    return MC2.cut();
}

//added AVT.SVT 01.09.2017
template <typename T> inline McCormick<T>
saturation_temperature
(const McCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4 = 0,
 const double p5 = 0, const double p6 = 0, const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SATURATION_TEMPERATURE );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );
    MC2._I = Op<T>::saturation_temperature( MC1._I, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);

	if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
		double r = Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
		MC2._cv =  Op<T>::l(MC2._I)+r*(MC1._cv-Op<T>::l(MC1._I));
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._cvsub[i] =  (MC1._const? 0.:MC1._cvsub[i])*r;
		}
	} else {
		MC2._cv = Op<T>::l(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._cvsub[i] = 0.;
		}
    }

	MC2._cc =  mc::saturation_temperature(MC1._cc, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
	double d = mc::der_saturation_temperature(MC1._cc, type, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
    for( unsigned int i=0; i<MC2._nsub; i++ )
    {
      MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*d;
    }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "saturation_temperature";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

    return MC2.cut();
}


//added AVT.SVT 22.08.2017
template <typename T> inline McCormick<T>
enthalpy_of_vaporization
(const McCormick<T>&MC1, const double type, const double p1, const double p2, const double p3, const double p4,
 const double p5, const double p6 = 0 )
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::ENTHALPY_OF_VAPORIZATION );

	McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );

	if ( Op<T>::l(MC1._I) >= p1 ) {	// completely in supercritical region, everything is zero

		MC2._I = T(0.0);

		MC2._cv = 0;
		MC2._cc = 0;

		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._cvsub[i] = 0.;
		  MC2._ccsub[i] = 0.;
		}

	}
	else if ( Op<T>::u(MC1._I) <= p1 ) {	// completely in subcritical region, use regular correlation

		MC2._I = Op<T>::enthalpy_of_vaporization( MC1._I, type, p1, p2, p3, p4, p5, p6);

		MC2._cc =  mc::enthalpy_of_vaporization(MC1._cv, type, p1, p2, p3, p4, p5, p6);
		double d = mc::der_enthalpy_of_vaporization(MC1._cv, type, p1, p2, p3, p4, p5, p6);
		for( unsigned int i=0; i<MC2._nsub; i++ )
		{
		  MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*d;
		}

		if (!isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )) {
			double r = -Op<T>::diam(MC2._I)/Op<T>::diam(MC1._I);
			MC2._cv =  Op<T>::u(MC2._I)+r*(MC1._cc-Op<T>::l(MC1._I));
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
			}
		} else {
			MC2._cv = Op<T>::l(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[i] = 0.;
			}
		}

	}
	else {  // temperature interval contains critical point


		MC2._I = Op<T>::enthalpy_of_vaporization( MC1._I, type, p1, p2, p3, p4, p5, p6);

		// Convex relaxation
		if ( MC1._cc >= p1 ) {
			MC2._cv = 0.;
			for( unsigned int i=0; i<MC2._nsub; i++ )
			{
			  MC2._cvsub[i] = 0.;
			}
		} else {
			if (!isequal( Op<T>::l(MC1._I), p1 )) {
				double r = (0.-Op<T>::u(MC2._I))/(p1-Op<T>::l(MC1._I));
				MC2._cv =  Op<T>::u(MC2._I)+r*(MC1._cc-Op<T>::l(MC1._I));
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._cvsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
				}
			} else {
				MC2._cv = Op<T>::l(MC2._I);
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._cvsub[i] = 0.;
				}
			}
		}

		// Concave relaxation
		if( !McCormick<T>::options.ENVEL_USE ){

			// simply use interval bounds
			MC2._cc = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._ccsub[i] = 0.;
			}

		}
		else {

			double xj;
			double xu = Op<T>::u(MC1._I);
			std::vector<double> parameters = {p1,p2,p3,p4,p5,p6};
			int dummy = (int)type;
			int * ptr = &dummy;
			try{
				xj = McCormick<T>::_secant( Op<T>::l(MC1._I), (Op<T>::l(MC1._I)+p1)/2, Op<T>::l(MC1._I), p5 , McCormick<T>::_dhvapenv_func, &xu, ptr, parameters );
			}
			catch( typename McCormick<T>::Exceptions ){
				xj = McCormick<T>::_goldsect( Op<T>::l(MC1._I), p1 , McCormick<T>::_dhvapenv_func, &xu, ptr, parameters );
			}
			if (MC1._cv <= xj) {
				MC2._cc =  mc::enthalpy_of_vaporization(MC1._cv, type, p1, p2, p3, p4, p5, p6);
				double d = mc::der_enthalpy_of_vaporization(MC1._cv, type, p1, p2, p3, p4, p5, p6);
				for( unsigned int i=0; i<MC2._nsub; i++ )
				{
				  MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*d;
				}
			} else {
				if (!isequal(xu,xj)) {
					double fxj = mc::enthalpy_of_vaporization(xj, type, p1, p2, p3, p4, p5, p6);
					double r = (0.-fxj)/(xu-xj);
					MC2._cc =  fxj + r*(MC1._cv-xj);
					for( unsigned int i=0; i<MC2._nsub; i++ )
					{
					  MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
					}
				} else {
					// simply use interval bounds
					MC2._cc = Op<T>::u(MC2._I);
					for( unsigned int i=0; i<MC2._nsub; i++ ){
						MC2._ccsub[i] = 0.;
					}
				}
			}

		}

	}

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "enthalpy_of_vaporization";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

    return MC2.cut();
}


//added AVT.SVT 06.11.2017
template <typename T> inline McCormick<T>
cost_function
(const McCormick<T>&MC1, const double type, const double p1, const double p2, const double p3 )
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::COST_FUNCTION );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );

	switch((int)type){ // for Guthrie, we have specific calculations
		case 1:{ // Guthrie
			// Note that we can't precompute the values since most of them depend on interval bounds
			unsigned int monotonicity = McCormick<T>::MON_NONE;
			unsigned int convexity = McCormick<T>::CONV_NONE;
			//sets monotonicity and convexity
			if(McCormick<T>::options.ENVEL_USE){
				McCormick<T>::_cost_Guthrie_mon_conv(monotonicity,convexity,p1,p2,p3,Op<T>::l(MC1._I),Op<T>::u(MC1._I));
			}else{
				return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
			}

			switch(monotonicity){
			  case McCormick<T>::MON_NONE:	//not monotonic at all in the given interval but we can still compute exact range bounds
			  {
			    double min_point = 0.;
				double max_point = 0.;
				double extreme_point = 0.;

				if(p3>0.){
					extreme_point = std::exp(-p2*std::log(10.)/(2*p3));
					MC2._I = T(mc::cost_function(extreme_point,type,p1,p2,p3),std::max(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)));
					min_point = extreme_point;
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)>mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						max_point = Op<T>::l(MC1._I);
					}else{
						max_point = Op<T>::u(MC1._I);
					}
				}
				else if (p3<0.){
					extreme_point = std::exp(-p2*std::log(10.)/(2*p3));
					MC2._I = T(std::min(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)),mc::cost_function(extreme_point,type,p1,p2,p3));
					max_point = extreme_point;
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)<mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						min_point = Op<T>::l(MC1._I);
					}else{
						min_point = Op<T>::u(MC1._I);
					}
				}
				else { // case p3 = 0
					if(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3)>mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)){
						max_point = Op<T>::l(MC1._I);
						min_point = Op<T>::u(MC1._I);
					}else{
						max_point = Op<T>::u(MC1._I);
						min_point = Op<T>::l(MC1._I);
					}

					MC2._I = T(mc::cost_function(min_point,type,p1,p2,p3),mc::cost_function(max_point,type,p1,p2,p3));
				}
				switch(convexity){
					case McCormick<T>::CONV_NONE: //not convex not concave
					{
					    if(p3>0.){
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         mc::cost_function(extreme_point,type,p1,p2,p3),std::max(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)) );
						}
						else if (p3 < 0.){
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         std::min(mc::cost_function(Op<T>::l(MC1._I),type,p1,p2,p3),mc::cost_function(Op<T>::u(MC1._I),type,p1,p2,p3)),mc::cost_function(extreme_point,type,p1,p2,p3) );
						}
						else{
							return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.))),
							                         mc::cost_function(min_point,type,p1,p2,p3),mc::cost_function(max_point,type,p1,p2,p3) );
						}
						break;
					case McCormick<T>::CONVEX: //convex
						{   {int imid = -1;
								MC2._cv = mc::cost_function( mid( MC1._cv, MC1._cc, min_point, imid ),type,p1,p2,p3);
								for( unsigned int i=0; i<MC2._nsub; i++ ){
								  MC2._cvsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * mc::der_cost_function(mid( MC1._cv, MC1._cc, min_point, imid ),type,p1,p2,p3);
								}
							}
							{int imid = -1;
								double r = 0.;
								if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
									r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
								}
								MC2._cc =  mc::cost_function(max_point, type,p1,p2,p3)+r*( mid( MC1._cv, MC1._cc, max_point, imid )-max_point);
								for( unsigned int i=0; i<MC2._nsub; i++ ){
									MC2._ccsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
								}
							}
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function not monotonic convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
						}

						return MC2.cut();
						break;
					}
					case McCormick<T>::CONCAVE: //concave
					{       {int imid = -1;
								MC2._cc = mc::cost_function( mid( MC1._cv, MC1._cc, max_point, imid ),type,p1,p2,p3);
								for( unsigned int i=0; i<MC2._nsub; i++ ){
								  MC2._ccsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * mc::der_cost_function(mid( MC1._cv, MC1._cc, max_point, imid ),type,p1,p2,p3);
								}
							}
							{int imid = -1;
								double r = 0.;
								if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
									r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
								}
								MC2._cv =  mc::cost_function(min_point, type,p1,p2,p3)+r*( mid( MC1._cv, MC1._cc, min_point, imid )-min_point);
								for( unsigned int i=0; i<MC2._nsub; i++ ){
									MC2._cvsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
								}
							}
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function not monotonic concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
						return MC2.cut();
						break;
					}
					default:
						return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
						break;
				}//end of switch(convexity) in case 0 of switch(monotonicity)
				break;
			  }//end of case 0
			  case McCormick<T>::MON_INCR: // increasing
			  {
				MC2._I = T(mc::cost_function( Op<T>::l(MC1._I), type,p1,p2,p3),mc::cost_function( Op<T>::u(MC1._I), type,p1,p2,p3));
				switch(convexity){
					case McCormick<T>::CONVEX: //convex
						{
						  MC2._cv =  mc::cost_function(MC1._cv, type,p1,p2,p3);
						  double r = 0.;
						  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
							r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						  }
						  MC2._cc =  mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)+r*(MC1._cc-Op<T>::u(MC1._I));

						  for( unsigned int i=0; i<MC2._nsub; i++ )
						  {
							MC2._cvsub[i] = mc::der_cost_function(MC1._cv, type,p1,p2,p3)*(MC1._const? 0.:MC1._cvsub[i]);
							MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
						  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function increasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

						  return MC2.cut();
						}
						break;
					case McCormick<T>::CONCAVE: //concave
						{
						  MC2._cc =  mc::cost_function(MC1._cc, type,p1,p2,p3);
						  double r = 0.;
						  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						  }
						  MC2._cv =  mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3)+r*(MC1._cv-Op<T>::l(MC1._I));

						  for( unsigned int i=0; i<MC2._nsub; i++ )
						  {
							MC2._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
							MC2._ccsub[i] = mc::der_cost_function(MC1._cc, type,p1,p2,p3)*(MC1._const? 0.:MC1._ccsub[i]);
						  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function increasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

						  return MC2.cut();
						}
						break;
					case McCormick<T>::CONV_NONE: //not convex not concave
					default:
						return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) ),Op<T>::l(MC2._I),Op<T>::u(MC2._I));
						break;
				}//end of switch(convexity)	in case 1 of switch(monotonicity)
			  }//end of case 1
			  case McCormick<T>::MON_DECR: //decreasing
			  {
				MC2._I = T(mc::cost_function( Op<T>::u(MC1._I), type,p1,p2,p3),mc::cost_function( Op<T>::l(MC1._I), type,p1,p2,p3));
				switch(convexity){
					case McCormick<T>::CONVEX: //convex
						{
						  MC2._cv =  mc::cost_function(MC1._cc, type,p1,p2,p3);
						  double r = 0.;
						  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						  }
						  MC2._cc =  mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3)+r*(MC1._cv-Op<T>::l(MC1._I));

						  for( unsigned int i=0; i<MC2._nsub; i++ )
						  {
							MC2._cvsub[i] = mc::der_cost_function(MC1._cc, type,p1,p2,p3)*(MC1._const? 0.:MC1._ccsub[i]);
							MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
						  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function decreasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
						  return MC2.cut();
						}
						break;
					case McCormick<T>::CONCAVE: //concave
						{
						  MC2._cc =  mc::cost_function(MC1._cv, type,p1,p2,p3);
						  double r = 0.;
						  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						   r = (mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)-mc::cost_function(Op<T>::l(MC1._I), type,p1,p2,p3))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
						  }
						  MC2._cv =  mc::cost_function(Op<T>::u(MC1._I), type,p1,p2,p3)+r*(MC1._cc-Op<T>::u(MC1._I));

						  for( unsigned int i=0; i<MC2._nsub; i++ )
						  {
							MC2._cvsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
							MC2._ccsub[i] = mc::der_cost_function(MC1._cv, type,p1,p2,p3)*(MC1._const? 0.:MC1._cvsub[i]);
						  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "cost_function decreasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
						  return MC2.cut();
						}
						break;
					case McCormick<T>::CONV_NONE: //not convex not concave
					default:
						return bounding_func(pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) ),Op<T>::l(MC2._I),Op<T>::u(MC2._I));
						break;
				}//end of switch(convexity)	in case 2 of switch(monotonicity)
				default:
				//throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::COST_FUNCTION_MON );
				return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
				break;
			  }//end of case 2
			}//end of switch(monotonicity)
		}//end of function type case 1 Guthrie
			break;
		default: // if something went wrong, just return the Guthrie formula
			return pow(10., p1 + log(MC1)/std::log(10.) * (p2 + p3*log(MC1)/std::log(10.)) );
			break;
    }//end of switch type
}


//added AVT.SVT 23.11.2017
template <typename T> inline McCormick<T>
nrtl_tau
(const McCormick<T>&MC1, const double a, const double b, const double e, const double f)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::NRTL_TAU );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );

	//First, check monotonicity
	unsigned int monotonicity = McCormick<T>::MON_NONE; //0 means it is neither incr nor decr, 1 is incr, 2 is decr
	unsigned int convexity = McCormick<T>::CONV_NONE; //0 means it is neither convex nor concave, 1 is convex, 2 is concave
	double new_l = 1e51,new_u = -1e51,zmin = 0.,zmax = 1e51; //needed in the case of non-monotonicity
	if(McCormick<T>::options.ENVEL_USE){
		McCormick<T>::_nrtl_tau_mon_conv(monotonicity, convexity, a, b, e, f, Op<T>::l(MC1._I), Op<T>::u(MC1._I), new_l, new_u, zmin, zmax); //sets monotonicity and convexity parameters
	}else{
		return a + b/MC1 + e * log(MC1) + f*MC1;
	}

	switch(monotonicity){
	  case McCormick<T>::MON_INCR:// increasing
	  {
		MC2._I = T(mc::nrtl_tau( Op<T>::l(MC1._I), a, b, e, f),mc::nrtl_tau( Op<T>::u(MC1._I), a, b, e, f));
		switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
				  MC2._cv =  mc::nrtl_tau(MC1._cv, a, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cc =  mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)+r*(MC1._cc-Op<T>::u(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = mc::nrtl_dtau(MC1._cv, b, e, f)*(MC1._const? 0.:MC1._cvsub[i]);
					MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau increasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
				  MC2._cc =  mc::nrtl_tau(MC1._cc, a, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				    r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cv =  mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f)+r*(MC1._cv-Op<T>::l(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
					MC2._ccsub[i] = mc::nrtl_dtau(MC1._cc, b, e, f)*(MC1._const? 0.:MC1._ccsub[i]);
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau increasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 1 of switch(monotonicity)
	  }//end of case 1
	  case McCormick<T>::MON_DECR ://decreasing
	  {
		MC2._I = T(mc::nrtl_tau( Op<T>::u(MC1._I), a, b, e, f),mc::nrtl_tau( Op<T>::l(MC1._I), a, b, e, f));
		switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
				  MC2._cv =  mc::nrtl_tau(MC1._cc, a, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				   r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cc =  mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f)+r*(MC1._cv-Op<T>::l(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = mc::nrtl_dtau(MC1._cc, b, e, f)*(MC1._const? 0.:MC1._ccsub[i]);
					MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau decreasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
				  MC2._cc =  mc::nrtl_tau(MC1._cv, a, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				   r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cv =  mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)+r*(MC1._cc-Op<T>::u(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
					MC2._ccsub[i] = mc::nrtl_dtau(MC1._cv, b, e, f)*(MC1._const? 0.:MC1._cvsub[i]);
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau decreasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE : //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 2 of switch(monotonicity)
	  }//end of case 2
	  case McCormick<T>::MON_NONE:	//not monotonic at all in the given interval
	  {
		  MC2._I = T(new_l,new_u);
		  switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
					//convex
				  { int imid = -1;
					double vmid = mid( MC1._cv, MC1._cc, zmin, imid );
					MC2._cv = mc::nrtl_tau( vmid, a, b, e, f );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._cvsub[i] = mc::nrtl_dtau(vmid, b, e, f) * mid( MC1._cvsub, MC1._ccsub, i, imid );
				  }
					//concave
				  { int imid = -1;
					double r = 0.;
					if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
					}
					MC2._cc = mc::nrtl_tau(zmax, a, b, e, f) + r * ( mid( MC1._cv, MC1._cc, zmax, imid ) - zmax );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._ccsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
				  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau not monotonic convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
					//convex
				 { int imid = -1;
					double r = 0.;
					if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						r = (mc::nrtl_tau(Op<T>::u(MC1._I), a, b, e, f)-mc::nrtl_tau(Op<T>::l(MC1._I), a, b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
					}
					MC2._cv = mc::nrtl_tau(zmin, a, b, e, f) + r * ( mid( MC1._cv, MC1._cc, zmin, imid ) - zmin );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._cvsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
				  }
					//concave
				 { int imid = -1;
					double vmid = mid( MC1._cv, MC1._cc, zmax, imid );
					MC2._cc = mc::nrtl_tau( vmid, a, b, e, f );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._ccsub[i] = mc::nrtl_dtau(vmid, b, e, f) * mid( MC1._cvsub, MC1._ccsub, i, imid );
				  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_tau not monotonic concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func(a + b/MC1 + e * log(MC1) + f*MC1,Op<T>::l(MC2._I),Op<T>::u(MC2._I));
				break;
		  }
	  }
	  default:
		return a + b/MC1 + e * log(MC1) + f*MC1;
		break;

	}//end of switch(monotonicity)

}


//added AVT.SVT 10.01.2019
template <typename T> inline McCormick<T>
nrtl_dtau
(const McCormick<T>&MC1, const double b, const double e, const double f)
{
    if ( Op<T>::l(MC1._I) <= 0. )
      throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::NRTL_DTAU );

    McCormick<T> MC2;
    MC2._sub( MC1._nsub, MC1._const );

	//First, check monotonicity
	unsigned int monotonicity = McCormick<T>::MON_NONE; //0 means it is neither incr nor decr, 1 is incr, 2 is decr
	unsigned int convexity = McCormick<T>::CONV_NONE; //0 means it is neither convex nor concave, 1 is convex, 2 is concave
	double new_l = 1e51,new_u = -1e51,zmin = 0.,zmax = 1e51; //needed in the case of non-monotonicity
	if(McCormick<T>::options.ENVEL_USE){
		McCormick<T>::_nrtl_der_tau_mon_conv(monotonicity, convexity, b, e, f, Op<T>::l(MC1._I), Op<T>::u(MC1._I), new_l, new_u, zmin, zmax); //sets monotonicity and convexity parameters
	}else{
		return -b/sqr(MC1) + e/MC1 + f;
	}

	switch(monotonicity){
	  case McCormick<T>::MON_INCR:// increasing
	  {
		MC2._I = T(mc::nrtl_dtau( Op<T>::l(MC1._I), b, e, f),mc::nrtl_dtau( Op<T>::u(MC1._I), b, e, f));
		switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
				  MC2._cv =  mc::nrtl_dtau(MC1._cv, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
					r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cc =  mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)+r*(MC1._cc-Op<T>::u(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = mc::der2_nrtl_tau(MC1._cv, b, e)*(MC1._const? 0.:MC1._cvsub[i]);
					MC2._ccsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau increasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
				  MC2._cc =  mc::nrtl_dtau(MC1._cc, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				    r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cv =  mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f)+r*(MC1._cv-Op<T>::l(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
					MC2._ccsub[i] = mc::der2_nrtl_tau(MC1._cc, b, e)*(MC1._const? 0.:MC1._ccsub[i]);
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau increasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func( -b/sqr(MC1) + e/MC1 + f, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 1 of switch(monotonicity)
	  }//end of case 1
	  case McCormick<T>::MON_DECR ://decreasing
	  {
		MC2._I = T(mc::nrtl_dtau( Op<T>::u(MC1._I), b, e, f),mc::nrtl_dtau( Op<T>::l(MC1._I), b, e, f));
		switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
				  MC2._cv =  mc::nrtl_dtau(MC1._cc, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				   r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cc =  mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f)+r*(MC1._cv-Op<T>::l(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = mc::der2_nrtl_tau(MC1._cc, b, e)*(MC1._const? 0.:MC1._ccsub[i]);
					MC2._ccsub[i] = (MC1._const? 0.:MC1._cvsub[i])*r;
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau decreasing convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
				  MC2._cc =  mc::nrtl_dtau(MC1._cv, b, e, f);
				  double r = 0.;
				  if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
				   r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
				  }
				  MC2._cv =  mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)+r*(MC1._cc-Op<T>::u(MC1._I));

				  for( unsigned int i=0; i<MC2._nsub; i++ )
				  {
					MC2._cvsub[i] = (MC1._const? 0.:MC1._ccsub[i])*r;
					MC2._ccsub[i] = mc::der2_nrtl_tau(MC1._cv, b, e)*(MC1._const? 0.:MC1._cvsub[i]);
				  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau decreasing concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE : //not convex not concave
			default:
				return bounding_func(-b/sqr(MC1) + e/MC1 + f, Op<T>::l(MC2._I), Op<T>::u(MC2._I));
				break;
		}//end of switch(convexity)	in case 2 of switch(monotonicity)
	  }//end of case 2
	  case McCormick<T>::MON_NONE:	//not monotonic at all in the given interval
	  {
		  MC2._I = T(new_l,new_u);
		  switch(convexity){
			case McCormick<T>::CONVEX ://convex
				{
					//convex
				  { int imid = -1;
					double vmid = mid( MC1._cv, MC1._cc, zmin, imid );
					MC2._cv = mc::nrtl_dtau( vmid, b, e, f );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._cvsub[i] = mc::der2_nrtl_tau(vmid, b, e) * mid( MC1._cvsub, MC1._ccsub, i, imid );
				  }
					//concave
				  { int imid = -1;
					double r = 0.;
					if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
					}
					MC2._cc = mc::nrtl_dtau(zmax, b, e, f) + r * ( mid( MC1._cv, MC1._cc, zmax, imid ) - zmax );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._ccsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
				  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau not monotonic convex";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif
				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONCAVE ://concave
				{
					//convex
				 { int imid = -1;
					double r = 0.;
					if( !isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) )){
						r = (mc::nrtl_dtau(Op<T>::u(MC1._I), b, e, f)-mc::nrtl_dtau(Op<T>::l(MC1._I), b, e, f))/(Op<T>::u(MC1._I)-Op<T>::l(MC1._I));
					}
					MC2._cv = mc::nrtl_dtau(zmin, b, e, f) + r * ( mid( MC1._cv, MC1._cc, zmin, imid ) - zmin );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._cvsub[i] = mid( MC1._cvsub, MC1._ccsub, i, imid ) * r;
				  }
					//concave
				 { int imid = -1;
					double vmid = mid( MC1._cv, MC1._cc, zmax, imid );
					MC2._cc = mc::nrtl_dtau( vmid, b, e, f );
					for( unsigned int i=0; i<MC2._nsub; i++ )
					  MC2._ccsub[i] = mc::der2_nrtl_tau(vmid, b, e) * mid( MC1._cvsub, MC1._ccsub, i, imid );
				  }
#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "nrtl_dtau not monotonic concave";
	McCormick<T>::_debug_check(MC1, MC2, str);
#endif

				  return MC2.cut();
				}
				break;
			case McCormick<T>::CONV_NONE: //not convex not concave
			default:
				return bounding_func( -b/sqr(MC1) + e/MC1 + f ,Op<T>::l(MC2._I),Op<T>::u(MC2._I));
				break;
		  }
	  }
	  default:
		return -b/sqr(MC1) + e/MC1 + f;
		break;

	}//end of switch(monotonicity)
}


//added AVT.SVT 23.11.2017
template <typename T> inline McCormick<T>
nrtl_G
(const McCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha )
{
  if ( Op<T>::l(MC1._I) <= 0. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::NRTL_G );

  return exp(-alpha*nrtl_tau(MC1,a,b,e,f));
}


//added AVT.SVT 01.03.2018
template <typename T> inline McCormick<T>
nrtl_Gtau
( const McCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::NRTL_GTAU);

  return xexpax(nrtl_tau(MC1,a,b,e,f),-alpha);
}


//added AVT.SVT 22.03.2018
template <typename T> inline McCormick<T>
nrtl_Gdtau
( const McCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::NRTL_GDTAU);

  return nrtl_G(MC1,a,b,e,f,alpha)*nrtl_dtau(MC1,b,e,f);

}


//added AVT.SVT 22.03.2018
template <typename T> inline McCormick<T>
nrtl_dGtau
( const McCormick<T>&MC1, const double a, const double b, const double e, const double f, const double alpha)
{
  if( Op<T>::l(MC1._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::NRTL_DGTAU);

  return -alpha*nrtl_Gtau(MC1,a,b,e,f,alpha)*nrtl_dtau(MC1,b,e,f);

}


//added AVT.SVT J. Luethe, A. Schweidtmann, W. Huster 18.12.2017
template <typename T> inline McCormick<T>
p_sat_ethanol_schroeder( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub(  MC._nsub, MC._const );
  MC2._I = Op<T>::p_sat_ethanol_schroeder( MC._I );

  if( Op<T>::l(MC._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::P_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::u(MC._I) > _T_c_K){
	const double _N_Tsat_1 = -8.94161;
	const double _N_Tsat_2 = 1.61761;
	const double _N_Tsat_3 = -51.1428;
	const double _N_Tsat_4 = 53.1360;
	const double _k_Tsat_1 = 1.0;
	const double _k_Tsat_2 = 1.5;
	const double _k_Tsat_3 = 3.4;
	const double _k_Tsat_4 = 3.7;
	const double _p_c = 62.68;

	return _p_c*(exp(_T_c_K/MC*(_N_Tsat_1*pow((1-MC/_T_c_K),_k_Tsat_1) + _N_Tsat_2*pow((1-MC/_T_c_K),_k_Tsat_2) + _N_Tsat_3*pow((1-MC/_T_c_K),_k_Tsat_3) + _N_Tsat_4*pow((1-MC/_T_c_K),_k_Tsat_4))));
  }

  bool opt_test = McCormick<T>::options.ENVEL_USE;

  if( !McCormick<T>::options.ENVEL_USE ){
      MC2._cv = Op<T>::l(MC2._I);
      MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }

    return MC2.cut();
  }

  { int imid = -1;
    MC2._cv = mc::p_sat_ethanol_schroeder(mid( MC._cv,MC._cc, Op<T>::l(MC._I), imid ));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_p_sat_ethanol_schroeder(mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ));
    }
  }
  { int imid = -1;

    double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::p_sat_ethanol_schroeder(Op<T>::u(MC._I))-mc::p_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
    MC2._cc = mc::p_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid )-Op<T>::u(MC._I));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
  }
#ifdef MC__MCCORMICK_DEBUG
    std::string str = "p_sat_ethanol_schroeder";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}


template <typename T> inline McCormick<T>
rho_vap_sat_ethanol_schroeder( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub(  MC._nsub, MC._const );
  MC2._I = Op<T>::rho_vap_sat_ethanol_schroeder( MC._I );

  if( Op<T>::l(MC._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::RHO_VAP_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::u(MC._I) > _T_c_K){
	const double _N_vap_1 = -1.75362;
	const double _N_vap_2 = -10.5323;
	const double _N_vap_3 = -37.6407;
	const double _N_vap_4 = -129.762;
	const double _k_vap_1 = 0.21;
	const double _k_vap_2 = 1.1;
	const double _k_vap_3 = 3.4;
	const double _k_vap_4 = 10;
	const double _rho_c = 273.195;

	return _rho_c*(exp(_N_vap_1*pow((1 - MC/_T_c_K),_k_vap_1) + _N_vap_2*pow((1 - MC/_T_c_K),_k_vap_2) + _N_vap_3*pow((1 - MC/_T_c_K),_k_vap_3) + _N_vap_4*pow((1 - MC/_T_c_K),_k_vap_4)));
 }

  bool opt_test = McCormick<T>::options.ENVEL_USE;

  if( !McCormick<T>::options.ENVEL_USE ){
	  //not sure if this is right, took it from atan
      MC2._cv = Op<T>::l(MC2._I);
      MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }

    return MC2;
  }

  { int imid = -1;
    MC2._cv = mc::rho_vap_sat_ethanol_schroeder(mid( MC._cv,MC._cc, Op<T>::l(MC._I), imid ));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_rho_vap_sat_ethanol_schroeder(mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ));
    }
  }
  { int imid = -1;

    double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:( mc::rho_vap_sat_ethanol_schroeder(Op<T>::u(MC._I))- mc::rho_vap_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
    MC2._cc = mc::rho_vap_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid )-Op<T>::u(MC._I));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
  }
#ifdef MC__MCCORMICK_DEBUG
    std::string str = "rho_vap_sat_ethanol_schroeder";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}


// Only for T > 290.3
template <typename T> inline McCormick<T>
rho_liq_sat_ethanol_schroeder( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub(  MC._nsub, MC._const );
  MC2._I = Op<T>::rho_liq_sat_ethanol_schroeder( MC._I );

  bool opt_test = McCormick<T>::options.ENVEL_USE;

  if( Op<T>::l(MC._I)<= 0.)
    throw typename McCormick<T>::Exceptions(McCormick<T>::Exceptions::RHO_LIQ_SAT_ETHANOL_SCHROEDER);

  const double _T_c_K = 514.71;
  if( Op<T>::l(MC._I) <= 290.3 || Op<T>::u(MC._I) > _T_c_K){
	const double _N_liq_1=9.00921;
	const double _N_liq_2=-23.1668;
	const double _N_liq_3=30.9092;
	const double _N_liq_4=-16.5459;
	const double _N_liq_5=3.64294;
	const double _k_liq_1=0.5;
	const double _k_liq_2=0.8;
	const double _k_liq_3=1.1;
	const double _k_liq_4=1.5;
	const double _k_liq_5=3.3;
	const double _rho_c = 273.195;

	return _rho_c*(1 + _N_liq_1*pow((1 - MC/_T_c_K),_k_liq_1) + _N_liq_2*pow((1 - MC/_T_c_K),_k_liq_2) + _N_liq_3*pow((1 - MC/_T_c_K),_k_liq_3) + _N_liq_4*pow((1 - MC/_T_c_K),_k_liq_4) + _N_liq_5*pow((1 - MC/_T_c_K),_k_liq_5));
 }

  if( !McCormick<T>::options.ENVEL_USE ){
	  //not sure if this is right, took it from atan
      MC2._cv = Op<T>::l(MC2._I);
      MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }

    return MC2;
  }

  { int imid = -1;

    double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::rho_liq_sat_ethanol_schroeder(Op<T>::u(MC._I))-mc::rho_liq_sat_ethanol_schroeder(Op<T>::l(MC._I)))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
    MC2._cv = mc::rho_liq_sat_ethanol_schroeder(Op<T>::u(MC._I))+r*(mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid )-Op<T>::u(MC._I));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
  }

  { int imid = -1;
    MC2._cc = mc::rho_liq_sat_ethanol_schroeder(mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ));
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_rho_liq_sat_ethanol_schroeder(mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ));
    }
  }
#ifdef MC__MCCORMICK_DEBUG
    std::string str = "rho_liq_sat_ethanol_schroeder";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
covariance_function
( const McCormick<T>&MC, const double type )
{
  if ( Op<T>::l(MC._I) < 0.  )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::COVARIANCE_FUNCTION );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::covariance_function( MC._I, type );

  // the covariance functions are all convex and decreasing
  // convex
  int imid = 2; // we use the cc relaxation for cv
  double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
  MC2._cv = mc::covariance_function(vmid, type);
  for( unsigned int i=0; i<MC2._nsub; i++ ){
    MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_covariance_function(vmid, type);
  }

  // concave
  imid = 1; // concave envelope of covariance is decreasing so we use the cv relaxation for cc
  double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.:(mc::covariance_function(Op<T>::u(MC._I),type)-mc::covariance_function(Op<T>::l(MC._I),type))/(Op<T>::u(MC._I)-Op<T>::l(MC._I)) );
  MC2._cc = mc::covariance_function(Op<T>::l(MC._I),type)+r*(mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid )-Op<T>::l(MC._I));
  for( unsigned int i=0; i<MC2._nsub; i++ ){
    MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) *r;
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "covariance_function";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}


template <typename T> inline McCormick<T>
acquisition_function
( const McCormick<T>&MC1, const McCormick<T>&MC2, const double type, const double fmin )
{

 McCormick<T> MC3;
 if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );

  if ( Op<T>::l(MC2._I) < 0.  )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::ACQUISITION_FUNCTION );

  MC3._I = Op<T>::acquisition_function( MC1._I, MC2._I, type, fmin );

  switch((int)type){
	  case 1: // lower confidence bound
	      return MC1 - fmin*MC2;
	      break;
      case 2: // expected improvement
	  {
	      MC3._cv = mc::acquisition_function(MC1._cc,MC2._cv, type, fmin);
		  for( unsigned int i=0; i<MC3._nsub; i++ ){
			  MC3._cvsub[i] =  mc::der_x_acquisition_function(MC1._cc, MC2._cv, type, fmin)*(MC1._const? 0.:MC1._ccsub[i])
							 + mc::der_y_acquisition_function(MC1._cc, MC2._cv, type, fmin)*(MC2._const? 0.:MC2._cvsub[i]);
		  }
		  double xL = Op<T>::l(MC1._I);
		  double xU = Op<T>::u(MC1._I);
		  double yL = Op<T>::l(MC2._I);
		  double yU = Op<T>::u(MC2._I);
		  double cornerLL = mc::acquisition_function(xL, yL, type, fmin);
		  double cornerUU = mc::acquisition_function(xU, yU, type, fmin);
		  double cornerLU = mc::acquisition_function(xL, yU, type, fmin);
		  double cornerUL = mc::acquisition_function(xU, yL, type, fmin);
		  double r11,r12,r21,r22,val1,val2=0;
          if(cornerLL+cornerUU< cornerLU+cornerUL){
			  r11 = isequal(xL,xU) ? 0 : (cornerLL - cornerUL)/(xL-xU);
			  r12 = isequal(yL,yU) ? 0 : (cornerLL - cornerLU)/(yL-yU);
			  val1 = cornerLL + r11*(MC1._cv - xL) + r12*(MC2._cc - yL);

			  r21 = isequal(xL,xU) ? 0 : (cornerLU - cornerUU)/(xL-xU);
			  r22 = isequal(yL,yU) ? 0 : (cornerUL - cornerUU)/(yL-yU);
			  val2 = cornerUU + r21*(MC1._cv - xU) + r22*(MC2._cc - yU);

		  }
		  else{
			  r11 = isequal(xL,xU) ? 0 : (cornerLU - cornerUU)/(xL-xL);
			  r12 = isequal(yL,yU) ? 0 : (cornerLL - cornerLU)/(yL-yU);
			  val1 = cornerLU + r11*(MC1._cv - xL) + r12*(MC2._cc - yU);

			  r21 = isequal(xL,xU) ? 0 : (cornerLL - cornerUL)/(xL-xU);
			  r22 = isequal(yL,yU) ? 0 : (cornerUL - cornerUU)/(yL-yU);
			  val2 = cornerUL + r21*(MC1._cv - xU) + r22*(MC2._cc - yL);
		  }
		  if(val1<val2){
		      MC3._cc = val1;
			  for( unsigned int i=0; i<MC3._nsub; i++ ){
			      MC3._ccsub[i] =  r11*(MC1._const? 0.:MC1._cvsub[i])
							     + r12*(MC2._const? 0.:MC2._ccsub[i]);
		      }
		  }
		  else{
		      MC3._cc = val2;
			  for( unsigned int i=0; i<MC3._nsub; i++ ){
			      MC3._ccsub[i] =  r21*(MC1._const? 0.:MC1._cvsub[i])
							     + r22*(MC2._const? 0.:MC2._ccsub[i]);
		      }
		  }
	  }
	      break;
      case 3: // probability of improvement
	  {
		  // depending on MC1(=mu), MC2(=sigma), and fmin, there are 4 areas: I1, I2, I3, I4
		  //   I1: mu<=fmin, mu-fmin>=-sqrt(2)*sigma
		  //   I2: mu>=fmin, mu-fmin<= sqrt(2)*sigma
		  //   I3: mu<=fmin, mu-fmin<=-sqrt(2)*sigma
		  //   I4: mu>=fmin, mu-fmin>=+sqrt(2)*sigma
		  if ( (Op<T>::l(MC1._I)-fmin)>-std::sqrt(2.)*Op<T>::l(MC2._I) && (Op<T>::u(MC1._I)-fmin)<std::sqrt(2.)*Op<T>::l(MC2._I) ) {	// completely in I1 and/or I2
			// here we just use McCormick relaxations of factorable representation, since these are very good already
			MC3 = erf(1./std::sqrt(2)*((fmin-MC1)/MC2))/2.+0.5;
		  }
		  else if ((Op<T>::l(MC1._I)-fmin) >  std::sqrt(2.)*Op<T>::u(MC2._I)) {	// completely in I4
			// here the function is componentwise convex --> can use method of Mayer & Floudas for concave relaxation
			// furthermore, the mixed second partial derivatives are negative --> can use Theorem 1 from Najman, Bongartz, Mitsos (2019) CACE for convex relaxation
			// convex relaxation
			{
			  // since the mixed derivative is <0, we can use Theorem 1 with corners LU and UL
			  double cv[2], xslope[2], yslope[2];

			  // in this region, we are also increasing in MC2 and decreasing in MC1 -> MC2._cv, MC1._cc
			  // corner LU
			  cv[0] = mc::acquisition_function(MC1._cc,Op<T>::u(MC2._I),type,fmin) + mc::acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin) - mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
			  xslope[0] = mc::der_x_acquisition_function(MC1._cc,Op<T>::u(MC2._I),type,fmin);
			  yslope[0] = mc::der_y_acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
			  // corner UL
			  cv[1] = mc::acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin) + mc::acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin) - mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  xslope[1] = mc::der_x_acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin);
			  yslope[1] = mc::der_y_acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);

			  // finally, store the best relaxation with the corresponding subgradient
			  const unsigned irelax = argmax(2, cv);
			  MC3._cv = cv[irelax];
			  for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._cvsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._ccsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._cvsub[i]); }
			}
			// concave relaxation
			{
			  // the relaxation consists of 2 facets: 1 (LU,LL,UL), 2(LU,UU,UL)
			  double cc[2], xslope[2], yslope[2];

			  // first, values at corner points
			  const double fLL = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  const double fLU = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
			  const double fUL = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  const double fUU = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);

			  // actually compute facets, making sure we default to the highest corner in case a dimension is thin
			  // in this region, we are also increasing in MC2 and decreasing in MC1 -> MC2._cc, MC1._cv
			  const bool thinX = isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) );
			  const bool thinY = isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) );
			  xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<T>::diam(MC1._I);
			  yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<T>::diam(MC2._I);
			  xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<T>::diam(MC1._I);
			  yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<T>::diam(MC2._I);
			  cc[0] = fLU + xslope[0]*(MC1._cv - Op<T>::l(MC1._I)) +  yslope[0]*(MC2._cc - Op<T>::u(MC2._I));
			  cc[1] = fLU + xslope[1]*(MC1._cv - Op<T>::l(MC1._I)) +  yslope[1]*(MC2._cc - Op<T>::u(MC2._I));

			  // finally, store the best relaxation with the corresponding subgradient
			  const unsigned irelax = argmin(2, cc);
			  MC3._cc = cc[irelax];
			  for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._ccsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._cvsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._ccsub[i]); }
			}

		  }
		  else if ((Op<T>::u(MC1._I)-fmin) < -std::sqrt(2.)*Op<T>::u(MC2._I)) {	// completely in I3
			// here the function is componentwise concave --> can use method of Mayer & Floudas for convex relaxation
			// furthermore, the mixed second partial derivatives are negative --> can use Theorem 1 from Najman, Bongartz, Mitsos (2019) CACE for concave relaxation
			// concave relaxation
			{
			  // since the mixed derivative is <0, we can use Theorem 1 with corners LL and UU (recall this is the concave one, so opposite to Corollary 1)
			  double cc[2], xslope[2], yslope[2];

			  // in this region, we are also decreasing in both MC1 and MC2 -> MC1._cv, MC2._cv
			  // corner LL
			  cc[0] = mc::acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin) + mc::acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin) - mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  xslope[0] = mc::der_x_acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin);
			  yslope[0] = mc::der_y_acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
			  // corner UU
			  cc[1] = mc::acquisition_function(MC1._cv,Op<T>::u(MC2._I),type,fmin) + mc::acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin) - mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);
			  xslope[1] = mc::der_x_acquisition_function(MC1._cv,Op<T>::u(MC2._I),type,fmin);
			  yslope[1] = mc::der_y_acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);

			  // finally, store the best relaxation with the corresponding subgradient
			  const unsigned irelax = argmin(2, cc);
			  MC3._cc = cc[irelax];
			  for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._ccsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._cvsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._cvsub[i]); }
			}
			// convex relaxation
			{
			  // the relaxation consists of 2 facets: 1 (LL,LU,UU), 2(LL,UL,UU)
			  double cv[2], xslope[2], yslope[2];

			  // first, values at corner points
			  const double fLL = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  const double fLU = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
			  const double fUL = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin);
			  const double fUU = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);

			  // actually compute facets, making sure we default to the lowest corner in case a dimension is thin
			  // in this region, we are also decreasing in both MC1 and MC2 -> MC1._cc, MC2._cc
			  const bool thinX = isequal( Op<T>::l(MC1._I), Op<T>::u(MC1._I) );
			  const bool thinY = isequal( Op<T>::l(MC2._I), Op<T>::u(MC2._I) );
			  xslope[0] = thinX ? 0. : ( fUU - fLU )/Op<T>::diam(MC1._I);
			  yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<T>::diam(MC2._I);
			  xslope[1] = thinX ? 0. : ( fUL - fLL )/Op<T>::diam(MC1._I);
			  yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<T>::diam(MC2._I);
			  cv[0] = fUU + xslope[0]*(MC1._cc - Op<T>::u(MC1._I)) +  yslope[0]*(MC2._cc - Op<T>::u(MC2._I));
			  cv[1] = fUU + xslope[1]*(MC1._cc - Op<T>::u(MC1._I)) +  yslope[1]*(MC2._cc - Op<T>::u(MC2._I));

			  // finally, store the best relaxation with the corresponding subgradient
			  const unsigned irelax = argmax(2, cv);
			  MC3._cv = cv[irelax];
			  for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._cvsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._ccsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._ccsub[i]); }
			}

		  }
		  else if (Op<T>::u(MC1._I)<=fmin) {	// completely in I1 and I3
			  // convex
			  {
				// compute convex envelope at MC1_U using McCormick method for 1D functions, and MC2_U (secant of concave function)
				// because the function is decreasing w.r.t. MC1 and MC2 (-> MC1._cc, MC2._cc), these relaxations are valid everwhere
				double cv[2], xslope[2], yslope[2];
				const double fUU = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);

				// MC1_U
				if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return solution at MC2_U b/c of monotonicity
					xslope[0] = 0.;
					yslope[0] = 0.;
					cv[0] = fUU;
				}
				else {	// not thin in MC1, actually compute envelope
					double yIntersect; // point at which the tangent (or secant) may be anchored
					// first calculate inflection point
					const double yInflect = (fmin-Op<T>::u(MC1._I))/std::sqrt(2);
					// depending on where the inflection point is, we may or may not be fully convex or concave
					if (Op<T>::l(MC2._I)>=yInflect) { // completely in convex part
						yIntersect = yInflect; // this "deactivates" the tangent
					} else if(Op<T>::u(MC2._I)<=yInflect) { // completely in concave part
						yIntersect = Op<T>::u(MC2._I); // this makes tangent a secant over the whole interval
					} else { // inflection point is in interval, need to compute the tangent numerically
						double rusr[5];
						rusr[0] = Op<T>::u(MC1._I);
						rusr[1] = Op<T>::l(MC2._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin);
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							yIntersect = numerics::newton( Op<T>::u(MC2._I), yInflect+1e-6*(Op<T>::u(MC2._I)-yInflect), Op<T>::u(MC2._I), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							yIntersect = numerics::goldsect( yInflect, Op<T>::u(MC2._I), myfPtr, rusr, NULL );
						}
					}

					// now compute relaxation
					if( MC2._cc < yIntersect ){   // ok, we are actually on the tangent part
						const double fIntersect = mc::acquisition_function(Op<T>::u(MC1._I),yIntersect,type,fmin);
						xslope[0] = 0.;
						yslope[0] = isequal(Op<T>::l(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin))/(yIntersect-Op<T>::l(MC2._I));
						cv[0] = fIntersect + yslope[0]*(MC2._cc-yIntersect);
					}
					else {   // no, we are still on the convex part
						xslope[0] = 0.;
						yslope[0] = mc::der_y_acquisition_function(Op<T>::u(MC1._I),MC2._cc,type,fmin);
						cv[0] = mc::acquisition_function(Op<T>::u(MC1._I),MC2._cc,type,fmin);
					}
				}


				// MC2_U
				if (isequal(Op<T>::diam(MC1._I),0.)) {	// box is thin in MC1, so just return solution at MC1_U b/c of monotonicity
					xslope[1] = 0.;
					yslope[1] = 0.;
					cv[1] = fUU;
				}
				else {
					xslope[1] = (fUU-mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin))/Op<T>::diam(MC1._I);
					yslope[1] = 0.;
					cv[1] = fUU + xslope[1]*(MC1._cc-Op<T>::u(MC1._I));
				}

			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmax(2, cv);
			    MC3._cv = cv[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._cvsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._ccsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._ccsub[i]); }
			  }
			  // concave
			  {

				// compute concave envelope at MC1_L using McCormick method for 1D functions, and the function itself MC2_L (concave w.r.t. MC1)
				// because the function is decreasing w.r.t. MC1 and MC2 (-> MC1._cv, MC2._cv), these relaxations are valid everwhere
				double cc[2], xslope[2], yslope[2];

				// MC1_L
				if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return solution at MC2_L b/c of monotonicity
					xslope[0] = 0.;
					yslope[0] = 0.;
					cc[0] = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin);
				}
				else {	// not thin in MC1, actually compute envelope
					double yIntersect; // point at which the tangent (or secant) may be anchored
					// first calculate inflection point
					const double yInflect = (fmin-Op<T>::l(MC1._I))/std::sqrt(2);
					// depending on where the inflection point is, we may or may not be fully convex or concave
					if (Op<T>::l(MC2._I)>=yInflect) { // completely in convex part
						yIntersect = Op<T>::l(MC2._I); // this makes tangent a secant over the whole interval
					} else if(Op<T>::u(MC2._I)<=yInflect) { // completely in concave part
						yIntersect = yInflect; // this "deactivates" the tangent
					} else { // inflection point is in interval, need to compute the tangent numerically
						double rusr[5];
						rusr[0] = Op<T>::l(MC1._I);
						rusr[1] = Op<T>::u(MC2._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							yIntersect = numerics::newton( Op<T>::l(MC2._I), Op<T>::l(MC2._I), yInflect-1e-6*(yInflect-Op<T>::l(MC2._I)), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							yIntersect = numerics::goldsect( Op<T>::l(MC2._I), yInflect, myfPtr, rusr, NULL );
						}
					}

					// now compute relaxation
					if( MC2._cv > yIntersect ){   // ok, we are actually on the tangent part
						const double fIntersect = mc::acquisition_function(Op<T>::l(MC1._I),yIntersect,type,fmin);
						xslope[0] = 0.;
						yslope[0] = isequal(Op<T>::u(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin))/(yIntersect-Op<T>::u(MC2._I));
						cc[0] = fIntersect + yslope[0]*(MC2._cv-yIntersect);
					}
					else {   // no, we are still on the concave part
						xslope[0] = 0.;
						yslope[0] = mc::der_y_acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
						cc[0] = mc::acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
					}
				}

				// MC2_l
				xslope[1] = mc::der_x_acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin);
				yslope[1] = 0.;
				cc[1] = mc::acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin);

			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmin(2, cc);
			    MC3._cc = cc[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._ccsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._cvsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._cvsub[i]); }
			  }
		  }
		  else if (Op<T>::l(MC1._I)>=fmin) {	// completely in I2 and I4
			  // convex
			  {
				// compute convex envelope at MC1_U using McCormick method for 1D functions, and function itself MC2_L (convex w.r.t. MC1)
				// because the function is decreasing w.r.t. MC1 and increasing w.r.t. MC2 (-> MC1._cc, MC2._cv), these relaxations are valid everwhere
				double cv[2], xslope[2], yslope[2];
				const double fUL = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::l(MC2._I),type,fmin);

				// MC1_U
				if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return solution at MC2_L b/c of monotonicity
					xslope[0] = 0.;
					yslope[0] = 0.;
					cv[0] = fUL;
				}
				else {	// not thin in MC1, actually compute envelope
					double yIntersect; // point at which the tangent (or secant) may be anchored
					// first calculate inflection point
					const double yInflect = (Op<T>::u(MC1._I)-fmin)/std::sqrt(2);
					// depending on where the inflection point is, we may or may not be fully convex or concave
					if (Op<T>::u(MC2._I)<=yInflect) { // completely in convex part
						yIntersect = yInflect; // this "deactivates" the tangent
					} else if(Op<T>::l(MC2._I)>=yInflect) { // completely in concave part
						yIntersect = Op<T>::l(MC2._I); // this makes tangent a secant over the whole interval
					} else { // inflection point is in interval, need to compute the tangent numerically
						double rusr[5];
						rusr[0] = Op<T>::u(MC1._I);
						rusr[1] = Op<T>::u(MC2._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							yIntersect = numerics::newton( Op<T>::l(MC2._I), Op<T>::l(MC2._I), yInflect-1e-6*(yInflect-Op<T>::l(MC2._I)), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							yIntersect = numerics::goldsect( Op<T>::l(MC2._I), yInflect, myfPtr, rusr, NULL );
						}
					}

					// now compute relaxation
					if( MC2._cv > yIntersect ){   // ok, we are actually on the tangent part
						const double fIntersect = mc::acquisition_function(Op<T>::u(MC1._I),yIntersect,type,fmin);
						xslope[0] = 0.;
						yslope[0] = isequal(Op<T>::u(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin))/(yIntersect-Op<T>::u(MC2._I));
						cv[0] = fIntersect + yslope[0]*(MC2._cv-yIntersect);
					}
					else {   // no, we are still on the convex part
						xslope[0] = 0.;
						yslope[0] = mc::der_y_acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);
						cv[0] = mc::acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);
					}
				}


				// MC2_L
				xslope[1] = mc::der_x_acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin);
				yslope[1] = 0.;
				cv[1] = mc::acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin);

			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmax(2, cv);
			    MC3._cv = cv[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._cvsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._ccsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._cvsub[i]); }

			  }
			  // concave
			  {

				// compute concave envelope at MC1_L using McCormick method for 1D functions, and the secant at MC2_U (convex w.r.t. MC1)
				// because the function is decreasing w.r.t. MC1 and increasing w.r.t. MC2 (-> MC1._cv, MC2._cc), these relaxations are valid everwhere
				double cc[2], xslope[2], yslope[2];
				const double fLU = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);

				// MC1_L
				if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return solution at MC2U b/c of monotonicity
					xslope[0] = 0.;
					yslope[0] = 0.;
					cc[0] = fLU;
				}
				else {	// not thin in MC1, actually compute envelope
					double yIntersect; // point at which the tangent (or secant) may be anchored
					// first calculate inflection point
					const double yInflect = (Op<T>::l(MC1._I)-fmin)/std::sqrt(2);
					// depending on where the inflection point is, we may or may not be fully convex or concave
					if (Op<T>::l(MC2._I)>=yInflect) { // completely in concave part
						yIntersect = yInflect; // this "deactivates" the tangent
					} else if(Op<T>::u(MC2._I)<=yInflect) { // completely in convex part
						yIntersect = Op<T>::u(MC2._I); // this makes tangent a secant over the whole interval
					} else { // inflection point is in interval, need to compute the tangent numerically
						double rusr[5];
						rusr[0] = Op<T>::l(MC1._I);
						rusr[1] = Op<T>::l(MC2._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin);
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							yIntersect = numerics::newton( Op<T>::u(MC2._I), yInflect+1e-6*(Op<T>::u(MC2._I)-yInflect), Op<T>::u(MC2._I), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							yIntersect = numerics::goldsect( yInflect, Op<T>::u(MC2._I), myfPtr, rusr, NULL );
						}
					}

					// now compute relaxation
					if( MC2._cc < yIntersect ){   // ok, we are actually on the tangent part
						const double fIntersect = mc::acquisition_function(Op<T>::l(MC1._I),yIntersect,type,fmin);
						xslope[0] = 0.;
						yslope[0] = isequal(Op<T>::l(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::l(MC2._I),type,fmin))/(yIntersect-Op<T>::l(MC2._I));
						cc[0] = fIntersect + yslope[0]*(MC2._cc-yIntersect);
					}
					else {   // no, we are still on the concave part
						xslope[0] = 0.;
						yslope[0] = mc::der_y_acquisition_function(Op<T>::l(MC1._I),MC2._cc,type,fmin);
						cc[0] = mc::acquisition_function(Op<T>::l(MC1._I),MC2._cc,type,fmin);
					}
				}

				// MC2_U
				if (isequal(Op<T>::diam(MC1._I),0.)) {	// box is thin in MC1, so just return solution at MC1_U b/c of monotonicity
					xslope[1] = 0.;
					yslope[1] = 0.;
					cc[1] = fLU;
				}
				else {
					xslope[1] = (mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin)-fLU)/Op<T>::diam(MC1._I);
					yslope[1] = 0.;
					cc[1] = fLU + xslope[1]*(MC1._cc-Op<T>::l(MC1._I));
				}

			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmin(2, cc);
			    MC3._cc = cc[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._ccsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._cvsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._ccsub[i]); }
			  }
		  }
		  else {
			 // convex
			  {
				// we compute two relaxations and choose the strongest
				double cv[2], xslope[2], yslope[2];

				// first relaxation
				{
					// compute convex envelope at MC1_U using McCormick method for 1D functions
					// because the function is decreasing w.r.t. MC1, this relaxation is valid everwhere
					if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return interval solution
						xslope[0] = 0.;
						yslope[0] = 0.;
						cv[0] = Op<T>::l(MC3._I);
					}
					else {	// not thin in MC1, actually compute envelope
						// we know that MC1_U>0 --> increasing w.r.t. MC2 --> MC2._cv
						double yIntersect; // point at which the tangent (or secant) may be anchored
						// first calculate inflection point
						const double yInflect = (Op<T>::u(MC1._I)-fmin)/std::sqrt(2);
						// depending on where the inflection point is, we may or may not be fully convex or concave
						if (Op<T>::u(MC2._I)<=yInflect) { // completely in convex part
							yIntersect = yInflect; // this "deactivates" the tangent
						} else if(Op<T>::l(MC2._I)>=yInflect) { // completely in concave part
							yIntersect = Op<T>::l(MC2._I); // this makes tangent a secant over the whole interval
						} else { // inflection point is in interval, need to compute the tangent numerically
							double rusr[5];
							rusr[0] = Op<T>::u(MC1._I);
							rusr[1] = Op<T>::u(MC2._I);
							rusr[2] = type;
							rusr[3] = fmin;
							rusr[4] = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);
							double (*myfPtr)(const double,const double*,const int*);
							double (*mydfPtr)(const double,const double*,const int*);
							myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
							mydfPtr = [](const double x, const double*rusr, const int*iusr) {
								return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x);
							};
							try{
								yIntersect = numerics::newton( Op<T>::l(MC2._I), Op<T>::l(MC2._I), yInflect-1e-6*(yInflect-Op<T>::l(MC2._I)), myfPtr, mydfPtr, rusr, NULL );
							}
							catch( ... ){
								yIntersect = numerics::goldsect( Op<T>::l(MC2._I), yInflect, myfPtr, rusr, NULL );
							}
						}
						// now compute relaxation
						if( MC2._cv > yIntersect ){   // ok, we are actually on the tangent part
							const double fIntersect = mc::acquisition_function(Op<T>::u(MC1._I),yIntersect,type,fmin);
							xslope[0] = 0.;
							yslope[0] = isequal(Op<T>::u(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin))/(yIntersect-Op<T>::u(MC2._I));
							cv[0] = fIntersect + yslope[0]*(MC2._cv-yIntersect);
						} else {
							xslope[0] = 0.;
							yslope[0] = mc::der_y_acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);
							cv[0] = mc::acquisition_function(Op<T>::u(MC1._I),MC2._cv,type,fmin);
						}
					}
				}
				// second relaxation
				{
					// compute a convex relaxation at MC2_L BUT making sure that it remains below f(MC1_L,MC2_U):
					// this way, we know it is valid everywhere, because it is a valid realaxation both for MC1>0 (increasing in MC2)
					// and MC2<0 (decreasing in MC2 and concave in MC1, and >=0.5 everywhere)
					const double fLU = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
					if (Op<T>::l(MC2._I)==0) {
						if (MC1._cc>=0) {
							xslope[1] = 0.;
							yslope[1] = 0.;
							cv[1] = 0.;
						}
						else {
							xslope[1] = isequal(Op<T>::l(MC1._I),0.) ? 0. : (0.-fLU)/(0.-Op<T>::l(MC1._I));
							yslope[1] = 0.;
							cv[1] = 0. + xslope[1]*(MC1._cc-0.);
						}
					}
					else {
						double xIntersect;
						double rusr[5];
						rusr[0] = Op<T>::l(MC2._I);
						rusr[1] = Op<T>::l(MC1._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = fLU;
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(x,rusr[0],rusr[2],rusr[3]) + mc::der_x_acquisition_function(x,rusr[0],rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_x2_acquisition_function(x,rusr[0],rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							xIntersect = numerics::newton( Op<T>::u(MC1._I), fmin+1e-6*(Op<T>::u(MC1._I)-fmin), Op<T>::u(MC1._I), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							xIntersect = numerics::goldsect( fmin, Op<T>::u(MC1._I), myfPtr, rusr, NULL );
						}
						if (MC1._cc<xIntersect) { // secant part
							const double fIntersect = mc::acquisition_function(xIntersect,Op<T>::l(MC2._I),type,fmin);
							xslope[1] = isequal(Op<T>::l(MC1._I),xIntersect) ? 0. : (fIntersect-fLU)/(xIntersect-Op<T>::l(MC1._I));
							yslope[1] = 0.;
							cv[1] = fIntersect + xslope[1]*(MC1._cc-xIntersect);
						} else { // still function itself
							xslope[1] = mc::der_x_acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin);
							yslope[1] = 0.;
							cv[1] = mc::acquisition_function(MC1._cc,Op<T>::l(MC2._I),type,fmin);
						}

					}
				}

			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmax(2, cv);
			    MC3._cv = cv[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._cvsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._ccsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._cvsub[i]); }

			  }
			  // concave
			  {
				// we compute two relaxations and choose the strongest
				double cc[2], xslope[2], yslope[2];

                // first relaxation
				{
					// compute convex envelope at MC1_L using McCormick method for 1D functions
					// because the function is decreasing w.r.t. MC1, this relaxation is valid everwhere
					if (isequal(Op<T>::diam(MC2._I),0.)) {	// box is thin in MC2, so just return interval solution
						xslope[0] = 0.;
						yslope[0] = 0.;
						cc[0] = Op<T>::u(MC3._I);
					}
					else {	// not thin in MC2, actually compute envelope
						// We know that xL<0 --> decreasing w.r.t. MC2 -> MC2._cv
						double yIntersect; // point at which the tangent (or secant) may be anchored
						// first calculate inflection point
						const double yInflect = (fmin-Op<T>::l(MC1._I))/std::sqrt(2);
						// depending on where the inflection point is, we may or may not be fully convex or concave
						if (Op<T>::l(MC2._I)>=yInflect) { // completely in convex part
							yIntersect = Op<T>::l(MC2._I); // this makes tangent a secant over the whole interval
						} else if(Op<T>::u(MC2._I)<=yInflect) { // completely in concave part
							yIntersect = yInflect; // this "deactivates" the tangent
						} else { // inflection point is in interval, need to compute the tangent numerically
							double rusr[5];
							rusr[0] = Op<T>::l(MC1._I);
							rusr[1] = Op<T>::u(MC2._I);
							rusr[2] = type;
							rusr[3] = fmin;
							rusr[4] = mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin);
							double (*myfPtr)(const double,const double*,const int*);
							double (*mydfPtr)(const double,const double*,const int*);
							myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(rusr[0],x,rusr[2],rusr[3]) + mc::der_y_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
							mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_y2_acquisition_function(rusr[0],x,rusr[2],rusr[3])*(rusr[1]-x); };
							try{
								yIntersect = numerics::newton( Op<T>::l(MC2._I), Op<T>::l(MC2._I), yInflect-1e-6*(yInflect-Op<T>::l(MC2._I)), myfPtr, mydfPtr, rusr, NULL );
							}
							catch( ... ){
								yIntersect = numerics::goldsect( Op<T>::l(MC2._I), yInflect, myfPtr, rusr, NULL );
							}
						}

						// now compute relaxation
						if( MC2._cv > yIntersect ){   // ok, we are actually on the tangent part
							const double fIntersect = mc::acquisition_function(Op<T>::l(MC1._I),yIntersect,type,fmin);
							xslope[0] = 0.;
							yslope[0] = isequal(Op<T>::u(MC2._I),yIntersect)? 0. : (fIntersect-mc::acquisition_function(Op<T>::l(MC1._I),Op<T>::u(MC2._I),type,fmin))/(yIntersect-Op<T>::u(MC2._I));
							cc[0] = fIntersect + yslope[0]*(MC2._cv-yIntersect);
						}
						else {   // no, we are still on the concave part
							xslope[0] = 0.;
							yslope[0] = mc::der_y_acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
							cc[0] = mc::acquisition_function(Op<T>::l(MC1._I),MC2._cv,type,fmin);
						}
					}
				}
				// second relaxation
				{

					// compute a convex relaxation at MC2_L BUT making sure that it remains above f(MC1_U,MC2_U):
					// this way, we know it is valid everywhere, because it is a valid realaxation both for MC1<0 (increasing in MC2)
					// and MC2>0 (increasing in MC2 and convex in MC1, and <=0.5 everywhere)
					const double fUU = mc::acquisition_function(Op<T>::u(MC1._I),Op<T>::u(MC2._I),type,fmin);
					if (Op<T>::l(MC2._I)==0) {
						if (MC1._cc<=0) {
							xslope[1] = 0.;
							yslope[1] = 0.;
							cc[1] = 1.;
						}
						else {
							xslope[1] = isequal(Op<T>::u(MC1._I),0.) ? 0. : (fUU-1.)/(Op<T>::u(MC1._I) - 0.);
							yslope[1] = 0.;
							cc[1] = 1. + xslope[1]*(MC1._cv-0.);
						}
					}
					else {
						double xIntersect;
						double rusr[5];
						rusr[0] = Op<T>::l(MC2._I);
						rusr[1] = Op<T>::u(MC1._I);
						rusr[2] = type;
						rusr[3] = fmin;
						rusr[4] = fUU;
						double (*myfPtr)(const double,const double*,const int*);
						double (*mydfPtr)(const double,const double*,const int*);
						myfPtr = [](const double x, const double*rusr, const int*iusr) { return  mc::acquisition_function(x,rusr[0],rusr[2],rusr[3]) + mc::der_x_acquisition_function(x,rusr[0],rusr[2],rusr[3])*(rusr[1]-x) - rusr[4]; };
						mydfPtr = [](const double x, const double*rusr, const int*iusr) { return mc::der_x2_acquisition_function(x,rusr[0],rusr[2],rusr[3])*(rusr[1]-x); };
						try{
							xIntersect = numerics::newton( Op<T>::l(MC1._I), Op<T>::l(MC1._I), fmin-1e-6*(fmin-Op<T>::l(MC1._I)), myfPtr, mydfPtr, rusr, NULL );
						}
						catch( ... ){
							xIntersect = numerics::goldsect( Op<T>::l(MC1._I), fmin, myfPtr, rusr, NULL );
						}
						if (MC1._cv>xIntersect) { // secant part
							const double fIntersect = mc::acquisition_function(xIntersect,Op<T>::l(MC2._I),type,fmin);
							xslope[1] = isequal(Op<T>::u(MC1._I),xIntersect) ? 0. : (fUU - fIntersect)/(Op<T>::u(MC1._I) - xIntersect);
							yslope[1] = 0.;
							cc[1] = fIntersect + xslope[1]*(MC1._cv-xIntersect);
						} else { // still function itself
							xslope[1] = mc::der_x_acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin);
							yslope[1] = 0.;
							cc[1] = mc::acquisition_function(MC1._cv,Op<T>::l(MC2._I),type,fmin);
						}

					}
				}


			    // finally, store the best relaxation with the corresponding subgradient
			    const unsigned irelax = argmin(2, cc);
			    MC3._cc = cc[irelax];
			    for( unsigned int i=0; i<MC3._nsub; i++ ){ MC3._ccsub[i] = xslope[irelax]*(MC1._const? 0.: MC1._cvsub[i])+yslope[irelax]*(MC2._const? 0.:MC2._ccsub[i]); }
			  }
		  }
		  break;
	  }
      default:
		  throw std::runtime_error("mc::McCormick\t Probability of improvement acquisition called with unknown type.\n");
	      break;
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "acquisition_function";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif

  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

template <typename T> inline McCormick<T>
gaussian_probability_density_function
( const McCormick<T>&MC )
{
  // This function is monotonically increasing for xU <= 0 and monotonically decreasing for xL>=0
  // It is convex on xU <= -1 or xL >= 1 and concave on [-1,1]
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::gaussian_probability_density_function(MC._I);

  double xL = Op<T>::l(MC._I);
  double xU = Op<T>::u(MC._I);

  if(xU <= -1){ // The function is convex and monotonically increasing
      // convex relaxation
      int imid = 1; // we use the cv relaxation for cv
      double vmid = mid( MC._cv, MC._cc, xL, imid );
      MC2._cv = mc::gaussian_probability_density_function(vmid);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
      }

      // concave relaxation
      imid = 2; // concave envelope is increasing so we use the cc relaxation for cc
      double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
      MC2._cc = mc::gaussian_probability_density_function(xU)+r*(mid( MC._cv, MC._cc, xU, imid )-xU);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) *r;
      }
  }
  else if(xL >= 1){ // The function is convex and monotonically decreasing
	  // convex relaxation
      int imid = 2; // we use the cc relaxation for cv since it is decreasing
      double vmid = mid( MC._cv, MC._cc, xU, imid );
      MC2._cv = mc::gaussian_probability_density_function(vmid);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
      }

      // concave relaxation
      imid = 1; // concave envelope is decreasing so we use the cv relaxation for cc
      double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
      MC2._cc = mc::gaussian_probability_density_function(xL)+r*(mid( MC._cv, MC._cc, xL, imid )-xL);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) *r;
      }
  }
  else if(-1 <= xL && xU <= 1){
	  // The function is concave
	  double minPoint;
	  double maxPoint;
	  if(mc::gaussian_probability_density_function(xL) < mc::gaussian_probability_density_function(xU)){
		  minPoint = xL;
		  maxPoint = xU;
	  }
	  else{
		  minPoint = xU;
		  maxPoint = xL;
	  }
	  if(xL<= 0. && 0. <= xU){
		  maxPoint = 0.;
	  }
	  // convex relaxation
      int imid = -1;
      double r = ( isequal( xL, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(xL))/(xU-xL) );
      MC2._cv = mc::gaussian_probability_density_function(minPoint)+r*(mid( MC._cv, MC._cc, minPoint, imid )-minPoint);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
      }
      // concave relaxation
      imid = -1;
      double vmid = mid( MC._cv, MC._cc, maxPoint, imid );
      MC2._cc = mc::gaussian_probability_density_function(vmid);
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
      }
  }
  else if(xL <= -1 && -1 < xU && xU < 1){
	  // Left convex-concave part
	  // convex relaxation is increasing
	  double starting_point = (xL + std::min(xU,0.))/2.;
	  double solPointCv = McCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,-1./*right bound*/,xL/*starting point for Newton*/,xU/*fixed point in the equality*/);
	  if(MC._cv<solPointCv){
		  int imid = 1; // we use the cv relaxation for cv
		  double vmid = mid( MC._cv, MC._cc, xL, imid );
		  MC2._cv = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }
	  }
	  else{
		  int imid = 1; // we use the cv relaxation for cv
		  double r = ( isequal( solPointCv, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCv))/(xU-solPointCv) );
          MC2._cv = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cv - solPointCv);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
	  // concave relaxation is increasing (up to 0)
	  double solPointCc = McCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,std::min(xU,0.)/*right bound*/,std::min(xU,0.)/*starting point for Newton*/,xL/*fixed point in the equality*/);
	  int imid = -1; // we cannot know which to use for the concave relaxation
	  double vmid = mid( MC._cv, MC._cc, 0, imid );
	  if( vmid>solPointCc){
		  MC2._cc = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }
	  }
	  else{
		  double r = ( isequal( solPointCc, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCc))/(xL-solPointCc) );
          MC2._cc = mc::gaussian_probability_density_function(solPointCc)+r*( vmid - solPointCc);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
  }
  else if(-1 < xL && xL < 1 && xU >= 1){
	  // Right convex-concave part
	  // convex relaxation is decreasing
	  double solPointCv = McCormick<T>::_gpdf_compute_sol_point(1./*left bound*/,xU/*right bound*/,xU/*starting point for Newton*/,xL/*fixed point in the equality*/);
	  if(MC._cc>solPointCv){
		  int imid = 2; // we use the cc relaxation for cv
		  double vmid = mid( MC._cv, MC._cc, xU, imid );
		  MC2._cv = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }
	  }
	  else{
		  int imid = 2; // we use the cc relaxation for cv
		  double r = ( isequal( solPointCv, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCv))/(xL-solPointCv) );
          MC2._cv = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cc - solPointCv);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
	  // concave relaxation is decreasing
	  double solPointCc = McCormick<T>::_gpdf_compute_sol_point(std::max(xL,0.)/*left bound*/,xU/*right bound*/,std::max(xL,0.)/*starting point for Newton*/,xU/*fixed point in the equality*/);
	  if(MC._cv<solPointCc){
		  int imid = 1; // we use the cv relaxation for cc
		  double vmid = mid( MC._cv, MC._cc, xU, imid );
		  MC2._cc = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }
	  }
	  else{
		  int imid = 1; // we use the cv relaxation for cc
		  double r = ( isequal( solPointCc, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCc))/(xU-solPointCc) );
          MC2._cc = mc::gaussian_probability_density_function(solPointCc)+r*( MC._cv - solPointCc);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
  }
  else{ // xL <= -1 && xU >= 1
      // convex relaxation may be increasing or decreasing
	  if(xL + xU < 0){
          // convex relaxation is increasing
		  double solPointCv = xL;
		  if(McCormick<T>::_gpdf_func(xL,&xU,0)*McCormick<T>::_gpdf_func(-1.,&xU,0)<0){ // make sure there is a root to avoid errors in newton computations
			  solPointCv = McCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,std::min(xU,-1.)/*right bound*/,xL/*starting point for Newton*/,xU/*fixed point in the equality*/);
		  }
		  if(MC._cv<solPointCv){
			  int imid = 1; // we use the cv relaxation for cv
			  double vmid = mid( MC._cv, MC._cc, xL, imid );
			  MC2._cv = mc::gaussian_probability_density_function(vmid);
			  for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
			  }
		  }
		  else{
			  int imid = 1; // we use the cv relaxation for cv
			  double r = ( isequal( solPointCv, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCv))/(xU-solPointCv) );
			  MC2._cv = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cv - solPointCv);
			  for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
			  }
		  }
	  }
	  else if(xL+xU>0){
          // convex relaxation is decreasing
		  double solPointCv = xU;
		  if(McCormick<T>::_gpdf_func(1.,&xL,0)*McCormick<T>::_gpdf_func(xU,&xL,0)<0){
		      solPointCv = McCormick<T>::_gpdf_compute_sol_point(std::max(xL,1.)/*left bound*/,xU/*right bound*/,xU/*starting point for Newton*/,xL/*fixed point in the equality*/);
		  }
		  if(MC._cc>solPointCv){
			  int imid = 2; // we use the cc relaxation for cv
			  double vmid = mid( MC._cv, MC._cc, xU, imid );
			  MC2._cv = mc::gaussian_probability_density_function(vmid);
			  for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
			  }
		  }
		  else{
			  int imid = 2; // we use the cc relaxation for cv
			  double r = ( isequal( solPointCv, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCv))/(xL-solPointCv) );
			  MC2._cv = mc::gaussian_probability_density_function(solPointCv)+r*( MC._cc - solPointCv);
			  for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
			  }
		  }
	  }
	  else{
		  // we need this case to avoid numerical issues in newton
		  int imid = 2; // we use the cc relaxation for cv
		  double r = 0;
		  MC2._cv = mc::gaussian_probability_density_function(xL);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
		  }
	  }
	  // concave relaxation
	  double solPointCc1 = McCormick<T>::_gpdf_compute_sol_point(xL/*left bound*/,0./*right bound*/,0./*starting point for Newton*/,xL/*fixed point in the equality*/);
	  double solPointCc2 = McCormick<T>::_gpdf_compute_sol_point(0./*left bound*/,xU/*right bound*/,0./*starting point for Newton*/,xU/*fixed point in the equality*/);
	  int imid = -1; // we cannot know which relaxation to use
	  double vmid = mid( MC._cv, MC._cc, 0., imid );
	  if(vmid <= solPointCc1){
		  double r = ( isequal( solPointCc1, xL )? 0.:(mc::gaussian_probability_density_function(xL)-mc::gaussian_probability_density_function(solPointCc1))/(xL-solPointCc1) );
          MC2._cc = mc::gaussian_probability_density_function(solPointCc1)+r*( vmid - solPointCc1);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
	  else if(vmid >= solPointCc2){
		  double r = ( isequal( solPointCc2, xU )? 0.:(mc::gaussian_probability_density_function(xU)-mc::gaussian_probability_density_function(solPointCc2))/(xU-solPointCc2) );
          MC2._cc = mc::gaussian_probability_density_function(solPointCc2)+r*( vmid - solPointCc2);
          for( unsigned int i=0; i<MC2._nsub; i++ ){
            MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
          }
	  }
	  else{
		  MC2._cc = mc::gaussian_probability_density_function(vmid);
		  for( unsigned int i=0; i<MC2._nsub; i++ ){
			MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * mc::der_gaussian_probability_density_function(vmid);
		  }
	  }
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "gaussian probability density function";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
regnormal
( const McCormick<T>&MC, const double a, const double b )
{
  if ( a <= 0. || b <= 0.  )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::REGNORMAL );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::regnormal( MC._I, a, b );

 // convex relaxation
 { int imid = -1;
    const double* cvenv = McCormick<T>::_regnormal_cv( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ), a, b, Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  // concave relaxation
  { int imid = -1;
    const double* ccenv = McCormick<T>::_regnormal_cc( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ), a, b, Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "regnormal";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
sqrt
( const McCormick<T>&MC )
{
  if ( Op<T>::l(MC._I) < 0. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SQRT );
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::sqrt( MC._I );

  { double r = 0.;
    if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
      r = ( std::sqrt( Op<T>::u(MC._I) ) - std::sqrt( Op<T>::l(MC._I) ) )
        / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
    int imid = -1;
    double vmid = mid_ndiff( MC._cv, MC._cc, Op<T>::l(MC._I), imid );
    MC2._cv = std::sqrt( Op<T>::l(MC._I) ) + r * ( vmid - Op<T>::l(MC._I) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }

  { int imid = -1;
    double vmid = mid_ndiff( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
	if(vmid > 0){
		 MC2._cc = std::sqrt( vmid );
		for( unsigned int i=0; i<MC2._nsub; i++ )
		  MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) / (2.*MC2._cc);
	}
	else{
		MC2._cc = Op<T>::u(MC2._I);
		for( unsigned int i=0; i<MC2._nsub; i++ )
			MC2._ccsub[i] = 0.0;
	}

  }
#ifdef MC__MCCORMICK_DEBUG
    std::string str = "sqrt";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
erfc
( const McCormick<T> &MC )
{
  return ( 1. - erf( MC ) );
}

template <typename T> inline McCormick<T>
erf
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::erf( MC._I );

  if( !McCormick<T>::options.ENVEL_USE ){
     MC2._cv = Op<T>::l(MC2._I);
     MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }
    return MC2;
  }

  { int imid = -1;
    const double* cvenv = McCormick<T>::_erfcv( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  { int imid = -1;
    const double* ccenv = McCormick<T>::_erfcc( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }
#ifdef MC__MCCORMICK_DEBUG
   	std::string str = "erf ";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif
  return MC2.cut();
}

template <typename T> inline McCormick<T>
pow
( const McCormick<T>&MC, const int n )
{
  if( n == 0 ){
    return 1.;
  }

  if( n == 1 ){
    return MC;
  }

  if( n == 2){
	  return sqr(MC);
  }
  if( n > 2 && !(n%2) ){ // even exponent
    McCormick<T> MC2;
    MC2._sub( MC._nsub, MC._const );
    MC2._I = Op<T>::pow( MC._I, n );
    { int imid = -1;
      double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
      imid = -1;
      MC2._cv = std::pow( mid( MC._cv, MC._cc, zmin, imid ), n );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = n * mid( MC._cvsub, MC._ccsub, i, imid )
          * std::pow( mid( MC._cv, MC._cc, zmin, imid ), n-1 );
    }
    { int imid = -1;
      double zmax = (std::pow( Op<T>::l(MC._I), n )>std::pow( Op<T>::u(MC._I), n ) ? Op<T>::l(MC._I) : Op<T>::u(MC._I));
      double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.: ( std::pow( Op<T>::u(MC._I), n ) - std::pow( Op<T>::l(MC._I), n ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) );
      MC2._cc = std::pow( zmax, n ) + r * ( mid( MC._cv, MC._cc, zmax, imid ) - zmax );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow1(MC, int)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();
  }

  if( n >= 3 && McCormick<T>::options.ENVEL_USE ){ // odd exponent
    McCormick<T> MC2;
    MC2._sub( MC._nsub, MC._const );
    MC2._I = Op<T>::pow( MC._I, n );
    { int imid = -1;
      const double* cvenv = McCormick<T>::_oddpowcv( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
      MC2._cv = cvenv[0];
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
      }
    }
    { int imid = -1;
      const double* ccenv = McCormick<T>::_oddpowcc( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
      MC2._cc = ccenv[0];
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
      }
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow2(MC, int)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();
  }

  if( n >= 3 ){
    return pow( MC, n-1 ) * MC;
  }

  if( n == -1 ){
    return inv( MC );
  }

  if ( Op<T>::l(MC._I) <= 0. && Op<T>::u(MC._I) >= 0. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::INV );
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::pow( MC._I, n );

  if ( Op<T>::l(MC._I) > 0. ){
    { int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
      MC2._cv = std::pow( vmid, n );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = n* mid( MC._cvsub, MC._ccsub, i, imid ) * std::pow( vmid, n-1 );
    }
    { double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
      for( int i=1; i<=-n-2; i++ )
         r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
      r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
      int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid );
      MC2._cc = std::pow( Op<T>::l(MC._I), n ) + r * ( vmid - Op<T>::l(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = r * mid( MC._cvsub, MC._ccsub, i, imid );
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow3(MC, int)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();
  }

  if( (-n)%2 ){
    { double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
      for( int i=1; i<=-n-2; i++ )
         r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
      r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
      int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
      MC2._cv = std::pow( Op<T>::u(MC._I), n ) + r * ( vmid - Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = r * mid( MC._cvsub, MC._ccsub, i, imid );
    }
    { int imid = -1;
      double vmid = mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid );
      MC2._cc = std::pow( vmid, n );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = n* mid( MC._cvsub, MC._ccsub, i, imid ) * std::pow( vmid, n-1 );
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow4(MC, int)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();
  }

  { int imid = -1;
    double vmid = mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid );
    MC2._cv = std::pow( vmid, n );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = n* mid( MC._cvsub, MC._ccsub, i, imid ) * std::pow( vmid, n-1 );
  }
    { double r = std::pow( Op<T>::l(MC._I), -n-1 ) + std::pow( Op<T>::u(MC._I), -n-1 );
      for( int i=1; i<=-n-2; i++ )
         r += std::pow( Op<T>::l(MC._I), i ) * std::pow( Op<T>::u(MC._I), -n-1-i );
      r /= - std::pow( Op<T>::l(MC._I), -n ) * std::pow( Op<T>::u(MC._I), -n );
    int imid = -1;
    double vmid = mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid );
    MC2._cc = std::pow( Op<T>::u(MC._I), n ) + r * ( vmid - Op<T>::u(MC._I) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = r * mid( MC._cvsub, MC._ccsub, i, imid );
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow5(MC, int)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
pow
( const McCormick<T> &MC, const double a )
{
  if ( Op<T>::l(MC._I) < 0.)
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::DPOW ); // no negative values allowed

  if( a==0. ){
    return 1.;
  }

  if( a==1. ){
    return MC;
  }

  if( a<0. ){
    return inv( pow(MC,-a) ); // if the exponent is negative simply compute (MC^a)^-1
  }

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::pow(MC._I,a); // compute the correct interval

	if(isequal(Op<T>::l(MC._I),Op<T>::u(MC._I))){ // in case of thin interval, simply use interval bounds
			MC2._cv = Op<T>::l(MC2._I);
			MC2._cc = Op<T>::u(MC2._I);
			for( unsigned int i=0; i<MC2._nsub; i++ ){
				MC2._cvsub[i] = 0.;
				MC2._ccsub[i] = 0.;
			}

		return MC2.cut();
	}

  if(a>1. && McCormick<T>::options.ENVEL_USE){ // for a > 1 the function is convex
    { int imid = -1; // convex
      double zmin = Op<T>::l(MC._I);
      MC2._cv = std::pow( mid( MC._cv, MC._cc, zmin, imid ), a ); // convex relaxation
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = a * mid( MC._cvsub, MC._ccsub, i, imid ) * std::pow( mid( MC._cv, MC._cc, zmin, imid ), a-1 );
    }
    { int imid = -1; // concave
      double zmax = Op<T>::u(MC._I);
      double r = (std::pow( Op<T>::u(MC._I), a ) - std::pow( Op<T>::l(MC._I), a ) ) / // slope of concave relaxation
                 ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
      MC2._cc = std::pow( Op<T>::u(MC._I), a ) + r * ( mid( MC._cv, MC._cc, zmax,imid ) - Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow(MC, double)";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();

  }

  if(a<1. && McCormick<T>::options.ENVEL_USE){ // for 0< a < 1 the function is concave
    { int imid = -1; //convex
      double zmin = Op<T>::l(MC._I);
      double r = (std::pow( Op<T>::u(MC._I), a ) - std::pow( Op<T>::l(MC._I), a ) ) / // slope of convex relaxation
                 ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) ;
      MC2._cv = std::pow( Op<T>::l(MC._I), a ) + r * ( mid( MC._cv, MC._cc, zmin,imid ) - Op<T>::l(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] =  mid( MC._cvsub, MC._ccsub, i, imid ) * r;
    }
    { int imid = -1; //concave
      double zmax = Op<T>::u(MC._I);
      MC2._cc = std::pow( mid( MC._cv, MC._cc, zmax, imid ), a );  // concave relaxation
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = a * mid( MC._cvsub, MC._ccsub, i, imid )* std::pow( mid( MC._cv, MC._cc, zmax, imid ), a-1 );
    }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "pow(MC, double <1 )";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

    return MC2.cut();

  }

  return exp( a * log( MC ) ); // if no envelope is required simply use exp(a*log(MC)) resulting in worse relaxations
                               // and not allowing 0. in interval since log(0.) is not defined
}

template <typename T> inline McCormick<T>
pow
( const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  return exp( MC2 * log( MC1 ) );
}

template <typename T> inline McCormick<T>
pow
( const double a, const McCormick<T> &MC )
{
  return exp( MC * std::log( a ) );
}

template <typename T> inline McCormick<T>
prod
( const unsigned int n, const McCormick<T>*MC )
{
  switch( n ){
   case 0:  return 1.;
   case 1:  return MC[0];
   default: return MC[0] * prod( n-1, MC+1 );
  }
}

template <typename T> inline McCormick<T>
monom
( const unsigned int n, const McCormick<T>*MC, const unsigned*k )
{
  switch( n ){
   case 0:  return 1.;
   case 1:  return pow( MC[0], (int)k[0] );
   default: return pow( MC[0], (int)k[0] ) * monom( n-1, MC+1, k+1 );
  }
}

template <typename T> inline McCormick<T>
cheb
( const McCormick<T> &MC, const unsigned n )
{
  if ( !isequal(Op<T>::l(MC._I),-1.) || !isequal(Op<T>::u(MC._I),1.) )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::CHEB );

  switch( n ){
    case 0:  return 1.;
    case 1:  return MC;
    case 2:  return 2*sqr(MC)-1;
    default: break;
  }

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::cheb( MC._I, n );
  if( !(n%2) ){
    { int imid = -1;
      const double* cvenv = McCormick<T>::_evenchebcv( mid( MC._cv,
        MC._cc, Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
      MC2._cv = cvenv[0];
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
      }
    }
    { MC2._cc = 1.;
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._ccsub[i] = 0.;
    }
  }
  else{
    { int imid = -1;
      const double* cvenv = McCormick<T>::_oddchebcv( mid( MC._cv,
        MC._cc, Op<T>::l(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
      MC2._cv = cvenv[0];
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
      }
    }
    { int imid = -1;
      const double* ccenv = McCormick<T>::_oddchebcc( mid( MC._cv,
        MC._cc, Op<T>::u(MC._I), imid ), n, Op<T>::l(MC._I), Op<T>::u(MC._I) );
      MC2._cc = ccenv[0];
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
      }
    }
  }
  //McCormick<T> MCcheb = 2.*MC*cheb(MC,n-1)-cheb(MC,n-2);
  //return( inter( MCcheb, MCcheb, McCormick<T>(T(-1.,1.)) )? MCcheb: McCormick<T>(T(-1.,1.))

  return MC2.cut();
}

template <typename T> inline McCormick<T>
fabs
( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::fabs( MC._I );

  { int imid = -1;
    double zmin = mid_ndiff( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
    imid = -1;
    double vmid = mid_ndiff( MC._cv, MC._cc, zmin, imid );
    MC2._cv = std::fabs( vmid );
    if( vmid >= 0. )
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid );
    else
      for( unsigned int i=0; i<MC2._nsub; i++ )
        MC2._cvsub[i] = - mid( MC._cvsub, MC._ccsub, i, imid );
  }

  { int imid = -1;
    double zmax = (std::fabs( Op<T>::l(MC._I) )>std::fabs( Op<T>::u(MC._I) )? Op<T>::l(MC._I):
      Op<T>::u(MC._I));
    double r = ( isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) )? 0.: ( std::fabs( Op<T>::u(MC._I) )- std::fabs( Op<T>::l(MC._I) ) ) / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) ) );
    MC2._cc = std::fabs( zmax ) + r * ( mid_ndiff( MC._cv, MC._cc, zmax, imid ) - zmax );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }
#ifdef MC__MCCORMICK_DEBUG
   	std::string str = "fabs ";
	McCormick<T>::_debug_check(MC, MC2, str);
#endif

  return MC2.cut();
}

template <typename T> inline McCormick<T>
min
( const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  McCormick<T> MC3;
  if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );
  MC3._I = Op<T>::min( MC1._I, MC2._I );

  if( Op<T>::u(MC1._I) <= Op<T>::l(MC2._I) ){
    MC3._cv = MC1._cv;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._cvsub[i] = (MC1._const? 0.: MC1._cvsub[i]);
  }
  else if( Op<T>::u(MC2._I) <= Op<T>::l(MC1._I) ){
    MC3._cv = MC2._cv;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._cvsub[i] = (MC2._const? 0.: MC2._cvsub[i]);
  }
  else if( McCormick<T>::options.MVCOMP_USE ){
     double minL1L2 = std::min( Op<T>::l(MC1._I), Op<T>::l(MC2._I) );
     double minL1U2 = std::min( Op<T>::l(MC1._I), Op<T>::u(MC2._I) );
     double minU1L2 = std::min( Op<T>::u(MC1._I), Op<T>::l(MC2._I) );
     double minU1U2 = std::min( Op<T>::u(MC1._I), Op<T>::u(MC2._I) );

     bool thin1 = isequal( Op<T>::diam(MC1._I), 0. );
     double r11 = ( thin1?  0.: ( minU1L2 - minL1L2 ) / Op<T>::diam(MC1._I) );
     double r21 = ( thin1?  0.: ( minL1U2 - minU1U2 ) / Op<T>::diam(MC1._I) );

     bool thin2 = isequal( Op<T>::diam(MC2._I), 0. );
     double r12 = ( thin2?  0.: ( minL1U2 - minL1L2 ) / Op<T>::diam(MC2._I) );
     double r22 = ( thin2?  0.: ( minU1L2 - minU1U2 ) / Op<T>::diam(MC2._I) );

     double g1cv = minL1L2 + r11 * ( MC1._cv - Op<T>::l(MC1._I) )
                           + r12 * ( MC2._cv - Op<T>::l(MC2._I) );
     double g2cv = minU1U2 - r21 * ( MC1._cv - Op<T>::u(MC1._I) )
                           - r22 * ( MC2._cv - Op<T>::u(MC2._I) );

	 if(thin1 || thin2){
		MC3._cv = g1cv;
		for( unsigned int i=0; i< MC3._nsub; i++ )
        MC3._cvsub[i] = (MC1._const? 0.: r11*MC1._cvsub[i])
                      + (MC2._const? 0.: r12*MC2._cvsub[i]);
	 }
	else{
		 if( g1cv > g2cv ){
		   MC3._cv = g1cv;
		  for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._cvsub[i] = (MC1._const? 0.: r11*MC1._cvsub[i])
						  + (MC2._const? 0.: r12*MC2._cvsub[i]);
		 }
		 else{
		   MC3._cv = g2cv;
		  for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._cvsub[i] = - (MC1._const? 0.: r21*MC1._cvsub[i])
							- (MC2._const? 0.: r22*MC2._cvsub[i]);
		 }
	}
  }
  else{
    McCormick<T> MCMin = 0.5*( MC1 + MC2 - fabs( MC2 - MC1 ) );
    MC3._cv = MCMin._cv;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._cvsub[i] = MCMin._cvsub[i];
  }

  MC3._cc = std::min( MC1._cc, MC2._cc );
  for( unsigned int i=0; i< MC3._nsub; i++ )
    MC3._ccsub[i] = ( MC1._cc<=MC2._cc? (MC1._const? 0.: MC1._ccsub[i])
                                      : (MC2._const? 0.: MC2._ccsub[i]) );

#ifdef MC__MCCORMICK_DEBUG
	std::string str = "min(MC,MC)";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif

  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return  MC3.cut();
}

template <typename T> inline McCormick<T>
max
( const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  McCormick<T> MC3;
  if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );
  MC3._I = Op<T>::max( MC1._I, MC2._I );

  if( Op<T>::u(MC1._I) <= Op<T>::l(MC2._I) ){
    MC3._cc = MC2._cc;										// Changed from ... = MC1._cc;, AVT.SVT on 04/04/16
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._ccsub[i] = (MC2._const? 0.: MC2._ccsub[i]);		// Changed from MC1. ..., AVT.SVT on 04/04/16
  }
  else if( Op<T>::u(MC2._I) <= Op<T>::l(MC1._I) ){
    MC3._cc = MC1._cc;										// Changed from ... = MC2._cc;, AVT.SVT on 04/04/16
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._ccsub[i] = (MC1._const? 0.: MC1._ccsub[i]);		// Changed from MC2. ..., AVT.SVT on 04/04/16
  }
  else if ( McCormick<T>::options.MVCOMP_USE ){
     double maxL1L2 = std::max( Op<T>::l(MC1._I), Op<T>::l(MC2._I) );
     double maxL1U2 = std::max( Op<T>::l(MC1._I), Op<T>::u(MC2._I) );
     double maxU1L2 = std::max( Op<T>::u(MC1._I), Op<T>::l(MC2._I) );
     double maxU1U2 = std::max( Op<T>::u(MC1._I), Op<T>::u(MC2._I) );

     bool thin1 = isequal( Op<T>::diam(MC1._I), 0., McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL);
     double r11 = ( thin1?  0.: ( maxU1L2 - maxL1L2 ) / Op<T>::diam(MC1._I) );
     double r21 = ( thin1?  0.: ( maxL1U2 - maxU1U2 ) / Op<T>::diam(MC1._I) );

     bool thin2 = isequal( Op<T>::diam(MC2._I), 0., McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL);
     double r12 = ( thin2?  0.: ( maxL1U2 - maxL1L2 ) / Op<T>::diam(MC2._I) );
     double r22 = ( thin2?  0.: ( maxU1L2 - maxU1U2 ) / Op<T>::diam(MC2._I) );

     double g1cc = maxL1L2 + r11 * ( MC1._cc - Op<T>::l(MC1._I) )
                           + r12 * ( MC2._cc - Op<T>::l(MC2._I) );
     double g2cc = maxU1U2 - r21 * ( MC1._cc - Op<T>::u(MC1._I) )
                           - r22 * ( MC2._cc - Op<T>::u(MC2._I) );
     if(thin1 || thin2){
		MC3._cc = g2cc;
	    for( unsigned int i=0; i< MC3._nsub; i++ )
		  MC3._ccsub[i] = - (MC1._const? 0.: r21*MC1._ccsub[i])
						- (MC2._const? 0.: r22*MC2._ccsub[i]);
	 }
	 else{
		 if( g2cc > g1cc ){
		   MC3._cc = g1cc;
		  for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._ccsub[i] = (MC1._const? 0.: r11*MC1._ccsub[i])
						  + (MC2._const? 0.: r12*MC2._ccsub[i]);
		 }
		 else{
		   MC3._cc = g2cc;
		  for( unsigned int i=0; i< MC3._nsub; i++ )
			MC3._ccsub[i] = - (MC1._const? 0.: r21*MC1._ccsub[i])
							- (MC2._const? 0.: r22*MC2._ccsub[i]);
		 }
	 }
  }
  else{
    McCormick<T> MCMax = 0.5*( MC1 + MC2 + fabs( MC1 - MC2 ) );
    MC3._cc = MCMax._cc;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._ccsub[i] = MCMax._ccsub[i];
  }

  MC3._cv = std::max( MC1._cv, MC2._cv );
  for( unsigned int i=0; i< MC3._nsub; i++ ){
    MC3._cvsub[i] = ( MC1._cv>=MC2._cv? (MC1._const? 0.: MC1._cvsub[i])
                                      : (MC2._const? 0.: MC2._cvsub[i]) );
  }
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "max(MC,MC)";
	McCormick<T>::_debug_check(MC1, MC2, MC3, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return  MC3.cut();
}

///////////////////////////////////////////////////////////////////////////////////
// Added @ AVT.SVT, Aug 30, 2016
template <typename T> inline McCormick<T>
max
( const McCormick<T> &MC, const double a  ) {
	return max( a, MC );
}

template <typename T> inline McCormick<T>
max
( const double a, const McCormick<T> &MC ) {

	McCormick<T> MC2 = a;
	return max( MC, MC2 );
}

template <typename T> inline McCormick<T>
min
( const McCormick<T> &MC, const double a  ) {
	McCormick<T> MC2 = a;
	return min( MC, MC2 );
}

template <typename T> inline McCormick<T>
min
( const double a, const McCormick<T> &MC ) {
	McCormick<T> MC2 = a;
	return min( MC, MC2 );
}
///////////////////////////////////////////////////////////////////////////////////

template <typename T> inline McCormick<T>
min
( const unsigned int n, const McCormick<T>*MC )
{
  McCormick<T> MC2( n==0 || !MC ? 0.: MC[0] );
  for( unsigned int i=1; i<n; i++ ) MC2 = min( MC2, MC[i] );
  return MC2;
}

template <typename T> inline McCormick<T>
max
( const unsigned int n, const McCormick<T>*MC )
{
  McCormick<T> MC2( n==0 || !MC ? 0.: MC[0] );
  for( unsigned int i=1; i<n; i++ ) MC2 = max( MC2, MC[i] );
  return MC2;
}

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 27.06.2017
//Note that we only change the convex relaxation and the lower interval bound
template <typename T> inline McCormick<T>
pos
( const McCormick<T> &MC  ) {
	if(MC._cc < McCormick<T>::options.MVCOMP_TOL){
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::POS );
	}
	McCormick<T> MC2(MC);
	MC2._I = Op<T>::max(MC._I, McCormick<T>::options.MVCOMP_TOL);
	MC2._cv = std::max(MC._cv, McCormick<T>::options.MVCOMP_TOL);
	for( unsigned int i=0; i< MC2._nsub; i++ ){
	    MC2._cvsub[i] = ( MC._cv>=McCormick<T>::options.MVCOMP_TOL? (MC._const? 0.: MC._cvsub[i]) : 0.);
	}
#ifdef MC__MCCORMICK_DEBUG

	std::string str = "pos";
	McCormick<T>::_debug_check(MC, MC2, str);

#endif

	return MC2;
}
//////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 25.07.2017
//Note that we only change the concave relaxation and the upper interval bound
template <typename T> inline McCormick<T>
neg
( const McCormick<T> &MC  ) {
	if(MC._cv > -McCormick<T>::options.MVCOMP_TOL){
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::NEGAT );
	}
	McCormick<T> MC2(MC);
	MC2._I = Op<T>::min(MC._I, -McCormick<T>::options.MVCOMP_TOL);
	MC2._cc = std::min(MC._cc, -McCormick<T>::options.MVCOMP_TOL);
	for( unsigned int i=0; i< MC2._nsub; i++ ){
		MC2._ccsub[i] = ( MC._cc<=-McCormick<T>::options.MVCOMP_TOL? (MC._const? 0.: MC._ccsub[i]): 0. );
}
#ifdef MC__MCCORMICK_DEBUG

	std::string str = "neg";
	McCormick<T>::_debug_check(MC, MC2, str);

#endif
	return MC2;
}
//////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//added @AVT.SVT 28.09.2017
//Note that we only change the convex relaxation and the upper interval bound
template <typename T> inline McCormick<T>
lb_func
( const McCormick<T> &MC1, const double lb  ) {

#ifdef MC__MCCORMICK_DEBUG
	std::string str = "lb_func";
	McCormick<T> dummy(lb);
	McCormick<T>::_debug_check(MC1,dummy,str);
#endif
	if(MC1._cc < lb && !isequal(MC1._cc, lb, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL)){
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::LB_FUNC );
	}
    McCormick<T> MC2(MC1);
    MC2._I = Op<T>::max( MC1._I, lb);
	MC2._cv = std::max(MC1._cv,lb);
	for( unsigned int i=0; i< MC2._nsub; i++ ){
	    MC2._cvsub[i] = ( MC1._cv>lb? (MC1._const? 0.: MC1._cvsub[i]) : 0.);
	}

	return MC2.cut();
}

//Note that we only change the concave relaxation and the upper interval bound
template <typename T> inline McCormick<T>
ub_func
( const McCormick<T> &MC1, const double ub  ) {
#ifdef MC__MCCORMICK_DEBUG
	std::string str = "ub_func";
	McCormick<T> dummy(ub);
	McCormick<T>::_debug_check(MC1,dummy,str);
#endif
    if(MC1._cv > ub && !isequal(MC1._cv, ub, McCormick<T>::options.MVCOMP_TOL, McCormick<T>::options.MVCOMP_TOL)){
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::UB_FUNC );
	}
    McCormick<T> MC2(MC1);
    MC2._I = Op<T>::min( MC1._I, ub);
	MC2._cc = std::min(MC1._cc,ub);
	for( unsigned int i=0; i< MC2._nsub; i++ ){
	    MC2._ccsub[i] = ( MC1._cc<ub? (MC1._const? 0.: MC1._ccsub[i]) : 0.);
	}

	return MC2.cut();
}

template <typename T> inline McCormick<T>
bounding_func
( const McCormick<T> &MC, const double lb, const double ub  ) {
    McCormick<T> MC2 = lb_func(MC,lb);
	return ub_func(MC2,ub);
}

//Note that we don't throw any exception here, since when using this function, the user has to make sure (through linear inequalities) that x is only feasible for [lb,ub]
template <typename T> inline McCormick<T>
squash_node
( const McCormick<T> &MC, const double lb, const double ub  ) {

    McCormick<T> MC2(MC);
    MC2._I = Op<T>::squash_node(MC._I, lb, ub);
	MC2._cv = MC2._cv>ub ? lb : std::max(MC._cv,lb);
	MC2._cc = MC2._cc<lb ? ub : std::min(MC._cc,ub);
	for( unsigned int i=0; i< MC2._nsub; i++ ){
	    MC2._cvsub[i] = ( MC._cv>lb? (MC._const? 0.: MC._cvsub[i]) : 0.);
	    MC2._ccsub[i] = ( MC._cc<ub? (MC._const? 0.: MC._ccsub[i]) : 0.);
	}
	return MC2.cut();
}

// AVT.SVT 10.07.2018
template <typename T> inline McCormick<T>
sum_div
( const std::vector< McCormick<T> > &MC, const std::vector<double> &coeff)
{
	McCormick<T> MC3;
	// check for possible subgradient exceptions
	bool all_const = true;
	std::vector<unsigned int> not_const = {};
	for(unsigned int i=0; i<MC.size();i++){
	  if( !MC[i]._const ){
		not_const.push_back(i);
		all_const = false;
	  }
	}
	if(all_const){
		MC3._sub(MC[0]._nsub, MC[0]._const);
	}else{
		for(unsigned int i = 0; i<not_const.size()-1;i++){
			if(MC[not_const[i]]._nsub != MC[not_const[i+1]]._nsub){
				throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
			}
		}
		MC3._sub(MC[not_const[0]]._nsub, MC[not_const[0]]._const);
	}

	std::vector<T> MCI(MC.size());
	for(unsigned int i=0;i<MC.size();i++){
		// check positivity of each variable
		 if ( Op<T>::l(MC[i]._I) <= 0. ){
			 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUM_DIV_VAR );
		 }
		 // save intervals
		 MCI[i] = MC[i]._I;
		 // check positivity of coefficients
		 if ( coeff[i] <= 0. ){
			 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUM_DIV_COEFF );
		 }
	}
	// check last coefficient
	if ( coeff[coeff.size()-1] <= 0. ){
		throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUM_DIV_COEFF );
	}
	// compute interval extensions
	MC3._I=Op<T>::sum_div(MCI,coeff);

	// auxiliary variables to avoid re-computations
	double xL=Op<T>::l(MC[0]._I);
	double xU=Op<T>::u(MC[0]._I);
	double s1 = MC[0]._cv + std::sqrt(xL)*std::sqrt(xU);			// (xcv + sqrt(xL)*sqrt(xU))
	double s2 = std::pow(std::sqrt(xL) + std::sqrt(xU), 2); 		// (sqrt(xL) + sqrt(xU))^2
	double s3 = std::pow(MC[0]._cv+std::sqrt(xL)*std::sqrt(xU), 2); // (x + sqrt(xL)*sqrt(xU))^2
	double scv4 = coeff[1]*MC[0]._cv;								// c*xcv
	double scc4 = coeff[1]*MC[0]._cc;								// c*xcc
	std::vector<double> yL(MC.size()-1);
	std::vector<double> yU(MC.size()-1);
	for(unsigned int i =1;i<MC.size();i++){
		yL[i-1] = Op<T>::l(MC[i]._I);
		yU[i-1] = Op<T>::u(MC[i]._I);
		scv4 += coeff[i+1]*MC[i]._cc;								// c*xcv + d1*y1cc + d2*y2cc + ...
		scc4 += coeff[i+1]*MC[i]._cv;								// c*xcc + d1*y1cv + d2*y2cv + ...
	}
	double scv5 = std::pow(scv4, 2);								// (c*xcv + d1*y1cc + d2*y2cc + ...)^2
	double scc5 = std::pow(scc4, 2);								// (c*xcc + d1*y1cv + d2*y2cv + ...)^2

	bool is_cvenvelope = true;										// under certain conditions, we only get the convex envelope if we compute additional affine relaxation,
																	// this is expensive in higher dimensions and the gain is not high in these cases,
																	// since we still have the exact range bounds, so just stick to the classic McCormick
	double l1=coeff[1]*std::sqrt(xL)*std::sqrt(xU),l2=coeff[1]*std::sqrt(xL)*std::sqrt(xU);
	for(unsigned int i=0;i<MC.size()-1;i++){
		l1 -= 2*coeff[i+2]*yL[i];
		l2 -= 2*coeff[i+2]*yU[i];
	}
	if((l1/coeff[1]>xL && l1/coeff[1]<xU) || ( l2/coeff[2]>xL || l2/coeff[2]<xU) ){ is_cvenvelope = false;}

	if(is_cvenvelope){
		// convex relaxation   a*(xcv + sqrt(xL)*sqrt(xU))^2 / ( (sqrt(xL)+sqrt(xU))^2 * (c*xcv + d1*y1cc + d2*y2cc + ... )
		MC3._cv = (coeff[0]*std::pow(s1,2))/(s2*scv4);
		// subgradient convex
		for(unsigned int i = 0; i< MC3._nsub;i++){
			// subgradient convex first term
			MC3._cvsub[i] = (2*s1/(s2*scv4) - coeff[1]*s3/(s2*scv5) ) *(MC[0]._const? 0.:MC[0]._cvsub[i]); // for x we take cv
			// subgradient convex following terms
			for(unsigned int j = 1; j< MC.size();j++){
				MC3._cvsub[i] -= coeff[j+1]*s3/(s2*scv5)*(MC[j]._const? 0.:MC[j]._ccsub[i]); 			   // for everything else cc
			}
			MC3._cvsub[i] *= coeff[0];
		}
	} // not the convex envelope
	else{
		McCormick<T> MC1(coeff[0]*MC[0]);
		McCormick<T> MC2(coeff[1]*MC[0]);
		for(unsigned int i=1; i< MC.size();i++){
			MC2 += coeff[i+1]*MC[i];
		}
		// use convex envelope for x/y
		int imidcv1 = 1, imidcv2 = 2; // We can easily solve the convex problem given in Theorem 2 in Tsoukalas & Mitsos 2014,
									  // since the minimum of the convex relaxation is at point f1cv, f2cc, which can be shown through directional derivatives
		double fmidcv1 = ( mid(MC1._cv, MC1._cc, Op<T>::l(MC1._I), imidcv1)+ std::sqrt(Op<T>::l(MC1._I) * Op<T>::u(MC1._I)) )/ ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) );
		double fmidcv2 = mid(MC2._cv, MC2._cc, Op<T>::u(MC2._I), imidcv2);
		MC3._cv = mc::sqr(fmidcv1) / fmidcv2;
		for( unsigned int i=0; i<MC3._nsub; i++ )
		  MC3._cvsub[i] = 2. * fmidcv1 / fmidcv2 / ( std::sqrt(Op<T>::l(MC1._I)) + std::sqrt(Op<T>::u(MC1._I)) )* (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcv1 ))
			- mc::sqr( fmidcv1 / fmidcv2 )* (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcv2 ));
	}

	bool is_ccenvelope = true;										// under certain conditions, we only get the concave envelope if we compute additional affine relaxation,
																	// this is expensive in higher dimensions (much more expensive than for convex) and the gain is not high in these cases,
																	// since we have exact range bounds, so just stick to the classic McCormick
	if(MC.size()==2){ // for 2-dim case, the formula differs slightly
		double p1 = (coeff[2]*std::sqrt(yL[0]*yU[0])-2*coeff[1]*xL)/coeff[2];
		double p2 = (coeff[2]*std::sqrt(yL[0]*yU[0])-2*coeff[1]*xU)/coeff[2];
		if((p1>yL[0] && p1<yU[0]) || ( p2>yL[0] && p2<yU[0]) ){ is_ccenvelope = false;}
	}
	else{	// check if the maximum of the concave relaxation is within bounds => then it's not the envelope anymore
		for(unsigned int i = 1; i<MC.size() && is_ccenvelope;i++){
			double l1 = coeff[1]*xU+coeff[i+1]*yL[i-1], l2 = coeff[1]*xL+coeff[i+1]*yU[i-1];
			double k1 = std::pow(coeff[1]*xU,2) + std::pow(coeff[i+1],2)*yL[i-1]*yU[i-1]-2*coeff[1]*coeff[i+1]*xU*std::sqrt(yL[i-1]*yU[i-1]);
			double k2 = std::pow(coeff[1]*xL,2) + std::pow(coeff[i+1],2)*yL[i-1]*yU[i-1]-2*coeff[1]*coeff[i+1]*xL*std::sqrt(yL[i-1]*yU[i-1]);
			double s1=0,s2=0;
			for(unsigned int j = 1; j<MC.size(); j++){
				if(j!=i){
					l1 += coeff[j+1]*yL[j-1]; l2 += coeff[j+1]*yU[j-1];
					s1 += std::pow(coeff[j+1]*yL[j-1],2) + (2*coeff[1]*xU*coeff[j+1] + coeff[i+1]*coeff[j+1]*(yL[i-1] +yU[i-1]))*yL[j-1];
					s2 += std::pow(coeff[j+1]*yU[j-1],2) + (2*coeff[1]*xL*coeff[j+1] + coeff[i+1]*coeff[j+1]*(yL[i-1] +yU[i-1]))*yU[j-1];
				}
				double mix1=0,mix2=0;
				for(unsigned int k=j+1;k<MC.size();k++){
					if(k!=i){
						mix1 += 2*coeff[j+1]*coeff[k+1]*yL[j-1]*yL[k-1];
						mix2 += 2*coeff[j+1]*coeff[k+1]*yU[j-1]*yU[k-1];
					}
				}
				s1 += mix1; s2 += mix2;
			}
			double res1 = -(l1-std::sqrt(s1))/coeff[i+1] ,res2 = -(l2-std::sqrt(s2))/coeff[i+1];
			if((res1>yL[i-1] && res1<yU[i-1]) || ( res2>yL[i-1] && res2<yU[i-1]) ){ is_ccenvelope = false;}
		}
	}
	if(is_ccenvelope){
		// concave relaxation a*(xcc/(c*xcc + d1*y1cv + ...) + d1*(yL1-y1cv)*(y1cv - yU1)/(c*(sqrt(yL1)+sqrt(yU1)^2 * (c*xcc + d1*y1cv + ...)) + d2*(yL2-y2cv)*(y2cv - yU2)/(c*(sqrt(yL2)+sqrt(yU2)^2 * (c*xcc + d1*y2cv + ...)) + ...
		MC3._cc= MC[0]._cc / scc4;
		for(unsigned int i = 1; i < MC.size(); i++){
			MC3._cc += coeff[i+1]*(yL[i-1] - MC[i]._cv)*(MC[i]._cv - yU[i-1])/( coeff[1]*std::pow( std::sqrt(yL[i-1])+std::sqrt(yU[i-1]), 2) * scc4 );
		}
		MC3._cc *= coeff[0];
		// subgradient concave
		for( unsigned int i=0; i<MC3._nsub; i++ ){
			double temp = 0;
			temp += 1/scc4 - coeff[1]*MC[0]._cc/scc5;
			for(unsigned int j = 1; j< MC.size();j++){
				temp += coeff[j+1]*(MC[j]._cv - yL[j-1])*(MC[j]._cv - yU[j-1])/( std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2) * scc5 );
			}

			MC3._ccsub[i] = coeff[0]*temp*(MC[0]._const? 0.:MC[0]._ccsub[i]);		// for x we take cc
			for(unsigned int j = 1; j< MC.size();j++){
				temp = 0;
				temp += coeff[j+1]*(MC[j]._cv - yL[j-1])*(MC[j]._cv - yU[j-1])/( coeff[1]*std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2) * scc5 )
						- ((MC[j]._cv - yL[j-1]) + (MC[j]._cv - yU[j-1]))/(coeff[1]*std::pow( std::sqrt(yL[j-1])+std::sqrt(yU[j-1]), 2)*scc4)
						- MC[0]._cc/scc5;
				for(unsigned int k = 1; k < MC.size();k++){
					if(j != k){
						temp += coeff[k+1]*(MC[k]._cv - yL[k-1])*(MC[k]._cv - yU[k-1])/( coeff[1]*std::pow( std::sqrt(yL[k-1])+std::sqrt(yU[k-1]), 2) * scc5 );
					}
				}
				MC3._ccsub[i] += coeff[0]*coeff[j+1]*temp*(MC[j]._const? 0.:MC[j]._cvsub[i]);	// for everything else cv
			}
		}
	} // not concave envelope
	else{
		McCormick<T> MC1(coeff[0]*MC[0]);
		McCormick<T> MC2(coeff[1]*MC[0]);
		for(unsigned int i=1; i< MC.size();i++){
			MC2 += coeff[i+1]*MC[i];
		}
		int imidcc1 = -1, imidcc2 = -1;
		// use concave envelope for x/y
		double fmidcc1 = mid_ndiff(MC1._cv, MC1._cc, Op<T>::u(MC1._I), imidcc1);
		double fmidcc2 = mid_ndiff(MC2._cv, MC2._cc, Op<T>::l(MC2._I), imidcc2);
		double gcc1 = Op<T>::u(MC2._I) * fmidcc1 - Op<T>::l(MC1._I) * fmidcc2 + Op<T>::l(MC1._I) * Op<T>::l(MC2._I);
		double gcc2 = Op<T>::l(MC2._I) * fmidcc1 - Op<T>::u(MC1._I) * fmidcc2 + Op<T>::u(MC1._I) * Op<T>::u(MC2._I);

		if(  gcc1 <= gcc2 ){ //uses equation (31) in multivariate McCormick paper
		  MC3._cc = gcc1 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
		  for( unsigned int i=0; i<MC3._nsub; i++ ){
			MC3._ccsub[i] = 1. / Op<T>::l(MC2._I) * (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcc1 ))
			  - Op<T>::l(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) ) * (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcc2 ));
		  }
		}
		else{
		  MC3._cc = gcc2 / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) );
		  for( unsigned int i=0; i<MC3._nsub; i++ )
			MC3._ccsub[i] = 1. / Op<T>::u(MC2._I) * (MC1._const? 0.: mid( MC1._cvsub, MC1._ccsub, i, imidcc1 ))
			  - Op<T>::u(MC1._I) / ( Op<T>::l(MC2._I) * Op<T>::u(MC2._I) ) * (MC2._const? 0.: mid( MC2._cvsub, MC2._ccsub, i, imidcc2 ));
		}
	}

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "sum_div";
	McCormick<T>::_debug_check(MC, MC3, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}

// AVT.SVT 10.07.2018
template <typename T> inline McCormick<T>
xlog_sum
( const std::vector< McCormick<T> > &MC, const std::vector<double> &coeff)
{
	McCormick<T> MC3;
	// check for possible subgradient exceptions
	bool all_const = true;
	std::vector<unsigned int> not_const = {};
	for(unsigned int i=0; i<MC.size();i++){
	  if( !MC[i]._const ){
		not_const.push_back(i);
		all_const = false;
	  }
	}
	if(all_const){
		MC3._sub(MC[0]._nsub, MC[0]._const);
	}
	else{
		for(unsigned int i = 0; i<not_const.size()-1;i++){
			if(MC[not_const[i]]._nsub != MC[not_const[i+1]]._nsub){
				throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
			}
		}
		MC3._sub(MC[not_const[0]]._nsub, MC[not_const[0]]._const);
	}

	// Special case x*log(a*x), it is anologous to xlog, the only difference is in zmin and the derivative
	if(MC.size()==1){
		// check positivity of each variable
		if ( Op<T>::l(MC[0]._I) <= 0. ){
		 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::XLOG_SUM_VAR );
		}
		// check positivity of coefficients
		if ( coeff[0] <= 0. ){
		 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::XLOG_SUM_COEFF );
		}
		std::vector<T> interval = {MC[0]._I};
		MC3._I = Op<T>::xlog_sum(interval,coeff);
		int imid = -1;
		double zmin = mid( Op<T>::l(MC[0]._I), Op<T>::u(MC[0]._I), std::exp(-1.)/coeff[0], imid );
		imid = -1;
		double vmid = mid( MC[0]._cv, MC[0]._cc, zmin, imid );
		MC3._cv = vmid * std::log(coeff[0]*vmid);
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  MC3._cvsub[i] = (std::log(coeff[0]* vmid ) + 1.) * mid( MC[0]._cvsub, MC[0]._ccsub, i, imid );
		}

		imid = -1;
		double valL = Op<T>::l(MC[0]._I)*std::log(coeff[0]*Op<T>::l(MC[0]._I));
		double valU = Op<T>::u(MC[0]._I)*std::log(coeff[0]*Op<T>::u(MC[0]._I));
		double zmax = ( valU >= valL)? Op<T>::u(MC[0]._I): Op<T>::l(MC[0]._I) ;
		double r, pt;
		if( isequal( Op<T>::l(MC[0]._I), Op<T>::u(MC[0]._I) )){
			r = 0.;
			pt = zmax;
		}
		else {
			r = ( valU - valL ) / ( Op<T>::u(MC[0]._I) - Op<T>::l(MC[0]._I) );
			pt = Op<T>::l(MC[0]._I);
		}
		imid = -1;
		MC3._cc = pt*std::log(coeff[0]*pt) + r * ( mid( MC[0]._cv, MC[0]._cc, zmax, imid ) - pt );
		for( unsigned int i=0; i<MC3._nsub; i++ )
		  MC3._ccsub[i] = mid( MC[0]._cvsub, MC[0]._ccsub, i, imid ) * r;

#ifdef MC__MCCORMICK_DEBUG
			std::string str = "xlog_sum 1 dim";
			McCormick<T>::_debug_check(MC[0], MC3, str);
#endif
		  // if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
		  return MC3.cut();
	}

	// In the case of thin X interval, we simply return the normal McCormick relaxation. We do this to avoid numerical problems in _newton
	if(isequal(Op<T>::l(MC[0]._I),Op<T>::u(MC[0]._I))){
		MC3 = 0;
		for(size_t i = 0;i<MC.size();i++){
			MC3 += coeff[i]*MC[i];
		}
		MC3 = MC[0]*log(MC3);
	    // if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
		return MC3.cut();
	}

	// Dimension > 1
	std::vector<T> MCI(MC.size());
	std::vector<double> intervalLowerBounds(MC.size());
	std::vector<double> intervalUpperBounds(MC.size());
	std::vector<double> pointCv(MC.size());
	std::vector<double> pointCc(MC.size());
	for(unsigned int i=0;i<MC.size();i++){
		// check positivity of each variable
		 if ( Op<T>::l(MC[i]._I) <= 0. ){
			 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::XLOG_SUM_VAR );
		 }
		 // check positivity of coefficients
		 if ( coeff[i] <= 0. ){
			 throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::XLOG_SUM_COEFF );
		 }
		 // save intervals
		 MCI[i] = MC[i]._I;
		 intervalLowerBounds[i] = Op<T>::l(MC[i]._I);
		 intervalUpperBounds[i] = Op<T>::u(MC[i]._I);
		 pointCv[i] = MC[i]._cv;
		 pointCc[i] = MC[i]._cc;

	}
	// compute interval extensions
	MC3._I=Op<T>::xlog_sum(MCI,coeff);

	// convex is currently not possible if dimension > 2, so we simply use the normal McCormick relaxations
	if(MC.size()>2){
		McCormick<T> MCtmp;
		MCtmp = 0.;
		for(size_t i = 0;i<MC.size();i++){
			MCtmp += coeff[i]*MC[i];
		}
		MCtmp = MC[0]*log(MCtmp);
		// convex is currently not possible if dimension > 2
		MC3._cv= MCtmp._cv;
		const double* ccrel = McCormick<T>::_xlog_sum_cc(pointCc,MC[0]._cv,coeff,intervalLowerBounds,intervalUpperBounds);
		MC3._cc = ccrel[0];
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  double subgradientCv = 0;
		  double subgradientCc = 0;
		  // In the case of dimension > 2, we take the McCormick subgradient
			  subgradientCv = MCtmp._cvsub[i];
			  subgradientCc += McCormick<T>::_xlog_sum_cc_partial_derivative(pointCc,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],0)
									   *(MC[0]._cv==pointCc[0]?MC[0]._cvsub[i]: MC[0]._ccsub[i]);
			  for(unsigned int j = 1; j<MC.size();j++){
					subgradientCc += McCormick<T>::_xlog_sum_cc_partial_derivative(pointCc,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],j)
										   * (MC[j]._const? 0.:MC[j]._ccsub[i]); // in all y directions, the relaxation is monotonically decreasing
			  }
			  MC3._cvsub[i] = subgradientCv;
			  MC3._ccsub[i] = subgradientCc;
		  }
	}
	else{ // MC.size() == 2
		// Compute convex and concave relaxations and get the facet id
		const double* cvrel = McCormick<T>::_xlog_sum_cv(pointCv,MC[0]._cc,coeff,intervalLowerBounds,intervalUpperBounds,pointCc);
		MC3._cv = cvrel[0];
		const double* ccrel = McCormick<T>::_xlog_sum_cc(pointCc,MC[0]._cv,coeff,intervalLowerBounds,intervalUpperBounds);
		MC3._cc = ccrel[0];

		// Subgradients
		for( unsigned int i=0; i<MC3._nsub; i++ ){
		  double subgradientCv = 0;
		  double subgradientCc = 0;
		  if(MC[0]._cv < pointCv[0] && pointCv[0] < MC[0]._cc){
			// we are at the minimum w.r.t. x so we would have to multiply with 0 meaning that we simply add nothing
		  }
		  else{
			 subgradientCv += McCormick<T>::_xlog_sum_cv_partial_derivative(pointCv,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)cvrel[1],0)
								   *(MC[0]._cv==pointCv[0]?MC[0]._cvsub[i]: MC[0]._ccsub[i]);
		  }
		  subgradientCc += McCormick<T>::_xlog_sum_cc_partial_derivative(pointCc,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],0)
								   *(MC[0]._cv==pointCc[0]?MC[0]._cvsub[i]: MC[0]._ccsub[i]);
		  for(unsigned int j = 1; j<MC.size();j++){
				subgradientCv += McCormick<T>::_xlog_sum_cv_partial_derivative(pointCv,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)cvrel[1],j)
									   * (MC[j]._const? 0.:MC[j]._cvsub[i]); // in all y directions, the relaxation is monotonically increasing
				subgradientCc += McCormick<T>::_xlog_sum_cc_partial_derivative(pointCc,coeff,intervalLowerBounds,intervalUpperBounds,(unsigned)ccrel[1],j)
									   * (MC[j]._const? 0.:MC[j]._ccsub[i]); // in all y directions, the relaxation is monotonically decreasing
		  }
		  MC3._cvsub[i] = subgradientCv;
		  MC3._ccsub[i] = subgradientCc;
	    }
	}// end of if MC.size() > 2

#ifdef MC__MCCORMICK_DEBUG
  	std::string str = "xlog_sum";
	McCormick<T>::_debug_check(MC, MC3, str);
#endif
  if(McCormick<T>::options.SUB_INT_HEUR_USE) return MC3.cut().apply_subgradient_interval_heuristic();
  return MC3.cut();
}


template <typename T> inline McCormick<T>
mc_print
( const McCormick<T> &MC, const int number){
	std::cout << "McCormick relaxation #" << number << ": " << MC << std::endl;
	return MC;
}
//////////////////////////////////////////////////////////////////////////////////

template <typename T> inline McCormick<T>
fstep
( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  if( Op<T>::l( MC._I ) >= 0 )
    MC2._I = 1.;
  else if( Op<T>::u( MC._I ) < 0 )
    MC2._I = 0.;
  else
    MC2._I = Op<T>::zeroone();

  { int imid = -1;
    double zmin = Op<T>::l(MC._I);
    double vmid = mid( MC._cv, MC._cc, zmin, imid );
    const double* cvenv = McCormick<T>::_stepcv( vmid, Op<T>::l(MC._I),
      Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid )*cvenv[1];
  }

  { int imid = -1;
    double zmax = Op<T>::u(MC._I);
    double vmid = mid( MC._cv, MC._cc, zmax, imid );
    const double* ccenv = McCormick<T>::_stepcc( vmid, Op<T>::l(MC._I),
      Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid )*ccenv[1];
  }


  return  MC2.cut();
}

template <typename T> inline McCormick<T>
bstep
( const McCormick<T> &MC )
{
  return fstep( -MC );
}

template <typename T> inline McCormick<T>
ltcond
( const T &I0, const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  if( Op<T>::u( I0 ) < 0. )       return MC1;
  else if( Op<T>::l( I0 ) >= 0. ) return MC2;

  McCormick<T> MC3;
  if( MC2._const )
    MC3._sub( MC1._nsub, MC1._const );
  else if( MC1._const )
    MC3._sub( MC2._nsub, MC2._const );
  else if( MC1._nsub != MC2._nsub )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );
  else
    MC3._sub( MC1._nsub, MC1._const||MC2._const );

  MC3._I = Op<T>::hull( MC1._I, MC2._I );
  McCormick<T> MCMin  = 0.5*( MC1 + MC2 - fabs( MC2 - MC1 ) );
  McCormick<T> MCMax  = 0.5*( MC1 + MC2 + fabs( MC1 - MC2 ) );
  MC3._cv = MCMin._cv;
  MC3._cc = MCMax._cc;
  for( unsigned int i=0; i< MC3._nsub; i++ ){
    MC3._cvsub[i] = MCMin._cvsub[i];
    MC3._ccsub[i] = MCMax._ccsub[i];
  }
  return  MC3.cut();
}

template <typename T> inline McCormick<T>
ltcond
( const McCormick<T> &MC0, const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  McCormick<T> MC3 = ltcond( MC0._I, MC1, MC2 );
  McCormick<T> MCStep = fstep(-MC0)*MC1 + fstep(MC0)*MC2;
  if( MCStep._cv > MC3._cv ){
    MC3._cv = MCStep._cv;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._cvsub[i] = MCStep._cvsub[i];
  }
  if( MCStep._cc < MC3._cc ){
    MC3._cc = MCStep._cc;
    for( unsigned int i=0; i< MC3._nsub; i++ )
      MC3._ccsub[i] = MCStep._ccsub[i];
  }
  return  MC3.cut();
}

template <typename T> inline McCormick<T>
gtcond
( const T &I0, const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  return ltcond( -I0, MC1, MC2 );
}

template <typename T> inline McCormick<T>
gtcond
( const McCormick<T> &MC0, const McCormick<T> &MC1, const McCormick<T> &MC2 )
{
  return ltcond( -MC0, MC1, MC2 );
}

template <typename T> inline McCormick<T>
cos
( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::cos( MC._I );

  if( !McCormick<T>::options.ENVEL_USE ){
     MC2._cv = Op<T>::l(MC2._I);
     MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }
    return MC2;
  }

  double*argbnd = McCormick<T>::_cosarg( Op<T>::l(MC._I), Op<T>::u(MC._I) );
  { int imid = -1;
    const double* cvenv = McCormick<T>::_coscv( mid( MC._cv, MC._cc, argbnd[0], imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i< MC2._nsub; i++ ){
       MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  { int imid = -1;
    const double* ccenv = McCormick<T>::_coscc( mid( MC._cv, MC._cc, argbnd[1], imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
     MC2._cc = ccenv[0];
    for( unsigned int i=0; i< MC2._nsub; i++ ){
       MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return  MC2.cut();
}

template <typename T> inline McCormick<T>
sin
( const McCormick<T> &MC )
{
  return cos( MC - PI/2. );
}

template <typename T> inline McCormick<T>
asin
( const McCormick<T> &MC )
{
  if ( Op<T>::l(MC._I) <= -1. || Op<T>::u(MC._I) >= 1. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::ASIN );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::asin( MC._I );

  if( !McCormick<T>::options.ENVEL_USE ){
    { int imid = -1;
      MC2._cv = Op<T>::l(MC2._I) + ( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid )
        - Op<T>::l(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid );
      }
    }
    { int imid = -1;
      MC2._cc = Op<T>::u(MC2._I) + ( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid )
        - Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid );
      }
    }

    return MC2.cut();
  }

  { int imid = -1;
    const double* cvenv = McCormick<T>::_asincv( mid( MC._cv,
      MC._cc, Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  { int imid = -1;
    const double* ccenv = McCormick<T>::_asincc( mid( MC._cv,
      MC._cc, Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
acos
( const McCormick<T> &MC )
{
  return asin( -MC ) + PI/2.;
}

template <typename T> inline McCormick<T>
tan
( const McCormick<T> &MC )
{
  if ( Op<T>::diam(MC._I) >= PI )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::TAN );
  const double shift = PI*std::ceil(-Op<T>::l(MC._I)/PI-1./2.);
  const double xL1 = Op<T>::l(MC._I)+shift, xU1 = Op<T>::u(MC._I)+shift;
  if ( xL1 <= -PI/2. || xU1 >= PI/2. )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::TAN );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::tan( MC._I );

  if( !McCormick<T>::options.ENVEL_USE ){
    { int imid = -1;
      MC2._cv = Op<T>::l(MC2._I) + ( mid( MC._cv, MC._cc, Op<T>::l(MC._I), imid )
        - Op<T>::l(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid );
      }
    }
    { int imid = -1;
      MC2._cc = Op<T>::u(MC2._I) + ( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid )
        - Op<T>::u(MC._I) );
      for( unsigned int i=0; i<MC2._nsub; i++ ){
        MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid );
      }
    }

    return MC2.cut();
  }

  { int imid = -1;
    const double* cvenv = McCormick<T>::_tancv( mid( MC._cv+shift,
      MC._cc+shift, Op<T>::l(MC._I)+shift, imid ), Op<T>::l(MC._I)+shift,
      Op<T>::u(MC._I)+shift );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  { int imid = -1;
    const double* ccenv = McCormick<T>::_tancc( mid( MC._cv+shift,
      MC._cc+shift, Op<T>::u(MC._I)+shift, imid ), Op<T>::l(MC._I)+shift,
      Op<T>::u(MC._I)+shift );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
atan
( const McCormick<T> &MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::atan( MC._I );

  if( !McCormick<T>::options.ENVEL_USE ){
     MC2._cv = Op<T>::l(MC2._I);
     MC2._cc = Op<T>::u(MC2._I);
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = MC2._ccsub[i] = 0.;
    }
    return MC2;
  }

  { int imid = -1;
    const double* cvenv = McCormick<T>::_atancv( mid( MC._cv,
      MC._cc, Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }
  { int imid = -1;
    const double* ccenv = McCormick<T>::_atancc( mid( MC._cv,
      MC._cc, Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
cosh
( const McCormick<T>&MC )
{
  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::cosh( MC._I );

  { int imid = -1;
    double zmin = mid( Op<T>::l(MC._I), Op<T>::u(MC._I), 0., imid );
    imid = -1;
    MC2._cv = std::cosh( mid( MC._cv, MC._cc, zmin, imid ));
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * std::sinh( mid( MC._cv, MC._cc, zmin, imid ));
  }

  { int imid = -1;
    double r = 0.;
    if( !isequal( Op<T>::l(MC._I), Op<T>::u(MC._I) ))
      r = ( std::cosh( Op<T>::u(MC._I) ) - std::cosh( Op<T>::l(MC._I) ) )
        / ( Op<T>::u(MC._I) - Op<T>::l(MC._I) );
    MC2._cc = std::cosh( Op<T>::u(MC._I) ) + r * ( mid( MC._cv, MC._cc, Op<T>::u(MC._I), imid )
      - Op<T>::u(MC._I) );
    for( unsigned int i=0; i<MC2._nsub; i++ )
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * r;
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
sinh
( const McCormick<T>&MC )
{
  if( !McCormick<T>::options.ENVEL_USE )
    return( (exp(MC)-exp(-MC))/2. );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::sinh( MC._I );

  { int imid = -1;
    const double* cvenv = McCormick<T>::_sinhcv( mid( MC._cv,
        MC._cc, Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }

  { int imid = -1;
    const double* ccenv = McCormick<T>::_sinhcc( mid( MC._cv,
      MC._cc, Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
tanh
( const McCormick<T> &MC )
{
  if( !McCormick<T>::options.ENVEL_USE )
    return( (exp(MC)-exp(-MC))/(exp(MC)+exp(-MC)) );

  McCormick<T> MC2;
  MC2._sub( MC._nsub, MC._const );
  MC2._I = Op<T>::tanh( MC._I );

  { int imid = -1;
    const double* cvenv = McCormick<T>::_tanhcv( mid( MC._cv,
      MC._cc, Op<T>::l(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cv = cvenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._cvsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * cvenv[1];
    }
  }

  { int imid = -1;
    const double* ccenv = McCormick<T>::_tanhcc( mid( MC._cv,
      MC._cc, Op<T>::u(MC._I), imid ), Op<T>::l(MC._I), Op<T>::u(MC._I) );
    MC2._cc = ccenv[0];
    for( unsigned int i=0; i<MC2._nsub; i++ ){
      MC2._ccsub[i] = mid( MC._cvsub, MC._ccsub, i, imid ) * ccenv[1];
    }
  }

  return MC2.cut();
}

template <typename T> inline McCormick<T>
coth
( const McCormick<T> &MC )
{
	McCormick<T> MC2 = tanh(MC);
	if ( Op<T>::l(MC2._I) <= 0. && Op<T>::u(MC2._I) >= 0. ) throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::COTH );
	return inv( MC2 );
}


template <typename T> inline std::ostream&
operator<<
( std::ostream&out, const McCormick<T>&MC)
{
  out << std::scientific << std::setprecision(McCormick<T>::options.DISPLAY_DIGITS) << std::right
      << "[ " << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.l() << " : "
              << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.u()
      << " ] [ "  << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.cv() << " : "
                  << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.cc() << " ]";
  if( MC._nsub ){
    out << " [ (";
    for( unsigned int i=0; i<MC._nsub-1; i++ )
      out << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.cvsub(i) << ",";
    out << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.cvsub(MC._nsub-1) << ") : (";
    for( unsigned int i=0; i<MC._nsub-1; i++ )
      out << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.ccsub(i) << ",";
    out << std::setw(McCormick<T>::options.DISPLAY_DIGITS+7) << MC.ccsub(MC._nsub-1) << ") ]";
  }
  return out;
}


template <typename T> inline McCormick<T>
hull
( const McCormick<T>&X, const McCormick<T>&Y )
{
  if( !X._const && !Y._const && (X._nsub != Y._nsub) )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );

  McCormick<T> CV = min(X,Y);
  McCormick<T> CC = max(X,Y);
  McCormick<T> XUY( Op<T>::hull(X.I(),Y.I()), CV.cv(), CC.cc() );
  if( !X._const )
    XUY._sub( X._nsub, X._const );
  else
    XUY._sub( Y._nsub, Y._const );
  for( unsigned int is=0; is<XUY._nsub; is++ ){
    XUY._cvsub[is] = CV.cvsub(is);
    XUY._ccsub[is] = CC.ccsub(is);
  }
  return XUY;
}

template <typename T> inline bool
inter
( McCormick<T>&XIY, const McCormick<T>&X, const McCormick<T>&Y )
{
  if( !X._const && !Y._const && (X._nsub != Y._nsub) )
    throw typename McCormick<T>::Exceptions( McCormick<T>::Exceptions::SUB );

  if( !Op<T>::inter( XIY._I, X._I, Y._I ) ) return false;
  McCormick<T> CV = max(X,Y);
  McCormick<T> CC = min(X,Y);
  if( CV.cv() > CC.cc() ) return false;
  XIY._cv = CV.cv();
  XIY._cc = CC.cc();
  if( !X._const )
    XIY._sub( X._nsub, X._const );
  else
    XIY._sub( Y._nsub, Y._const );
  for( unsigned int is=0; is<XIY._nsub; is++ ){
    XIY._cvsub[is] = CV.cvsub(is);
    XIY._ccsub[is] = CC.ccsub(is);
  }
  return true;
}

template <typename T> inline bool
operator==
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::eq(MC1._I,MC2._I) && MC1._cv == MC2._cv && MC1._cc == MC2._cc );
}

template <typename T> inline bool
operator!=
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::ne(MC1._I,MC2._I) || MC1._cv != MC2._cv || MC1._cc != MC2._cc );
}

template <typename T> inline bool
operator<=
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::le(MC1._I,MC2._I) && MC1._cv >= MC2._cv && MC1._cc <= MC2._cc );
}

template <typename T> inline bool
operator>=
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::ge(MC1._I,MC2._I) && MC1._cv <= MC2._cv && MC1._cc >= MC2._cc );
}

template <typename T> inline bool
operator<
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::lt(MC1._I,MC2._I) && MC1._cv > MC2._cv && MC1._cc < MC2._cc );
}

template <typename T> inline bool
operator>
( const McCormick<T>&MC1, const McCormick<T>&MC2 )
{
  return( Op<T>::gt(MC1._I,MC2._I) && MC1._cv < MC2._cv && MC1._cc > MC2._cc );
}

template <typename T> typename McCormick<T>::Options McCormick<T>::options;

template <typename T> thread_local typename McCormick<T>::SubHeur McCormick<T>::subHeur;

} // namespace mc


#include "mcfadbad.hpp"
//#include "fadbad.h"

namespace fadbad
{

//! @brief Specialization of the structure fadbad::Op to allow usage of the type mc::McCormick of MC++ as a template parameter of the classes fadbad::F, fadbad::B and fadbad::T of FADBAD++
template<typename T> struct Op< mc::McCormick<T> >
{
  typedef mc::McCormick<T> MC;
  typedef double Base;
  static Base myInteger( const int i ) { return Base(i); }
  static Base myZero() { return myInteger(0); }
  static Base myOne() { return myInteger(1);}
  static Base myTwo() { return myInteger(2); }
  static double myPI() { return mc::PI; }
  static MC myPos( const MC& x ) { return  x; }
  static MC myNeg( const MC& x ) { return -x; }
  template <typename U> static MC& myCadd( MC& x, const U& y ) { return x+=y; }
  template <typename U> static MC& myCsub( MC& x, const U& y ) { return x-=y; }
  template <typename U> static MC& myCmul( MC& x, const U& y ) { return x*=y; }
  template <typename U> static MC& myCdiv( MC& x, const U& y ) { return x/=y; }
  static MC myInv( const MC& x ) { return mc::inv( x ); }
  static MC mySqr( const MC& x ) { return mc::pow( x, 2 ); }
  template <typename X, typename Y> static MC myPow( const X& x, const Y& y ) { return mc::pow( x, y ); }
  //static MC myCheb( const MC& x, const unsigned n ) { return mc::cheb( x, n ); }
  static MC mySqrt( const MC& x ) { return mc::sqrt( x ); }
  static MC myLog( const MC& x ) { return mc::log( x ); }
  static MC myExp( const MC& x ) { return mc::exp( x ); }
  static MC mySin( const MC& x ) { return mc::sin( x ); }
  static MC myCos( const MC& x ) { return mc::cos( x ); }
  static MC myTan( const MC& x ) { return mc::tan( x ); }
  static MC myAsin( const MC& x ) { return mc::asin( x ); }
  static MC myAcos( const MC& x ) { return mc::acos( x ); }
  static MC myAtan( const MC& x ) { return mc::atan( x ); }
  static MC mySinh( const MC& x ) { return mc::sinh( x ); }
  static MC myCosh( const MC& x ) { return mc::cosh( x ); }
  static MC myTanh( const MC& x ) { return mc::tanh( x ); }
  static bool myEq( const MC& x, const MC& y ) { return x==y; }
  static bool myNe( const MC& x, const MC& y ) { return x!=y; }
  static bool myLt( const MC& x, const MC& y ) { return x<y; }
  static bool myLe( const MC& x, const MC& y ) { return x<=y; }
  static bool myGt( const MC& x, const MC& y ) { return x>y; }
  static bool myGe( const MC& x, const MC& y ) { return x>=y; }
};

} // end namespace fadbad


//#include "mcop.hpp"
#include "IAPWS/iapwsMcCormick.h"

namespace mc
{

//! @brief Specialization of the structure mc::Op to allow usage of the type mc::McCormick as a template parameter in the classes mc::TModel, mc::TVar, and mc::SpecBnd
// template <> template<typename T> struct Op< mc::McCormick<T> >
template<typename T> struct Op< mc::McCormick<T> > // Modified at AVT.SVT in July 2016 to avoid problems with Intel C++ or MSVC++
{
  typedef mc::McCormick<T> MC;
  static MC point( const double c ) { return MC(c); }
  static MC zeroone() { return MC( mc::Op<T>::zeroone() ); }
  static void I(MC& x, const MC&y) { x = y; }
  static double l(const MC& x) { return x.l(); }
  static double u(const MC& x) { return x.u(); }
  static double abs (const MC& x) { return mc::Op<T>::abs(x.I());  }
  static double mid (const MC& x) { return mc::Op<T>::mid(x.I());  }
  static double diam(const MC& x) { return mc::Op<T>::diam(x.I()); }
  static MC inv (const MC& x) { return mc::inv(x);  }
  static MC sqr (const MC& x) { return mc::sqr(x);  }
  static MC sqrt(const MC& x) { return mc::sqrt(x); }
  static MC exp (const MC& x) { return mc::exp(x);  }
  static MC log (const MC& x) { return mc::log(x);  }
  static MC xlog(const MC& x) { return mc::xlog(x); }
  static MC fabsx_times_x(const MC& x) { return mc::fabsx_times_x(x); }
  static MC xexpax(const MC& x, const double a) { return mc::xexpax(x,a); }
  static MC centerline_deficit(const MC& x, const double xLim, const double type) { return mc::centerline_deficit(x,xLim,type); }
  static MC wake_profile(const MC& x, const double type) { return mc::wake_profile(x,type); }
  static MC wake_deficit(const MC& x, const MC& r, const double a, const double alpha, const double rr, const double type1, const double type2) { return mc::wake_deficit(x,r,a,alpha,rr,type1,type2); }
  static MC power_curve(const MC& x, const double type) { return mc::power_curve(x,type); }
  static MC lmtd(const MC& x,const MC& y) { return mc::lmtd(x,y); }
  static MC rlmtd(const MC& x,const MC& y) { return mc::rlmtd(x,y); }
  static MC mid(const MC& x, const MC& y, const double k) { return mc::mid(x, y, k); }
  static MC pinch(const MC& Th, const MC& Tc, const MC& Tp) { return mc::pinch(Th, Tc, Tp); }
  static MC euclidean_norm_2d(const MC& x,const MC& y) { return mc::euclidean_norm_2d(x,y); }
  static MC expx_times_y(const MC& x,const MC& y) { return mc::expx_times_y(x,y); }
  static MC vapor_pressure(const MC& x, const double type, const double p1, const double p2, const double p3, const double p4 = 0, const double p5 = 0, const double p6 = 0,
							const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0) { return mc::vapor_pressure(x,type,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10);}
  static MC ideal_gas_enthalpy(const MC& x, const double x0, const double type, const double p1, const double p2, const double p3, const double p4, const double p5, const double p6 = 0,
							   const double p7 = 0) { return mc::ideal_gas_enthalpy(x,x0,type,p1,p2,p3,p4,p5,p6,p7);}
  static MC saturation_temperature(const MC& x, const double type, const double p1, const double p2, const double p3, const double p4 = 0, const double p5 = 0, const double p6 = 0,
									const double p7 = 0, const double p8 = 0, const double p9 = 0, const double p10 = 0) { return mc::saturation_temperature(x,type,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10);}
  static MC enthalpy_of_vaporization(const MC& x, const double type, const double p1, const double p2, const double p3, const double p4, const double p5, const double p6 = 0) { return mc::enthalpy_of_vaporization(x,type,p1,p2,p3,p4,p5,p6);}
  static MC cost_function(const MC& x, const double type, const double p1, const double p2, const double p3) { return mc::cost_function(x,type,p1,p2,p3);}
  static MC sum_div(const std::vector< McCormick<T> > &x, const std::vector<double> &coeff) { return mc::sum_div(x,coeff);  }
  static MC xlog_sum(const std::vector< McCormick<T> > &x, const std::vector<double> &coeff) { return mc::xlog_sum(x,coeff);  }
  static MC nrtl_tau(const MC& x, const double a, const double b, const double e, const double f) { return mc::nrtl_tau(x,a,b,e,f);}
  static MC nrtl_dtau(const MC& x, const double b, const double e, const double f) { return mc::nrtl_dtau(x,b,e,f);}
  static MC nrtl_G(const MC& x, const double a, const double b, const double e, const double f, const double alpha ) { return mc::nrtl_G(x,a,b,e,f,alpha);}
  static MC nrtl_Gtau(const MC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_Gtau(x,a,b,e,f,alpha);}
  static MC nrtl_Gdtau(const MC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_Gdtau(x,a,b,e,f,alpha);}
  static MC nrtl_dGtau(const MC& x, const double a, const double b, const double e, const double f, const double alpha) { return mc::nrtl_dGtau(x,a,b,e,f,alpha);}
  static MC iapws(const MC& x, const double type) { return mc::iapws(x,type); }
  static MC iapws(const MC& x, const MC& y, const double type) { return mc::iapws(x,y,type); }
  static MC p_sat_ethanol_schroeder(const MC& x) { return mc::p_sat_ethanol_schroeder(x); }
  static MC rho_vap_sat_ethanol_schroeder(const MC& x) { return mc::rho_vap_sat_ethanol_schroeder(x); }
  static MC rho_liq_sat_ethanol_schroeder(const MC& x) { return mc::rho_liq_sat_ethanol_schroeder(x); }
  static MC covariance_function(const MC& x, const double type) { return mc::covariance_function(x,type); }
  static MC acquisition_function(const MC& x, const MC& y, const double type, const double fmin) { return mc::acquisition_function(x,y,type,fmin); }
  static MC gaussian_probability_density_function(const MC& x) { return mc::gaussian_probability_density_function(x); }
  static MC regnormal(const MC& x, const double a, const double b) { return mc::regnormal(x,a,b); }
  static MC fabs(const MC& x) { return mc::fabs(x); }
  static MC sin (const MC& x) { return mc::sin(x);  }
  static MC cos (const MC& x) { return mc::cos(x);  }
  static MC tan (const MC& x) { return mc::tan(x);  }
  static MC asin(const MC& x) { return mc::asin(x); }
  static MC acos(const MC& x) { return mc::acos(x); }
  static MC atan(const MC& x) { return mc::atan(x); }
  static MC sinh(const MC& x) { return mc::sinh(x); }
  static MC cosh(const MC& x) { return mc::cosh(x); }
  static MC tanh(const MC& x) { return mc::tanh(x); }
  static MC coth(const MC& x) { return mc::coth(x); }
  static MC asinh(const MC& x) { throw std::runtime_error("McCormick::asinh -- operation not implemented");  }
  static MC acosh(const MC& x) { throw std::runtime_error("McCormick::acosh -- operation not implemented"); }
  static MC atanh(const MC& x) { throw std::runtime_error("McCormick::atanh -- operation not implemented"); }
  static MC acoth(const MC& x) { throw std::runtime_error("McCormick::acoth -- operation not implemented"); }
  static MC erf (const MC& x) { return mc::erf(x);  }
  static MC erfc(const MC& x) { return mc::erfc(x); }
  static MC fstep(const MC& x) { return mc::fstep(x); }
  static MC bstep(const MC& x) { return mc::bstep(x); }
  static MC hull(const MC& x, const MC& y) { return mc::Op<T>::hull(x.I(),y.I()); }
  static MC min (const MC& x, const MC& y) { return mc::min(x,y);  }
  static MC max (const MC& x, const MC& y) { return mc::max(x,y);  }
  static MC pos (const MC& x) { return mc::pos(x);  }
  static MC neg (const MC& x) { return mc::neg(x);  }
  static MC lb_func (const MC& x, const double lb) { return mc::lb_func(x,lb);  }
  static MC ub_func (const MC& x, const double ub) { return mc::ub_func(x,ub);  }
  static MC bounding_func (const MC& x, const double lb, const double ub) { return mc::bounding_func(x,lb,ub);  }
  static MC squash_node (const MC& x, const double lb, const double ub) { return mc::squash_node(x,lb,ub);  }
  static MC mc_print ( const MC& x, const int number) {return mc::mc_print(x,number); }
  static MC arh (const MC& x, const double k) { return mc::arh(x,k); }
  template <typename X, typename Y> static MC pow(const X& x, const Y& y) { return mc::pow(x,y); }
  static MC cheb (const MC& x, const unsigned n) { return mc::cheb(x,n); }
  static MC prod (const unsigned int n, const MC* x) { return mc::prod(n,x); }
  static MC monom (const unsigned int n, const MC* x, const unsigned* k) { return mc::monom(n,x,k); }
  static bool inter(MC& xIy, const MC& x, const MC& y) { return mc::inter(xIy,x,y); }
  static bool eq(const MC& x, const MC& y) { return x==y; }
  static bool ne(const MC& x, const MC& y) { return x!=y; }
  static bool lt(const MC& x, const MC& y) { return x<y;  }
  static bool le(const MC& x, const MC& y) { return x<=y; }
  static bool gt(const MC& x, const MC& y) { return x>y;  }
  static bool ge(const MC& x, const MC& y) { return x>=y; }
};

} // namespace mc

#endif
