/**
 * @file iapwsMcCormick.h
 *
 * @brief File containing implementation of relaxations for the IAPWS-IF97 model in MC++.
 *
 * Original model: Wagner, W.; Cooper, J. R.; Dittmann, A.; Kijima, J.; Kretzschmar, H.-J.; Kruse, A.; Mareš, R.; Oguchi, K.; Sato, H.; Stocker, I.; Sifner, O.; Takaishi, Y.; Tanishita, I.; Trubenbach, J. & Willkommen, T.:
 *                 The IAPWS Industrial Formulation 1997 for the Thermodynamic Properties of Water and Steam. Journal of Engineering for Gas Turbines and Power -- Transactions of the ASME, 2000, 122, 150-182.
 *
 * Revised model used for this implementation: Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic Properties of Water and Steam.
 *                                             The International Association for the Properties of Water and Steam, Technical Report IAPWS R7-97(2012), 2012. http://www.iapws.org/relguide/IF97-Rev.html.
 *
 * ==============================================================================\n
 * © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
 * ==============================================================================\n
 *
 * @author Dominik Bongartz, Jaromil Najman, Alexander Mitsos
 * @date 15.08.2019
 *
 */

#pragma once

#include "mccormick.hpp"

#include "iapws.h"

#include <functional>


namespace mc {


    // 1d functions of IAPWS-IF97 model
    template <typename U> McCormick<U> iapws
    (const McCormick<U>&x, const double type)
    {

      namespace r2 = iapws_if97::region2;
      namespace r4 = iapws_if97::region4;

      McCormick<U> result;
      result._sub( x._nsub, x._const );
      result._I = Op<U>::iapws( x._I, type );   // The interval library is responsible for checking domain violations, thus we do not check them here

      switch((int)type){
        case 11:    // region 1, h(p,T)
        case 12:    // region 1, s(p,T)
        case 13:    // region 1, T(p,h)
        case 14:    // region 1, T(p,s)
        case 15:    // region 1, h(p,s)
        case 16:    // region 1, s(p,h)
        case 21:    // region 2, h(p,T)
        case 22:    // region 2, s(p,T)
        case 23:    // region 2, T(p,h)
        case 24:    // region 2, T(p,s)
        case 25:   // region 2, h(p,s)
        case 26:   // region 2, s(p,h)
        case 43:    // region 4-1/2, h(p,x)
        case 44:    // region 4-1/2, h(T,x)
        case 45:    // region 4-1/2, s(p,x)
        case 46:    // region 4-1/2, s(T,x)
        case 47:    // region 4-1/2, x(p,h)
        case 48:    // region 4-1/2, x(p,s)
        case 49:    // region 4-1/2, h(p,s)
        case 410:   // region 4-1/2, s(p,h)
            throw std::runtime_error("\nmc::McCormick\t IAPWS called with one argument but a 2d type (" + std::to_string((int)type) + ")");
        case 29:    // region 2, boundary to 3, p(T)
        {
            // increasing and convex
            {   // convex part
                result._cv = r2::get_b23_p_T(x._cv);
                double d = r2::derivatives::get_b23_dp_dT(x._cv);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*d; }
            }
            { //concave part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cc =  Op<U>::l(result._I)+r*(x._cc-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*r; }
                } else {
                    result._cc = Op<U>::u(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                }
            }
            break;
        }
        case 210:   // region 2, boundary to 3, T(p)
        {
            // increasing and concave
            {   // convex part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cv =  Op<U>::l(result._I)+r*(x._cv-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*r; }
                } else {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                }
            }
            {   // concave part
                result._cc = r2::get_b23_T_p(x._cc);
                double d = r2::derivatives::get_b23_dT_dp(x._cc);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*d; }
            }
            break;
        }
        case 211:   // region 2, boundary 2b/2c, p(h)
        {
            // increasing and convex
            {   // convex part
                result._cv = r2::get_b2bc_p_h(x._cv);
                double d = r2::derivatives::get_b2bc_dp_dh(x._cv);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*d; }
            }
            {   //concave part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cc =  Op<U>::l(result._I)+r*(x._cc-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*r; }
                } else {
                    result._cc = Op<U>::u(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                }
            }
            break;
        }
        case 212:   // region 2, boundary 2b/2c, h(p)
        {
            // increasing and concave
            { // convex part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cv =  Op<U>::l(result._I)+r*(x._cv-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*r; }
                } else {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                }
            }
            { // concave part
                result._cc = r2::get_b2bc_h_p(x._cc);
                double d = r2::derivatives::get_b2bc_dh_dp(x._cc);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*d; }
            }
            break;
        }
        case 41:    // region 4, p(T)
        {
            // increasing and convex
            {   // convex part
                result._cv = r4::get_ps_T(x._cv);
                double d = r4::derivatives::get_dps_dT(x._cv);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*d; }
            }
            {   //concave part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cc =  Op<U>::l(result._I)+r*(x._cc-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*r; }
                } else {
                    result._cc = Op<U>::u(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                }
            }
            break;
        }
        case 42:    // region 4, T(p)
        {
            // increasing and concave
            {   // convex part
                if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                    double r = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cv =  Op<U>::l(result._I)+r*(x._cv-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*r; }
                } else {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                }
            }
            {   // concave part
                result._cc = r4::get_Ts_p(x._cc);
                double d = r4::derivatives::get_dTs_dp(x._cc);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*d; }
            }
            break;
        }
        case 411:   // region 4-1/2, hliq(p)
        {
            if (Op<U>::u(x._I) < r4::data::pmaxD2hliq12dp2lt0) { // Monotonically increasing and concave
                {   // convex part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cv =  Op<U>::l(result._I) + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cv = Op<U>::l(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                    }
                }
                {   // concave part
                    result._cc = r4::get_hliq_p_12(x._cc);
                    const double slope = r4::derivatives::get_dhliq_dp_12(x._cc);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
            }
            else {  // monotonically increasing, but not quite concave
                {   // convex part
                    // build a concave underestimator using alphaBB first and then take the secant as convex relaxations
                    std::function<double(const double)> concaveUnderest = [x](const double p) { return r4::get_hliq_p_12(p) - r4::data::alphaD2hliq12dp2*sqr(p-Op<U>::mid(x._I));  };
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double fConcL = concaveUnderest(Op<U>::l(x._I));
                        const double fConcU = concaveUnderest(Op<U>::u(x._I));
                        const double slope = (fConcU-fConcL)/Op<U>::diam(x._I);
                        result._cv =  fConcL + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cv = Op<U>::l(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                    }
                }
                {   // concave part
                    // use alphaBB directly to get concave relaxation
                    result._cc = r4::get_hliq_p_12(x._cc) - r4::data::alphaD2hliq12dp2*(x._cc-Op<U>::l(x._I))*(x._cc-Op<U>::u(x._I));
                    const double slope = r4::derivatives::get_dhliq_dp_12(x._cc) - 2.*r4::data::alphaD2hliq12dp2*x._cc + r4::data::alphaD2hliq12dp2*(Op<U>::l(x._I)+Op<U>::u(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
            }
            break;
        }
        case 412:   // region 4-1/2, hliq(T)
        {
            if (Op<U>::l(x._I) > r4::data::TminD2hliq12dT2gt0) { // Monotonically increasing and convex
                {   // convex part
                    result._cv = r4::get_hliq_T_12(x._cv);
                    const double slope = r4::derivatives::get_dhliq_dT_12(x._cv);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
                {   // concave part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cc =  Op<U>::l(result._I) + slope*(x._cc-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            else {  // monotonically increasing, but not quite convex
                {   // convex part
                    // use alphaBB to get convex relaxation
                    result._cv = r4::get_hliq_T_12(x._cv) + r4::data::alphaD2hliq12dT2*(x._cv-Op<U>::l(x._I))*(x._cv-Op<U>::u(x._I));
                    const double slope = r4::derivatives::get_dhliq_dT_12(x._cv) + 2.*r4::data::alphaD2hliq12dT2*x._cv - r4::data::alphaD2hliq12dT2*(Op<U>::l(x._I)+Op<U>::u(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
                {   // concave part
                    // build a convex overestimator using alphaBB first and then take the secant as concave relaxations
                    std::function<double(const double)> convexOverest = [x](const double T) { return r4::get_hliq_T_12(T) + r4::data::alphaD2hliq12dT2*sqr(T-Op<U>::mid(x._I));  };
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double fConvL = convexOverest(Op<U>::l(x._I));
                        const double fConvU = convexOverest(Op<U>::u(x._I));
                        const double slope = (fConvU-fConvL)/Op<U>::diam(x._I);
                        result._cc =  fConvL + slope*(x._cc-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            break;
        }
        case 413:   // region 4-1/2, hvap(p)
        {
            // hvap12(p) is concave, but has a maximum at pmaxhvap12
            {   // concave part
                int imid = -1;
                const double xmax = mid( x._cv, x._cc, r4::data::pmaxhvap12, imid );
                result._cc = r4::get_hvap_p_12(xmax);
                const double slope = r4::derivatives::get_dhvap_dp_12(xmax);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.: mid( x._cvsub, x._ccsub, i, imid ))*slope; }
            }
            {   // convex part
                if( !isequal( Op<U>::l(x._I), Op<U>::u(x._I) )){
                    const double hxL = r4::get_hvap_p_12(Op<U>::l(x._I));
                    const double hxU = r4::get_hvap_p_12(Op<U>::u(x._I));
                    const double xmin = (hxL<=hxU)? Op<U>::l(x._I) : Op<U>::u(x._I);
                    const double slope = (hxU-hxL) / Op<U>::diam(x._I);
                    int imid = -1;
                    result._cv = hxL + slope*(mid(x._cv,x._cc,xmin,imid) - Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.: mid( x._cvsub, x._ccsub, i, imid ))*slope; }
                } else {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                }
            }
            break;
        }
        case 414:   // region 4-1/2, hvap(T)
        {
            // hvap12(T) is concave, but has a maximum at Tmaxhvap12
            {   // concave part
                int imid = -1;
                const double xmax = mid( x._cv, x._cc, r4::data::Tmaxhvap12, imid );
                result._cc = r4::get_hvap_T_12(xmax);
                const double slope = r4::derivatives::get_dhvap_dT_12(xmax);
                for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.: mid( x._cvsub, x._ccsub, i, imid ))*slope; }
            }
            {   // convex part
                if( !isequal( Op<U>::l(x._I), Op<U>::u(x._I) )){
                    const double hxL = r4::get_hvap_T_12(Op<U>::l(x._I));
                    const double hxU = r4::get_hvap_T_12(Op<U>::u(x._I));
                    const double xmin = (hxL<=hxU)? Op<U>::l(x._I) : Op<U>::u(x._I);
                    const double slope = (hxU-hxL) / Op<U>::diam(x._I);
                    int imid = -1;
                    result._cv = hxL + slope*(mid(x._cv,x._cc,xmin,imid) - Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.: mid( x._cvsub, x._ccsub, i, imid ))*slope; }
                } else {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                }
            }
            break;
        }
        case 415:   // region 4-1/2, sliq(p)
        {
            if (Op<U>::u(x._I) < r4::data::pmaxD2sliq12dp2lt0) { // Monotonically increasing and concave
                {   // convex part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cv =  Op<U>::l(result._I) + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cv = Op<U>::l(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                    }
                }
                {   // concave part
                    result._cc = r4::get_sliq_p_12(x._cc);
                    const double slope = r4::derivatives::get_dsliq_dp_12(x._cc);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
            }
            else {  // monotonically increasing, but not quite concave
                {   // convex part
                    // build a concave underestimator using alphaBB first and then take the secant as convex relaxations
                    std::function<double(const double)> concaveOverest = [x](const double p) { return r4::get_sliq_p_12(p) - r4::data::alphaD2sliq12dp2*sqr(p-Op<U>::mid(x._I));  };
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double fConcL = concaveOverest(Op<U>::l(x._I));
                        const double fConcU = concaveOverest(Op<U>::u(x._I));
                        const double slope = (fConcU-fConcL)/Op<U>::diam(x._I);
                        result._cv =  fConcL + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cv = Op<U>::l(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                    }
                }
                {   // concave part
                    // use alphaBB directly to get concave relaxation
                    result._cc = r4::get_sliq_p_12(x._cc) - r4::data::alphaD2sliq12dp2*(x._cc-Op<U>::l(x._I))*(x._cc-Op<U>::u(x._I));
                    const double slope = r4::derivatives::get_dsliq_dp_12(x._cc) - 2.*r4::data::alphaD2sliq12dp2*x._cc + r4::data::alphaD2sliq12dp2*(Op<U>::l(x._I)+Op<U>::u(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
            }
            break;
        }
        case 416:   // region 4-1/2, sliq(T)
        {
            if (Op<U>::l(x._I) > r4::data::TminD2sliq12dT2gt0) {        // increasing and convex
                {   // convex part
                    result._cv = r4::get_sliq_T_12(x._cv);
                    const double slope = r4::derivatives::get_dsliq_dT_12(x._cv);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
                {   // concave part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cc =  Op<U>::l(result._I) + slope*(x._cc-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            else if (Op<U>::u(x._I) < r4::data::TmaxD2sliq12dT2lt0) { // increasing and concave
                {   // concave part
                    result._cc = r4::get_sliq_T_12(x._cc);
                    const double slope = r4::derivatives::get_dsliq_dT_12(x._cc);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
                {   // convex part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cv =  Op<U>::l(result._I) + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cv = Op<U>::l(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = 0.; }
                    }
                }
            }
            else {    // neither concave nor convex, using alphaBB to make it convex first
                {   // convex part
                    // use alphaBB to get convex relaxation
                    result._cv = r4::get_sliq_T_12(x._cv) + r4::data::alphaD2sliq12dT2*(x._cv-Op<U>::l(x._I))*(x._cv-Op<U>::u(x._I));
                    const double slope = r4::derivatives::get_dsliq_dT_12(x._cv) + 2.*r4::data::alphaD2sliq12dT2*x._cv - r4::data::alphaD2sliq12dT2*(Op<U>::l(x._I)+Op<U>::u(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
                {   // concave part
                    // build a convex overestimator using alphaBB first and then take the secant as concave relaxation
                    std::function<double(const double)> convexOverest = [x](const double T) { return r4::get_sliq_T_12(T) + r4::data::alphaD2sliq12dT2*sqr(T-Op<U>::mid(x._I));  };
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double fConvL = convexOverest(Op<U>::l(x._I));
                        const double fConvU = convexOverest(Op<U>::u(x._I));
                        const double slope = (fConvU-fConvL)/Op<U>::diam(x._I);
                        result._cc =  fConvL + slope*(x._cc-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            break;
        }
        case 417:   // region 4-1/2, svap(p)
        {
            if (Op<U>::u(x._I) < r4::data::pmaxD2svap12dp2gt0) { // Monotonically decreasing and convex
                {   // convex part
                    result._cv = r4::get_svap_p_12(x._cc);
                    const double slope = r4::derivatives::get_dsvap_dp_12(x._cc);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
                {   // concave part
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double slope = -Op<U>::diam(result._I)/Op<U>::diam(x._I);
                        result._cc =  Op<U>::u(result._I) + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            else {  // monotonically decreasing, but not quite convex
                {   // convex part
                    // use alphaBB to get convex relaxation
                    result._cv = r4::get_svap_p_12(x._cc) + r4::data::alphaD2svap12dp2*(x._cc-Op<U>::l(x._I))*(x._cc-Op<U>::u(x._I));
                    const double slope = r4::derivatives::get_dsvap_dp_12(x._cc) + 2.*r4::data::alphaD2svap12dp2*x._cc - r4::data::alphaD2svap12dp2*(Op<U>::l(x._I)+Op<U>::u(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
                {   // concave part
                    // build a convex overestimator using alphaBB first and then take the secant as concave relaxations
                    std::function<double(const double)> convexOverest = [x](const double p) { return r4::get_svap_p_12(p) + r4::data::alphaD2svap12dp2*sqr(p-Op<U>::mid(x._I));  };
                    if (!isequal( Op<U>::l(x._I), Op<U>::u(x._I) )) {
                        const double fConvL = convexOverest(Op<U>::l(x._I));
                        const double fConvU = convexOverest(Op<U>::u(x._I));
                        const double slope = (fConvU-fConvL)/Op<U>::diam(x._I);
                        result._cc =  fConvL + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    } else {
                        result._cc = Op<U>::u(result._I);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = 0.; }
                    }
                }
            }
            break;
        }
        case 418:     // region 4-1/2, svap(T)
        {
            if (Op<U>::u(x._I)<r4::data::TzeroD2svap12dT2) {        // decreasing and convex
                {   // convex part
                    result._cv = r4::get_svap_T_12(x._cc);
                    const double slope = r4::derivatives::get_dsvap_dT_12(x._cc);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
                {   // concave part
                    const double slope = isequal( Op<U>::l(x._I), Op<U>::u(x._I) )? 0. : -Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cc =  Op<U>::u(result._I) + slope*(x._cv-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
            }
            else if (Op<U>::l(x._I)>r4::data::TzeroD2svap12dT2) {   // decreasing and concave
                {   // concave part
                    result._cc = r4::get_svap_T_12(x._cv);
                    const double slope = r4::derivatives::get_dsvap_dT_12(x._cv);
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                }
                {   // convex part
                    const double slope = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 0. : -Op<U>::diam(result._I)/Op<U>::diam(x._I);
                    result._cv =  Op<U>::u(result._I) + slope*(x._cc-Op<U>::l(x._I));
                    for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                }
            }
            else {  // decreasing and convexo-concave
                {   // convex part
                    double rusr[2];
                    rusr[0] = Op<U>::u(x._I);
                    rusr[1] = r4::get_svap_T_12(Op<U>::u(x._I));
                    double (*myfPtr)(const double,const double*,const int*);
                    double (*mydfPtr)(const double,const double*,const int*);
                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::get_svap_T_12(x) - rusr[1] + r4::derivatives::get_dsvap_dT_12(x)*(rusr[0]-x); };
                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_d2svap_dT2_12(x)*(rusr[0]-x); };
                    double xIntersect;
                    try{
                        xIntersect = numerics::newton( Op<U>::l(x._I), Op<U>::l(x._I), r4::data::TzeroD2svap12dT2, myfPtr, mydfPtr, rusr, NULL );
                    }
                    catch( typename McCormick<U>::Exceptions ){
                        xIntersect = numerics::goldsect( Op<U>::l(x._I), r4::data::TzeroD2svap12dT2, myfPtr, rusr, NULL );
                    }
                    if( x._cc > xIntersect ){   // ok, we are actually on the secant part
                        const double fIntersect = r4::get_svap_T_12(xIntersect);
                        const double slope = isequal(Op<U>::u(x._I),xIntersect)? 0. : (rusr[1] - fIntersect)/(Op<U>::u(x._I)-xIntersect);
                        result._cv = fIntersect + slope*(x._cc-xIntersect);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    }
                    else {   // no, we are still on the convex part
                        result._cv = r4::get_svap_T_12(x._cc);
                        const double slope = r4::derivatives::get_dsvap_dT_12(x._cc);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._cvsub[i] = (x._const? 0.:x._ccsub[i])*slope; }
                    }
                }
                {   // concave part
                    double rusr[2];
                    rusr[0] = Op<U>::l(x._I);
                    rusr[1] = r4::get_svap_T_12(Op<U>::l(x._I));
                    double (*myfPtr)(const double,const double*,const int*);
                    double (*mydfPtr)(const double,const double*,const int*);
                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::get_svap_T_12(x) - rusr[1] + r4::derivatives::get_dsvap_dT_12(x)*(rusr[0]-x); };
                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_d2svap_dT2_12(x)*(rusr[0]-x); };
                    double xIntersect;
                    try{
                        xIntersect = numerics::newton( Op<U>::u(x._I), r4::data::TzeroD2svap12dT2, Op<U>::u(x._I), myfPtr, mydfPtr, rusr, NULL );
                    }
                    catch( typename McCormick<U>::Exceptions ){
                        xIntersect = numerics::goldsect( r4::data::TzeroD2svap12dT2, Op<U>::u(x._I), myfPtr, rusr, NULL );
                    }
                    if( x._cv < xIntersect ){   // ok, we are actually on the secant part
                        const double fIntersect = r4::get_svap_T_12(xIntersect);
                        const double slope = isequal(xIntersect,Op<U>::l(x._I))? 0. : (fIntersect - rusr[1])/(xIntersect-Op<U>::l(x._I));
                        result._cc = rusr[1] + slope*(x._cv-Op<U>::l(x._I));
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    }
                    else {   // no, we are still on the concave part
                        result._cc = r4::get_svap_T_12(x._cv);
                        const double slope = r4::derivatives::get_dsvap_dT_12(x._cv);
                        for( unsigned int i=0; i<result._nsub; i++ ) { result._ccsub[i] = (x._const? 0.:x._cvsub[i])*slope; }
                    }
                }
            }
            break;


        }
        default:
            throw std::runtime_error("\nmc::McCormick\t IAPWS called with unkown type (" + std::to_string((int)type) + ").");
      }

#ifdef MC__MCCORMICK_DEBUG
      std::string str = "iapws-1d," + std::to_string(type);
      McCormick<U>::_debug_check(x, result, str);
#endif
      return result.cut();

    }


    // 2d functions of IAPWS-IF97 model
    template <typename U> McCormick<U> iapws
    (const McCormick<U>&x, const McCormick<U>&y, const double type)
    {

      namespace r1 = iapws_if97::region1;
      namespace r2 = iapws_if97::region2;
      namespace r4 = iapws_if97::region4;

      switch((int)type){
            // 2d functions:
        case 11:    // region 1, h(p,T)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );

            {   // convex part
                // Depending on where we are, h(p,T) may be component-wise convex or only almost so. In the latter cases, we apply alphaBB first to get a component-wise convex underestimator
                    std::function<double(const double,const double)> compConvUnderest, dcompConvUnderest_dT, dcompConvUnderest_dp;
                    if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23)) ) { // if temperature is high enough (approx. >369K, or >353K if p<16.5MPa), we have comp. convexity in both p and T
                        compConvUnderest = [](const double p, const double T) { return r1::get_h_pT_uncut(p,T); };
                        dcompConvUnderest_dT = [](const double p, const double T) { return r1::derivatives::get_dh_pT_dT_uncut(p,T); };
                        dcompConvUnderest_dp = [](const double p, const double T) { return r1::derivatives::get_dh_pT_dp_uncut(p,T); };
                    } else if ( (Op<U>::l(y._I)>r1::data::TminD2hdT2Gt0) || (Op<U>::l(x._I)>r1::data::pminD2hdT2Gt0) ) {    // if temperature is high enough (approx. >314K) or pressure is high enough (approx. >26.0 MPa), we have comp. convexity w.r.t. T but not p
                        compConvUnderest = [x](const double p, const double T) { return r1::get_h_pT_uncut(p,T) + r1::data::alphaD2hdp2*(p-Op<U>::l(x._I))*(p-Op<U>::u(x._I)); };
                        dcompConvUnderest_dT = [](const double p, const double T) { return r1::derivatives::get_dh_pT_dT_uncut(p,T); };
                        dcompConvUnderest_dp = [x](const double p, const double T) { return r1::derivatives::get_dh_pT_dp_uncut(p,T) + 2.*r1::data::alphaD2hdp2*p - r1::data::alphaD2hdp2*(Op<U>::l(x._I)+Op<U>::u(x._I)) ; };
                    } else {    // no component-wise convexity, need alphaBB in both p and T to achieve it first
                        compConvUnderest = [x,y](const double p, const double T) { return r1::get_h_pT_uncut(p,T) + r1::data::alphaD2hdp2*(p-Op<U>::l(x._I))*(p-Op<U>::u(x._I)) + r1::data::alphaD2hdT2*(T-Op<U>::l(y._I))*(T-Op<U>::u(y._I)); };
                        dcompConvUnderest_dT = [y](const double p, const double T) { return r1::derivatives::get_dh_pT_dT_uncut(p,T) + 2.*r1::data::alphaD2hdT2*T - r1::data::alphaD2hdT2*(Op<U>::l(y._I)+Op<U>::u(y._I)) ; };
                        dcompConvUnderest_dp = [x](const double p, const double T) { return r1::derivatives::get_dh_pT_dp_uncut(p,T) + 2.*r1::data::alphaD2hdp2*p - r1::data::alphaD2hdp2*(Op<U>::l(x._I)+Op<U>::u(x._I)) ; };
                    }

                // Since we now have componentwise convexity and d2h/dpT<0, we can apply Theorem 1 from Najman, Bongartz, Mitsos (2019), CACE, to get two valid relaxations
                    double cv[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];

                    // within this region, we always have dh/dT>0. However, depending on where we are, h(p,T) and its relaxations may or may not be monotonic w.r.t. p.
                    // we thus need to distinguish different cases to select the correct p coordinate for the solution of the auxiliary problem for multivariate McCormick (for T (i.e., y), it is always at T^cv)
                    {   // first relaxation: h(p,T^U)+h(p^L,T)-h(p^L,T^U). The pressure dependence is equal to that of h(p,T^U).
                        bool xslopeZero = false;
                        double xRel1;
                        // Select the correct p coordinate for multivariate McCormick
                        if (Op<U>::u(y._I)<r1::data::TmaxDhdpGt0) { // if T^U<~517K, this relaxation is monotonically increasing in p
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        }
                        else {
                            // here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            const double psat = r4::original::get_ps_T(Op<U>::u(y._I));
                            const double dhhatReldp = dcompConvUnderest_dp(psat,Op<U>::u(y._I)) ;
                            bool shortcutTaken = false;
                            double xLower = x._cv;
                            if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23))  ) { // when not using alphaBB, the slope in the extended region is constant --> may be able to use a shortcut
                                if (dhhatReldp>=0) {
                                    // if the derivative w.r.t. p is positive at this boundary, it is positive for all p>psat(T) (since we have component-wise convexity w.r.t. p in this region of T) and p<psat(T) (since it is constant there)
                                    xRel1 = x._cv;
                                    xSub[0] = x._cvsub;
                                    shortcutTaken = true;
                                } else if (x._cc <= psat) {
                                    xRel1 = x._cc;
                                    xSub[0] = x._ccsub;
                                    shortcutTaken = true;
                                } else {
                                    xLower = std::min(std::max(x._cv,psat),x._cc);
                                }
                            }
                            if (!shortcutTaken) {   // we actually need to solve dh/dp=0 for p*
                                // first, define the functions needed to use the _comput_root function
                                const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                                double (*myfPtr)(const double,const double*,const int*);
                                double (*mydfPtr)(const double,const double*,const int*);
                                if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23)) ) { // we again need to make a distinction in order to construct the correct functions to be solved
                                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dh_pT_dp_uncut(x,rusr[3]); };    // we want to find the root of dh(p,T^U)/dp=0
                                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2h_pT_dp2_uncut(x,rusr[3]); };
                                } else {
                                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dh_pT_dp_uncut(x,rusr[3]) + 2.*r1::data::alphaD2hdp2*x - r1::data::alphaD2hdp2*(rusr[0]+rusr[1]) ; };
                                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2h_pT_dp2_uncut(x,rusr[3]) + 2.*r1::data::alphaD2hdp2; };
                                }
                                // then, solve and determine where we ended up
                                xRel1 = _compute_root(xLower,xLower,x._cc,myfPtr,mydfPtr,bounds,NULL);
                                if(isequal(xRel1,x._cv)) {
                                    xSub[0] = x._cvsub;
                                } else if (isequal(xRel1,x._cc)) {
                                    xSub[0] = x._ccsub;
                                } else {
                                    xSub[0] = x._ccsub; // this is a dummy that will not be used since slope1x=0.;
                                    xslopeZero = true;
                                }
                            }
                        }
                        cv[0] = compConvUnderest(xRel1,Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                        xslope[0] = xslopeZero? 0. : dcompConvUnderest_dp(xRel1,Op<U>::u(y._I));
                        yslope[0] = dcompConvUnderest_dT(Op<U>::l(x._I),y._cv);
                    }
                    {   // second relaxation: h(p^U,T)+h(p,T^L)-h(p^U,T^L). The pressure dependence is equal to that of h(p,T^L).
                        bool xslopeZero = false;
                        double xRel2;
                        if (Op<U>::l(y._I)<r1::data::TmaxDhdpGt0) { // if T^L<~517K, this relaxation is monotonically increasing in p
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                        }
                        else {
                            // here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            const double psat = r4::original::get_ps_T(Op<U>::l(y._I));
                            const double dhhatReldp = dcompConvUnderest_dp(psat,Op<U>::l(y._I)) ;
                            bool shortcutTaken = false;
                            double xLower = x._cv;
                            if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23))  ) { // when not using alphaBB, the slope in the extended region is constant --> may be able to use a shortcut
                                if (dhhatReldp>=0) {
                                    // if the derivative w.r.t. p is positive at this boundary, it is positive for all p>psat(T) (since we have component-wise convexity w.r.t. p in this region of T) and p<psat(T) (since it is constant there)
                                    xRel2 = x._cv;
                                    xSub[1] = x._cvsub;
                                    shortcutTaken = true;
                                } else if (x._cc <= psat) {
                                    xRel2 = x._cc;
                                    xSub[1] = x._ccsub;
                                    shortcutTaken = true;
                                } else {
                                    xLower = std::min(std::max(x._cv,psat),x._cc);
                                }
                            }
                            if (!shortcutTaken) {   // we actually need to solve dh/dp=0 for p*
                                // first, define the functions needed to use the _compute_root function
                                const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                                double (*myfPtr)(const double,const double*,const int*);
                                double (*mydfPtr)(const double,const double*,const int*);
                                if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23)) ) { // we again need to make a distinction in order to construct the correct functions to be solved
                                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dh_pT_dp_uncut(x,rusr[2]); };    // we want to find the root of dh(p,T^L)/dp=0
                                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2h_pT_dp2_uncut(x,rusr[2]); };
                                } else {
                                    myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dh_pT_dp_uncut(x,rusr[2]) + 2.*r1::data::alphaD2hdp2*x - r1::data::alphaD2hdp2*(rusr[0]+rusr[1]) ; };
                                    mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2h_pT_dp2_uncut(x,rusr[2]) + 2.*r1::data::alphaD2hdp2; };
                                }
                                // then, solve and determine where we ended up
                                xRel2 = _compute_root(xLower,xLower,x._cc,myfPtr,mydfPtr,bounds,NULL);
                                if(isequal(xRel2,x._cv)) {
                                    xSub[1]  = x._cvsub;
                                } else if (isequal(xRel2,x._cc)) {
                                    xSub[1]  = x._ccsub;
                                } else {
                                    xSub[1]  = x._ccsub;    // this is a dummy that will not be used since slope2x=0.;
                                    xslopeZero = true;
                                }
                            }
                        }
                        cv[1] = compConvUnderest(Op<U>::u(x._I),y._cv) + compConvUnderest(xRel2,Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I));
                        xslope[1] = xslopeZero? 0. : dcompConvUnderest_dp(xRel2,Op<U>::l(y._I));
                        yslope[1] = dcompConvUnderest_dT(Op<U>::u(x._I),y._cv);
                    }

                // We now have to correct these two relaxations, since these were built using h_pT_uncut without considering that the function h_pT is cut off at hmax
#ifdef LIVE_DANGEROUSLY
                    // Since we constructed the relaxation using the uncut version of h(p,T) (i.e., that before taking min(hmax,...)), the convex relaxation may be larger than hmax (and hence the final h(p,T) at that point).
                    // However, since this only occurs at points that *should* be infeasible because of suitable constraints (e.g., p>=psat(T)), we can simply default the relaxations to the interval bounds.
                    // While this results in the relaxations being discontinuous (and hence not even convex any more!), this is okay, because all derived affine relaxations are still valid for any feasible point.
                    // If, however, the user forgets to include these constraints, weird things can happen... Therefore, the "safe" version of actually taking min(hmax,...) is implemented in the #else clause below. It does however lead to weaker relaxations...
                    if ( (cv[0]>r1::data::hmax) || (cv[1]>r1::data::hmax) ) {
                        cv[0] = Op<U>::l(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cv[1] = Op<U>::l(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // Actually do min(h_pT_uncut,hmax)
                    if( Op<U>::u(result._I) >= r1::data::hmax ){
                        // First, we determine the maximum of the convex underestimators on the entire host sets, which may be at (p^L,T^U) or (p^U,T^U) (recall that the underestimators are increasing in T and comp. convex)
                        // First relaxation: max of h(p,T^U)+h(p^L,T)-h(p^L,T^U)
                        const double cvU1 = std::max (  compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) */,
                                                        compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) */);
                        const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, r1::data::hmax ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                        cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                        xslope[0] = k1*xslope[0];
                        yslope[0] = k1*yslope[0];
                        // Second relaxation: max of h(p^U,T)+h(p,T^L)-h(p^U,T^L)
                        const double cvU2 = std::max(   compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)),
                                                        compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) */ );
                        const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, r1::data::hmax ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                        cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                        xslope[1] = k2*xslope[1];
                        yslope[1] = k2*yslope[1];
                    }
#endif

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmax(2, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
            }
            {   // concave part
                // Depending on where we are, h(p,T) may be component-wise convex or only almost so. In the latter cases, we apply alphaBB first to get a component-wise convex overestimator
                    std::function<double(const double,const double)> compConvOverest;
                    if ( (Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0) || ((Op<U>::l(y._I)>r1::data::TminD2hdp2Gt0AtPminB23)&&(Op<U>::u(x._I)<r2::data::pminB23)) ) { // if temperature is high enough (approx. >369K, or >353K if p<16.5MPa), we have comp. convexity in both p and T
                        compConvOverest = [](const double p, const double T) { return r1::get_h_pT_uncut(p,T); };
                    } else if ( (Op<U>::l(y._I)>r1::data::TminD2hdT2Gt0) || (Op<U>::l(x._I)>r1::data::pminD2hdT2Gt0) ) {    // if temperature is high enough (approx. >314K) or pressure is high enough (approx. >26.0 MPa), we have comp. convexity w.r.t. T but not p
                        compConvOverest = [x](const double p, const double T) { return r1::get_h_pT_uncut(p,T) + r1::data::alphaD2hdp2*sqr(p-Op<U>::mid(x._I)); };
                    } else {    // no component-wise convexity, need alphaBB in both p and T to achieve it first
                        compConvOverest = [x,y](const double p, const double T) { return r1::get_h_pT_uncut(p,T) + r1::data::alphaD2hdp2*sqr(p-Op<U>::mid(x._I)) + r1::data::alphaD2hdT2*sqr(T-Op<U>::mid(y._I)); };
                    }

                // Since we now have componentwise convexity, we can compute the vertex polyhedral concave envelope (2 affine overestimators)
                // Note that for the concave one, the .cut() operation at the end will take care of the min(hmax,...) for the actual (=cut) version of h(p,T)
                    double cc[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];

                    // Calculate function values at corner point
                    double fLL = compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I));
                    double fLU = compConvOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                    double fUL = compConvOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                    double fUU = compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));

                    // Determine which ones are the correct facets
                    double fmidLLUU = 0.5*(fLL + fUU);
                    double fmidLUUL = 0.5*(fLU + fUL);
                    if (fmidLUUL < fmidLLUU) {

                        // In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
                        // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dh/dT>0 and hence also dh^cc/dT>0
                        // - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                        double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                        double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, UL, UU), 2: (LL, LU, UU))
                        double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
                        // First, determine the "highest" corner for facet 1 (LL, UL, UU), considering that we are increasing in T (i.e., f(UU)>=f(UL))
                        if (fUU>=fLL) {
                            fUpper1 = fUU;
                            xUpper1 = Op<U>::u(x._I);
                            yUpper1 = Op<U>::u(y._I);
                        } else {
                            fUpper1 = fLL;
                            xUpper1 = Op<U>::l(x._I);
                            yUpper1 = Op<U>::l(y._I);
                        }
                        // Next, determine monotonicity of facet 1
                        if (fUL>=fLL) {
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        } else {
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        }
                        // Next, determine the "highest" corner & monotonicity for facet 2 (LL, LU, UU), considering that we are increasing in T (i.e., f(LU)>=f(LL))
                        if (fUU>=fLU) {
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                            fUpper2 = fUU;
                            xUpper2 = Op<U>::u(x._I);
                        } else {
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                            fUpper2 = fLU;
                            xUpper2 = Op<U>::l(x._I);
                        }
                        // Now we can compute the facets
                        const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                        const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                        xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                        xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                        cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
                        cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));

                    }
                    else {

                        // In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
                        // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dh/dT>0 and hence also dh^cc/dT>0
                        // - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                        double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                        double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, LU, UL), 2: (LU, UL, UU))
                        double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
                        // First, determine the "highest" corner for facet 1 (LL, LU, UL), considering that we are increasing in T (i.e., f(LU)>=f(LL))
                        if (fUL>=fLU) {
                            fUpper1 = fUL;
                            xUpper1 = Op<U>::u(x._I);
                            yUpper1 = Op<U>::l(y._I);
                        } else {
                            fUpper1 = fLU;
                            xUpper1 = Op<U>::l(x._I);
                            yUpper1 = Op<U>::u(y._I);
                        }
                        // Next, determine monotonicity of facet 1
                        if (fUL>=fLL) {
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        } else {
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        }
                        // Next, determine the "highest" corner & monotonicity for facet 2 (LU, UL,UU), considering that we are increasing in T (i.e., f(UU)>=f(LL))
                        if (fUU>=fLU) {
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                            fUpper2 = fUU;
                            xUpper2 = Op<U>::u(x._I);
                        } else {
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                            fUpper2 = fLU;
                            xUpper2 = Op<U>::l(x._I);
                        }
                        // Now we can compute the facets
                        const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                        const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                        xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                        xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                        cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
                        cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));

                    }


                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(2, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 12:    // region 1, s(p,T)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );

            {   // convex part
                // We apply alphaBB first to get a component-wise convex underestimator
                    std::function<double(const double,const double)> compConvUnderest, dcompConvUnderest_dT, dcompConvUnderest_dp;
                    if (Op<U>::l(y._I)>r1::data::TminD2sdp2Gt0) {    // if temperature is high enough (approx. >318K), we have comp. convexity w.r.t. p but not T
                        compConvUnderest = [y](const double p, const double T) { return r1::get_s_pT_uncut(p,T) + r1::data::alphaD2sdT2*(T-Op<U>::l(y._I))*(T-Op<U>::u(y._I)); };
                        dcompConvUnderest_dT = [y](const double p, const double T) { return r1::derivatives::get_ds_pT_dT_uncut(p,T) + 2.*r1::data::alphaD2sdT2*T - r1::data::alphaD2sdT2*(Op<U>::l(y._I)+Op<U>::u(y._I)) ; };
                        dcompConvUnderest_dp = [](const double p, const double T) { return r1::derivatives::get_ds_pT_dp_uncut(p,T); };
                    } else {    // no component-wise convexity, need alphaBB in both p and T to achieve it first
                        compConvUnderest = [x,y](const double p, const double T) { return r1::get_s_pT_uncut(p,T) + r1::data::alphaD2sdp2*(p-Op<U>::l(x._I))*(p-Op<U>::u(x._I)) + r1::data::alphaD2sdT2*(T-Op<U>::l(y._I))*(T-Op<U>::u(y._I)); };
                        dcompConvUnderest_dT = [y](const double p, const double T) { return r1::derivatives::get_ds_pT_dT_uncut(p,T) + 2.*r1::data::alphaD2sdT2*T - r1::data::alphaD2sdT2*(Op<U>::l(y._I)+Op<U>::u(y._I)) ; };
                        dcompConvUnderest_dp = [x](const double p, const double T) { return r1::derivatives::get_ds_pT_dp_uncut(p,T) + 2.*r1::data::alphaD2sdp2*p - r1::data::alphaD2sdp2*(Op<U>::l(x._I)+Op<U>::u(x._I)) ; };
                    }

                // Since we now have componentwise convexity and d2h/dpT<0, we can apply Theorem 1 from Najman, Bongartz, Mitsos (2019), CACE, to get two valid relaxations
                    double cv[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];

                    // within this region, we always have dh/dT>0. However, depending on where we are, h(p,T) and its relaxations may or may not be monotonic w.r.t. p.
                    // we thus need to distinguish different cases to select the correct p coordinate for the solution of the auxiliary problem for multivariate McCormick (for T (i.e., y), it is always at T^cv)
                    {   // first relaxation: h(p,T^U)+h(p^L,T)-h(p^L,T^U). The pressure dependence is equal to that of h(p,T^U).
                        bool xslopeZero = false;
                        double xRel1;
                        // Select the correct p coordinate for multivariate McCormick
                        if ( (Op<U>::u(y._I)>r1::data::TminDsdpLt0) || (Op<U>::l(x._I)>r1::data::pminDsdpLt0) ) {   // If temperature or pressure are high enough (>~277K or >~19MPa), we are decreasing in p
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        }
                        else {
                            // here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            // first, define the functions needed to use the _comput_root function
                            const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                            double (*myfPtr)(const double,const double*,const int*);
                            double (*mydfPtr)(const double,const double*,const int*);
                            if (Op<U>::l(y._I)>r1::data::TminD2sdp2Gt0) {    // we again need to make a distinction in order to construct the correct functions to be solved
                                myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_ds_pT_dp_uncut(x,rusr[3]); };    // we want to find the root of ds(p,T^U)/dp=0
                                mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2s_pT_dp2_uncut(x,rusr[3]); };
                            } else {
                                myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_ds_pT_dp_uncut(x,rusr[3]) + 2.*r1::data::alphaD2sdp2*x - r1::data::alphaD2sdp2*(rusr[0]+rusr[1]) ; };
                                mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2s_pT_dp2_uncut(x,rusr[3]) + 2.*r1::data::alphaD2sdp2; };
                            }
                            // then, solve and determine where we ended up
                            xRel1 = _compute_root(x._cv,x._cv,x._cc,myfPtr,mydfPtr,bounds,NULL);
                            if(isequal(xRel1,x._cv)) {
                                xSub[0] = x._cvsub;
                            } else if (isequal(xRel1,x._cc)) {
                                xSub[0] = x._ccsub;
                            } else {
                                xSub[0] = x._ccsub; // this is a dummy that will not be used since slope1x=0.;
                                xslopeZero = true;
                            }
                        }
                        cv[0] = compConvUnderest(xRel1,Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                        xslope[0] = xslopeZero? 0. : dcompConvUnderest_dp(xRel1,Op<U>::u(y._I));
                        yslope[0] = dcompConvUnderest_dT(Op<U>::l(x._I),y._cv);
                    }
                    {   // second relaxation: h(p^U,T)+h(p,T^L)-h(p^U,T^L). The pressure dependence is equal to that of h(p,T^L).
                        bool xslopeZero = false;
                        double xRel2;
                        if ( (Op<U>::l(y._I)>r1::data::TminDsdpLt0) || (Op<U>::l(x._I)>r1::data::pminDsdpLt0) ) {   // If temperature or pressure are high enough (>~277K or >~19MPa), we are decreasing in p
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                        }
                        else {
                            // here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            // first, define the functions needed to use the _compute_root function
                            const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                            double (*myfPtr)(const double,const double*,const int*);
                            double (*mydfPtr)(const double,const double*,const int*);
                             if (Op<U>::l(y._I)>r1::data::TminD2sdp2Gt0) {    // we again need to make a distinction in order to construct the correct functions to be solved
                                myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_ds_pT_dp_uncut(x,rusr[2]); };    // we want to find the root of ds(p,T^L)/dp=0
                                mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2s_pT_dp2_uncut(x,rusr[2]); };
                            } else {
                                myfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_ds_pT_dp_uncut(x,rusr[2]) + 2.*r1::data::alphaD2sdp2*x - r1::data::alphaD2sdp2*(rusr[0]+rusr[1]) ; };
                                mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2s_pT_dp2_uncut(x,rusr[2]) + 2.*r1::data::alphaD2sdp2; };
                            }
                            // then, solve and determine where we ended up
                            xRel2 = _compute_root(x._cv,x._cv,x._cc,myfPtr,mydfPtr,bounds,NULL);
                            if(isequal(xRel2,x._cv)) {
                                xSub[1]  = x._cvsub;
                            } else if (isequal(xRel2,x._cc)) {
                                xSub[1]  = x._ccsub;
                            } else {
                                xSub[1]  = x._ccsub;    // this is a dummy that will not be used since slope2x=0.;
                                xslopeZero = true;
                            }
                        }
                        cv[1] = compConvUnderest(Op<U>::u(x._I),y._cv) + compConvUnderest(xRel2,Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I));
                        xslope[1] = xslopeZero? 0. : dcompConvUnderest_dp(xRel2,Op<U>::l(y._I));
                        yslope[1] = dcompConvUnderest_dT(Op<U>::u(x._I),y._cv);
                    }

                // We now have to correct these two relaxations, since these were built using s_pT_uncut without considering that the function s_pT is cut off at smax
#ifdef LIVE_DANGEROUSLY
                    // Just default to interval bounds if we violate the smax cut (cf. explanation in case 11)
                    if ( (cv[0]>r1::data::smax) || (cv[1]>r1::data::smax) ) {
                        cv[0] = Op<U>::l(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cv[1] = Op<U>::l(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // actually do min(s_pT_uncut,smax)
                    if( Op<U>::u(result._I) >= r1::data::smax ){
                        // First, we determine the maximum of the convex underestimators on the entire host sets, which may be at (p^L,T^U) or (p^U,T^U) (recall that the underestimators are increasing in T and comp. convex)
                        // First relaxation: max of h(p,T^U)+h(p^L,T)-h(p^L,T^U)
                        const double cvU1 = std::max (  compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) */,
                                                        compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) */);
                        const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, r1::data::smax ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                        cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                        xslope[0] = k1*xslope[0];
                        yslope[0] = k1*yslope[0];
                        // Second relaxation: max of h(p^U,T)+h(p,T^L)-h(p^U,T^L)
                        const double cvU2 = std::max(   compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)),
                                                        compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) */ );
                        const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, r1::data::smax ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                        cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                        xslope[1] = k2*xslope[1];
                        yslope[1] = k2*yslope[1];
                    }
#endif

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmax(2, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
            }
            {   // concave part
                // We apply alphaBB first to get a component-wise convex overestimator
                    std::function<double(const double,const double)> compConvOverest;
                    if (Op<U>::l(y._I)>r1::data::TminD2sdp2Gt0) {    // if temperature is high enough (approx. >318K), we have comp. convexity w.r.t. p but not T
                        compConvOverest = [y](const double p, const double T) { return r1::get_s_pT_uncut(p,T) + r1::data::alphaD2sdT2*sqr(T-Op<U>::mid(y._I)); };
                    } else {    // no component-wise convexity, need alphaBB in both p and T to achieve it first
                        compConvOverest = [x,y](const double p, const double T) { return r1::get_s_pT_uncut(p,T) + r1::data::alphaD2sdp2*sqr(p-Op<U>::mid(x._I)) + r1::data::alphaD2sdT2*sqr(T-Op<U>::mid(y._I)); };
                    }

                // Since we now have componentwise convexity, we can compute the vertex polyhedral concave envelope (2 affine overestimators)
                // Note that for the concave one, the .cut() operation at the end will take care of the min(smax,...) for the actual (=cut) version of s(p,T)
                    double cc[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];
                    // Calculate function values at corner point
                    double fLL = compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I));
                    double fLU = compConvOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                    double fUL = compConvOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                    double fUU = compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));
                    // Determine which ones are the correct facets
                    double fmidLLUU = 0.5*(fLL + fUU);
                    double fmidLUUL = 0.5*(fLU + fUL);
                    if (fmidLUUL >= fmidLLUU) {
                        // In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
                        // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dh/dT>0 and hence also dh^cc/dT>0
                        // - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                        double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                        double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, LU, UL), 2: (LU, UL, UU))
                        double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
                        // First, determine the "highest" corner for facet 1 (LL, LU, UL), considering that we are increasing in T (i.e., f(LU)>=f(LL))
                        if (fUL>=fLU) {
                            fUpper1 = fUL;
                            xUpper1 = Op<U>::u(x._I);
                            yUpper1 = Op<U>::l(y._I);
                        } else {
                            fUpper1 = fLU;
                            xUpper1 = Op<U>::l(x._I);
                            yUpper1 = Op<U>::u(y._I);
                        }
                        // Next, determine monotonicity of facet 1
                        if (fUL>=fLL) {
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        } else {
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        }
                        // Next, determine the "highest" corner & monotonicity for facet 2 (LU, UL,UU), considering that we are increasing in T (i.e., f(UU)>=f(UL))
                        if (fUU>=fLU) {
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                            fUpper2 = fUU;
                            xUpper2 = Op<U>::u(x._I);
                        } else {
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                            fUpper2 = fLU;
                            xUpper2 = Op<U>::l(x._I);
                        }
                        // Now we can compute the facets
                        const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                        const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                        xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                        xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                        cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
                        cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));
                    }
                    else {
                        // In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
                        // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dh/dT>0 and hence also dh^cc/dT>0
                        // - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                        double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                        double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, UL, UU), 2: (LL, LU, UU))
                        double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
                        // First, determine the "highest" corner for facet 1 (LL, UL, UU), considering that we are increasing in T (i.e., f(UU)>=f(UL))
                        if (fUU>=fLL) {
                            fUpper1 = fUU;
                            xUpper1 = Op<U>::u(x._I);
                            yUpper1 = Op<U>::u(y._I);
                        } else {
                            fUpper1 = fLL;
                            xUpper1 = Op<U>::l(x._I);
                            yUpper1 = Op<U>::l(y._I);
                        }
                        // Next, determine monotonicity of facet 1
                        if (fUL>=fLL) {
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        } else {
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        }
                        // Next, determine the "highest" corner & monotonicity for facet 2 (LL, LU, UU), considering that we are increasing in T (i.e., f(LU)>=f(LL))
                        if (fUU>=fLU) {
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                            fUpper2 = fUU;
                            xUpper2 = Op<U>::u(x._I);
                        } else {
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                            fUpper2 = fLU;
                            xUpper2 = Op<U>::l(x._I);
                        }
                        // Now we can compute the facets
                        const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                        const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                        xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                        xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                        cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
                        cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));
                    }


                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(2, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 13:    // region 1, T(p,h)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );

            {   // convex part
                // First, we construct a component-wise concave underestimator via alphaBB
                    std::function<double(const double,const double)> compConcUnderest;
                    if ( (Op<U>::l(y._I)>r1::data::hminD2Tdh2Lt0) || (Op<U>::l(x._I)>r1::data::pminD2Tdh2Lt0) ) { // if enthalpy is high enough (>~165 kJ/kg) or pressure is high enough (>~16 MPa), we have comp. concavity in both p and h already
                        compConcUnderest = [y](const double p, const double h) { return r1::get_T_ph_uncut(p,h); };
                    } else {
                        compConcUnderest = [y](const double p, const double h) { return r1::get_T_ph_uncut(p,h) - r1::data::alphaD2Tdh2*sqr(h-Op<U>::mid(y._I)); };
                    }

                // Since we now have componentwise concavity, we can construct a convex understimator consisting of two affine facets
                    double cv[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];
                    // Get function values at corner point
                        const double fLL = compConcUnderest(Op<U>::l(x._I),Op<U>::l(y._I));
                        const double fLU = compConcUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                        const double fUL = compConcUnderest(Op<U>::u(x._I),Op<U>::l(y._I));
                        const double fUU = compConcUnderest(Op<U>::u(x._I),Op<U>::u(y._I));
                    // Make sure we have the correct facets
                        const double fmidLLUU = 0.5*(fLL + fUU);
                        const double fmidLUUL = 0.5*(fLU + fUL);
                        if (fmidLUUL > fmidLLUU) {

                            // In general, we get two facets. However, we need to determine the monotonicity properties & minimum points of the relaxation first:
                            // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dT/dh>=0 and hence also dT^cc/dh>=0
                            // - the minimum point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                            double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                            double fLower1, fLower2;                    // smallest function values at the corners of the two facets (1: (LL, UL, UU), 2: (LL, LU UU))
                            double xLower1, xLower2, yLower2;           // x (and y, for facet 2) values at which fLower1 and fLower2 2 are achieved
                            // First, determine the "lowest" corner for facet 2 (LL, LU UU), considering that we are increasing in h (i.e., f(LU)>=f(LL))
                            if (fUU<=fLL) {
                                fLower2 = fUU;
                                xLower2 = Op<U>::u(x._I);
                                yLower2 = Op<U>::u(y._I);
                            } else {
                                fLower2 = fLL;
                                xLower2 = Op<U>::l(x._I);
                                yLower2 = Op<U>::l(y._I);
                            }
                            // Next, determine monotonicity of facet 2 (LL, LU UU)
                            if (fLU<=fUU) {
                                xRel2 = x._cv;
                                xSub[1] = x._cvsub;
                            } else {
                                xRel2 = x._cv;
                                xSub[1] = x._cvsub;
                            }
                            // Next, determine the "lowest" corner & monotonicity for facet 1 (LL, UL, UU), considering that we are increasing in h (i.e., f(UU)>=f(UL))
                            if (fUL<=fLL) {
                                xRel1 = x._cc;
                                xSub[0] = x._ccsub;
                                fLower1 = fUL;
                                xLower1 = Op<U>::u(x._I);
                            } else {
                                xRel1 = x._cv;
                                xSub[0] = x._cvsub;
                                fLower1 = fLL;
                                xLower1 = Op<U>::l(x._I);
                            }
                            // Actually compute the facets
                            const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                            const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                            xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                            yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                            cv[0] = fLower1 + xslope[0]*(xRel1 - xLower1) +  yslope[0]*(y._cv - Op<U>::l(y._I));
                            xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                            yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                            cv[1] = fLower2 + xslope[1]*(xRel2 - xLower2) +  yslope[1]*(y._cv - yLower2);

                        }
                        else {

                            // In general, we get two facets. However, we need to determine the monotonicity properties & minimum points of the relaxation first:
                            // - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dT/dh>=0 and hence also dT^cc/dh>=0
                            // - the minimum point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
                            double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
                            double fLower1, fLower2;                    // smallest function values at the corners of the two facets (1: (LL, LU, UL), 2: (LU, UL, UU))
                            double xLower1, xLower2, yLower2;           // x (and y, for facet 2) values at which fLower1 and fLower2 2 are achieved
                            // First, determine the "lowest" corner for facet 2 (LU, UL,UU), considering that we are increasing in h (i.e., f(LU)>=f(LL))
                            if (fUL<=fLU) {
                                fLower2 = fUL;
                                xLower2 = Op<U>::u(x._I);
                                yLower2 = Op<U>::l(y._I);
                            } else {
                                fLower2 = fLU;
                                xLower2 = Op<U>::l(x._I);
                                yLower2 = Op<U>::u(y._I);
                            }
                            // Next, determine monotonicity of facet 2 (LU, UL,UU)
                            if (fUU>=fLU) {
                                xRel2 = x._cv;
                                xSub[1] = x._cvsub;
                            } else {
                                xRel2 = x._cv;
                                xSub[1] = x._cvsub;
                            }
                            // Next, determine the "lowest" corner & monotonicity for facet 1 (LL, LU, UL), considering that we are increasing in h (i.e., f(LU)>=f(LL))
                            if (fLL>=fUL) {
                                xRel1 = x._cc;
                                xSub[0] = x._ccsub;
                                fLower1 = fUL;
                                xLower1 = Op<U>::u(x._I);
                            } else {
                                xRel1 = x._cv;
                                xSub[0] = x._cvsub;
                                fLower1 = fLL;
                                xLower1 = Op<U>::l(x._I);
                            }
                            // Actually compute the facets
                            const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                            const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                            xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                            yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                            cv[0] = fLower1 + xslope[0]*(xRel1 - xLower1) +  yslope[0]*(y._cv - Op<U>::l(y._I));
                            xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                            yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                            cv[1] = fLower2 + xslope[1]*(xRel2 - xLower2) +  yslope[1]*(y._cv - yLower2);

                        }

                // We now have to correct these two relaxations, since these were built using T_ph_uncut without considering that the function T_ph is cut off at Tmax (and Tmin, but this is being taken care of by the .cut() function (and not critical for the convex relaxation))
#ifdef LIVE_DANGEROUSLY
                    // Just default to interval bounds if we violate the Tmax cut (cf. explanation in case 11)
                    if ( (cv[0]>r1::data::Tmax) || (cv[1]>r1::data::Tmax) ) {
                        cv[0] = Op<U>::l(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cv[1] = Op<U>::l(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // Actually do min(T_ph_uncut,Tmax)
                    if( Op<U>::u(result._I) >= r1::data::Tmax ){
                        // First, we determine the maximum of the convex underestimators on the entire host sets, which may be at (p^L,T^U) or (p^U,T^U) (recall that the underestimators are increasing in T and comp. convex)
                        // First relaxation: max of h(p,T^U)+h(p^L,T)-h(p^L,T^U)
                        const double cvU1 = std::max ( fUL, fLU);
                        const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, r1::data::Tmax ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                        cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                        xslope[0] = k1*xslope[0];
                        yslope[0] = k1*yslope[0];
                        // Second relaxation: max of h(p^U,T)+h(p,T^L)-h(p^U,T^L)
                        const double cvU2 = std::max( fUU, fLU );
                        const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, r1::data::Tmax ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                        cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                        xslope[1] = k2*xslope[1];
                        yslope[1] = k2*yslope[1];
                    }
#endif

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmax(2, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
            }
            {   // concave part
                // Depending on where we are, T(p,h) may be component-wise concave or only almost so. In the latter cases, we apply alphaBB first to get a component-wise concave overestimator
                    std::function<double(const double,const double)> compConcOverest, dcompConcOverest_dh, dcompConcOverest_dp;
                    if ( (Op<U>::l(y._I)>r1::data::hminD2Tdh2Lt0) || (Op<U>::l(x._I)>r1::data::pminD2Tdh2Lt0) ) { // if enthalpy is high enough (>~165 kJ/kg) or pressure is high enough (>~16 MPa), we have comp. concavity in both p and h
                        compConcOverest = [](const double p, const double h) { return r1::get_T_ph_uncut(p,h); };
                        dcompConcOverest_dh = [](const double p, const double h) { return r1::derivatives::get_dT_ph_dh_uncut(p,h); };
                        dcompConcOverest_dp = [](const double p, const double h) { return r1::derivatives::get_dT_ph_dp_uncut(p,h); };
                   } else {    // no component-wise concavity in h, need alphaBB in h first to achieve it
                        compConcOverest = [y](const double p, const double h) { return r1::get_T_ph_uncut(p,h) - r1::data::alphaD2Tdh2*(h-Op<U>::l(y._I))*(h-Op<U>::u(y._I)); };
                        dcompConcOverest_dh = [y](const double p, const double h) { return r1::derivatives::get_dT_ph_dh_uncut(p,h) - 2.*r1::data::alphaD2Tdh2*h + r1::data::alphaD2Tdh2*(Op<U>::l(y._I)+Op<U>::u(y._I)); };
                        dcompConcOverest_dp = [](const double p, const double h) { return r1::derivatives::get_dT_ph_dp_uncut(p,h); };
                    }


                // Since we now have componentwise concavity and d2T/dph>=0, we can apply Theorem 1 from Najman, Bongartz, Mitsos (2019), CACE, to get two valid relaxations
                    double cc[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
                    double* xSub[2];

                    // Within this region, we always have dT/dh>0. However, depending on where we are, T(p,h) and its relaxations may or may not be monotonic w.r.t. p.
                    // We thus need to distinguish different cases to select the correct p coordinate for the solution of the auxiliary problem for multivariate McCormick (for h (i.e., y), it is always at h^cc)
                    {   // First relaxation: T(p,h^U)+T(p^L,h)-T(p^L,h^U). The pressure dependence is equal to that of T(p,h^U).
                        bool xslopeZero = false;
                        double xRel1;
                        // Select the correct p coordinate for multivariate McCormick
                        if (Op<U>::u(y._I)>r1::data::hminDTdpLt0) { // if h^U>~1517kJ/kg, this relaxation is monotonically increasing in p
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        }
                        else {
                            // Here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            // First, we need to search for the limit between the relaxed physical region and the extension at h^U (i.e., pBound s.t. hliq(pBound)=h^U)
                                const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                                double (*myfPtr)(const double,const double*,const int*) = [](const double x, const double*rusr, const int*iusr) {
                                    const double Ts = r4::original::get_Ts_p(x);
                                    return r2::original::get_h_pT(x,Ts)-rusr[3];
                                };    // we want to solve of hliq(p)-h^U=0
                                double (*mydfPtr)(const double,const double*,const int*)= [](const double x, const double*rusr, const int*iusr) {
                                    const double Ts = r4::original::get_Ts_p(x);
                                    const double dTsdp = r4::original::derivatives::get_dTs_dp(x);
                                    return r2::original::derivatives::get_dh_pT_dp(x,Ts) + r2::original::derivatives::get_dh_pT_dT(x,Ts)*dTsdp;
                                };
                                const double pBound = _compute_root(Op<U>::l(x._I),Op<U>::l(x._I),Op<U>::u(x._I),myfPtr,mydfPtr,bounds,NULL);
                            // Now we can check a few things to avoid having to solve dT/dp=0 for p*
                                bool shortcutTaken = false;
                                if (pBound > x._cc) {   // we are completely in the extension, where dT/dp>=0
                                    xRel1 = x._cc;
                                    xSub[0] = x._ccsub;
                                    shortcutTaken = true;
                                } else {
                                    if (Op<U>::u(y._I)<r1::data::hmaxDTdpGt0) { // if h^U<1056~1517kJ/kg, the relaxation in the physical region is monotonically decreasing in p
                                        if (pBound > x._cv) {
                                            xRel1 = pBound;
                                            xSub[0] = x._cvsub; // this is a dummy that will not be used since slope1x=0.;
                                            xslopeZero = true;
                                        } else {
                                            xRel1 = x._cv;
                                            xSub[0] = x._cvsub;
                                        }
                                        shortcutTaken = true;
                                    } else {
                                        const double dhhatReldpTop = dcompConcOverest_dp(x._cc,Op<U>::u(y._I)) ;
                                        if (dhhatReldpTop>=0) { // this ensures that we are completely in a region where dT/dp>=0
                                            xRel1 = x._cc;
                                            xSub[0] = x._ccsub;
                                            shortcutTaken = true;
                                        } else {
                                            if (pBound > x._cv) {
                                                const double dhhatReldpBound = dcompConcOverest_dp(pBound,Op<U>::u(y._I)) ;
                                                if (dhhatReldpBound <= 0) {
                                                    xRel1 = pBound;
                                                    xSub[0] = x._cvsub; // this is a dummy that will not be used since slope1x=0.;
                                                    xslopeZero = true;
                                                    shortcutTaken = true;
                                                }
                                            } else {
                                                const double dhhatReldpBot = dcompConcOverest_dp(x._cv,Op<U>::u(y._I)) ;
                                                if (dhhatReldpBot <= 0) {
                                                    xRel1 = x._cv;
                                                    xSub[0] = x._cvsub;
                                                    shortcutTaken = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            // If none of the checks succeeded, we actually need to solve dT/dp=0 for p*
                                if (!shortcutTaken) {
                                    // first, define the functions needed to use the _computes_root function
                                    double (*myfPtr)(const double,const double*,const int*) = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dT_ph_dp_uncut(x,rusr[3]); };    // we want to find the root of dT(p,h^U)/dp=0
                                    double (*mydfPtr)(const double,const double*,const int*)= [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2T_ph_dp2_uncut(x,rusr[3]); };
                                    // then, solve and determine where we ended up
                                    const double xLower = std::max(x._cv,pBound);
                                    xRel1 = _compute_root(xLower,xLower,x._cc,myfPtr,mydfPtr,bounds,NULL);
                                    if(isequal(xRel1,x._cv)) {
                                        xSub[0] = x._cvsub;
                                    } else if (isequal(xRel1,x._cc)) {
                                        xSub[0] = x._ccsub;
                                    } else {
                                        xSub[0] = x._ccsub; // this is a dummy that will not be used since slope1x=0.;
                                        xslopeZero = true;
                                    }
                                }
                        }
                        cc[0] = compConcOverest(xRel1,Op<U>::u(y._I)) + compConcOverest(Op<U>::l(x._I),y._cc) - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                        xslope[0] = xslopeZero? 0. : dcompConcOverest_dp(xRel1,Op<U>::u(y._I));
                        yslope[0] = dcompConcOverest_dh(Op<U>::l(x._I),y._cc);
                    }
                    {   // second relaxation: T(p^U,h)+T(p,h^L)-T(p^U,h^L). The pressure dependence is equal to that of T(p,h^L).
                        bool xslopeZero = false;
                        double xRel2;
                        // Select the correct p coordinate for multivariate McCormick
                        if (Op<U>::l(y._I)>r1::data::hminDTdpLt0) { // if h^l>~1517kJ/kg, this relaxation is monotonically increasing in p
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                        }
                        else {
                            // Here, we need to look more closely since the relaxation may not be monotonic w.r.t. p
                            // First, we need to search for the limit between the relaxed physical region and the extension at h^U (i.e., pBound s.t. hliq(pBound)=h^L)
                                const double bounds[4] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I)};
                                double (*myfPtr)(const double,const double*,const int*) = [](const double x, const double*rusr, const int*iusr) {
                                    const double Ts = r4::original::get_Ts_p(x);
                                    return r2::original::get_h_pT(x,Ts)-rusr[2];
                                };    // we want to solve of hliq(p)-h^L=0
                                double (*mydfPtr)(const double,const double*,const int*)= [](const double x, const double*rusr, const int*iusr) {
                                    const double Ts = r4::original::get_Ts_p(x);
                                    const double dTsdp = r4::original::derivatives::get_dTs_dp(x);
                                    return r2::original::derivatives::get_dh_pT_dp(x,Ts) + r2::original::derivatives::get_dh_pT_dT(x,Ts)*dTsdp;
                                };
                                const double pBound = _compute_root(Op<U>::l(x._I),Op<U>::l(x._I),Op<U>::u(x._I),myfPtr,mydfPtr,bounds,NULL);
                            // Now we can check a few things to avoid having to solve dT/dp=0 for p*
                                bool shortcutTaken = false;
                                if (pBound > x._cc) {   // we are completely in the extension, where dT/dp>=0
                                    xRel2 = x._cc;
                                    xSub[1] = x._ccsub;
                                    shortcutTaken = true;
                                } else {
                                    if (Op<U>::l(y._I)<r1::data::hmaxDTdpGt0) { // if h^U<1056~1517kJ/kg, the relaxation in the physical region is monotonically decreasing in p
                                        if (pBound > x._cv) {
                                            xRel2 = pBound;
                                            xSub[1] = x._cvsub; // this is a dummy that will not be used since slope1x=0.;
                                            xslopeZero = true;
                                        } else {
                                            xRel2 = x._cv;
                                            xSub[1] = x._cvsub;
                                        }
                                        shortcutTaken = true;
                                    } else {
                                        const double dhhatReldpTop = dcompConcOverest_dp(x._cc,Op<U>::l(y._I)) ;
                                        if (dhhatReldpTop>=0) { // this ensures that we are completely in a region where dT/dp>=0
                                            xRel2 = x._cc;
                                            xSub[1] = x._ccsub;
                                            shortcutTaken = true;
                                        } else {
                                            if (pBound > x._cv) {
                                                const double dhhatReldpBound = dcompConcOverest_dp(pBound,Op<U>::l(y._I)) ;
                                                if (dhhatReldpBound <= 0) {
                                                    xRel2 = pBound;
                                                    xSub[1] = x._cvsub; // this is a dummy that will not be used since slope1x=0.;
                                                    xslopeZero = true;
                                                    shortcutTaken = true;
                                                }
                                            } else {
                                                const double dhhatReldpBot = dcompConcOverest_dp(x._cv,Op<U>::l(y._I)) ;
                                                if (dhhatReldpBot <= 0) {
                                                    xRel2 = x._cv;
                                                    xSub[1] = x._cvsub;
                                                    shortcutTaken = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            // If none of the checks succeeded, we actually need to solve dT/dp=0 for p*
                                if (!shortcutTaken) {
                                    // first, define the functions needed to use the _computes_root function
                                    double (*myfPtr)(const double,const double*,const int*) = [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_dT_ph_dp_uncut(x,rusr[2]); };    // we want to find the root of dT(p,h^L)/dp=0
                                    double (*mydfPtr)(const double,const double*,const int*)= [](const double x, const double*rusr, const int*iusr) { return r1::derivatives::get_d2T_ph_dp2_uncut(x,rusr[2]); };
                                    // then, solve and determine where we ended up
                                    const double xLower = std::max(x._cv,pBound);
                                    xRel2 = _compute_root(xLower,xLower,x._cc,myfPtr,mydfPtr,bounds,NULL);
                                    if(isequal(xRel2,x._cv)) {
                                        xSub[1] = x._cvsub;
                                    } else if (isequal(xRel2,x._cc)) {
                                        xSub[1] = x._ccsub;
                                    } else {
                                        xSub[1] = x._ccsub; // this is a dummy that will not be used since slope1x=0.;
                                        xslopeZero = true;
                                    }
                                }
                        }
                        cc[1] = compConcOverest(Op<U>::u(x._I),y._cc) + compConcOverest(xRel2,Op<U>::l(y._I)) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                        xslope[1] = xslopeZero? 0. : dcompConcOverest_dp(xRel2,Op<U>::l(y._I));
                        yslope[1] = dcompConcOverest_dh(Op<U>::u(x._I),y._cc);
                    }


                // We now have to correct these two relaxations, since these were built using T_ph_uncut without considering that the function T_ph is cut off at Tmin (and Tmax, but this is being taken care of by the .cut() function (and not critical for the concave relaxation))
#ifdef LIVE_DANGEROUSLY
                    // Just default to interval bounds if we violate the Tmin cut (cf. explanation in case 11)
                    if ( (cc[0]<r1::data::Tmin) || (cc[1]<r1::data::Tmin) ) {
                        cc[0] = Op<U>::u(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cc[1] = Op<U>::u(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // Actually do max(T_ph_uncut,Tmin)
                    if( Op<U>::l(result._I) <= r1::data::Tmin ){
                        // Minimum of the relaxation on the entire domain:
                        const double ccL1 = std::min( /* compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I)) */ + compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I)) /* - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I)) */,
                                                         compConcOverest(Op<U>::u(x._I),Op<U>::u(y._I))    + compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I))    - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I))    );
                        const double k1 = isequal( Op<U>::u(result._I), ccL1) ? 0. : (( std::max( ccL1, r1::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL1)) ;
                        cc[0] = Op<U>::u(result._I) - k1 * ( cc[0] - Op<U>::u(result._I) );
                        xslope[0] = - k1*xslope[0];
                        yslope[0] = - k1*yslope[0];
                        // Minimum of the relaxation on the entire domain:
                        const double ccL2 = std::min( /* compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */  + compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I)) /* - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */ ,
                                                      /* compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */  + compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) /* - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */ );
                       const double k2 = isequal( Op<U>::u(result._I), ccL2) ? 0. : (( std::max( ccL2, r1::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL2)) ;
                        cc[1] = Op<U>::u(result._I) - k2 * ( cc[1] - Op<U>::u(result._I) );
                        xslope[1] = - k2*xslope[1];
                        yslope[1] = - k2*yslope[1];
                    }
#endif

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(2, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 14:   // region 1, T(p,s)
        {
            // Since we do not invoke the interval library for this function, we have to check bound violations here
            if (Op<U>::l(x._I)<r1::data::pmin) { throw std::runtime_error("mc::McCormick\t IAPWS-IF97, region 1, T(p,s) with p<pmin in range: " + std::to_string(Op<U>::l(x._I))); }
            if (Op<U>::u(x._I)>r1::data::pmax) { throw std::runtime_error("mc::McCormick\t IAPWS-IF97, region 1, T(p,s) with p>pmax in range: " + std::to_string(Op<U>::u(x._I))); }
            if (Op<U>::l(y._I)<r1::data::smin) { throw std::runtime_error("mc::McCormick\t IAPWS-IF97, region 1, T(p,s) with s<smin in range: " + std::to_string(Op<U>::l(y._I))); }
            if (Op<U>::u(y._I)>r1::data::smax) { throw std::runtime_error("mc::McCormick\t IAPWS-IF97, region 1, T(p,s) with s>smax in range: " + std::to_string(Op<U>::u(y._I))); }


            // Compute regular McCormick relaxations first, since these turn out not to be too bad
                McCormick<U> result = r1::original::get_T_ps(x,y);

            // However, we now have to correct the relaxations, since they were built using the original T_ps without considering that the function T_ps is cut off at Tmin and Tmax
#ifdef LIVE_DANGEROUSLY
                // Just default to interval bounds if we violate the Tmin/Tmax cuts (cf. explanation in case 11)
                if ( result._cc<r1::data::Tmin ) {
                    result._cc = Op<U>::u(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ){
                        result._ccsub[i] =  0.;
                    }
                }
                if ( result._cv>r1::data::Tmax ) {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ){
                        result._cvsub[i] =  0.;
                    }
                }
                result._I = U( std::max(Op<U>::l(result._I),r1::data::Tmin),
                               std::min(Op<U>::u(result._I),r1::data::Tmax));
#else
                // Actually do max(T_ps_uncut, hmin)
                result = max(min(result,r1::data::Tmax),r1::data::Tmin);
#endif

            // Now, adding two ad-hox cuts (one for the concave relaxation, one for the convex one) that help for large intervals
                {   // convex part
                    const double cvAdhoc = 270. + 65.*(y._cv+0.0086) + 5.8*std::pow(y._cv+0.0086,2);
                    if (cvAdhoc > result._cv) {
                        result._cv = cvAdhoc;
                        const double yslope = 65. + 2.*5.8*(y._cv+0.0086);
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = /* 0.*(x._const? 0.: x._cvsub(i)) */ + yslope*(y._const? 0.:y._cvsub[i]); }

                    }
                }
                {   // concave part
                    const double ccAdhoc = 278. + 105.*(y._cc+0.0086);
                    if (ccAdhoc < result._cc) {
                        result._cc = ccAdhoc;
                        const double yslope = 105.;
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = /* 0.*(x._const? 0.: x._ccsub(i)) */ + yslope*(y._const? 0.:y._ccsub[i]); }
                    }
                }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 15:    // region 1, h(p,s)
        {
            return iapws(x,iapws(x,y,14),11);   // h(p,T(p,s))
        }
        case 16:    // region 1, s(p,h)
        {
            return iapws(x,iapws(x,y,13),12);   // s(p,T(p,h))
        }
        case 21:    // region 2, h(p,T)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );
            if( isequal(Op<U>::u(result._I),r2::data::hmin) ){
                result._cv = Op<U>::l(result._I);
                result._cc = Op<U>::u(result._I);
                for( unsigned int i=0; i<result._nsub; i++ ){
                    result._cvsub[i] =  0.;
                    result._ccsub[i] =  0.;
                }
                return result;
            }

            if ( (Op<U>::u(x._I)<=r2::data::pmaxD2hdpTgt0) ) { // if pressure is low enough (~< 60MPa), we have d2h/dpT>=0 and we can achieve componentwise concavity w.r.t. p and T using alphaBB and then use Theorem 1 (Najman, Bongartz, Mitsos (2019) CACE)

                {   // convex part
                    double cv[6], xslope[6], yslope[6]; // We will derive up to six relaxations and choose the strongest
                    unsigned nrelax = 0.;               // Number of actually derived relaxations (not all are applicable in all situations)

                    // First, we construct a component-wise concave underestimator via component-wise alphaBB: h(p,T)-alphaD2hdT2*(T-Tmid)^2-alphaD2hdp2*(p-pmid)^2; we only need the last term if p>~28.9MPa
                        double alphaT;
                        if ((Op<U>::u(y._I)<=350.)||(Op<U>::l(y._I)>=375.)) { // if our domain does not contain the temperature range (350,375), we can use a smaller alpha for T
                            alphaT = r2::data::alphaD2hdT2Tlt350orgt375;
                        } else {
                            alphaT = r2::data::alphaD2hdT2;
                        }
                        std::function<double(const double,const double)> compConcUnderest;
                        if (Op<U>::u(x._I)<r2::data::pmaxD2hdp2Lt0) {   // if pressure is low enough (p<~28.7MPa), we do not need the term with alphaD2hdp2
                            compConcUnderest  = [y,alphaT](const double p, const double T) { return r2::get_h_pT_uncut(p,T) - alphaT*sqr(T-Op<U>::mid(y._I));  };
                        } else {
                            compConcUnderest = [x,y,alphaT](const double p, const double T) { return r2::get_h_pT_uncut(p,T) - alphaT*sqr(T-Op<U>::mid(y._I)) - r2::data::alphaD2hdp2*sqr(p-Op<U>::mid(x._I));  };
                        }

                    // Next, construct the piecewise affine convex envelope of this component-wise concave underestimator
                        // Get function values at corner point (we now that the minimum is at fUL and the maximum at fLU
                        const double fLL = compConcUnderest(Op<U>::l(x._I),Op<U>::l(y._I));
                        const double fLU = compConcUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                        const double fUL = compConcUnderest(Op<U>::u(x._I),Op<U>::l(y._I));
                        const double fUU = compConcUnderest(Op<U>::u(x._I),Op<U>::u(y._I));
                        // Make sure we have the correct facets
                        const double fmidLLUU = 0.5*(fLL + fUU);
                        const double fmidLUUL = 0.5*(fLU + fUL);
                        if (fmidLUUL > fmidLLUU) {
                            throw std::runtime_error("mc::McCormick: Error in IAPWS region 2, h(p,T), convex relaxations: facets are different than expected! Please report this to the developers");
                        }
                        // Actually compute the facets
                        const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                        const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                        xslope[nrelax] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
                        yslope[nrelax] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
                        cv[nrelax] = fUL + xslope[nrelax]*(x._cc - Op<U>::u(x._I)) +  yslope[nrelax]*(y._cv - Op<U>::l(y._I));
                        nrelax++;
                        xslope[nrelax] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
                        yslope[nrelax] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
                        cv[nrelax] = fUL + xslope[nrelax]*(x._cc - Op<U>::u(x._I)) +  yslope[nrelax]*(y._cv - Op<U>::l(y._I));
                        nrelax++;

                    // Next, since the actual function values of h(p,T) go really far below zero for values (p,T) that are actually cut off by constraints (they are in regions 1 or 3), we introduce 2 additional cuts that strengthen the relaxations when considering intervals containing such regions
                        const double Tlim = (Op<U>::u(x._I)<=r2::data::pminB23) ? r4::get_Ts_p(Op<U>::u(x._I)) : r2::get_b23_T_p(Op<U>::u(x._I)); // calculate temperature for cutoff at hmin (conservative cutoff at max(Tsat(p^U),Tb23(p^U)))
                         if ( (fUU>r2::data::hmin) && (!isequal(Op<U>::u(y._I),Tlim)) && (y._cv>Tlim) ) {
                            xslope[nrelax] = 0.;
                            yslope[nrelax] = ( fUU - r2::data::hmin )/( Op<U>::u(y._I) - Tlim );
                            cv[nrelax] = r2::data::hmin + yslope[nrelax]*( y._cv - Tlim );
                            nrelax++;
                        }
                        const double plim = (Op<U>::l(y._I)<=r1::data::Tmax) ? r4::get_ps_T(Op<U>::l(y._I)) : r2::get_b23_p_T(Op<U>::l(y._I)); // calculate pressure for cutoff at hmin (conservative cutoff at min(psat(T^L),pB23(T^L)))
                        if ( (fLL>r2::data::hmin) && (!isequal(Op<U>::l(x._I),plim)) && (x._cc<plim) ) {
                            xslope[nrelax] = ( r2::data::hmin - fLL )/( plim - Op<U>::l(x._I));
                            yslope[nrelax] = 0.;
                            cv[nrelax] = r2::data::hmin + xslope[nrelax]*( x._cc - plim );
                            nrelax++;
                        }

                    // Since the above relaxations are somewhat weak for large intervals, we include two "ad hoc" cuts that are valid on the entire domain.
                    // They do not converge, but help for large intervals. For smaller intervals, the above relaxations take over.
                        xslope[nrelax] = -5;
                        yslope[nrelax] = 5.5;
                        cv[nrelax] = 4150. + xslope[nrelax]*x._cc       + yslope[nrelax]*(y._cv-1073.15);
                        nrelax++;
                        xslope[nrelax] = -25;
                        yslope[nrelax] = 2.6;
                        cv[nrelax] = 2500. + xslope[nrelax]*(x._cc-15.) + yslope[nrelax]*(y._cv- 630.  ) ;
                        nrelax++;

                    // Finally, store the best relaxation with the corresponding subgradient
                        const unsigned irelax = argmax(nrelax, cv);
                        result._cv = cv[irelax];
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
                }
                {   // concave part
                    double cc[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest

                    // First, we construct a component-wise concave overestimator via component-wise alphaBB: h(p,T) - alphaT*(T-T^L)*(T-T^U) - alphaD2hdp2*(p-p^L)*(p-p^U) + kAlphaD2hdp2*(p-p^U) ;
                        // The last two terms are only needed if p>~28.9MPa. The linear term in p is added to make sure that we maintain monotonicity of the relaxation
                            double alphaT;
                            if ((Op<U>::u(y._I)<=350.)||(Op<U>::l(y._I)>=375.)) { // if our domain does not contain the temperature range (350,375), we can use a smaller alpha for T
                                alphaT = r2::data::alphaD2hdT2Tlt350orgt375;
                            } else {
                                alphaT = r2::data::alphaD2hdT2;
                            }
                            std::function<double(const double,const double)> compConcOverest, dcompConcOverest_dp, dcompConcOverest_dT;
                            if (Op<U>::u(x._I)<r2::data::pmaxD2hdp2Lt0) {   // if pressure is low enough (p<~28.7MPa), we do not need the term with alphaD2hdp2
                                compConcOverest = [y,alphaT](const double p, const double T) { return r2::get_h_pT_uncut(p,T) - alphaT*(T-Op<U>::l(y._I))*(T-Op<U>::u(y._I));  };
                                dcompConcOverest_dp = [](const double p, const double T) { return r2::derivatives::get_dh_pT_dp_uncut(p,T);  };
                                dcompConcOverest_dT = [y,alphaT](const double p, const double T) { return r2::derivatives::get_dh_pT_dT_uncut(p,T) - 2.*alphaT*T + alphaT*(Op<U>::l(y._I)+Op<U>::u(y._I)); };
                            } else {
                                compConcOverest = [x,y,alphaT](const double p, const double T) { return r2::get_h_pT_uncut(p,T) - alphaT*(T-Op<U>::l(y._I))*(T-Op<U>::u(y._I))
                                                                                                                                - r2::data::alphaD2hdp2*(p-Op<U>::l(x._I))*(p-Op<U>::u(x._I)) + r2::data::kAlphaD2hdp2*(p-Op<U>::u(x._I)); };
                                dcompConcOverest_dp = [x](const double p, const double T) { return r2::derivatives::get_dh_pT_dp_uncut(p,T) - 2.*r2::data::alphaD2hdp2*p + r2::data::alphaD2hdp2*(Op<U>::l(x._I)+Op<U>::u(x._I)) + r2::data::kAlphaD2hdp2;  };
                                dcompConcOverest_dT = [y,alphaT](const double p, const double T) { return r2::derivatives::get_dh_pT_dT_uncut(p,T) - 2.*alphaT*T + alphaT*(Op<U>::l(y._I)+Op<U>::u(y._I)); };
                            }

                    // We then apply Theorem 1 of Najman, Bongartz, Mitsos: CACE (2019) to compConcOverest and add some additional cuts
                        // Get first concave relaxation based on Theorem 1: f(x,y^L)+f(x^U,y)-f(x^U,y^L)
                        cc[0] = compConcOverest(x._cv,Op<U>::l(y._I)) + compConcOverest(Op<U>::u(x._I),y._cc) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                        xslope[0] = dcompConcOverest_dp(x._cv,Op<U>::l(y._I));
                        yslope[0] = dcompConcOverest_dT(Op<U>::u(x._I),y._cc);
                        // Get second concave relaxation based on Theorem 1: f(x,y^u)+f(x^L,y)-f(x^L,y^U)
                        cc[1] = compConcOverest(x._cv,Op<U>::u(y._I)) + compConcOverest(Op<U>::l(x._I),y._cc) - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                        xslope[1] = dcompConcOverest_dp(x._cv,Op<U>::u(y._I));
                        yslope[1] = dcompConcOverest_dT(Op<U>::l(x._I),y._cc);

                    // We now have to correct these two relaxations, since these were built using h_pT_uncut without considering that the function h_pT is cut off at hmin
#ifdef LIVE_DANGEROUSLY
                        // Just default to interval bounds if we violate the hmin cut (cf. explanation in case 11)
                        if ( (cc[0]<r2::data::hmin) || (cc[1]<r2::data::hmin) ) {
                            cc[0] = Op<U>::u(result._I);
                            xslope[0] = 0.;
                            yslope[0] = 0.;
                            cc[1] = Op<U>::u(result._I);
                            xslope[1] = 0.;
                            yslope[1] = 0.;
                        }
#else
                        // Actually do max(h_pT_uncut, hmin)
                        if( Op<U>::l(result._I) <= r2::data::hmin ){
                            const double ccL1 = compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)); // Minimum of the relaxation on the entire domain
                            const double k1 = isequal( Op<U>::u(result._I), ccL1) ? 0. : (( std::max( ccL1, r2::data::hmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL1)) ;
                            cc[0] = Op<U>::u(result._I) - k1 * ( cc[0] - Op<U>::u(result._I) );
                            xslope[0] = - k1*xslope[0];
                            yslope[0] = - k1*yslope[0];
                            const double ccL2 = compConcOverest(Op<U>::u(x._I),Op<U>::u(y._I)) + compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I)) - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I));// Minimum of the relaxation on the entire domain
                            const double k2 = isequal( Op<U>::u(result._I), ccL2) ? 0. : (( std::max( ccL2, r2::data::hmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL2)) ;
                            cc[1] = Op<U>::u(result._I) - k2 * ( cc[1] - Op<U>::u(result._I) );
                            xslope[1] = - k2*xslope[1];
                            yslope[1] = - k2*yslope[1];
                        }
#endif

                    // Since the above relaxations are somewhat weak for large intervals, we include two "ad hoc" cuts that are valid on the entire domain.
                    // They do not converge, but help for large intervals. For smaller intervals, the above relaxations take over.
                        xslope[2] = 0.;
                        yslope[2] = 2.075;
                        xslope[3] = - 4.5;
                        yslope[3] = 1.75;
                        cc[2] = 2510.                         + yslope[2]*(y._cc-273.15);
                        cc[3] = 2510. + xslope[3]*(x._cv-60.) + yslope[3]*(y._cc-273.15) ;

                    // Finally, store the best relaxation with the corresponding subgradient
                        const unsigned irelax = argmin(4, cc);
                        result._cc = cc[irelax];
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._cvsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
                }

            }
            else {  // no special convexity properties :-( need to rely on (exact) interval bounds and ad-hoc cuts

                {   // convex part
                    double cv[2], xslope[2], yslope[2]; // Using 2 ad-hoc cuts
                    xslope[0] = -5;
                    yslope[0] = 5.5;
                    xslope[1] = -25;
                    yslope[1] = 2.6;
                    cv[0] = 4150. + xslope[0]*x._cc       + yslope[0]*(y._cv-1073.15);
                    cv[1] = 2500. + xslope[1]*(x._cc-15.) + yslope[1]*(y._cv- 630.  ) ;
                    const unsigned irelax = argmax(2, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
                }
                {   // concave part
                    double cc[2], xslope[2], yslope[2]; // Using 2 ad-hoc cuts
                    xslope[0] = 0.;
                    yslope[0] = 2.075;
                    xslope[1] = - 4.5;
                    yslope[1] = 1.54;
                    cc[0] = 2510.                          + yslope[0]*(y._cc-273.15);
                    cc[1] = 2510. + xslope[1]*(x._cv-100.) + yslope[1]*(y._cc-273.15) ;
                    const unsigned irelax = argmin(2, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._cvsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
                }

            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 22:    // region 2, s(p,T)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );
            if( isequal(Op<U>::u(result._I),r2::data::smin) ){
                result._cv = Op<U>::l(result._I);
                result._cc = Op<U>::u(result._I);
                for( unsigned int i=0; i<result._nsub; i++ ){
                    result._cvsub[i] =  0.;
                    result._ccsub[i] =  0.;
                }
                return result;
            }

            // We want to exploit component-wise convexity using Theorem 1 of Najman, Bongartz, Mitsos: CACE (2019) and Meyer & Floudas: Math. Prog. (2005)
            // However, s_pT_uncut is component-wise convex w.r.t. p only in some areas; in others, need alphaBB to make it so first; constructing in a way to preserve monotonicity w.r.t. p
                double alphaP;
                if ( Op<U>::l(y._I) > r2::data::TminD2sdp2Gt0 ) { // For T>~793K, we have component-wise convexity already
                    alphaP = 0.;
                } else if ( Op<U>::u(y._I) < r2::auxiliary::Tlim_p(Op<U>::l(x._I)) ) {  // Here, we are completely in the extension where a small alpha suffices
                    alphaP = r2::data::alphaD2sHatdp2;
                } else if ( Op<U>::u(x._I) < r2::data::pminB23 ) {
                    if ( Op<U>::u(y._I) < 513 ) {
                        alphaP = r2::data::alphaD2sdp2PltPminB23Tlt513;
                    } else {
                        alphaP = r2::data::alphaD2sdp2PltPminB23;
                    }
                } else {
                    if ( Op<U>::l(y._I) > 700. ) {
                        alphaP = r2::data::alphaD2sdp2PgtPminB23Tgt700;
                    } else {
                        alphaP = r2::data::alphaD2sdp2PgtPminB23;
                    }
                }


            {   // convex part
                double cv[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest

                // First, relaxations based on Theorem 1 of Najman, Bongartz, Mitsos: CACE (2019).
                // In order to ensure that we have an underestimator that fulfills the conditions of Corollary 1 of Najman, Bongartz, Mitsos: CACE (2019), we also need a mixed-variable alphaPT depending on the region
                    double alphaPT;
                    if ( (Op<U>::u(x._I)<r2::data::pmaxD2sdpTGt0) || (Op<U>::u(y._I)<r2::data::TmaxD2sdpTGt0) || (Op<U>::l(x._I)>r2::data::TminD2sdpTGt0)  ) {
                        alphaPT = 0.;
                    } else {
                        alphaPT = r2::data::alphaD2sdpT;
                    }
                    // s_pT_uncut is component-wise concave w.r.t. T for T>Tsat(p) and linear below. Taking the secant (or linear extrapolation beyond T=Tsat(p)) w.r.t. T for T>Tsat(p) to make it component-wise convex (cf. Najman, Bongartz, Mitsos: CACE (2019)).
                    std::function<double(const double,const double)> compConvUnderest = [x,y,alphaP,alphaPT](const double p, const double T) {
                        if ( Op<U>::l(y._I) >= r2::auxiliary::Tlim_p(Op<U>::u(x._I)) ) {    // we are completely in the physical part (T>Tsat(p)), using secant: compConvUnderest(p,T) = saBB(p,T^L) + k*(saBB(p,T^U)-saBB(p,T^L)), with k = (T-T^L)/(T^U-T^L), and saBB being the aBB underestimator of s_pT_uncut
                            const double k = isequal( Op<U>::diam(y._I), 0. ) ? 0. : (T - Op<U>::l(y._I)) / Op<U>::diam(y._I);
                            const double sLeftT  = r2::get_s_pT_uncut(p,Op<U>::l(y._I)) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2))    + alphaPT*(p-Op<U>::l(x._I))*(Op<U>::l(y._I)-Op<U>::u(y._I))   ;
                            const double sRightT = r2::get_s_pT_uncut(p,Op<U>::u(y._I)) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2)) /* + alphaPT*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                            return sLeftT + k*(sRightT-sLeftT);
                        }
                        else {  // we have part of the extension, so just extrapolate to make sure we stay convex
                            const double Tlim = r2::auxiliary::Tlim_p(p);
                            if ( T <= Tlim ) {
                                return r2::get_s_pT_uncut(p,T) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2)) + alphaPT*(p-Op<U>::l(x._I))*(T-Op<U>::u(y._I));
                            }
                            else {
                                const double sTlim = r2::get_s_pT_uncut(p,Tlim) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2)) + alphaPT*(p-Op<U>::l(x._I))*(Tlim-Op<U>::u(y._I));
                                return sTlim + (0.003 + alphaPT*(p-Op<U>::l(x._I)))*(T-Tlim);
                            }
                        }
                    };
                    std::function<double(const double,const double)> dcompConvUnderest_dp = [x,y,alphaP,alphaPT](const double p, const double T) {
                        if ( Op<U>::l(y._I) >= r2::auxiliary::Tlim_p(Op<U>::u(x._I)) ) {    // we are completely in the physical part (T>Tsat(p)), using secant: compConvUnderest(p,T) = saBB(p,T^L) + k*(saBB(p,T^U)-saBB(p,T^L)), with k = (T-T^L)/(T^U-T^L), and saBB being the aBB underestimator of s_pT_uncut
                            const double k = isequal( Op<U>::diam(y._I), 0. ) ? 0. : (T - Op<U>::l(y._I)) / Op<U>::diam(y._I);
                            const double dsLeftTdp = r2::derivatives::get_ds_pT_dp_uncut(p,Op<U>::l(y._I)) + alphaP*2.*(p-Op<U>::u(x._I)) + alphaPT*(Op<U>::l(y._I)-Op<U>::u(y._I));
                            const double dsRightTdp = r2::derivatives::get_ds_pT_dp_uncut(p,Op<U>::u(y._I)) + alphaP*2.*(p-Op<U>::u(x._I)) /* + alphaPT*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                            return dsLeftTdp + k*(dsRightTdp-dsLeftTdp);
                        }
                        else {  // we have part of the extension, so just extrapolate to make sure we stay convex
                            const double Tlim = r2::auxiliary::Tlim_p(p);
                            if ( T <= Tlim ) {
                                return r2::derivatives::get_ds_pT_dp_uncut(p,T) + alphaP*2.*(p-Op<U>::u(x._I)) + alphaPT*(T-Op<U>::u(y._I));
                            }
                            else {
                                const double dsTlimdp = r2::derivatives::get_ds_pT_dp_uncut(p,Tlim) + alphaP*2.*(p-Op<U>::u(x._I)) + alphaPT*(Tlim-Op<U>::u(y._I));
                                const double dsTlimdTlim = r2::derivatives::get_ds_pT_dT_uncut(p,Tlim) + alphaPT*(p-Op<U>::l(x._I));
                                const double dTlimdp = r2::auxiliary::derivatives::dTlim_dp(p);
                                return dsTlimdp + (dsTlimdTlim - (0.003+alphaPT*(p-Op<U>::l(x._I))))*dTlimdp + alphaPT*(T-Tlim);
                            }
                        }
                    };
                    std::function<double(const double,const double)> dcompConvUnderest_dT = [x,y,alphaP,alphaPT](const double p, const double T) {
                        if ( Op<U>::l(y._I) >= r2::auxiliary::Tlim_p(Op<U>::u(x._I)) ) {    // we are completely in the physical part (T>Tsat(p)), using secant: compConvUnderest(p,T) = saBB(p,T^L) + k*(saBB(p,T^U)-saBB(p,T^L)), with k = (T-T^L)/(T^U-T^L), and saBB being the aBB underestimator of s_pT_uncut
                            if ( isequal( Op<U>::diam(y._I), 0. ) ) {
                                return 0.;
                            } else {
                                const double dkdT = 1. / Op<U>::diam(y._I);
                                const double sLeftT  = r2::get_s_pT_uncut(p,Op<U>::l(y._I)) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2))    + alphaPT*(p-Op<U>::l(x._I))*(Op<U>::l(y._I)-Op<U>::u(y._I))   ;
                                const double sRightT = r2::get_s_pT_uncut(p,Op<U>::u(y._I)) + alphaP*(std::pow(p-Op<U>::u(x._I),2)-std::pow(Op<U>::diam(x._I),2)) /* + alphaPT*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                                return dkdT*(sRightT-sLeftT);
                            }
                        }
                        else {  // we have part of the extension, so just extrapolate to make sure we stay convex
                            return 0.003 + alphaPT*(p-Op<U>::l(x._I));
                        }
                    };

                // Now, get actual convex underestimators from Theorem 1
                    {   // first relaxation: h(p,T^L)+h(p^L,T)-h(p^L,T^L).
                        cv[0] = compConvUnderest(x._cc,Op<U>::l(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I));
                        xslope[0] = dcompConvUnderest_dp(x._cc,Op<U>::l(y._I));
                        yslope[0] = dcompConvUnderest_dT(Op<U>::l(x._I),y._cv);
                    }
                    {   // second relaxation: h(p^U,T)+h(p,T^U)-h(p^U,T^U).
                        cv[1] = compConvUnderest(Op<U>::u(x._I),y._cv) + compConvUnderest(x._cc,Op<U>::u(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I));
                        xslope[1] = dcompConvUnderest_dp(x._cc,Op<U>::u(y._I));
                        yslope[1] = dcompConvUnderest_dT(Op<U>::u(x._I),y._cv);
                    }

                // Since the above relaxations are somewhat weak for large intervals because of the large required alphaP, we introduce additional "ad hoc" relaxations.
                // These are valid on the entire domain, but do not converge as the bounds shrink. Note that this is not required since the above relaxations take over at some point and thus ensure convergence.
                    cv[2] = 3.3 + std::exp(0.1*(9.-x._cc)) + 0.0015*(100-x._cc) + 0.003*(y._cv-273.15);
                    xslope[2] = -0.1*std::exp(0.1*(9.-x._cc)) - 0.0015;
                    yslope[2] = 0.003;
                    cv[3] = 6. + std::exp(0.1*(7.5-x._cc)) + 0.0035*(100-x._cc) + 0.0045*(y._cv-1073.15);
                    xslope[3] = -0.1*std::exp(0.1*(7.5-x._cc)) - 0.0035;
                    yslope[3] = 0.0045;

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmax(4, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
            }
            {   // concave part
                double cc[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest

                // First, get a component-wise convex overestimator. Note that this one is computed using s_pT directly (i.e., the version that is cut at smin, so this is already taken care of).
                    std::function<double(const double,const double)> compConvOverest = [x,y,alphaP](const double p, const double T) {
                        return r2::get_s_pT(p,T) + alphaP*std::pow(p-Op<U>::mid(x._I),2) + r2::data::alphaD2sdT2*std::pow(T-Op<U>::mid(y._I),2);
                    };

                // Now, calculate the affine concave envelope of this component-wise convex overestimator based on Meyer & Floudas: Math. Prog. (2005)
                    // First, calculate values of overestimator at corner points
                    const double fLL = compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I));
                    const double fLU = compConvOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                    const double fUL = compConvOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                    const double fUU = compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));
                    const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
                    const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
                    // Make sure we select the correct faces
                    if ((fLU+fUL)<(fLL+fUU)) {
                        // Facet 0: LL, UL, UU - first need to determine which corner is the highest
                        double fUpper, xUpper, yUpper;
                        if (fUU >= fLL) {
                            fUpper = fUU;
                            xUpper = Op<U>::u(x._I);
                            yUpper = Op<U>::u(y._I);
                        } else {
                            fUpper = fLL;
                            xUpper = Op<U>::l(x._I);
                            yUpper = Op<U>::l(y._I);
                        }
                        xslope[0] = thinX ? 0. : ( fUL - fLL ) / Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fUU - fUL ) / Op<U>::diam(y._I);
                        cc[0] = fUpper + xslope[0]*(x._cv - xUpper) + yslope[0]*(y._cc - yUpper);
                        // Facet 1: LL, LU, UU - we know the maximum is at LU
                        xslope[1] = thinX ? 0. : ( fUU - fLU ) / Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fLU - fLL ) / Op<U>::diam(y._I);
                        cc[1] = fLU + xslope[1]*(x._cv - Op<U>::l(x._I)) + yslope[1]*(y._cc - Op<U>::u(y._I));
                    } else {
                        // Facet 0: UL, LL, LU - we know the maximum is at LU
                        xslope[0] = thinX ? 0. : ( fUL - fLL ) / Op<U>::diam(x._I);
                        yslope[0] = thinY ? 0. : ( fLU - fLL ) / Op<U>::diam(y._I);
                        cc[0] = fLU + xslope[0]*(x._cv - Op<U>::l(x._I)) + yslope[0]*(y._cc - Op<U>::u(y._I));
                        // Facet 1: UL, UU, LU - we know the maximum is at LU
                        xslope[1] = thinX ? 0. : ( fUU - fLU ) / Op<U>::diam(x._I);
                        yslope[1] = thinY ? 0. : ( fUU - fUL ) / Op<U>::diam(y._I);
                        cc[1] = fLU + xslope[1]*(x._cv - Op<U>::l(x._I)) + yslope[1]*(y._cc - Op<U>::u(y._I));
                    }

                // Since the above cuts are somewhat weak for large intervals because of the large required alphaP, we introduce additional "ad hoc" cuts.
                // These are valid on the entire domain (or tighter versions on smaller subdomains), but do not converge as the bounds shrink. Note that this is not required since the above cuts take over at some point and thus ensure convergence.
                    double a2, a3;
                    if ( Op<U>::l(x._I) >= 1. ) {
                        a2 = 8.6;
                        a3 = 6.11;
                        if ( Op<U>::u(x._I) <= r2::data::pminB23 ) {
                            xslope[2] = - 0.085;
                            yslope[2] = 0.0021;
                            xslope[3] = - 0.061;
                            yslope[3] = 0.0036;
                        } else {
                            xslope[2] = - 0.0254;
                            yslope[2] = 0.00125;
                            xslope[3] = - 0.01025;
                            yslope[3] = 0.0036;
                        }
                    } else {
                        a2 = 11.95;
                        a3 = 9.25;
                        if ( Op<U>::u(x._I) <= r2::data::pminB23 ) {
                            xslope[2] = - 0.285;
                            yslope[2] = 0.00175;
                            xslope[3] = - 0.25;
                            yslope[3] = 0.0055;
                        } else {
                            xslope[2] = -0.0585;
                            yslope[2] = 0.00125;
                            xslope[3] = -0.042;
                            yslope[3] = 0.0055;
                        }
                    }
                    cc[2] = a2 + xslope[2]*x._cv + yslope[2]*(y._cc-1073.15);
                    cc[3] = a3 + xslope[3]*x._cv + yslope[3]*(y._cc-273.15);

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(4, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._cvsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 23:    // region 2, T(p,h)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );
            if( isequal(Op<U>::l(result._I),r2::data::Tmax) ){
                result._cv = Op<U>::l(result._I);
                result._cc = Op<U>::u(result._I);
                for( unsigned int i=0; i<result._nsub; i++ ){
                    result._cvsub[i] =  0.;
                    result._ccsub[i] =  0.;
                }
                return result;
            }

            if ( (Op<U>::l(x._I)>r2::data::pminD2Tphdp2lt0) || (Op<U>::l(y._I)>r2::data::hminD2Tphdp2lt0) ) {   // if p>=~0.027 MPa or h>~2617kJ/kg, we have component-wise concavity in p (otherwise, in that bottomw left corner, d2T/dp2 goes to ridiculously large positive values, so not attempting anything there for now)

                // We want to exploit component-wise convexity / concavity using Theorem 1 of Najman, Bongartz, Mitsos: CACE (2019) and Meyer & Floudas: Math. Prog. (2005)
                // However, T_ph_uncut is component-wise convex w.r.t. h only in some areas; in others, need alphaBB to make it so first; constructing in a way to preserve monotonicity w.r.t. h
                // Also, to ensure we have a constant sign of the mixed second derivative, applying alphaBB with a mixed term in p and h in some areas
                    double alphaH;
                    const bool avoidedUpperSpot = (Op<U>::u(x._I)<r2::data::pmaxD2Tdh2Gt0) || (Op<U>::l(y._I)>r2::data::hminD2Tdh2Gt0pGt4MPa);
                    const bool avoidedLowerSpot = (Op<U>::l(x._I)>r2::data::pminD2Tdh2Gt0) || ( (Op<U>::l(x._I)>r2::data::pminB) && (Op<U>::u(y._I)<r2::data::hmaxD2Tdh2Gt0pGt4MPa));
                    if (avoidedUpperSpot && avoidedLowerSpot) {
                        alphaH = 0.;
                    } else {
                        alphaH = r2::data::alphaD2Tdh2;
                    }
                    double alphaPH;
                    if ( (Op<U>::u(x._I)<r2::data::pminB23) || (Op<U>::l(y._I)>r2::data::hminalphaPhys) ) {
                        alphaPH = r2::data::alphaD2TdphPhys;
                    } else {
                        alphaPH = r2::data::alphaD2Tdph;
                    }


                {   // convex part
                    double cv[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest

                    // We have component-wise concavity in p. To achieve component-wise convexity in both components and a negative mixed derivative, we
                    //  - first apply alphaBB w.r.t. h
                    //  - apply alphaBB w.r.t. h&p to achieve a negative mixed derivative
                    //  - take the secant w.r.t. p to get component-wise convexity in p as well
                        std::function<double(const double,const double)> compConvUnderest = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double Tleft  = r2::get_T_ph_uncut(Op<U>::l(x._I),h) + alphaH*(h-Op<U>::l(y._I))*(h-Op<U>::u(y._I))    - alphaPH*(Op<U>::l(x._I)-Op<U>::u(x._I))*(h-Op<U>::u(y._I))   ;
                            const double Tright = r2::get_T_ph_uncut(Op<U>::u(x._I),h) + alphaH*(h-Op<U>::l(y._I))*(h-Op<U>::u(y._I)) /* - alphaPH*(Op<U>::u(x._I)-Op<U>::u(x._I))*(h-Op<U>::u(y._I)) */;
                            const double a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 1. : (Op<U>::u(x._I) - p)/Op<U>::diam(x._I);
                            return a*Tleft + (1.-a)*Tright;
                        };
                        std::function<double(const double,const double)> dcompConvUnderest_dh = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double dTleftdh  = r2::derivatives::get_dT_ph_dh_uncut(Op<U>::l(x._I),h) + 2.*alphaH*h - alphaH*(Op<U>::l(y._I)+Op<U>::u(y._I)) - alphaPH*(Op<U>::l(x._I)-Op<U>::u(x._I)) ;
                            const double dTrightdh = r2::derivatives::get_dT_ph_dh_uncut(Op<U>::u(x._I),h) + 2.*alphaH*h - alphaH*(Op<U>::l(y._I)+Op<U>::u(y._I)) /* - alphaPH*(Op<U>::u(x._I)-Op<U>::u(x._I)) */;
                            const double a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 1. : (Op<U>::u(x._I) - p)/Op<U>::diam(x._I);
                            return a*dTleftdh + (1.-a)*dTrightdh;
                        };
                        std::function<double(const double,const double)> dcompConvUnderest_dp = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double Tleft  = r2::get_T_ph_uncut(Op<U>::l(x._I),h) + alphaH*(h-Op<U>::l(y._I))*(h-Op<U>::u(y._I))    - alphaPH*(Op<U>::l(x._I)-Op<U>::u(x._I))*(h-Op<U>::u(y._I))   ;
                            const double Tright = r2::get_T_ph_uncut(Op<U>::u(x._I),h) + alphaH*(h-Op<U>::l(y._I))*(h-Op<U>::u(y._I)) /* - alphaPH*(Op<U>::u(x._I)-Op<U>::u(x._I))*(h-Op<U>::u(y._I)) */;
                            return isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 0. : (Tright - Tleft)/Op<U>::diam(x._I);
                        };

                    // Now, construct the convex relaxation using theorem 1 from Najman, Bongartz, Mitsos: CACE (2019)
                        {   // First relaxation: f(x,y^L) + f(x^U,y) - f(x^U,y^L)
                            cv[0] = compConvUnderest(x._cv,Op<U>::l(y._I)) + compConvUnderest(Op<U>::u(x._I),y._cv) - compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I));
                            xslope[0] = dcompConvUnderest_dp(x._cv,Op<U>::l(y._I));
                            yslope[0] = dcompConvUnderest_dh(Op<U>::u(x._I),y._cv);
                        }
                        {   // Second relaxation: f(x,y^U) + f(x^L,y) - f(x^L,y^U)
                            cv[1] = compConvUnderest(x._cv,Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                            xslope[1] = dcompConvUnderest_dp(x._cv,Op<U>::u(y._I));
                            yslope[1] = dcompConvUnderest_dh(Op<U>::l(x._I),y._cv);
                        }


                    // We now have to correct these two relaxations, since these were built using T_ph_uncut without considering that the function T_ph is cut off at Tmax
#ifdef LIVE_DANGEROUSLY
                        // Simply default to interval bounds if we exceed Tmax (cv. explanation for ID 11 above)
                        if ( (cv[0]>r2::data::Tmax) || (cv[1]>r2::data::Tmax) ) {
                            cv[0] = Op<U>::l(result._I);
                            xslope[0] = 0.;
                            yslope[0] = 0.;
                            cv[1] = Op<U>::l(result._I);
                            xslope[1] = 0.;
                            yslope[1] = 0.;
                        }
#else
                        // Actually do min(T_ph_uncut,Tmax)
                        if( Op<U>::u(result._I) >= r2::data::Tmax ){
                            // First, we determine the maximum of the convex underestimators on the entire host set.
                            // Second relaxation: max of f(x,y^L) + f(x^U,y) - f(x^U,y^L), which is at (x^U,y^U)
                            const double cvU1 = /*compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) +*/ compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*- compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I))*/;
                            const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                            cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                            xslope[0] = k1*xslope[0];
                            yslope[0] = k1*yslope[0];
                            // First relaxation: max of f(x,y^U) + f(x^L,y) - f(x^L,y^U), which is at (x^U,y^U)
                            const double cvU2 = cvU1; // compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) + compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I));
                            const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                            cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                            xslope[1] = k2*xslope[1];
                            yslope[1] = k2*yslope[1];
                        }
#endif

                    // Since the above cuts are somewhat weak for large intervals because of the large required alphaH and alphaPH, we introduce additional "ad hoc" cuts.
                    // These are valid on the entire domain (or tighter versions on smaller subdomains), but do not converge as the bounds shrink. Note that this is not required since the above cuts take over at some point and thus ensure convergence.
                        xslope[2] = 0.;
                        yslope[2] = 0.49;
                        xslope[3] = 5.2;
                        yslope[3] = 0.17;
                        cv[2] = 1073. + /*xslope[2]*(x._cv-100.)*/ + yslope[2]*(y._cv-4161.);
                        cv[3] = 1073. +   xslope[3]*(x._cv-100.)   + yslope[3]*(y._cv-4161.);

                    // Finally, store the best relaxation with the corresponding subgradient
                        const unsigned irelax = argmax(4, cv);
                        result._cv = cv[irelax];
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:x._cvsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
                }
                {   // concave part
                    double cc[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest

                    // We have component-wise concavity in p. To achieve component-wise concavity in both components and a negative mixed derivative, we
                    //  - first apply alphaBB w.r.t. h
                    //  - apply alphaBB w.r.t. h&p to achieve a negative mixed derivative
                    //  - take the secant w.r.t. h to get component-wise concavity in h as well
                        std::function<double(const double,const double)> compConvOverest = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double Tleft  = r2::get_T_ph_uncut(p,Op<U>::l(y._I)) + /*alphaH*sqr(Op<U>::l(y._I)-Op<U>::l(y._I))*/  + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::l(y._I))   ;
                            const double Tright = r2::get_T_ph_uncut(p,Op<U>::u(y._I)) +   alphaH*sqr(Op<U>::u(y._I)-Op<U>::l(y._I))  /*+ alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                            const double a = isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? 1. : (Op<U>::u(y._I) - h)/Op<U>::diam(y._I);
                            return a*Tleft + (1.-a)*Tright;
                        };
                        std::function<double(const double,const double)> dcompConvOverest_dp = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double dTleftdp  = r2::derivatives::get_dT_ph_dp_uncut(p,Op<U>::l(y._I))   + alphaPH*(Op<U>::u(y._I)-Op<U>::l(y._I))   ;
                            const double dTrightdp = r2::derivatives::get_dT_ph_dp_uncut(p,Op<U>::u(y._I)) /*+ alphaPH*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                            const double a = isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? 1. : (Op<U>::u(y._I) - h)/Op<U>::diam(y._I);
                            return a*dTleftdp + (1.-a)*dTrightdp;
                        };
                        std::function<double(const double,const double)> dcompConvOverest_dh = [x,y,alphaH,alphaPH](const double p, const double h) {
                            const double Tleft  = r2::get_T_ph_uncut(p,Op<U>::l(y._I)) + /*alphaH*sqr(Op<U>::l(y._I)-Op<U>::l(y._I))*/  + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::l(y._I))   ;
                            const double Tright = r2::get_T_ph_uncut(p,Op<U>::u(y._I)) +   alphaH*sqr(Op<U>::u(y._I)-Op<U>::l(y._I))  /*+ alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::u(y._I)) */;
                            return isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? 0. : (Tright - Tleft)/Op<U>::diam(y._I);
                        };

                    // Now, construct the concave relaxation using theorem 1 from Najman, Bongartz, Mitsos: CACE (2019)
                        {   // First relaxation: f(x,y^L) + f(x^L,y) - f(x^L,y^L)
                            cc[0] = compConvOverest(x._cc,Op<U>::l(y._I)) + compConvOverest(Op<U>::l(x._I),y._cc) - compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I));
                            xslope[0] = dcompConvOverest_dp(x._cc,Op<U>::l(y._I));
                            yslope[0] = dcompConvOverest_dh(Op<U>::l(x._I),y._cc);
                        }
                        {   // Second relaxation: f(x,y^U) + f(x^L,y) - f(x^L,y^U)
                            cc[1] = compConvOverest(x._cc,Op<U>::u(y._I)) + compConvOverest(Op<U>::u(x._I),y._cc) - compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));
                            xslope[1] = dcompConvOverest_dp(x._cc,Op<U>::u(y._I));
                            yslope[1] = dcompConvOverest_dh(Op<U>::u(x._I),y._cc);
                        }

                    // We now have to correct these two relaxations, since these were built using T_ph_uncut without considering that the function T_ph is cut off at Tmin
#ifdef LIVE_DANGEROUSLY
                        // Just default to interval bounds if we violate the Tmin cut (cf. explanation in case 11)
                        if ( (cc[0]<r2::data::Tmin) || (cc[1]<r2::data::Tmin) ) {
                            cc[0] = Op<U>::u(result._I);
                            xslope[0] = 0.;
                            yslope[0] = 0.;
                            cc[1] = Op<U>::u(result._I);
                            xslope[1] = 0.;
                            yslope[1] = 0.;
                        }
#else
                        // Actually do max(T_ph_uncut, Tmin)
                        if( Op<U>::l(result._I) <= r2::data::Tmin ){
                            const double ccL1 = compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I)) /*+ compConvOverest(Op<U>::l(x._I)Op<U>::l(y._I)) - compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I))*/; // Minimum of the relaxation on the entire domain
                            const double k1 = isequal( Op<U>::u(result._I), ccL1) ? 0. : (( std::max( ccL1, r2::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL1)) ;
                            cc[0] = Op<U>::u(result._I) - k1 * ( cc[0] - Op<U>::u(result._I) );
                            xslope[0] = - k1*xslope[0];
                            yslope[0] = - k1*yslope[0];
                            const double ccL2 = compConvOverest(Op<U>::l(x._I),Op<U>::u(y._I)) + compConvOverest(Op<U>::u(x._I),Op<U>::l(y._I)) - compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));// Minimum of the relaxation on the entire domain
                            const double k2 = isequal( Op<U>::u(result._I), ccL2) ? 0. : (( std::max( ccL2, r2::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL2)) ;
                            cc[1] = Op<U>::u(result._I) - k2 * ( cc[1] - Op<U>::u(result._I) );
                            xslope[1] = - k2*xslope[1];
                            yslope[1] = - k2*yslope[1];
                        }
#endif

                    // Since the above cuts are somewhat weak for large intervals because of the large required alphaH and alphaPH, we introduce additional "ad hoc" cuts.
                    // These are valid on the entire domain (or tighter versions on smaller subdomains), but do not converge as the bounds shrink. Note that this is not required since the above cuts take over at some point and thus ensure convergence.
                        xslope[2] = 0.9;
                        yslope[2] = 0.225;
                        xslope[3] = 125./(x._cc+1-611.2127e-6);
                        yslope[3] = 0.55;
                        cc[2] = 810. + xslope[2]*(x._cc-100.) + yslope[2]*(y._cc-2500.);
                        cc[3] = 280. + 125.*std::log(x._cc+1.-611.2127e-6) + yslope[3]*(y._cc-2500.);

                    // Finally, store the best relaxation with the corresponding subgradient
                        const unsigned irelax = argmin(4, cc);
                        result._cc = cc[irelax];
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
                }

            }
            else {  // no special properties, relying on (exact) range bounds and ad hoc cuts (cf. above)

                {   // convex part
                    double cv[2], xslope[2], yslope[2]; // using 2 ad hoc cuts
                    xslope[0] = 0.;
                    yslope[0] = 0.49;
                    xslope[1] = 5.2;
                    yslope[1] = 0.17;
                    cv[0] = 1073. + /*xslope[0]*(x._cv-100.)*/ + yslope[0]*(y._cv-4161.);
                    cv[1] = 1073. +   xslope[1]*(x._cv-100.)   + yslope[1]*(y._cv-4161.);
                    const unsigned irelax = argmax(2, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:x._cvsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
                }
                {   // concave part
                        double cc[2], xslope[2], yslope[2]; // using 2 ad hoc cuts
                        xslope[0] = 0.9;
                        yslope[0] = 0.225;
                        xslope[1] = 125./(x._cc+1-611.2127e-6);
                        yslope[1] = 0.55;
                        cc[0] = 810. + xslope[0]*(x._cc-100.) + yslope[0]*(y._cc-2500.);
                        cc[1] = 280. + 125.*std::log(x._cc+1.-611.2127e-6) + yslope[1]*(y._cc-2500.);
                        const unsigned irelax = argmin(2, cc);
                        result._cc = cc[irelax];
                        for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
                }

            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 24:   // region 2, T(p,s)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );
            if( isequal(Op<U>::l(result._I),r2::data::Tmax) ){
                result._cv = Op<U>::l(result._I);
                result._cc = Op<U>::u(result._I);
                for( unsigned int i=0; i<result._nsub; i++ ){
                    result._cvsub[i] =  0.;
                    result._ccsub[i] =  0.;
                }
                return result;
            }

            // We want to exploit component-wise convexity / concavity using Theorem 1 of Najman, Bongartz, Mitsos: CACE (2019)
            // However, T_ps_uncut is component-wise concave w.r.t. p only in some areas; in others, need alphaBB to make it so first
            // Also, to ensure we have a constant sign of the mixed second derivative, applying alphaBB with a mixed term in p and s in some areas; constructed to at least preserve monotonicity in s
                double alphaP;
                if ( (Op<U>::l(x._I)>r2::data::pminD2Tpsdp2lt0) || (Op<U>::u(x._I)<r2::data::pmaxD2Tpsdp2lt0) || (Op<U>::l(y._I)>r2::data::sminD2Tpsdp2lt0) ) {
                    alphaP = 0.;
                } else {
                    alphaP = r2::data::alphaD2Tpsdp2;
                }
                double alphaPH; // alphaPH is only needed if we are below the lower bound of the (relaxed) physical region, which is delimited by svap(p)=s2(p,Ts(p)), which in turn is decreasing in p
                const double slower = r2::original::get_s_pT(Op<U>::l(x._I),r4::original::get_Ts_p(std::min(Op<U>::l(x._I),r4::data::pcrit)));
                if ( (Op<U>::l(y._I)>slower) ) {
                    alphaPH = 0.;
                } else {
                    alphaPH = r2::data::alphaD2Tpsdph;
                }

            {   // convex part
                double cv[5], xslope[5], yslope[5]; // We will derive five relaxations and choose the strongest
                double* xSub[5];

                // We have component-wise convexity in s. To achieve component-wise convexity in both components and a positive mixed derivative, we
                //  - first apply alphaBB w.r.t. p to get component-wise concavity
                //  - apply alphaBB w.r.t. s&p to achieve a positive mixed derivative
                //  - take the secant w.r.t. p to get component-wise convexity in p as well
                    std::function<double(const double,const double)> compConvUnderest = [x,y,alphaP,alphaPH](const double p, const double s) {
                        const double Tleft  = r2::get_T_ps_uncut(Op<U>::l(x._I),s)    - alphaP*sqr(Op<U>::l(x._I)-Op<U>::u(x._I)) /* + alphaPH*(Op<U>::l(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(Op<U>::u(x._I),s) /* - alphaP*sqr(Op<U>::u(x._I)-Op<U>::u(x._I)) */ + alphaPH*(Op<U>::u(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I));
                        const double a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? ( (Tleft<=Tright)? 1. : 0.) : (Op<U>::u(x._I) - p)/Op<U>::diam(x._I);
                        return a*Tleft + (1.-a)*Tright;
                    };
                    std::function<double(const double,const double)> dcompConvUnderest_ds = [x,y,alphaP,alphaPH](const double p, const double s) {
                        const double Tleft  = r2::get_T_ps_uncut(Op<U>::l(x._I),s)    - alphaP*sqr(Op<U>::l(x._I)-Op<U>::u(x._I)) /* + alphaPH*(Op<U>::l(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(Op<U>::u(x._I),s) /* - alphaP*sqr(Op<U>::u(x._I)-Op<U>::u(x._I)) */ + alphaPH*(Op<U>::u(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I));
                        const double dTleftds  = r2::derivatives::get_dT_ps_ds_uncut(Op<U>::l(x._I),s) /* + alphaPH*(Op<U>::l(x._I)-Op<U>::l(x._I)) */;
                        const double dTrightds = r2::derivatives::get_dT_ps_ds_uncut(Op<U>::u(x._I),s)    + alphaPH*(Op<U>::u(x._I)-Op<U>::l(x._I));
                        const double a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? ( (Tleft<=Tright)? 1. : 0.) : (Op<U>::u(x._I) - p)/Op<U>::diam(x._I);
                        return a*dTleftds + (1.-a)*dTrightds;
                    };
                    std::function<double(const double)> dcompConvUnderest_dp = [x,y,alphaP,alphaPH](const double s) {
                        const double Tleft  = r2::get_T_ps_uncut(Op<U>::l(x._I),s)    - alphaP*sqr(Op<U>::l(x._I)-Op<U>::u(x._I)) /* + alphaPH*(Op<U>::l(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(Op<U>::u(x._I),s) /* - alphaP*sqr(Op<U>::u(x._I)-Op<U>::u(x._I)) */ + alphaPH*(Op<U>::u(x._I)-Op<U>::l(x._I))*(s-Op<U>::u(y._I));
                        return isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 0. : (Tright - Tleft)/Op<U>::diam(x._I);
                    };

                // Now, construct the convex relaxation using theorem 1 from Najman, Bongartz, Mitsos: CACE (2019)
                    {   // First relaxation: f(x,y^L) + f(x^L,y) - f(x^L,y^L)
                        double xRel1;
                        xslope[0] = dcompConvUnderest_dp(Op<U>::l(y._I));
                        yslope[0] = dcompConvUnderest_ds(Op<U>::l(x._I),y._cv);
                        if (xslope[0]>=0) {
                            xRel1 = x._cv;
                            xSub[0] = x._cvsub;
                        } else {
                            xRel1 = x._cc;
                            xSub[0] = x._ccsub;
                        }
                        cv[0] = compConvUnderest(xRel1,Op<U>::l(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I));
                    }
                    {   // Second relaxation: f(x,y^U) + f(x^U,y) - f(x^U,y^U)
                        double xRel2;
                        xslope[1] = dcompConvUnderest_dp(Op<U>::u(y._I));
                        yslope[1] = dcompConvUnderest_ds(Op<U>::u(x._I),y._cv);
                        if (xslope[1]>=0) {
                            xRel2 = x._cv;
                            xSub[1] = x._cvsub;
                        } else {
                            xRel2 = x._cc;
                            xSub[1] = x._ccsub;
                        }
                        cv[1] = compConvUnderest(xRel2,Op<U>::u(y._I)) + compConvUnderest(Op<U>::u(x._I),y._cv) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I));
                    }

                // Deriving additional cuts that exploit the monotonicity properties:
                // Since the function is monotonically increasing w.r.t. p and s, anything that is
                //  i) constant w.r.t. a variable and
                //  ii) a relaxation at the lower bound of that variable
                // is a valid relaxation. Just need to make sure it is convex / concave...{
                    xslope[2] = 0.;
                    yslope[2] = r2::derivatives::get_dT_ps_ds_uncut(Op<U>::l(x._I),y._cv);
                    cv[2] = r2::get_T_ps_uncut(Op<U>::l(x._I),y._cv);
                    xSub[2] = x._cvsub; // dummy that will not be used since the xslope is 0
                    const double Tleft4  = r2::get_T_ps_uncut(Op<U>::l(x._I),Op<U>::l(y._I))    - alphaP*sqr(Op<U>::l(x._I)-Op<U>::u(x._I))   ;
                    const double Tright4 = r2::get_T_ps_uncut(Op<U>::u(x._I),Op<U>::l(y._I)) /* - alphaP*sqr(Op<U>::u(x._I)-Op<U>::u(x._I)) */;
                    xslope[3] = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ? 0. : (Tright4 - Tleft4)/Op<U>::diam(x._I);
                    yslope[3] = 0.;
                    double a;
                    if (Tleft4<=Tright4) {
                        a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ?  1. : (Op<U>::u(x._I) - x._cv)/Op<U>::diam(x._I);
                        xSub[3] = x._cvsub;
                    } else {
                        a = isequal( Op<U>::l(x._I), Op<U>::u(x._I) ) ?  0. : (Op<U>::u(x._I) - x._cc)/Op<U>::diam(x._I);
                        xSub[3] = x._ccsub;
                    }
                    cv[3] = a*Tleft4 + (1.-a)*Tright4;


                // We now have to correct these two relaxations, since these were built using T_ps_uncut without considering that the function T_ps is cut off at Tmax
#ifdef LIVE_DANGEROUSLY
                    // Simply default to interval bounds if we exceed Tmax (cv. explanation for ID 11 above)
                    if ( (cv[0]>r2::data::Tmax) || (cv[1]>r2::data::Tmax) || (cv[3]>r2::data::Tmax) ) {
                        cv[0] = Op<U>::l(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cv[1] = Op<U>::l(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                        cv[2] = Op<U>::l(result._I);
                        xslope[2] = 0.;
                        yslope[2] = 0.;
                        cv[3] = Op<U>::l(result._I);
                        xslope[3] = 0.;
                        yslope[3] = 0.;
                    }
#else
                    // Actually do min(T_ps_uncut,Tmax)
                    if( Op<U>::u(result._I) >= r2::data::Tmax ){
                        // First, we determine the maximum of the convex underestimators on the entire host sets, which may be at (p^L,s^U) or (p^U,s^U) (recall that the underestimators are increasing in s and comp. convex)
                        // First relaxation: max of compConvUnderest(x,y^L) + compConvUnderest(x^L,y) - compConvUnderest(x^L,y^L)
                        const double cvU1 = std::max( /* compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) +*/ compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /*- compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) */ ,
                                                         compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I)) +   compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I))   - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I))     );
                        const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                        cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                        xslope[0] = k1*xslope[0];
                        yslope[0] = k1*yslope[0];
                        // Second relaxation: max of compConvUnderest(x,y^U) + compConvUnderest(x^L,y) - compConvUnderest(x^L,y^U)
                        const double cvU2 = std::max( compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) */ ,
                                                      compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /*+ compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) */ );
                        const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                        cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                        xslope[1] = k2*xslope[1];
                        yslope[1] = k2*yslope[1];
                        // Third relaxation: max of f(x^L,y), which is at (x^L,y^U)
                        const double cvU3 = r2::get_T_ps_uncut(Op<U>::l(x._I),Op<U>::u(y._I));
                        const double k3 = isequal( Op<U>::l(result._I), cvU3) ? 0. : (( std::min( cvU3, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU3 - Op<U>::l(result._I))) ;
                        cv[2] = Op<U>::l(result._I) + k3 * ( cv[2] - Op<U>::l(result._I) );
                        xslope[2] = k3*xslope[2];
                        yslope[2] = k3*yslope[2];
                        // Fourth relaxation
                        const double cvU4 = std::max( Tleft4, Tright4 );
                        const double k4 = isequal( Op<U>::l(result._I), cvU4) ? 0. : (( std::min( cvU4, r2::data::Tmax ) - Op<U>::l(result._I) ) / (cvU4 - Op<U>::l(result._I))) ;
                        cv[3] = Op<U>::l(result._I) + k4 * ( cv[3] - Op<U>::l(result._I) );
                        xslope[3] = k4*xslope[3];
                        yslope[3] = k4*yslope[3];
                    }
#endif

                // Adding an ad-hoc cut for large intervals
                    xslope[4] = 5.9;
                    yslope[4] = 0.;
                    cv[4] = 273. + xslope[4]*(x._cv-2.);
                    xSub[4] = x._cvsub;

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmax(5, cv);
                    result._cv = cv[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] =    xslope[irelax]*(x._const? 0.:xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
            }
            {   // concave part
                double cc[3], xslope[3], yslope[3]; // We will derive three relaxations and choose the strongest
                double* xSub[3];

                // We have component-wise convexity in s. To achieve component-wise concavity in both components and a positive mixed derivative, we
                //  - first apply alphaBB w.r.t. p to get component-wise concavity
                //  - apply alphaBB w.r.t. s&p to achieve a positive mixed derivative
                //  - take the secant w.r.t. s to get component-wise concavity in s as well
                    std::function<double(const double,const double)> compConcOverest = [x,y,alphaP,alphaPH](const double p, const double s) {
                        const double Tleft  = r2::get_T_ps_uncut(p,Op<U>::l(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p) /* + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::l(y._I)-Op<U>::l(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(p,Op<U>::u(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p)    + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::l(y._I));
                        const double a = isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? ( (Tleft>=Tright)? 1. : 0.) : (Op<U>::u(y._I) - s)/Op<U>::diam(y._I);
                        return a*Tleft + (1.-a)*Tright;
                    };
                    std::function<double(const double,const double)> dcompConcOverest_dp = [x,y,alphaP,alphaPH](const double p, const double s) {
                        const double Tleft  = r2::get_T_ps_uncut(p,Op<U>::l(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p) /* + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::l(y._I)-Op<U>::l(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(p,Op<U>::u(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p)    + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::l(y._I));
                        const double dTleftdp  = r2::derivatives::get_dT_ps_dp_uncut(p,Op<U>::l(y._I)) - 2.*alphaP*p + alphaP*(Op<U>::l(x._I)+Op<U>::u(x._I)) /* + alphaPH*(Op<U>::l(y._I)-Op<U>::l(y._I)) */;
                        const double dTrightdp = r2::derivatives::get_dT_ps_dp_uncut(p,Op<U>::u(y._I)) - 2.*alphaP*p + alphaP*(Op<U>::l(x._I)+Op<U>::u(x._I))    + alphaPH*(Op<U>::u(y._I)-Op<U>::l(y._I));
                        const double a = isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? ( (Tleft>=Tright)? 1. : 0.) : (Op<U>::u(y._I) - s)/Op<U>::diam(y._I);
                        return a*dTleftdp + (1.-a)*dTrightdp;
                    };
                    std::function<double(const double)> dcompConcOverest_ds = [x,y,alphaP,alphaPH](const double p) {
                        const double Tleft  = r2::get_T_ps_uncut(p,Op<U>::l(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p) /* + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::l(y._I)-Op<U>::l(y._I)) */;
                        const double Tright = r2::get_T_ps_uncut(p,Op<U>::u(y._I)) + alphaP*(p-Op<U>::l(x._I))*(Op<U>::u(x._I)-p)    + alphaPH*(p-Op<U>::l(x._I))*(Op<U>::u(y._I)-Op<U>::l(y._I));
                        return isequal( Op<U>::l(y._I), Op<U>::u(y._I) ) ? 0. : (Tright - Tleft)/Op<U>::diam(y._I);
                    };

                // Now, construct the concave relaxation using theorem 1 from Najman, Bongartz, Mitsos: CACE (2019)
                    {   // First relaxation: f(x,y^L) + f(x^U,y) - f(x^U,y^L)
                        if (dcompConcOverest_dp(x._cc,Op<U>::l(y._I))>=0) {
                            cc[0] = compConcOverest(x._cc,Op<U>::l(y._I)) + compConcOverest(Op<U>::u(x._I),y._cc) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                            xslope[0] = dcompConcOverest_dp(x._cc,Op<U>::l(y._I));
                            yslope[0] = dcompConcOverest_ds(Op<U>::u(x._I));
                            xSub[0] = x._ccsub;
                        } else if (dcompConcOverest_dp(x._cv,Op<U>::l(y._I))<=0) {
                            cc[0] = compConcOverest(x._cv,Op<U>::l(y._I)) + compConcOverest(Op<U>::u(x._I),y._cc) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I));
                            xslope[0] = dcompConcOverest_dp(x._cv,Op<U>::l(y._I));
                            yslope[0] = dcompConcOverest_ds(Op<U>::u(x._I));
                            xSub[0] = x._cvsub;
                        } else {   // can't recognize monotonicity, default to interval bounds
                            cc[0] = Op<U>::u(result._I);
                            xslope[0] = 0.;
                            yslope[0] = 0.;
                            xSub[0] = x._cvsub;
                        }
                    }
                    {   // Second relaxation: f(x,y^U) + f(x^L,y) - f(x^L,y^U)
                        if (dcompConcOverest_dp(x._cc,Op<U>::u(y._I))>=0) {
                            cc[1] = compConcOverest(x._cc,Op<U>::u(y._I)) + compConcOverest(Op<U>::l(x._I),y._cc) - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                            xslope[1] = dcompConcOverest_dp(x._cc,Op<U>::u(y._I));
                            yslope[1] = dcompConcOverest_ds(Op<U>::l(x._I));
                            xSub[1] = x._ccsub;
                        } else if (dcompConcOverest_dp(x._cv,Op<U>::u(y._I))<=0) {
                            cc[1] = compConcOverest(x._cv,Op<U>::u(y._I)) + compConcOverest(Op<U>::l(x._I),y._cc) - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I));
                            xslope[1] = dcompConcOverest_dp(x._cv,Op<U>::u(y._I));
                            yslope[1] = dcompConcOverest_ds(Op<U>::l(x._I));
                            xSub[1] = x._cvsub;
                        } else {   // can't recognize monotonicity, default to interval bounds
                            cc[1] = Op<U>::u(result._I);
                            xslope[1] = 0.;
                            yslope[1] = 0.;
                            xSub[1] = x._cvsub;
                        }
                    }

                // We now have to correct these two relaxations, since these were built using T_ps_uncut without considering that the function T_ps is cut off at Tmin
#ifdef LIVE_DANGEROUSLY
                    // Just default to interval bounds if we violate the Tmin cut (cf. explanation in case 11)
                    if ( (cc[0]<r2::data::Tmin) || (cc[1]<r2::data::Tmin) ) {
                        cc[0] = Op<U>::u(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cc[1] = Op<U>::u(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // Actually do max(T_ps_uncut, Tmin)
                    if( Op<U>::l(result._I) <= r2::data::Tmin ){
                        const double ccL1 = std::min( compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I)) /* + compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */ ,
                                                      compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) /* + compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) - compConcOverest(Op<U>::u(x._I),Op<U>::l(y._I)) */ );
                        const double k1 = isequal( Op<U>::u(result._I), ccL1) ? 0. : (( std::max( ccL1, r2::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL1)) ;
                        cc[0] = Op<U>::u(result._I) - k1 * ( cc[0] - Op<U>::u(result._I) );
                        xslope[0] = - k1*xslope[0];
                        yslope[0] = - k1*yslope[0];
                        const double ccL2 = std::min( /* compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I)) + */ compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I)) /* - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I)) */ ,
                                                        compConcOverest(Op<U>::u(x._I),Op<U>::u(y._I)) +    compConcOverest(Op<U>::l(x._I),Op<U>::l(y._I))    - compConcOverest(Op<U>::l(x._I),Op<U>::u(y._I)) );
                        const double k2 = isequal( Op<U>::u(result._I), ccL2) ? 0. : (( std::max( ccL2, r2::data::Tmin ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL2)) ;
                        cc[1] = Op<U>::u(result._I) - k2 * ( cc[1] - Op<U>::u(result._I) );
                        xslope[1] = - k2*xslope[1];
                        yslope[1] = - k2*yslope[1];
                    }
#endif

                // Adding an ad-hoc cut for large intervals
                    xslope[2] = -1.6e-5*std::pow(x._cc-101.,3);
                    yslope[2] = 225.;
                    cc[2] = 870. - 4.e-6*std::pow(x._cc-101.,4) + yslope[2]*(y._cc-5.04);

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(3, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] =    xslope[irelax]*(x._const? 0.:x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
            }

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 25:   // region 2, h(p,s)
        {
            return iapws(x,iapws(x,y,24),21);   // h(p,T(p,s))
        }
        case 26:   // region 2, s(p,h)
        {
            return iapws(x,iapws(x,y,23),22);   // s(p,T(p,h))
        }
        case 43:    // region 4-1/2, h(p,x)
        {
            McCormick<U> result = y*iapws(x,413 /* r4-1/2, hvap(p) */) + (1.-y)*iapws(x,411 /* r4-1/2, hliq(p) */);
            result._I = Op<U>::iapws(x._I,y._I,type);
#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 44:    // region 4-1/2, h(T,x)
        {
            McCormick<U> result = y*iapws(x,414 /* r4-1/2, hvap(T) */) + (1.-y)*iapws(x,412 /* r4-1/2, hliq(T) */);
            result._I = Op<U>::iapws(x._I,y._I,type);
#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 45:    // region 4-1/2, s(p,x)
        {
            McCormick<U> result = y*iapws(x,417 /* r4-1/2, svap(p) */) + (1.-y)*iapws(x,415 /* r4-1/2, sliq(p) */);
            result._I = Op<U>::iapws(x._I,y._I,type);
#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 46:    // region 4-1/2, s(T,x)
        {
            McCormick<U> result = y*iapws(x,418 /* r4-1/2, svap(T) */) + (1.-y)*iapws(x,416 /* r4-1/2, sliq(T) */);
            result._I = Op<U>::iapws(x._I,y._I,type);
#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 47:    // region 4-1/2, x(p,h)
        {
            McCormick<U> result;
            if( x._const ) {
                result._sub( y._nsub, y._const );
            } else if( y._const ) {
                result._sub( x._nsub, x._const );
            } else if( x._nsub != y._nsub ) {
                throw typename McCormick<U>::Exceptions( McCormick<U>::Exceptions::SUB );
            } else {
                result._sub( x._nsub, x._const );
            }
            result._I = Op<U>::iapws( x._I, y._I, type );
            if( isequal(Op<U>::u(result._I),0.) ){
                result._cv = 0.;
                result._cc = 0.;
                for( unsigned int i=0; i<result._nsub; i++ ){
                    result._cvsub[i] =  0.;
                    result._ccsub[i] =  0.;
                }
                return result;
            }

			// We want to exploit componentwise convexity. However, while x(p,h) is convex (or, more precisely, linear) w.r.t. h, it is not always convex w.r.t. p --> comp. alphaBB
			double alphaP;
			if ( (Op<U>::u(x._I)<r4::data::pmaxD2x12phdp2Gt0) || (Op<U>::u(y._I)<r4::data::hmaxD2x12phdp2Gt0) || (Op<U>::l(y._I)>r4::data::hminD2x12phdp2Gt0) ) {
				alphaP = 0.;
			} else if (Op<U>::l(y._I)>=r4::get_hliq_p_12(Op<U>::u(x._I))) {
				alphaP = r4::data::alphaD2x12dp2hGthliq;
			} else {
				alphaP = r4::data::alphaD2x12dp2;
			}

			{	// convex part

				double cv[4], xslope[4], yslope[4]; // We will derive four relaxations and choose the strongest
                double* xSub[4];

				// First, construct a componentwise convex underestimator; constructing it in a way not to destroy the monotonicity properties we have
					std::function<double(const double,const double)> compConvUnderest = [x,alphaP](const double p, const double h) { return r4::get_x_ph_12_uncut(p,h) + alphaP*(sqr(p-Op<U>::u(x._I))-sqr(Op<U>::diam(x._I)));  };
					std::function<double(const double,const double)> dcompConvUnderest_dp = [x,alphaP](const double p, const double h) { return r4::derivatives::get_dx_ph_dp_12_uncut(p,h) + 2.*alphaP*(p-Op<U>::u(x._I));  };
					std::function<double(const double,const double)> dcompConvUnderest_dh = [](const double p, const double h) { return r4::derivatives::get_dx_ph_dh_12_uncut(p,h); };

				// Applying Theorem 1 from Najman, Bongartz, Mitsos (2019): CACE
					{	// First relaxation: x(p,h^L)+x(p^L,h)-x(p^L,h^L). The pressure dependence is equal to that of x(p,h^L).
						double xRel1;
						bool xslopeZero = false;
						if ((Op<U>::l(y._I)<=r4::data::hmaxDx12hpdpLt0)||(Op<U>::u(x._I)<=r4::data::pmaxDx12hpdpLt0)) {	// decreasing in p
							xRel1 = x._cc;
							xSub[0] = x._ccsub;
						} else {	// actually need to solve dx(p,h^L)/dp = 0 for p
							// first, define the functions needed to use the _comput_root function
							const double rusr[5] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I), alphaP};
							double (*myfPtr)(const double,const double*,const int*);
							double (*mydfPtr)(const double,const double*,const int*);
							myfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_dx_ph_dp_12_uncut(x,rusr[2]) + 2.*rusr[4]*(x-rusr[1]); };
							mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_d2x_ph_dp2_12_uncut(x,rusr[2]) + 2.*rusr[4]; };
							// then, solve and determine where we ended up
							xRel1 = _compute_root(x._cv,x._cv,x._cc,myfPtr,mydfPtr,rusr,NULL);
							if(isequal(xRel1,x._cv)) {
								xSub[0] = x._cvsub;
							} else if (isequal(xRel1,x._cc)) {
								xSub[0] = x._ccsub;
							} else {
								xSub[0] = x._ccsub; // this is a dummy that will not be used
								xslopeZero = true;
							}
						}
						cv[0] = compConvUnderest(xRel1,Op<U>::l(y._I)) + compConvUnderest(Op<U>::l(x._I),y._cv) - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I));
						xslope[0] = xslopeZero? 0. : dcompConvUnderest_dp(xRel1,Op<U>::l(y._I));
						yslope[0] = dcompConvUnderest_dh(Op<U>::l(x._I),y._cv);
					}
					{	// Second relaxation: x(p,h^U)+x(p^U,h)-x(p^U,h^U). The pressure dependence is equal to that of x(p,h^U).
						double xRel2;
						bool xslopeZero = false;
						if ((Op<U>::u(y._I)<=r4::data::hmaxDx12hpdpLt0)||(Op<U>::u(x._I)<=r4::data::pmaxDx12hpdpLt0)) {	// decreasing in p
							xRel2 = x._cc;
							xSub[1] = x._ccsub;
						} else {	// actually need to solve dx(p,h^U)/dp = 0 for p
							// first, define the functions needed to use the _comput_root function
							const double rusr[5] = {Op<U>::l(x._I), Op<U>::u(x._I), Op<U>::l(y._I), Op<U>::u(y._I), alphaP};
							double (*myfPtr)(const double,const double*,const int*);
							double (*mydfPtr)(const double,const double*,const int*);
							myfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_dx_ph_dp_12_uncut(x,rusr[3]) + 2.*rusr[4]*(x-rusr[1]); };
							mydfPtr = [](const double x, const double*rusr, const int*iusr) { return r4::derivatives::get_d2x_ph_dp2_12_uncut(x,rusr[3]) + 2.*rusr[4]; };
							// then, solve and determine where we ended up
							xRel2 = _compute_root(x._cv,x._cv,x._cc,myfPtr,mydfPtr,rusr,NULL);
							if(isequal(xRel2,x._cv)) {
								xSub[1] = x._cvsub;
							} else if (isequal(xRel2,x._cc)) {
								xSub[1] = x._ccsub;
							} else {
								xSub[1] = x._ccsub; // this is a dummy that will not be used
								xslopeZero = true;
							}
						}
						cv[1] = compConvUnderest(xRel2,Op<U>::u(y._I)) + compConvUnderest(Op<U>::u(x._I),y._cv) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I));
						xslope[1] = xslopeZero? 0. : dcompConvUnderest_dp(xRel2,Op<U>::u(y._I));
						yslope[1] = dcompConvUnderest_dh(Op<U>::u(x._I),y._cv);
					}
					// We now have to correct these two relaxations, since these were built using x_ph_uncut without considering that the function x_ph is cut off at 1 (and 0, but for the convex part this is being taken care of by the .cut() function)
#ifdef LIVE_DANGEROUSLY
                    // Just default to interval bounds if we violate the 1 cut (cf. explanation in case 11)
                    if ( (cv[0]>1) || (cv[1]>1) ) {
                        cv[0] = Op<U>::l(result._I);
                        xslope[0] = 0.;
                        yslope[0] = 0.;
                        cv[1] = Op<U>::l(result._I);
                        xslope[1] = 0.;
                        yslope[1] = 0.;
                    }
#else
                    // Actually do min(x_ph_uncut,1)
                    if( Op<U>::u(result._I) >= 1. ){
                        // First, we determine the maximum of the convex underestimators on the entire host sets, which may be at (p^L,h^U) or (p^U,h^U) (recall that the underestimators are increasing in h and comp. convex)
                        // First relaxation: max of x(p,h^L)+x(p^L,h)-x(p^L,h^L)
                        const double cvU1 = std::max(  /* compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) */ + compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /* - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I)) */,
													      compConvUnderest(Op<U>::u(x._I),Op<U>::l(y._I))    + compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I))    - compConvUnderest(Op<U>::l(x._I),Op<U>::l(y._I))     );
                        const double k1 = isequal( Op<U>::l(result._I), cvU1) ? 0. : (( std::min( cvU1, 1. ) - Op<U>::l(result._I) ) / (cvU1 - Op<U>::l(result._I))) ;
                        cv[0] = Op<U>::l(result._I) + k1 * ( cv[0] - Op<U>::l(result._I) );
                        xslope[0] = k1*xslope[0];
                        yslope[0] = k1*yslope[0];
                        // Second relaxation: max of x(p,h^U)+x(p^U,h)-x(p^U,h^U)
                        const double cvU2 = std::max(   compConvUnderest(Op<U>::l(x._I),Op<U>::u(y._I)) /* + compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) */,
													    compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) /* + compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) - compConvUnderest(Op<U>::u(x._I),Op<U>::u(y._I)) */  );
                        const double k2 = isequal( Op<U>::l(result._I), cvU2) ? 0. : (( std::min( cvU2, 1. ) - Op<U>::l(result._I) ) / (cvU2 - Op<U>::l(result._I))) ;
                        cv[1] = Op<U>::l(result._I) + k2 * ( cv[1] - Op<U>::l(result._I) );
                        xslope[1] = k2*xslope[1];
                        yslope[1] = k2*yslope[1];
                    }
#endif

				// Since the relaxations may be somewhat weak for large intervals, append ad hoc cuts
					constexpr double yslopeA = 8.75e-4;
					cv[2] = yslopeA*(y._cv - 1700);
					xslope[2] = 0.;
					yslope[2] = yslopeA;
					xSub[2] = x._cvsub; // dummy
					constexpr double yslopeB = 4.2e-4;
					cv[3] = yslopeB*(y._cv-(510.+420.*std::log(x._cc+1.)));
					xslope[3] = -yslopeB*420./(x._cc+1.);
					yslope[3] = yslopeB;
					xSub[3] = x._ccsub;

				// Finally, store the best relaxation with the corresponding subgradient
					const unsigned irelax = argmax(4, cv);
					result._cv = cv[irelax];
					for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }

			}
			{	// concave part
				double cc[3], xslope[3], yslope[3]; // We will derive three relaxations and choose the strongest
				double* xSub[3];

				// First, construct a componentwise convex overestimator
					std::function<double(const double,const double)> compConvOverest = [x,alphaP](const double p, const double h) { return r4::get_x_ph_12_uncut(p,h) + alphaP*sqr(p-Op<U>::mid(x._I));  };

				// Since we now have componentwise convexity, we can compute the vertex polyhedral concave envelope (2 affine overestimators)
					// Calculate function values at corner point
					double fLL = compConvOverest(Op<U>::l(x._I),Op<U>::l(y._I));
					double fLU = compConvOverest(Op<U>::l(x._I),Op<U>::u(y._I));
					double fUL = compConvOverest(Op<U>::u(x._I),Op<U>::l(y._I));
					double fUU = compConvOverest(Op<U>::u(x._I),Op<U>::u(y._I));

					// Determine which ones are the correct facets
					double fmidLLUU = 0.5*(fLL + fUU);
					double fmidLUUL = 0.5*(fLU + fUL);
					if (fmidLUUL < fmidLLUU) {
						// In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
						// - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dx/dh>0 and hence also dx^cc/dh>0
						// - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
						double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
						double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, UL, UU), 2: (LL, LU, UU))
						double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
						// First, determine the "highest" corner for facet 1 (LL, UL, UU), considering that we are increasing in y (i.e., f(UU)>=f(UL))
						if (fUU>=fLL) {
							fUpper1 = fUU;
							xUpper1 = Op<U>::u(x._I);
							yUpper1 = Op<U>::u(y._I);
						} else {
							fUpper1 = fLL;
							xUpper1 = Op<U>::l(x._I);
							yUpper1 = Op<U>::l(y._I);
						}
						// Next, determine monotonicity of facet 1
						if (fUL>=fLL) {
							xRel1 = x._cc;
							xSub[0] = x._ccsub;
						} else {
							xRel1 = x._cv;
							xSub[0] = x._cvsub;
						}
						// Next, determine the "highest" corner & monotonicity for facet 2 (LL, LU, UU), considering that we are increasing in y (i.e., f(LU)>=f(LL))
						if (fUU>=fLU) {
							xRel2 = x._cc;
							xSub[1] = x._ccsub;
							fUpper2 = fUU;
							xUpper2 = Op<U>::u(x._I);
						} else {
							xRel2 = x._cv;
							xSub[1] = x._cvsub;
							fUpper2 = fLU;
							xUpper2 = Op<U>::l(x._I);
						}
						// Now we can compute the facets
						const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
						const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
						xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
						yslope[0] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
						xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
						yslope[1] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
						cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
						cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));

					}
					else {
						// In general, we get two facets. However, we need to determine the monotonicity properties & maximum points of the relaxation first:
						// - the monotonicity is needed to select the correct solution point in multivariate McCormick; we already know that dx/dh>0 and hence also dx^cc/dh>0
						// - the highest point is needed in order to select a valid relaxation in case the width in one dimension is too small to compute the slope of the secant
						double xRel1, xRel2;                        // values of the relaxations of x at the solution point of the auxiliary problem in multivariate McCormick
						double fUpper1, fUpper2;                    // highest function values at the corners of the two facets (1: (LL, LU, UL), 2: (LU, UL, UU))
						double xUpper1, yUpper1, xUpper2;           // x (and y, for facet 1) values at which fUpper1 and fUpper 2 are achieved
						// First, determine the "highest" corner for facet 1 (LL, LU, UL), considering that we are increasing in y (i.e., f(LU)>=f(LL))
						if (fUL>=fLU) {
							fUpper1 = fUL;
							xUpper1 = Op<U>::u(x._I);
							yUpper1 = Op<U>::l(y._I);
						} else {
							fUpper1 = fLU;
							xUpper1 = Op<U>::l(x._I);
							yUpper1 = Op<U>::u(y._I);
						}
						// Next, determine monotonicity of facet 1
						if (fUL>=fLL) {
							xRel1 = x._cc;
							xSub[0] = x._ccsub;
						} else {
							xRel1 = x._cv;
							xSub[0] = x._cvsub;
						}
						// Next, determine the "highest" corner & monotonicity for facet 2 (LU, UL,UU), considering that we are increasing in y (i.e., f(UU)>=f(LL))
						if (fUU>=fLU) {
							xRel2 = x._cc;
							xSub[1] = x._ccsub;
							fUpper2 = fUU;
							xUpper2 = Op<U>::u(x._I);
						} else {
							xRel2 = x._cv;
							xSub[1] = x._cvsub;
							fUpper2 = fLU;
							xUpper2 = Op<U>::l(x._I);
						}
						// Now we can compute the facets
						const bool thinX = isequal( Op<U>::l(x._I), Op<U>::u(x._I) );
						const bool thinY = isequal( Op<U>::l(y._I), Op<U>::u(y._I) );
						xslope[0] = thinX ? 0. : ( fUL - fLL )/Op<U>::diam(x._I);
						yslope[0] = thinY ? 0. : ( fLU - fLL )/Op<U>::diam(y._I);
						xslope[1] = thinX ? 0. : ( fUU - fLU )/Op<U>::diam(x._I);
						yslope[1] = thinY ? 0. : ( fUU - fUL )/Op<U>::diam(y._I);
						cc[0] = fUpper1 + xslope[0]*(xRel1 - xUpper1) +  yslope[0]*(y._cc - yUpper1);
						cc[1] = fUpper2 + xslope[1]*(xRel2 - xUpper2) +  yslope[1]*(y._cc - Op<U>::u(y._I));

					}

					// We now have to correct these two relaxations, since these were built using x_ph_uncut without considering that the function x_ph is cut off at 0 (and 1, but for the concave part this is being taken care of by the .cut() function)
#ifdef LIVE_DANGEROUSLY
					// Just default to interval bounds if we violate the 0 cut (cf. explanation in case 11)
					if ( (cc[0]<0.) || (cc[1]<0.) ) {
						cc[0] = Op<U>::u(result._I);
						xslope[0] = 0.;
						yslope[0] = 0.;
						cc[1] = Op<U>::u(result._I);
						xslope[1] = 0.;
						yslope[1] = 0.;
					}
#else
					// Actually do max(x_ph_uncut, 0)
					if( Op<U>::l(result._I) <= 0. ){
						const double ccL1 = (fmidLUUL < fmidLLUU) ?
													std::min( fLL, fUL )  // minimum of facet (LL, UL, UU)
												:	std::min( fLL, fUL ); // minimum of facet (LU, UL, LL)
						const double k1 = isequal( Op<U>::u(result._I), ccL1) ? 0. : (( std::max( ccL1, 0. ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL1)) ;
						cc[0] = Op<U>::u(result._I) - k1 * ( cc[0] - Op<U>::u(result._I) );
						xslope[0] = - k1*xslope[0];
						yslope[0] = - k1*yslope[0];
						const double ccL2 = (fmidLUUL < fmidLLUU) ?
													std::min( fLL, fLL + xslope[1]*Op<U>::diam(x._I) )  // minimum of facet (LL, LU, UU)
												:	std::min( fUL, fUL - xslope[1]*Op<U>::diam(x._I) ); // minimum of facet (LU, UL, UU)
						const double k2 = isequal( Op<U>::u(result._I), ccL2) ? 0. : (( std::max( ccL2, 0. ) - Op<U>::u(result._I) ) / (Op<U>::u(result._I)-ccL2)) ;
						cc[1] = Op<U>::u(result._I) - k2 * ( cc[1] - Op<U>::u(result._I) );
						xslope[1] = - k2*xslope[1];
						yslope[1] = - k2*yslope[1];
					}
#endif

				// Since the relaxations may be somewhat weak for large intervals, append ad hoc cuts
					constexpr double yslopeA = 4e-4;
					cc[2] = yslopeA*(y._cc +20);
					xslope[2] = 0.;
					yslope[2] = yslopeA;
					xSub[2] = x._cvsub; // dummy

                // Finally, store the best relaxation with the corresponding subgradient
                    const unsigned irelax = argmin(3, cc);
                    result._cc = cc[irelax];
                    for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = xslope[irelax]*(x._const? 0.: xSub[irelax][i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }

			}


#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 48:    // region 4-1/2, x(p,s)
        {
            McCormick<U> sliq = iapws(x,415);  // region 4-1/2, sliq(p)
            McCormick<U> svap = iapws(x,417);  // region 4-1/2, svap(p)
            McCormick<U> result =  (y-sliq)/max(svap-sliq,McCormick<U>(r4::data::mindeltasvap12));

            // We now have to correct the relaxations, since they were built using the original formulation of x(p,s) without considering that the function x_ps is cut off at 0 and 1
#ifdef LIVE_DANGEROUSLY
                // Just default to interval bounds if we violate the 0/1 cuts (cf. explanation in case 11)
                result._I = Op<U>::iapws(x._I, y._I, type);
                if ( result._cc < 0. ) {
                    result._cc = Op<U>::u(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ){
                        result._ccsub[i] =  0.;
                    }
                }
                if ( result._cv > 1. ) {
                    result._cv = Op<U>::l(result._I);
                    for( unsigned int i=0; i<result._nsub; i++ ){
                        result._cvsub[i] =  0.;
                    }
                }
#else
                // Actually do the min/max operation
                result = min(max(result,0.),1.);
                result._I = Op<U>::iapws(x._I, y._I, type);
#endif

			// Now just add ad-hoc relaxations
			{	// convex part
				double cv[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
				constexpr double yslopeA = 0.1275;
				cv[0] = yslopeA*(y._cv-(2.+0.7*std::log(x._cc+0.4)));
				xslope[0] = -yslopeA*0.7/(x._cc+0.4);
				yslope[0] = yslopeA;
				constexpr double yslopeB = 0.185;
				cv[1] = yslopeB*(y._cv-3.8);
				xslope[1] = 0.;
				yslope[1] = yslopeB;
				// Finally, store the best relaxation with the corresponding subgradient
				const unsigned irelax = argmax(2, cv);
				if (cv[irelax]>result._cv) {
					result._cv = cv[irelax];
					for( unsigned int i=0; i<result._nsub; i++ ){ result._cvsub[i] = xslope[irelax]*(x._const? 0.: x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._cvsub[i]); }
				}
			}
			{	// convex part
				double cc[2], xslope[2], yslope[2]; // We will derive two relaxations and choose the strongest
				constexpr double yslopeA = 0.135;
				cc[0] = yslopeA*(y._cc + (0.75+0.6*std::log(x._cc+0.4)));
				xslope[0] = yslopeA*0.6/(x._cc+0.4);
				yslope[0] = yslopeA;
				constexpr double yslopeB = 0.195;
				cc[1] = yslopeB*(y._cc+0.05);
				xslope[1] = 0.;
				yslope[1] = yslopeB;
				// Finally, store the best relaxation with the corresponding subgradient
				const unsigned irelax = argmin(2, cc);
				if (cc[irelax]<result._cc) {
					result._cc = cc[irelax];
					for( unsigned int i=0; i<result._nsub; i++ ){ result._ccsub[i] = xslope[irelax]*(x._const? 0.: x._ccsub[i])+yslope[irelax]*(y._const? 0.:y._ccsub[i]); }
				}
			}

#ifdef MC__MCCORMICK_DEBUG
            std::string str = "iapws-2d," + std::to_string(type);
            McCormick<U>::_debug_check(x, y, result, str);
#endif
            if (McCormick<U>::options.SUB_INT_HEUR_USE) { return result.cut().apply_subgradient_interval_heuristic(); }
            return result.cut();
        }
        case 49:    // region 4-1/2, h(p,s)
        {
            return iapws(x,iapws(x,y,48),43);   // h(p,x(p,s))
        }
        case 410:   // region 4-1/2, s(p,h)
        {
            return iapws(x,iapws(x,y,47),45);   // s(p,x(p,h))
        }
            // 1d functions:
        case 29:    // region 2, boundary to 3, p(T)
        case 210:   // region 2, boundary to 3, T(p)
        case 211:   // region 2, boundary 2b/2c, p(h)
        case 212:   // region 2, boundary 2b/2c, h(p)
        case 41:    // region 4, p(T)
        case 42:    // region 4, T(p)
        case 411:   // region 4-1/2, hliq(p)
        case 412:   // region 4-1/2, hliq(T)
        case 413:   // region 4-1/2, hvap(p)
        case 414:   // region 4-1/2, hvap(T)
        case 415:   // region 4-1/2, sliq(p)
        case 416:   // region 4-1/2, sliq(T)
        case 417:   // region 4-1/2, svap(p)
        case 418:   // region 4-1/2, svap(T)
            throw std::runtime_error("\nmc::McCormick\t IAPWS called with two arguments but a 1d type (" + std::to_string((int)type) + ")");
        default:
            throw std::runtime_error("\nmc::McCormick\t IAPWS called with unkown type (" + std::to_string((int)type) + ").");
      }

    }


} // end namespace mc
